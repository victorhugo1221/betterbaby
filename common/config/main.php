<?php
use grupow\rest\Serializer;
$dotenv = Dotenv\Dotenv::createMutable(dirname(__DIR__, 2));
$dotenv->load();

//db
$dbName = $_ENV['DB_NAME'];
$dbUser = $_ENV['DB_USER'];
$dbPass = $_ENV['DB_PASS'];
$dbHost = $_ENV['DB_HOST'];
$dbPort = $_ENV['DB_PORT'];
$dbChar = $_ENV['DB_CHAR'];

//db Log
$dbLogName = $_ENV['DB_LOG_NAME'];
$dbLogUser = $_ENV['DB_LOG_USER'];
$dbLogPass = $_ENV['DB_LOG_PASS'];
$dbLogHost = $_ENV['DB_LOG_HOST'];
$dbLogPort = $_ENV['DB_LOG_PORT'];
$dbLogChar = $_ENV['DB_LOG_CHAR'];

//smtp
$smtpHost = $_ENV['SMTP_HOST'];
$smtpUser = $_ENV['SMTP_USER'];
$smtpPass = $_ENV['SMTP_PASS'];
$smtpPort = $_ENV['SMTP_PORT'];
$smtpSecure = $_ENV['SMTP_SECURE'];

return [
    'timeZone' => 'America/Sao_Paulo',
    'sourceLanguage' => 'pt_BR',
    'language' => 'pt_BR',
    'aliases' => [
        '@grupowrest' => '@common/components/grupow/yii2-restful-serializer/src/',
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host={$dbHost};port={$dbPort};dbname={$dbName}",
            'username' => $dbUser,
            'password' => $dbPass,
            'charset' => $dbChar,
        ],
        'dbLog' => [
            'class' => 'yii\db\Connection',
            'dsn' => "mysql:host={$dbLogHost};port={$dbLogPort};dbname={$dbLogName}",
            'username' => $dbLogUser,
            'password' => $dbLogPass,
            'charset' => $dbLogChar,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/modules/email',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => $smtpHost,
                'username' => $smtpUser,
                'password' => $smtpPass,
                'port' => $smtpPort,
                'encryption' => $smtpSecure,
            ],
            'useFileTransport' => FALSE,
        ],
        'serializer'    => [
            'class'    => Serializer::class,
            'defaultValidationFormat' => Serializer::VALIDATION_FORM,
            'metadirs' => [
                'common\modules\user'      => dirname(__DIR__).'/modules/user/models/metas',
            ],
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\modules\user\models\User',
            'enableAutoLogin' => TRUE,
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => FALSE,
                'yii\bootstrap\BootstrapPluginAsset' => FALSE,
            ],
            'appendTimestamp' => TRUE,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'itemTable' => 'auth_item',
            'assignmentTable' => 'auth_assignment',
            'itemChildTable' => 'auth_item_child',
            'ruleTable' => 'auth_rule',
        ],
    ],
    'modules' => require __DIR__ . '/modules.php',
];
