<?php
return [
    'components' => [
        'db' => [
            'dsn' => "mysql:host={$_ENV['DB_HOST']};dbname={$_ENV['DB_NAME']}",
        ],
    ],
];