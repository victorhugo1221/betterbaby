<?php
return [
    'admin-gw' => [
        'class' => 'common\modules\adminGw\Module',
    ],
    'user' => [
        'class' => 'common\modules\user\Module'
    ],
    'rating' => [
        'class' => 'common\modules\rating\Module'
    ],
    'news' => [
        'class' => 'common\modules\news\Module'
    ],
    'publicity' => [
        'class' => 'common\modules\publicity\Module'
    ],
    'user-provider' => [
        'class' => 'common\modules\userProvider\Module'
    ],
    'order' => [
        'class' => 'common\modules\order\Module'
    ],
    'plan' => [
        'class' => 'common\modules\plan\Module'
    ],
    'base-crm' => [
        'class' => 'common\modules\baseCrm\Module'
    ],
    'solicitation' => [
        'class' => 'common\modules\solicitation\Module'
    ],
    'email' => [
        'class' => 'common\modules\email\Module'
    ],
    'health-plan' => [
        'class' => 'common\modules\healthPlan\Module'
    ],
     'baby-week-data' => [
        'class' => 'common\modules\BabyWeekData\Module'
    ],
    
    
    
];