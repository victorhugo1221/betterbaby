<?php

namespace common\modules\favoriteProvider\models\query;

use common\components\utils\ModelTrait;
use common\modules\favoriteProvider\models\FavoriteProvider;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\favoriteProvider\models\FavoriteProvider]].
 *
 * @see \common\modules\favoriteProvider\models\FavoriteProvider
 */
class FavoriteProviderQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', FavoriteProvider::tableName() . '.deleted', null]);
    }
    public function filter($search) {
        $response = $this->defaultFilter($search);
        return $response;
    }
}