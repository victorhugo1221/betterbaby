<?php

namespace common\modules\favoriteProvider\models;

use Yii;


class FavoriteProvider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'favorite_provider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_client_id', 'user_provider_id'], 'required'],
            [['user_client_id', 'user_provider_id'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_client_id' => 'User Client ID',
            'user_provider_id' => 'User Provider ID',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\favoriteProvider\models\query\FavoriteProviderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\favoriteProvider\models\query\FavoriteProviderQuery(get_called_class());
    }
}
