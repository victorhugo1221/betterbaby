<?php

namespace common\modules\favoriteProvider;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('favoriteProviderService', __NAMESPACE__ . '\services\FavoriteProviderService');
    }
}