<?php

namespace common\modules\address\models\query;

use common\components\utils\ModelTrait;
use common\modules\address\models\Address;
use Yii;
use yii\db\ActiveQuery;

class AddressQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', Address::tableName() . '.deleted', null]);
    }
    
    public function filter($search) {
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}
