<?php

namespace common\modules\address\models;

use common\modules\city\models\City;
use common\modules\user\models\User;
use Yii;


class Address extends \yii\db\ActiveRecord
{
    /**
     * @var mixed|null
     */
    private $user_id;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city', 'state', 'street', 'district', 'zip_code',], 'required'],
            [['user_id', 'status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['street', 'number', 'complement', 'district'], 'string', 'max' => 100],
            [['latitude', 'longitude'], 'number'],
            [['city'], 'string', 'max' => 100],
            [['zip_code'], 'string', 'max' => 9],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'Cidade',
            'state' => 'Estado',
            'user_id' => 'User ID',
            'street' => 'Rua',
            'number' => 'Número',
            'complement' => 'Complemento',
            'district' => 'Bairro',
            'zip_code' => 'CEP',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'status' => 'Status',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * Gets query for [[City]].
     *
     * @return \yii\db\ActiveQuery|\common\modules\address\models\query\CityQuery
     */
//    public function getCity()
//    {
//        return $this->hasOne(City::className(), ['id' => 'city_id']);
//    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|\common\modules\address\models\query\UserQuery
     */
    public function getUserRel()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\address\models\query\AddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\address\models\query\AddressQuery(get_called_class());
    }
}
