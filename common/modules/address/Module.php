<?php

namespace common\modules\address;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
//        $this->set('addressService', __NAMESPACE__ . '\services\AddressService');
    }
}