<?php

namespace common\modules\city\services;

use common\modules\city\models\City;
use common\components\Service;

class CityService extends Service {
	public function init() {
		parent::init();
		$this->className = City::class;
	}

    public function findByIbgeCode(string $ibgeCode) : City {
        return City::findOne(['ibge_code' => $ibgeCode]);
    }

	public function findCities(?int $stateId = NULL, ?string $name = NULL, ?int $ibgeCode = NULL): array {
		$respose = City::find()->select(['id', 'name', 'ibge_code', 'state_id']);

		if (!empty($name)) {
			$respose->andWhere(['LIKE', 'name', $name]);
		}

		if (!empty($stateId)) {
			$respose->andWhere(['=', 'state_id', $stateId]);
		}

		if (!empty($ibgeCode)) {
			$respose->andWhere(['=', 'ibge_code', $ibgeCode]);
		}

		return $respose->orderBy('name')->asArray()->all();
	}

	public function getCityByCode(int $stateId, string $ibgeCode, string $cityName) {

		$city = City::find()->andWhere("ibge_code = '{$ibgeCode}'")->one();
		if (!$city) {
			$city            = new City();
			$city->state_id  = $stateId;
			$city->ibge_code = $ibgeCode;
			$city->name      = $cityName;
			$city->save();
		}

		return $city;
	}
}
