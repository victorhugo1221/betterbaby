<?php

namespace common\modules\city\behaviors;

use common\components\Behavior;
use common\modules\city\models\City;


class CityBehavior extends Behavior {
    public       $name;
    public       $token;
    public array $booleanFields = ['status'];

	public function events() {
        return [
            City::EVENT_BEFORE_INSERT => 'beforeCreate',
            City::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            City::EVENT_AFTER_INSERT  => 'afterCreate',
            City::EVENT_AFTER_UPDATE  => 'afterSave',
        ];
    }


	/*    public function afterSave($event) {
		$this->saveLog($this->owner->id, 'DiscountCoupon', $this->_action, $this->_log);
	}
	*/
    public function afterCreate($event){
        parent::afterSave($event);
        
        
    }
}