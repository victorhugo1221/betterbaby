<?php

namespace common\modules\city\models;

use common\components\Model;
use common\modules\city\behaviors\CityBehavior;
use yii\behaviors\TimestampBehavior;
use common\modules\city\models\query\CityQuery;
use common\modules\state\models\State;
use common\modules\user\models\UserClient;
use yii\db\ActiveQuery;

class City extends Model {
	public static function tableName() {
		return 'city';
	}

	public function rules() {
		return [
			[['name', 'ibge_code', 'state_id'], 'required'],
			[['ibge_code', 'state_id', 'status'], 'integer'],
			[['created', 'updated', 'deleted'], 'safe'],
			[['name'], 'string', 'max' => 150],
			[['state_id'], 'exist', 'skipOnError' => TRUE, 'targetClass' => State::class, 'targetAttribute' => ['state_id' => 'id']],
		];
	}

	public function attributeLabels() {
		return [
			'id'        => 'Id',
			'name'      => 'Cidade',
			'ibge_code' => 'Código IBGE',
			'state_id'  => 'Id da UF',
			'status'    => 'Status',
			'created'   => 'Data de Cadastro',
			'updated'   => 'Data da última atualização',
			'deleted'   => 'Data da exclusão',
		];
	}

	public function getBuyers(): ActiveQuery {
		return $this->hasMany(Buyer::class, ['city_id' => 'id']);
	}

	public function getStateRel(): ActiveQuery {
		return $this->hasOne(State::class, ['id' => 'state_id']);
	}

	public function getUserClients(): ActiveQuery {
		return $this->hasMany(UserClient::class, ['city_id' => 'id']);
	}

	public static function find(): CityQuery {
		return new CityQuery(get_called_class());
	}

	public function behaviors() {
		return [
			'dependencies' => [
				'class' => CityBehavior::class,
			],
			'timestamp'    => [
				'class'      => TimestampBehavior::class,
				'attributes' => [
					static::EVENT_BEFORE_INSERT => ['created', 'updated'],
					static::EVENT_BEFORE_UPDATE => ['updated'],
				],
				'value'      => date('Y-m-d H:i:s'),
			],
		];
	}
}
