<?php

namespace common\modules\city\models\query;

use yii\db\ActiveQuery;
use common\modules\city\models\City;
use common\components\utils\ModelTrait;

/**
 * This is the ActiveQuery class for [[\common\modules\city\models\City]].
 *
 * @see \common\modules\city\models\City
 */
class CityQuery extends ActiveQuery {
	use ModelTrait;

	public function init() {
		return $this->andWhere(['IS', City::tableName() . '.deleted', NULL]);
	}

	/**
	 * {@inheritdoc}
	 * @return City[]|array
	 */
	public function all($db = NULL) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return City|array|null
	 */
	public function one($db = NULL) {
		return parent::one($db);
	}
}
