<?php

namespace common\modules\city;

class Module extends \yii\base\Module {

	public $controllerNamespace = __NAMESPACE__ . '\controllers';

	public function init() {
		parent::init();
        $this->set('cityService', ['class' => __NAMESPACE__ . '\services\CityService']);
	}
}
