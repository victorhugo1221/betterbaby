<?php

use yii\helpers\Url;

?>
<div>
    <div style="padding-bottom: 2px;padding-top: 20px;font-size: 15px;">Parabéns, cadastro efetuado com sucesso!</div>
    <p><?= $name ?>, é um prazer receber você em nossa plataforma.</p>
    <p>O Better Baby é uma plataforma que conecta você aos profissionais e serviços localizados na sua região, além de trazer informações e dicas úteis no seu dia a dia, que irão auxiliar no desenvolvimento e jornada do seu bebê.</p>
    <p>Estamos à disposição e conte sempre conosco.</p>
</div>
