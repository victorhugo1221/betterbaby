<?php

use yii\helpers\Html;

$charset = Yii::$app->charset ?? 'utf-8';
$title = Html::encode($this->title) ?? $_ENV['PROJECT_NAME'];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C/DTD XHTML 1.0 Transational//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= $charset ?>"/>
    <title><?= $title ?></title>
    <?php $this->head() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<?php $this->beginBody() ?>

<div bgcolor="#EAEAEF" style="margin:0;padding:0;background-color:#eaeaef;height:100%">
    <table border="0" cellpadding="0" cellspacing="0" bgcolor="#EAEAEF" width="100%" style="background-color:#eaeaef;overflow-x:hidden">
        <tr>
            <td align="left">
                <table bgcolor="#EAEAEF" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;overflow-x:hidden;font-family:&#39;proxima-nova&#39;,sans-serif;width:100%;max-width:600px">
                    <tr>
                        <td align="center" style="padding:10px 0px 20px 0px;font-family:&#39;proxima-nova&#39;,sans-serif">
                            <a href="https://betterbaby.com.br" style="color:#6EBBCE" target="_blank">  <img src="https://profissional.betterbaby.com.br/images/logo-bb.png" alt="Better Baby Logo" width="105px" style="max-width:105px"></a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="overflow-x:hidden;margin:0px 10px 0px 10px;border-top:6px solid #6EBBCE">
                                <tr>
                                    <td align="left" bgcolor="#ffffff" style="padding:40px 30px 0px 30px;font-family:&#39;proxima-nova&#39;,sans-serif">
                                        <h1 style="margin:0px;padding:0;font-size:26px;color:#333f4c;line-height:32px">
                                            <?= $title ?>
                                        </h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#ffffff" style="padding:20px 30px 0px 30px;margin:0px">
                                        <table style="width:100%">
                                            <tr>
                                                <td align="center" style="border-top:solid 1px #eaeaef;border-left:none;border-right:none;border-bottom:none;border-collapse:collapse;border-spacing:0;width:100%;margin:0px 0px 0px 0px"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" bgcolor="#ffffff" style="padding:20px 30px 0px 30px;font-family:&#39;proxima-nova&#39;,sans-serif">
                                        <p style="padding:0px;color:#333f4c;margin:0;font-size:18px;line-height:28px">
                                            <?= $content; ?>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" bgcolor="#ffffff" style="padding:20px 30px 0px 30px;font-family:&#39;proxima-nova&#39;,sans-serif">
                                        <hr style="border:1px solid #fafafa">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" bgcolor="#ffffff" style="padding:40px 20px 0px 20px;font-family:&#39;proxima-nova&#39;,sans-serif;border-bottom-left-radius:10px;border-bottom-right-radius:10px">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" bgcolor="#EAEAEF" style="padding:40px 10px 0px 10px;font-family:&#39;proxima-nova&#39;,sans-serif">
                                        <p style="padding:0px;color:#7e8790;margin:0;font-size:16px;text-align:center"><b>Central de Atendimento Better Baby</b></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" bgcolor="#EAEAEF" style="padding:15px 10px 0px 10px;font-family:&#39;proxima-nova&#39;,sans-serif">
                                        <p style="padding:0px;color:#7e8790;margin:0;font-size:16px;text-align:center">Dúvidas? Saiba mais na nossa <a href="https://betterbaby.com.br/" alt="Centra de Ajuda" style="color:#6EBBCE" target="_blank"> Central de Ajuda.</a></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#EAEAEF" style="padding:30px 10px 20px 10px;font-family:&#39;proxima-nova&#39;,sans-serif">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr style="margin:0;padding:0">
                                                <td align="center" width="60">
                                                    <a href="https://www.facebook.com/Betterbaby" target="_blank"><img src="https://ci5.googleusercontent.com/proxy/JTode7rkV4h4YG35Wj8k5HDuI7Zcq5M_QgjYFovptId-x2uCMt1rIAdrqACF6KYZ9jZbGCu9ipcO9oyjN_akZ57MHe0=s0-d-e1-ft#https://cdn.picpay.com/transactional/facebook.png" style="border:0" alt="Facebook" width="40"></a>
                                                </td>

                                                <td align="center" width="60">
                                                    <a href="https://www.instagram.com/betterbaby.app/" target="_blank"><img src="https://ci3.googleusercontent.com/proxy/VGIDUQHM45j1JpiqwQKrcIb7zC7hkM9Yb_eJG8-E7sErDh521FfTZRgbsy6oVxjc2mTJ8enNjUDt1hg1PM7mgB0-9_Ss=s0-d-e1-ft#https://cdn.picpay.com/transactional/instagram.png" style="border:0" alt="Instagram " width="40"></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" bgcolor="#EAEAEF" style="padding:0px 10px 45px 10px;font-family:&#39;proxima-nova&#39;,sans-serif">
                                        <p style="padding:0px;color:#7e8790;margin:0;font-size:12px;text-align:center"> © Better Baby
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
