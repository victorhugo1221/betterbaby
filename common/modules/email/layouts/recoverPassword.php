<?php

use yii\helpers\Url;

?>
<h3 style="padding-bottom: 30px;margin: 0;font-size: 15px;padding-top: 15px;font-weight: normal;">
    Olá, <?=$name?>
</h3>
<p style="font-size: 15px;">
    Você solicitou uma nova senha.
    <a target="_blank" rel="noopener noreferrer" href="<?='https://profissional.betterbaby.com.br/change-password/' . $token ?>" style="color: #20789a;font-weight: bold;font-size: 14px;text-decoration: underline;" class="link">
        <b>Clique aqui para cadastrar uma nova senha.</b>
    </a>
</p>