<?php

use yii\helpers\Url;

?>
<div>
    <div style="padding-bottom: 2px;padding-top: 20px;font-size: 15px;">Olá, <?= $name ?></div>
    <p>seu atendimento foi confirmado!</p>
    <p style="font-size: 19px">Informações</p>
    <p>Nome do profissional: <?= $providerName ?></p>
    <p>Data: <?= $date ?> </p>
    <p>Atendimento: <?= $type ?></p>
    <p>Orientações do atendimento: <?= $guidelines ?></p>
</div>
