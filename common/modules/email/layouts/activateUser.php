<?php
use yii\helpers\Url;
?>
<div>
    <div style="padding-bottom: 2px;padding-top: 20px;font-size: 15px;">Parabéns, cadastro efetuado com sucesso!</div>
    <p style="padding-bottom: 20px; color:#444444 ">Olá <?=$name?>,</p>
    <p>É um prazer receber você em nossa plataforma.</p>
    <p>O Better Baby é uma plataforma inovadora que coloca você em contato com seus clientes trazendo agilidade na visibilidade dos seus serviços.
        Conectamos você de forma simples, rápida e eficiente com o objetivo de aumentar seu número de Clientes.</p>
    <p>Estamos á sua disposição, conte conosco sempre que precisar.</p>
    <a target="_blank" rel="noopener noreferrer" href="https://profissional.betterbaby.com.br" style="color: #20789a;font-weight: bold;font-size: 14px;text-decoration: underline;" class="link">
        <b>Clique aqui para acessar a plataforma web.</b>
    </a>
</div>
