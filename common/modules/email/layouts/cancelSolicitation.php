<?php

use yii\helpers\Url;

?>
<div>
    <div style="padding-bottom: 2px;padding-top: 20px;font-size: 15px;">Olá, <?= $name ?></div>
    <p>Seu atendimento foi cancelado :(</p>
    <p style="font-size: 19px">Informações</p>
    <p>Motivo do cancelamento: <?= $cancellationReason ?></p>
    <p>Nome do profissional: <?= $providerName ?></p>
    <p>Data: <?= $date ?> </p>
    <p>Atendimento: Online</p>
</div>
