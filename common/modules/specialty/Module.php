<?php

namespace common\modules\specialty;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('specialtyService', __NAMESPACE__ . '\services\SpecialtyService');
    }
}