<?php

namespace common\modules\specialty\services;

use common\components\Service;
use common\modules\order\models\Order;
use common\modules\specialty\models\Specialty;

class SpecialtyService extends Service {
    public function init() {
        parent::init();
        $this->className = Specialty::class;
    }
    
    
    public function ListSpecialty() : array {
        $result = [];
        $solicitation = Specialty::find()->orderBy(['name' => SORT_ASC])->all();
        
        foreach ($solicitation as $key => $soli) {
            
            $result[] = [
                'id'        => $soli->id,
                'name'        => $soli->name,
            ];
        }
        return $result;
    }
}