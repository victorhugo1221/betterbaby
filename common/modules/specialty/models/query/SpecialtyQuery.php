<?php

namespace common\modules\specialty\models\query;

use common\components\utils\ModelTrait;
use common\modules\specialty\models\Specialty;
use yii\db\ActiveQuery;

class SpecialtyQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['status' => 1])->andWhere(['IS', Specialty::tableName() . '.deleted', NULL]);
    }
    
    public function filter($search) {
        
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}

