<?php

namespace common\modules\specialty\models;

use Yii;


class Specialty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'specialty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','status'], 'required'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Título',
            'status' => 'Status',
            'created' => 'Data de criação',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    public static function find()
    {
        return new \common\modules\specialty\models\query\SpecialtyQuery(get_called_class());
    }
}
