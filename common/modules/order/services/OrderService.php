<?php

namespace common\modules\order\services;

use common\components\Service;
use common\modules\order\models\Order;

class OrderService extends Service {
    public function init() {
        parent::init();
        $this->className = Order::class;
    }
}