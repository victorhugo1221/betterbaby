<?php

namespace common\modules\order\models\query;

use common\components\utils\ModelTrait;
use common\modules\order\models\Order;
use Yii;
use yii\db\ActiveQuery;

class OrderQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', Order::tableName() . '.deleted', null]);
    }
    
    public function filter($search) {
        
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}
