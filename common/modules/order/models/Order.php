<?php

namespace common\modules\order\models;
use common\components\Model;
use common\modules\order\behaviors\OrderBehavior;
use common\modules\order\models\query\OrderQuery;
use common\modules\user\models\User;
use Yii;


class Order extends Model
{
   
    public const STATUS_PENDENT = 0;
    public const STATUS_ACTIVE = 1;
    
    public static array $_status = [
        self::STATUS_PENDENT => 'Pendente',
        self::STATUS_ACTIVE  => 'Confirmado',
    ];
    
    
    public static function tableName()
    {
        return 'order';
    }

    public function rules()
    {
        return [
            [[], 'required'],
            [[''], 'number'],
            [['status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
           
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'user id',
            'price' => 'Preço',
            'status' => 'Status',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

  
    public function getCompanyPlans()
    {
        return $this->hasMany(CompanyPlan::class, ['plan_id' => 'id']);
    }
    
    public function getUserRel()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
    
    
    public static function find()
    {
        return new \common\modules\order\models\query\OrderQuery(get_called_class());
    }
}
