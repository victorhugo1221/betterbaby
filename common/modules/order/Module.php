<?php

namespace common\modules\order;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('orderService', __NAMESPACE__ . '\services\OrderService');
    }
}