<?php

namespace common\modules\user\services;

use common\modules\address\models\Address;
use common\modules\adminGw\models\Token;
use common\modules\user\models\User;
use common\modules\user\models\UserClient;
use common\modules\userProvider\models\UserProvider;
use Exception;
use Yii;
use yii\db\ActiveRecord;

class UserService extends BaseService {
    public function find() {
        return UserClient::find()->andWhere(['>', User::tableName() . '.status', -1])->andWhere(['is_administrator' => User::IS_NOT_ADMINISTRATOR]);
    }

    public function findById($id) {
        return User::find()->andWhere(['id' => $id])->one();
    }
   
    
    public function findByEmail($email) {
        return User::find()->andWhere(['email' => $email])->one();
    }
    
    public function updateClient(User $user, UserClient $userClient): bool {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $saveUser = $this->update($user);
            $saveUserClient = $this->update($userClient);
            
            if ($saveUser && $saveUserClient) {
                $transaction->commit();
                return TRUE;
            } else {
                $transaction->rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function deleteClient(int $id): bool {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = User::findOne($id);
            $model->deleted = date('Y-m-d h:m:s');
            $model->status = User::STATUS_ADMIN_INACTIVE;
            $deleteUser = $model->save();
            
            $modelUserClient = UserClient::find()->andWhere(['user_id' => $id])->one();
            $modelUserClient->deleted = date('Y-m-d h:m:s');
            $modelUserClient->status = User::STATUS_ADMIN_INACTIVE;
            $deleteUserClient = $modelUserClient->save();
            
            if ($deleteUser && $deleteUserClient) {
                $transaction->commit();
                return TRUE;
            } else {
                $transaction->rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    
    public function updateAvatar(int $id, string $avatar) : bool {
        $user = UserProvider::find()->andWhere(['user_id' => $id])->one();
        if ($user && $user->avatar != $avatar) {
            $user->avatar = $avatar;
            $user->save();
        }
        return TRUE;
    }
    
    public function updateAvatarUser(int $id, string $avatar) : bool {
        $user = UserClient::find()->andWhere(['user_id' => $id])->one();
        if ($user && $user->avatar != $avatar) {
            $user->avatar = $avatar;
            $user->save();
        }
        return TRUE;
    }
    
    
    public function createUserApp(User $user) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user->status = User::STATUS_ACTIVE;
            $saveUser = $this->create($user);
            
            if ($saveUser) {
                $transaction->commit();
                return $saveUser;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function createUserClient (UserClient $userClient, $userId) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userClient->user_id = $userId;
            $saveUserClient = $this->create($userClient);
            
            if ($saveUserClient) {
                $transaction->commit();
                return $saveUserClient;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function createAddress(Address $address, $userId) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $address->user_id = $userId;
            $saveAddress = $this->create($address);
            if ($saveAddress) {
                $transaction->commit();
                return $saveAddress;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    
    
    public function userGetData($id) {
        return User::find()->select([
            'user.id',
            'user.name',
            'user.cpf',
            'user.cellphone',
            'user.email',
            'concat (\'' . $_ENV ["BACKENDURL"] . 'uploads/user-client/' . '\', user_client.avatar) as avatar',
            'address.city',
            'address.state',
            'address.street',
            'address.number',
            'address.complement',
            'address.district',
            'address.zip_code',
            'user_client.baby_date',
            'baby_week_data.size_title',
            'concat (\'' . $_ENV ["BACKENDURL"] . 'uploads/baby-week-data/' . '\', baby_week_data.size_photo) as sizePhoto',
            'FORMAT (DATEDIFF (CURDATE(), user_client.baby_date) / 7 + 40 ,0,0 ) as week',
        ])
            ->innerJoin('user_client', 'user.id = user_client.user_id')
            ->innerJoin('address', 'user.id = address.user_id')
            ->innerJoin('baby_week_data', 'week = baby_week_data.week')
            ->andWhere(['user.id' => $id])->asArray()->all();
        
    }
    
    
    
    public function createToken(Token $token) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $saveToken = $this->create($token);
            
            if ($saveToken) {
                $transaction->commit();
                return $saveToken;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    
    
    
}

