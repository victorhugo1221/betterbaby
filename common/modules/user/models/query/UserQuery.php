<?php

namespace common\modules\user\models\query;

use common\components\utils\ModelTrait;
use common\modules\user\models\User;
use common\modules\userProvider\models\UserProvider;
use yii\db\ActiveQuery;


class UserQuery extends ActiveQuery {
	use ModelTrait;

	public function init() {
		return $this->andWhere(['IS', User::tableName() . '.deleted', null]);
	}

	public function filter($search) {
		$response = $this->defaultFilter($search);
		$this->andWhere(['=', 'is_administrator', User::IS_ADMINISTRATOR]);
		return $response;
	}
    
    public function filterUserProvider($search) {
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}
