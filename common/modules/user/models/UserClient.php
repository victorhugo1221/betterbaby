<?php

namespace common\modules\user\models;

use common\components\Model;
use common\modules\address\models\Address;
use common\modules\adminGw\models\Page;
use common\modules\adminGw\models\Translation;
use common\modules\user\behaviors\UserClientDependenciesBehavior;
use common\modules\user\behaviors\UserDependenciesBehavior;
use common\modules\user\models\query\UserClientQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class UserClient extends Model {
    
    public static array $_directory_file = [
        'avatar' => ['folder' => 'uploads/user-client/'],
    ];
    
    public static function tableName() {
        return 'user_client';
    }
    
    public function rules() {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['baby_date', 'created', 'updated', 'deleted'], 'safe'],
            [['baby_name'], 'string', 'max' => 155],
            [['referral_code'], 'string', 'max' => 155],
            [['user_id'], 'unique'],
            [['avatar'], 'file', 'extensions' => 'png,jpg,jpeg,bmp,gif'],
        
        ];
    }
    
    public function attributeLabels() {
        return [
            'user_id'       => 'Usuário',
            'baby_name'     => 'Nome do bebe',
            'avatar'        => 'avatar',
            'baby_date'     => 'data de nascimento bebe',
            'referral_code' => 'Código de divulgação',
            'status'        => 'Status',
            'created'       => 'Data de cadastro',
            'updated'       => 'Data da última atualização',
            'deleted'       => 'Data de exclusão',
        ];
    }
    
    
    public function getUserRel() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getAddressRel() {
        return $this->hasOne(Address::class, ['user_id' => 'user_id']);
    }
    
    public static function find() {
        return new UserClientQuery(get_called_class());
    }
}
