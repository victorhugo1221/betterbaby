<?php

namespace common\modules\userProviderPlan\models\query;

use common\components\utils\ModelTrait;
use common\modules\userProviderPlan\models\UserProviderPlan;
use Yii;
use yii\db\ActiveQuery;

class UserProviderPlanQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', UserProviderPlan::tableName() . '.deleted', null]);
    }
    public function filter($search) {
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}

