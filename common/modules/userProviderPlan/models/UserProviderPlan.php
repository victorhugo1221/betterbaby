<?php

namespace common\modules\userProviderPlan\models;

use common\modules\plan\models\Plan;
use common\modules\user\models\User;
use Yii;


class UserProviderPlan extends \yii\db\ActiveRecord
{
   
    public static function tableName()
    {
        return 'user_provider_plan';
    }

    
    public function rules()
    {
        return [
            [['plan_id','end_date'], 'required'],
            [['user_id', 'plan_id'], 'integer'],
            [['end_date', 'created', 'updated', 'deleted'], 'safe'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'plan_id' => 'Plan ID',
            'end_date' => 'End Date',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    
    public function getPlanRel()
    {
        return $this->hasOne(Plan::class, ['id' => 'plan_id']);
    }

    
    public function getUserRel()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
    
    public static function find()
    {
        return new \common\modules\userProviderPlan\models\query\UserProviderPlanQuery(get_called_class());
    }
}
