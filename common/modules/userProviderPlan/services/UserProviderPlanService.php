<?php

namespace common\modules\userProviderPlan\services;

use common\components\Service;
use common\modules\userProviderPlan\models\UserProviderPlan;

class UserProviderPlanService extends Service {
    public function init() {
        parent::init();
        $this->className = UserProviderPlan::class;
    }
}