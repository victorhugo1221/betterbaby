<?php

namespace common\modules\userProviderPlan\behaviors;

use common\components\Behavior;
use common\modules\plan\models\Plan;
use common\modules\userProviderPlan\models\UserProviderPlan;

class UserProviderPlanBehavior extends Behavior {
    
    
    public function init() {
        parent::init();
    }
    
    public function events() {
        return [
            userProviderPlan::EVENT_BEFORE_INSERT     => 'beforeCreate',
            userProviderPlan::EVENT_BEFORE_UPDATE     => 'beforeUpdate',
            userProviderPlan::EVENT_AFTER_INSERT      => 'afterSave',
            userProviderPlan::EVENT_AFTER_UPDATE      => 'afterSave',
        ];
    }
}