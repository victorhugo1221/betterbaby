<?php

namespace common\modules\userProviderPlan;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('userProviderPlanService', __NAMESPACE__ . '\services\UserProviderPlanService');
    }
}