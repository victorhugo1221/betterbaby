<?php

namespace common\modules\userProviderHealthPlan\services;

use common\components\Service;
use common\modules\userProviderHealthPlan\models\UserProviderHealthPlan;
use common\modules\userProviderPlan\models\UserProviderPlan;

class UserProviderHealthPlanService extends Service {
    public function init() {
        parent::init();
        $this->className = UserProviderHealthPlan::class;
    }
}