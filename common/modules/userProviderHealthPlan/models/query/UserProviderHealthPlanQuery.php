<?php

namespace common\modules\userProviderHealthPlan\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\UserProviderHealthPlan\models\UserProviderHealthPlan]].
 *
 * @see \common\modules\userProviderHealthPlan\models\UserProviderHealthPlan
 */
class UserProviderHealthPlanQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\modules\userProviderHealthPlan\models\UserProviderHealthPlan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\userProviderHealthPlan\models\UserProviderHealthPlan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
