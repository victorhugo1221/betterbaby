<?php

namespace common\modules\userProviderHealthPlan\models;

use common\modules\healthPlan\models\HealthPlan;
use common\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "user_provider_health_plan".
 *
 * @property int $id
 * @property int $user_id
 * @property int $health_plan_id
 * @property string $created
 * @property string $updated
 * @property string|null $deleted
 *
 * @property HealthPlan $healthPlan
 * @property User $user
 */
class UserProviderHealthPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_provider_health_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'health_plan_id'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'health_plan_id' => 'Convênios ',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * Gets query for [[HealthPlan]].
     *
     * @return \yii\db\ActiveQuery|\common\modules\userProviderHealthPlan\models\query\HealthPlanQuery
     */
    public function getHealthPlan()
    {
        return $this->hasOne(HealthPlan::className(), ['id' => 'health_plan_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|\common\modules\userProviderHealthPlan\models\query\UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\userProviderHealthPlan\models\query\UserProviderHealthPlanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\userProviderHealthPlan\models\query\UserProviderHealthPlanQuery(get_called_class());
    }
}
