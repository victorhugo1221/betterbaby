<?php

namespace common\modules\userProviderHealthPlan\behaviors;

use common\components\Behavior;
use common\modules\plan\models\Plan;
use common\modules\UserProviderHealthPlan\models\UserProviderHealthPlan;
use common\modules\userProviderPlan\models\UserProviderPlan;

class UserProviderHealthPlanBehavior extends Behavior {
    
    
    public function init() {
        parent::init();
    }
    
    public function events() {
        return [
            userProviderHealthPlan::EVENT_BEFORE_INSERT     => 'beforeCreate',
            userProviderHealthPlan::EVENT_BEFORE_UPDATE     => 'beforeUpdate',
            userProviderHealthPlan::EVENT_AFTER_INSERT      => 'afterSave',
            userProviderHealthPlan::EVENT_AFTER_UPDATE      => 'afterSave',
        ];
    }
}