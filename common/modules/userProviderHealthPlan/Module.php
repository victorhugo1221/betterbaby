<?php

namespace common\modules\userProviderHealthPlan;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('userProviderHealthPlanService', __NAMESPACE__ . '\services\UserProviderHealthPlanService');
    }
}