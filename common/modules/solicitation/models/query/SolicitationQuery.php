<?php

namespace common\modules\solicitation\models\query;

use common\components\utils\ModelTrait;
use common\modules\solicitation\models\Solicitation;
use Yii;
use yii\db\ActiveQuery;

class SolicitationQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', Solicitation::tableName() . '.deleted', null]);
    }
    
    public function filter($search) {
        
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}

