<?php

namespace common\modules\solicitation\models;

use common\components\Model;
use common\modules\user\models\User;
use Yii;


class Solicitation extends Model {
    
    public $solicitation;
    
    public const STATUS_INACTIVE = 0;
    public const STATUS_PENDENT = 1;
    public const STATUS_ACTIVE = 2;
    public const STATUS_EXPIRED = 3;
    
    
    public static array $_status = [
        self::STATUS_INACTIVE => 'Cancelado',
        self::STATUS_PENDENT  => 'Aguardando confirmação',
        self::STATUS_ACTIVE   => 'Confirmado',
        self::STATUS_EXPIRED  => 'Vencido',
    ];
    
    
    public const STATUS_DEFAULT = 1;
    public const STATUS_DENOUNCED = 2;
    
    public static array $_rating_status = [
        self::STATUS_DEFAULT   => 'padrao',
        self::STATUS_DENOUNCED => 'bloqueado',
    
    ];
    
    
    public static function tableName() {
        return 'solicitation';
    }
    
    
    public function rules() {
        return [
            [['user_client', 'user_provider','rating_stars', 'rating_status', 'status'], 'integer'],
            [['scheduled_date', 'online_service'], 'required'],
            [['cancellation_date', 'scheduled_date', 'created', 'updated', 'deleted'], 'safe'],
            [['price'], 'string', 'max' => 10],
            [['name_client'], 'string', 'max' => 100],
            [['call_url', 'rating_text'], 'string', 'max' => 255],
            [['cancellation_reason'], 'string', 'max' => 255],
        
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'                  => 'ID',
            'user_client'         => 'User Client',
            'user_provider'       => 'User Provider',
            'name_client'         => 'Nome',
            'health_plan'         => 'Convênio',
            'price'               => 'Preço',
            'cancellation_date'   => 'Cancellation Date',
            'cancellation_reason' => 'Motivo do cancelamento',
            'scheduled_date'      => 'Data de agendamento',
            'online_service'      => 'Atendimento Online?',
            'rating_text'         => 'Texto',
            'rating_stars'        => 'Nota (0-5)',
            'rating_status'       => 'Status',
            'status'              => 'Status',
            'created'             => 'Created',
            'updated'             => 'Updated',
            'deleted'             => 'Deleted',
        ];
    }
    
    
    public function getUserClientRel() {
        return $this->hasOne(User::class, ['id' => 'user_client']);
    }
    
    
    public function getUserProviderRel() {
        return $this->hasOne(User::class, ['id' => 'user_provider']);
    }
    
    
    public static function find() {
        return new \common\modules\solicitation\models\query\SolicitationQuery(get_called_class());
    }
}
