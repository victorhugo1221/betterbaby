<?php

namespace common\modules\solicitation\services;

use common\components\Service;
use common\modules\solicitation\models\Solicitation;
use Yii;
use Exception;

setlocale(LC_TIME, 'pt_BR');

class SolicitationService extends Service {
    private $arr;
    
    public function init() {
        parent::init();
        $this->className = Solicitation::class;
    }
    
    public function findById($id) : array {
        $response = Solicitation::find()->andWhere(['user_provider' => $id])->asArray()->all();
        return $response;
        
    }
    
    public function createSolicitation(Solicitation $solicitation, $providerId) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $solicitation->user_provider = $providerId;
            if ($solicitation->online_service == 'nao') {
                $solicitation->classname = 'chill';
            }
            $saveSolicitation = $this->create($solicitation);
            
            if ($saveSolicitation) {
                $transaction->commit();
                return $saveSolicitation;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function createSolicitationApp(Solicitation $solicitation) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $saveSolicitation = $this->create($solicitation);
            
            if ($saveSolicitation) {
                $transaction->commit();
                return $saveSolicitation;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    
    public function confirmsolicitation(int $id) : bool {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = Solicitation::findOne($id);
            $model->status = Solicitation::STATUS_ACTIVE;
            $saveSolicitation = $model->save();
            
            if ($saveSolicitation) {
                $transaction->commit();
                return $saveSolicitation;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function ListSolicitationByUserProvider($id, $searchName, $status) : array {
        $result = [];
        if ($status == '') {
            $solicitation = Solicitation::find()
                ->innerJoinWith('userClientRel')
                ->andWhere(['user_provider' => $id])
                ->andWhere(['like', 'name', $searchName . '%', FALSE])
                ->orderBy(['scheduled_date' => SORT_DESC])
                ->all();
        } else {
            $solicitation = Solicitation::find()
                ->innerJoinWith('userClientRel')
                ->andWhere(['user_provider' => $id])
                ->andWhere(['like', 'name', $searchName . '%', FALSE])
                ->andWhere(['solicitation.status' => $status])
                ->orderBy(['scheduled_date' => SORT_DESC])
                ->all();
        }
        
        
        foreach ($solicitation as $key => $soli) {
            
            $result[] = [
                'id'          => $soli->id,
                'nameClient'  => $soli->name_client,
                'name'        => $soli->userClientRel->name,
                'avatar'      => $_ENV ["BACKENDURL"] . 'uploads/user-client/' . $soli->userClientRel->clientRel->avatar,
                'date'        => $soli->scheduled_date,
                'online'      => $soli->online_service,
                'price'       => $soli->price,
                'status'      => Solicitation::$_status[$soli->status],
                'observation' => $soli->note,
            ];
        }
        return $result;
    }
    
    public function ListSolicitationByUserClient($id) : array {
        $result = [];
        $solicitation = Solicitation::find()->andWhere(['user_client' => $id])->orderBy(['scheduled_date' => SORT_DESC])->all();
        
        foreach ($solicitation as $key => $soli) {
            
            $result[] = [
                'id'           => $soli->id,
                'name'         => $soli->userProviderRel->name,
                'avatar'       => $_ENV ["BASEURL"] . 'uploads/user-provider/' . $soli->userProviderRel->providerRel->avatar,
                'date'         => $soli->scheduled_date,
                'online'       => $soli->online_service,
                'price'        => $soli->price,
                'status'       => Solicitation::$_status[$soli->status],
                'observation'  => $soli->note,
                'rating_title' => $soli->rating_title,
                'rating_text'  => $soli->rating_text,
                'rating_stars' => $soli->rating_stars,
            ];
        }
        return $result;
    }
    
    
    public function ListDetailsSolicitation($id) : array {
        $result = [];
        $solicitation = Solicitation::find()->andWhere(['id' => $id])->all();
        
        foreach ($solicitation as $key => $soli) {
            $result[] = [
                'id'                  => $soli->id,
                'name'                => $soli->userProviderRel->name,
                'name_client'         => $soli->userClientRel->name,
                'cellphone'           => $soli->userClientRel->cellphone,
                'avatar'              => $_ENV ["BACKENDURL"] . 'uploads/user-client/' . $soli->userClientRel->clientRel->avatar,
                'avatarProvider'      => $_ENV ["BASEURL"] . 'uploads/user-provider/' . $soli->userProviderRel->providerRel->avatar,
                'date'                => $soli->scheduled_date,
                'callUrl'             => $soli->call_url,
                'online'              => $soli->online_service,
                'price'               => $soli->price,
                'status'              => Solicitation::$_status[$soli->status],
                'observation'         => $soli->note,
                'guidelines'          => $soli->userProviderRel->providerRel->guidelines,
                'cancellation_reason' => $soli->cancellation_reason,
                'rating_title'        => $soli->rating_title,
                'rating_text'         => $soli->rating_text,
                'rating_stars'        => $soli->rating_stars,
                'city'                => $soli->userProviderRel->providerRel->addressRel->city,
                'district'            => $soli->userProviderRel->providerRel->addressRel->district,
                'street'              => $soli->userProviderRel->providerRel->addressRel->street,
                'number'              => $soli->userProviderRel->providerRel->addressRel->number,
                'complement'          => $soli->userProviderRel->providerRel->addressRel->complement,
            ];
        }
        return $result;
    }
    
    
    public function ListRatingByUser($id) : array {
        $result = [];
        $solicitation = Solicitation::find()->andWhere(['user_provider' => $id])->andWhere(['rating_status' => 1])->andWhere(['not', ['rating_stars' => NULL]])->orderBy(['scheduled_date' => SORT_ASC])->all();
        
        foreach ($solicitation as $key => $soli) {
            
            $result[] = [
                'id'           => $soli->id,
                'name'         => $soli->name_client,
                'rating_title' => $soli->rating_title,
                'rating_text'  => $soli->rating_text,
                'rating_stars' => $soli->rating_stars,
                'date'         => $soli->scheduled_date,
            ];
        }
        return $result;
    }
    
    public function ListRatingByClient($id) : array {
        $result = [];
        $solicitation = Solicitation::find()->andWhere(['user_client' => $id])->andWhere(['rating_status' => 1])->andWhere(['not', ['rating_stars' => NULL]])->orderBy(['scheduled_date' => SORT_ASC])->all();
        
        foreach ($solicitation as $key => $soli) {
            
            $result[] = [
                'id'           => $soli->id,
                'name'         => $soli->userProviderRel->name,
                'rating_title' => $soli->rating_title,
                'rating_text'  => $soli->rating_text,
                'rating_stars' => $soli->rating_stars,
                'date'         => $soli->scheduled_date,
            ];
        }
        return $result;
    }
    
    
    public function ListSolicitationPending($id) : array {
        $result = [];
        $solicitation = Solicitation::find()->andWhere(['user_provider' => $id])->andWhere(['status' => 1])->orderBy(['scheduled_date' => SORT_ASC])->all();
        
        foreach ($solicitation as $key => $soli) {
            $result[] = [
                'id'          => "$soli->id",
                'name'        => $soli->userClientRel->name,
                'avatar'      => $_ENV ["BACKENDURL"] . 'uploads/user-client/' . $soli->userClientRel->clientRel->avatar,
                'date'        => $soli->scheduled_date,
                'online'      => $soli->online_service,
                'price'       => $soli->price,
                'status'      => Solicitation::$_status[$soli->status],
                'observation' => $soli->note,
            ];
        }
        return $result;
    }
    
    
    public function ListSchedule($id) : array {
        $result = [];
        $solicitation = Solicitation::find()->andWhere(['user_provider' => $id])->andWhere(['status' => 2])->orderBy(['scheduled_date' => SORT_ASC])->all();
        
        foreach ($solicitation as $key => $soli) {
            
            $result[] = [
                'id'     => $soli->id,
                'date'   => date('Y-m-d', strtotime($soli->scheduled_date,)),
                'name'   => $soli->userClientRel->name . ' - ' . date(' H:i', strtotime($soli->scheduled_date)),
                'online' => $soli->online_service,
            ];
        }
        return $result;
    }
    
    public function cancelSolicitation(int $solicitationId, $cancellationReason) : bool {
        try {
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            
            $soli = Solicitation::findOne(['id' => $solicitationId]);
            $soli->status = Solicitation::STATUS_INACTIVE;
            $soli->cancellation_reason = $cancellationReason;
            
            if (!$soli->save()) {
                $transaction->rollBack();
                return FALSE;
            }
            
            $transaction->commit();
            return TRUE;
        } catch (Exception $e) {
            $transaction->rollBack();
            return FALSE;
        }
    }
    
    
    public function expireSolicitation(int $solicitationId) : bool {
        try {
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            
            $itinerary = Solicitation::findOne(['id' => $solicitationId]);
            $itinerary->status = Solicitation::STATUS_EXPIRED;
            
            if (!$itinerary->save()) {
                $transaction->rollBack();
                return FALSE;
            }
            
            $transaction->commit();
            return TRUE;
        } catch (Exception $e) {
            $transaction->rollBack();
            return FALSE;
        }
    }
    
    
    public function activeSolicitation(int $solicitationId) : bool {
        try {
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            
            $solicitation = Solicitation::findOne(['id' => $solicitationId]);
            
            $dataAgendada = new \DateTime($solicitation->scheduled_date, new \DateTimeZone('America/Sao_Paulo'));
            $currentDate = new \DateTime(NULL, new \DateTimeZone('America/Sao_Paulo'));
            
            if ($currentDate < $dataAgendada) {
                $solicitation->status = Solicitation::STATUS_ACTIVE;
            } else {
                $solicitation->status = Solicitation::STATUS_EXPIRED;
            }
            
            if (!$solicitation->save()) {
                $transaction->rollBack();
                return FALSE;
            }
            
            $transaction->commit();
            return TRUE;
        } catch (Exception $e) {
            $transaction->rollBack();
            return FALSE;
        }
    }
    
    
    public function DashboardRevenueDataNew($id) : array {
        
        $currentDate = new \DateTime();
        $countArray = [];
        
        for ($i = 0; $i < 12; $i++) {
            $startDate = new \DateTime(sprintf('first day of -%d month', $i), $currentDate->getTimezone());
            $endDate = new \DateTime(sprintf('last day of -%d month', $i), $currentDate->getTimezone());
            $monthName = strftime('%b', $startDate->getTimestamp());
            
            $count = Solicitation::find()
                ->andWhere(['user_provider' => $id])
                ->andWhere(['status' => 2])
                ->andWhere(['>', 'scheduled_date', $startDate->format('Y-m-d')])
                ->andWhere(['<', 'scheduled_date', $endDate->format('Y-m-d')])
                ->sum('price');
            
            if ($count == NULL) {
                $count = 0;
            }
            
            $countArray[] = $count;
            $monthArray[] = $monthName;
            
        }
        
        $countArray = array_reverse($countArray);
        $monthArray = array_reverse($monthArray);
        
        return [
            'data'      => $countArray,
            'monthName' => $monthArray,
        ];
        
    }
    
    
    /**
     * @throws Exception
     */
    public function DashboardSolicitationDataNew($id) : array {
        
        $currentDate = new \DateTime();
        $countArray = [];
        
        for ($i = 0; $i < 12; $i++) {
            $startDate = new \DateTime(sprintf('first day of -%d month', $i), $currentDate->getTimezone());
            $endDate = new \DateTime(sprintf('last day of -%d month', $i), $currentDate->getTimezone());
            $monthName = strftime('%b', $startDate->getTimestamp());
            
            $count = Solicitation::find()
                ->andWhere(['user_provider' => $id])
                ->andWhere(['status' => 2])
                ->andWhere(['>', 'scheduled_date', $startDate->format('Y-m-d 00:01')])
                ->andWhere(['<', 'scheduled_date', $endDate->format('Y-m-d 23:59')])
                ->count();
            
            
            if ($count == NULL || $count == '0') {
                $count = 0;
            }
            
            $countArray[] = $count;
            $monthArray[] = $monthName;
            
        }
        
        $countArray = array_reverse($countArray);
        $monthArray = array_reverse($monthArray);
        
        return [
            'data'      => $countArray,
            'monthName' => $monthArray,
        ];
        
    }
    
    
}