<?php

namespace common\modules\solicitation;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('solicitationService', __NAMESPACE__ . '\services\SolicitationService');
    }
}