<?php

namespace common\modules\babyName\models;

use Yii;


class BabyName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'baby_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'gender'], 'required'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['name', 'origin'], 'string', 'max' => 55],
            [['gender'], 'string', 'max' => 10],
            [['meaning'], 'string', 'max' => 155],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nome',
            'gender' => 'Sexo',
            'origin' => 'Origem',
            'meaning' => 'Significado',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\babyName\models\query\BabyNameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\babyName\models\query\BabyNameQuery(get_called_class());
    }
}
