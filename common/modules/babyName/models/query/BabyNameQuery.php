<?php

namespace common\modules\babyName\models\query;

use common\modules\babyName\models\BabyName;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\babyName\models\BabyName]].
 *
 * @see \common\modules\babyName\models\BabyName
 */
class BabyNameQuery extends ActiveQuery {
    use \common\components\utils\ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', BabyName::tableName() . '.deleted', NULL]);
    }
    
    public function filter($search) {
        $response = $this->defaultFilter($search);
        return $response;
    }
}
