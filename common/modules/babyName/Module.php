<?php

namespace common\modules\babyName;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('babyNameService', __NAMESPACE__ . '\services\BabyNameService');
    }
}