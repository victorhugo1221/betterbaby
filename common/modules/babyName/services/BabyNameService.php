<?php

namespace common\modules\babyName\services;

use common\components\Service;
use common\modules\babyName\models\BabyName;
use common\modules\news\models\baseCrm;
use common\modules\publicity\models\Publicity;
use common\modules\solicitation\models\Solicitation;
use Yii;
use Exception;

class BabyNameService extends Service {
    public function init() {
        parent::init();
        $this->className = BabyName::class;
    }
    
    public function ListName($searchName) : array {
        $result = [];
        if (!$searchName){
           
            $babyName = BabyName::find()->orderBy('name')->all();
        } else {
            $babyName = BabyName::find()->where(['like', 'name', $searchName . '%', false])->orderBy('name')->all();
        }
        
        foreach ($babyName as $key => $name) {
            
            $result[] = [
                'id'      => $name->id,
                'name'    => $name->name,
                'gender'  => $name->gender,
                'origin'  => $name->origin,
                'meaning' => $name->meaning,
            ];
        }
        return $result;
    }
    
    
}