<?php

namespace common\modules\babyWeekData\services;

use common\components\Service;
use common\modules\babyName\models\BabyName;
use common\modules\babyTimeline\models\BabyTimeline;
use common\modules\babyWeekData\models\BabyWeekData;
use common\modules\news\models\baseCrm;
use common\modules\publicity\models\Publicity;
use common\modules\solicitation\models\Solicitation;
use Yii;
use Exception;

class BabyWeekDataService extends Service {
    public function init() {
        parent::init();
        $this->className = BabyWeekData::class;
    }
    
    public function ListSize() : array {
        return BabyWeekData::find()->
        select([
            'id',
            'size_title AS name',
            'concat (\'' . $_ENV ["BACKENDURL"] . 'uploads/baby-week-data/' . '\', size_photo) as url',
            'baby_size  AS size',
            'baby_weight  AS weight',
            'week_title_size  AS title',
        
        ])
            ->orderBy(['week_title_size' => SORT_ASC])->asArray()->all();
    }
    
    public function ListWeek() : array {
        return BabyWeekData::find()->
        select([
            'id',
            'week_title AS title',
            'week_text AS text',
            'concat (\'' . $_ENV ["BACKENDURL"] . 'uploads/baby-week-data/' . '\', week_photo) as url',
        
        ])
            ->asArray()->all();
    }
    
    
}