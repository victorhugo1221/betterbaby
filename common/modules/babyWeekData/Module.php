<?php

namespace common\modules\babyWeekData;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('babyWeekDataService', __NAMESPACE__ . '\services\BabyWeekDataService');
    }
}