<?php

namespace common\modules\babyWeekData\models\query;

use common\components\utils\ModelTrait;
use yii\db\ActiveQuery;


class BabyWeekDataQuery  extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['status' => 1]);
    }
    
    public function filter($search) {
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}

