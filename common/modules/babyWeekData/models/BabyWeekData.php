<?php

namespace common\modules\babyWeekData\models;

use Yii;


class BabyWeekData extends \yii\db\ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'baby_week_data';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['week_photo', 'week_title', 'week_text', 'size_photo', 'size_title', 'baby_size', 'baby_weight'], 'required'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['week_photo', 'size_photo'], 'string', 'max' => 255],
            [['week_title'], 'string', 'max' => 50],
            [['week_text', 'size_title', 'baby_size', 'baby_weight'], 'string', 'max' => 45],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'          => 'ID',
            'week'        => 'Semana',
            'week_photo'  => 'Week Photo',
            'week_title'  => 'Título semana',
            'week_text'   => 'Texto semana',
            'size_photo'  => 'Imagem (tamanho do bebe)',
            'size_title'  => 'Título (tamanho do bebe)',
            'baby_size'   => 'Tamanho do bebe (cm)',
            'baby_weight' => 'Peso do bebe (kg)',
            'created'     => 'Data de criação',
            'updated'     => 'Updated',
            'deleted'     => 'Deleted',
        ];
    }
    
    /**
     * {@inheritdoc}
     * @return \common\modules\babyWeekData\models\query\BabyWeekDataQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\modules\babyWeekData\models\query\BabyWeekDataQuery(get_called_class());
    }
}
