<?php

namespace common\modules\healthPlan;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('healthPlanService', __NAMESPACE__ . '\services\HealthPlanService');
    }
}