<?php

namespace common\modules\healthPlan\models\query;

use common\components\utils\ModelTrait;
use common\modules\healthPlan\models\HealthPlan;
use Yii;
use yii\db\ActiveQuery;

class HealthPlanQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', HealthPlan::tableName() . '.deleted', null]);
    }
    
    public function filter($search) {
        
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}
