<?php

namespace common\modules\healthPlan\models;

use common\modules\userProviderHealthPlan\models\UserProviderHealthPlan;
use Yii;


class HealthPlan extends \yii\db\ActiveRecord {
    
    
    public const STATUS_ACTIVE = 1;
    public const STATUS_CANCELED = 2;
    
    public static array $_status = [
        self::STATUS_ACTIVE   => 'Ativo',
        self::STATUS_CANCELED => 'Inativo',
    
    ];
    
    public static function tableName() {
        return 'health_plan';
    }
    
    
    public function rules() {
        return [
            [['title'], 'required'],
            [['status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['title'], 'string', 'max' => 100],
        ];
    }
    
    
    public function attributeLabels() {
        return [
            'id'      => 'ID',
            'title'   => 'Nome',
            'status'  => 'Status',
            'created' => 'Data de cadastro',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }
    
    
    public function getUserProviderHealthPlans() {
        return $this->hasMany(UserProviderHealthPlan::className(), ['health_plan_id' => 'id']);
    }
    
    
    public static function find() {
        return new \common\modules\healthPlan\models\query\HealthPlanQuery(get_called_class());
    }
}
