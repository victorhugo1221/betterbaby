<?php

namespace common\modules\HealthPlan\services;

use common\components\Service;
use common\modules\healthPlan\models\HealthPlan;
use common\modules\plan\models\Plan;

class HealthPlanService extends Service {
    public function init() {
        parent::init();
        $this->className = HealthPlan::class;
    }
    
    
    public function ListPlans() : array {
        return HealthPlan::find()->select(['title AS name','id'])->orderBy(['name' => SORT_ASC])->asArray()->all();
        
    }
}