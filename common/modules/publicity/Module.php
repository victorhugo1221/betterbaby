<?php

namespace common\modules\publicity;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('publicityService', __NAMESPACE__ . '\services\PublicityService');
    }
}