<?php

namespace common\modules\publicity\models\query;

use common\components\utils\ModelTrait;
use common\modules\news\models\baseCrm;
use common\modules\publicity\models\Publicity;
use yii\db\ActiveQuery;

class PublicityQuery extends  ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', Publicity::tableName() . '.deleted', null]);
    }
    public function filter($search) {
        $response = $this->defaultFilter($search);
        return $response;
    }
}
