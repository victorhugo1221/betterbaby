<?php

namespace common\modules\publicity\models;

use common\components\utils\Text;
use common\modules\publicityView\models\PublicityView;
use Yii;


class Publicity extends \yii\db\ActiveRecord {
    
    public static array $_directory_file = [
        'photo' => ['folder' => 'uploads/publicity/'],
    ];
    
    
    public static function tableName() {
        return 'publicity';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['photo'], 'required'],
            [['status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['title'], 'string', 'max' => 55],
            [['description', 'site'], 'string', 'max' => 500],
            [['phone', 'instagram', 'facebook'], 'string', 'max' => 55],
            [['photo'], 'file', 'extensions' => 'png,jpg,jpeg,bmp,gif'],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'          => 'ID',
            'title'       => 'Título',
            'description' => 'Descrição',
            'phone'       => 'Celular',
            'instagram'   => 'Instagram',
            'site'        => 'Site',
            'facebook'    => 'Facebook',
            'photo'       => 'Imagem',
            'status'      => 'Status',
            'created'     => 'Data de Criação',
            'updated'     => 'Updated',
            'deleted'     => 'Deleted',
        ];
    }
    
    
    public function upload($field) {
        if (empty($this->{$field}->tempName)) {
            return TRUE;
        }
        
        $caminho_pasta = \Yii::$app->basePath . '/web/' . self::$_directory_file[$field]['folder'] . '/';
        
        if (!is_dir($caminho_pasta)) {
            mkdir($caminho_pasta, 0755, TRUE);
        }
        
        $name = Text::slugify($this->{$field}->baseName) . '-' . time();
        $this->{$field}->saveAs($caminho_pasta . $name . '.' . $this->{$field}->extension);
        $this->{$field}->name = $name . '.' . $this->{$field}->extension;
        $this->{$field} = $name . '.' . $this->{$field}->extension;
        
        return TRUE;
    }
    
    public function getPublicityRel() {
        return $this->hasOne(PublicityView::class, ['publicity_id' => 'id']);
    }
    
    
    
    public static function find() {
        return new \common\modules\publicity\models\query\PublicityQuery(get_called_class());
    }
}
