<?php

namespace common\modules\publicity\services;

use common\components\Service;
use common\modules\news\models\baseCrm;
use common\modules\publicity\models\Publicity;
use common\modules\publicityView\models\PublicityView;
use common\modules\solicitation\models\Solicitation;
use Yii;
use Exception;

class PublicityService extends Service {
    public function init() {
        parent::init();
        $this->className = Publicity::class;
    }
    
    public function ListPublicity() : array {
        $result = [];
        $solicitation = Publicity::find()->all();
        
        foreach ($solicitation as $key => $soli) {
            
            $result[] = [
                'id'          => $soli->id,
                'title'       => $soli->title,
                'description' => $soli->description,
                'photo'       => $_ENV['BACKENDURL'] . 'uploads/publicity/' . $soli->photo,
            ];
        }
        return $result;
    }
    
    
    public function PublicityDetails($id) : array {
        $result = [];
        $publicity = Publicity::find()->andWhere(['id' => $id])->all();
        
        foreach ($publicity as $key => $publi) {
            
            $result[] = [
                'id'          => $publi->id,
                'title'       => $publi->title,
                'description' => $publi->description,
                'phone'       => $publi->phone,
                'instagram'   => $publi->instagram,
                'facebook'    => $publi->facebook,
                'site'        => $publi->site,
                'photo'       => $_ENV['BACKENDURL'] . 'uploads/publicity/' . $publi->photo,
            ];
        }
        return $result;
        
        
    }
    
    public function createPublicityView(PublicityView $publicity) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $savePublicity = $this->create($publicity);
            
            if ($savePublicity) {
                $transaction->commit();
                return $savePublicity;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    
}