<?php

namespace common\modules\adminGw\models\query;

use common\modules\adminGw\models\AuthRule;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\adminGw\models\AuthRule]].
 *
 * @see \common\modules\adminGw\models\AuthRule
 */
class AuthRuleQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AuthRule[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AuthRule|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
