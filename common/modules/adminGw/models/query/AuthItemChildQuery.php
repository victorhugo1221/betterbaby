<?php

namespace common\modules\adminGw\models\query;

use common\modules\adminGw\models\AuthItemChild;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\adminGw\models\AuthItemChild]].
 *
 * @see \common\modules\adminGw\models\AuthItemChild
 */
class AuthItemChildQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AuthItemChild[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AuthItemChild|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
