<?php

namespace common\modules\adminGw\models\query;

use common\components\utils\ModelTrait;
use yii\db\ActiveQuery;

class LogQuery extends ActiveQuery {

	use ModelTrait;

	public function filter($search) {
		if (!empty($search['action_id'])) {
			$this->andWhere(['=', 'action', $search['action_id']]);
		}

		$this->joinWith('userRel');
		if (!empty($search['term']) && !empty($search['field']) && !empty($search['operation'])) {
			switch ($search['field']) {
				case 'user':
					$search['field'] = 'user.name';
					break;
				case 'data':
					$search['field'] = 'data';
					break;
			}
		}
		$response = $this->defaultFilter($search);
		return $response;
	}
}
