<?php

namespace common\modules\adminGw\models\query;

use common\modules\adminGw\models\AuthItem;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\adminGw\models\AuthItem]].
 *
 * @see \common\modules\adminGw\models\AuthItem
 */
class AuthItemQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AuthItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AuthItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
