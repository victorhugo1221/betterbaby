<?php

namespace common\modules\adminGw\models\query;

use common\components\utils\ModelTrait;
use common\modules\adminGw\models\Page;
use yii\db\ActiveQuery;

class PageQuery extends ActiveQuery {
	use ModelTrait;

	public function init() {
		return $this->andWhere(['IS', Page::tableName() . '.deleted', null]);
	}

}