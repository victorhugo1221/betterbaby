<?php

namespace common\modules\adminGw\models\query;


use common\components\utils\ModelTrait;
use common\modules\adminGw\models\Translation;
use Yii;
use yii\db\ActiveQuery;

class TranslationQuery extends ActiveQuery {
	use ModelTrait;

	public function init() {
		return $this->andWhere(['IS', Translation::tableName() . '.deleted', null]);
	}

	public function filter($search) {

	    if(!Yii::$app->user->identity->userLoggedIsGw()){
            $this->andWhere(['=', 'editable_by_customer', true]);
        }

		$response = $this->defaultFilter($search);
		return $response;
	}
}
