<?php

namespace common\modules\adminGw\models;

use yii\behaviors\TimestampBehavior;
use common\modules\adminGw\models\query\AuthItemQuery;
use Yii;
use yii\db\ActiveRecord;
use common\modules\adminGw\behaviors\AuthItemBehavior;

class AuthItem extends ActiveRecord {
	public static function tableName() {
		return 'auth_item';
	}

	public function rules() {
		return [
			[['name', 'type'], 'required'],
			[['type', 'created_at', 'updated_at'], 'integer'],
			[['description', 'data'], 'string'],
			[['name', 'rule_name'], 'string', 'max' => 64],
			[['name'], 'unique'],
			[['rule_name'], 'exist', 'skipOnError' => TRUE, 'targetClass' => AuthRule::class, 'targetAttribute' => ['rule_name' => 'name']],
		];
	}

	public function attributeLabels() {
		return [
			'name'        => Yii::t('app', 'Name'),
			'type'        => Yii::t('app', 'Type'),
			'description' => Yii::t('app', 'Description'),
			'rule_name'   => Yii::t('app', 'Rule Name'),
			'data'        => Yii::t('app', 'Data'),
			'created_at'  => Yii::t('app', 'Created At'),
			'updated_at'  => Yii::t('app', 'Updated At'),
		];
	}

	public function getRuleName() {
		return $this->hasOne(AuthRule::class, ['name' => 'rule_name']);
	}

	public function getAuthItemChildChildRel() {
		return $this->hasMany(AuthItemChild::class, ['child' => 'name']);
	}

	public function getAuthItemChildParentRel() {
		return $this->hasMany(AuthItemChild::class, ['parent' => 'name']);
	}

	public function getParentsRel() {
		return $this->hasMany(AuthItem::class, ['name' => 'parent'])->viaTable('auth_item_child', ['child' => 'name']);
	}

	public function getChildrenRel() {
		return $this->hasMany(AuthItem::class, ['name' => 'child'])->viaTable('auth_item_child', ['parent' => 'name']);
	}

	public static function find() {
		return new AuthItemQuery(get_called_class());
	}

	public function behaviors() {
		return [
			'dependencies' => [
				'class' => AuthItemBehavior::class,
			],
			'timestamp' => [
				'class'      => TimestampBehavior::class,
				'attributes' => [
					static::EVENT_BEFORE_INSERT => ['created', 'updated'],
					static::EVENT_BEFORE_UPDATE => ['updated'],
				],
				'value'      => date('Y-m-d H:i:s'),
			],
		];
	}
}
