<?php

namespace common\modules\adminGw\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use common\modules\adminGw\models\query\AuthAssignmentQuery;
use common\modules\adminGw\behaviors\AuthAssignmentBehavior;

class AuthAssignment extends ActiveRecord {
	public static function tableName() {
		return 'auth_assignment';
	}

	public function rules() {
		return [
			[['item_name', 'user_id'], 'required'],
			[['created_at'], 'integer'],
			[['item_name', 'user_id'], 'string', 'max' => 64],
			[['item_name', 'user_id'], 'unique', 'targetAttribute' => ['item_name', 'user_id']],
			[['item_name'], 'exist', 'skipOnError' => TRUE, 'targetClass' => AuthItem::class, 'targetAttribute' => ['item_name' => 'name']],
		];
	}

	public function attributeLabels() {
		return [
			'item_name'  => Yii::t('app.authentication.model', 'Item Name'),
			'user_id'    => Yii::t('app.authentication.model', 'User ID'),
			'created_at' => Yii::t('app.authentication.model', 'Created At'),
		];
	}

	public static function find() {
		return new AuthAssignmentQuery(get_called_class());
	}

	public function behaviors() {
		return [
			'dependencies' => [
				'class' => AuthAssignmentBehavior::class,
			],
			'timestamp'    => [
				'class'      => TimestampBehavior::class,
				'attributes' => [
					static::EVENT_BEFORE_INSERT => ['created_at'],
				],
				'value'      => date('Y-m-d H:i:s'),
			],
		];
	}

	public function getItemRel() {
		return $this->hasOne(AuthItem::class, ['name' => 'item_name']);
	}
}
