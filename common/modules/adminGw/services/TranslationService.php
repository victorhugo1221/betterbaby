<?php

namespace common\modules\adminGw\services;

use common\components\Service;
use common\modules\adminGw\models\Translation;

class TranslationService extends Service {
    public function init() {
        parent::init();
        $this->className = Translation::class;
    }
    
    public function findTranslation(array $key, int $language = Translation::LANGUAGE_PT): array {
        $column = Translation::$column_language[$language];
        $translations = Translation::find()->select(['key', $column])->andWhere(['key' => $key])->all();
        $response = [];
        foreach ($translations as $translation){
            $response[$translation->key] = $translation->$column;
        }
        return $response;
    }
}