<?php

namespace common\modules\adminGw\services;

use api\components\utils\HttpCode;
use common\components\Service;
use common\components\utils\Text;
use common\modules\proposal\models\Proposal;
use common\modules\user\models\LoginForm;
use common\modules\user\models\User;
use common\modules\adminGw\models\Token;
use common\modules\user\models\UserClient;
use common\modules\userProvider\models\UserProvider;
use common\modules\userProviderPlan\models\UserProviderPlan;
use Exception;
use Redis;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Url;
use yii\web\UnauthorizedHttpException;

class AuthenticationService extends Service {
    
    public function __construct() {
        parent::__construct();
        $this->className = User::class;
    }
    
    public function loginAdministrator($login, bool $validatePassword = TRUE) : ?User {
        $identity = User::find()
            ->andWhere(['email' => $login->email])
            ->andWhere(['is_administrator' => User::IS_ADMINISTRATOR])
            ->one();
        
        if (!isset($identity) && !empty($identity) && $identity->id > 0) {
            return NULL;
        }
        
        return $this->login($login, $identity, $validatePassword);
    }
    
    public function loginUser($login, bool $validatePassword = TRUE) : ?User {
        $identity = User::find()
            ->andWhere(['email' => $login->email])
            ->one();
        
        if (!isset($identity) && !empty($identity) && $identity->id > 0) {
            return NULL;
        }
        
        return $this->loginFront($login, $identity, $validatePassword);
    }
    
    
    
    public function loginClientWeb($login, bool $validatePassword = TRUE) : ?User {
        $identity = User::find()->andWhere(['=', 'email', $login->email])->andWhere(['IS', 'user.deleted', NULL])->innerJoin('user_provider', 'user.id = user_provider.user_id')->one();
        
        if (!isset($identity) && !empty($identity) && $identity->id > 0) {
            return NULL;
        }
        
        return $this->loginFront($login, $identity, $validatePassword);
    }
    
    public function loginClient($login, bool $validatePassword = TRUE) : ?User {
        $identity = User::find()->andWhere(['=', 'email', $login->email])->andWhere(['IS', 'user.deleted', NULL])->innerJoin('user_client', 'user.id = user_client.user_id')->one();
        
        if (!isset($identity) && !empty($identity) && $identity->id > 0) {
            return NULL;
        }
        
        return $this->loginFront($login, $identity, $validatePassword);
    }
    
    
    public function loginAppProfessional($login, bool $validatePassword = TRUE) : ?User {
        $identity = User::find()->andWhere(['=', 'email', $login->email])->andWhere(['IS', 'user.deleted', NULL])->innerJoin('user_provider', 'user.id = user_provider.user_id')->one();
        
        if (!isset($identity) && !empty($identity) && $identity->id > 0) {
            return NULL;
        }
        
        $userProvider = UserProvider::find()->andWhere(['user_id' => $identity->id])->one();
        $userProviderPlan = UserProviderPlan::find()->andWhere(['user_id' => $identity->id])->one();
        $currentDate = new \DateTime(NULL, new \DateTimeZone('America/Sao_Paulo'));
        $endDate = new \DateTime($userProviderPlan->end_date, new \DateTimeZone('America/Sao_Paulo'));
        if (!empty($userProvider)) {
            if ($endDate < $currentDate) {
                return NULL;
            }
        }
        
        return $this->loginFront($login, $identity, $validatePassword);
    }
    
    private function login($login, $identity, bool $validatePassword = TRUE) : ?User {
        Yii::$app->user->logout();
        try {
            $validPassword = (!$validatePassword || Yii::$app->getSecurity()->validatePassword($login->password, $identity->password));
        } catch (InvalidArgumentException|Exception $e) {
            $validPassword = FALSE;
        }
        
        if ($identity && $validPassword && $identity->status == User::STATUS_ACTIVE) {
            Yii::$app->user->login($identity);
            $returnUrl = Yii::$app->user->getReturnUrl() ? : $this->getHomeUrl();
            $login->setReturnUrl($returnUrl);
            
            return $identity;
        }
        return NULL;
    }
    
    private function loginFront($login, $identity, bool $validatePassword = TRUE) : ?User {
        Yii::$app->user->logout();
        try {
            $validPassword = (!$validatePassword || Yii::$app->getSecurity()->validatePassword($login->password, $identity->password));
        } catch (InvalidArgumentException|Exception $e) {
            $validPassword = FALSE;
        }
        
        if ($identity && $validPassword && $identity->status == User::STATUS_ACTIVE) {
            Yii::$app->user->login($identity);
            $returnUrl = Yii::$app->user->getReturnUrl() ? : $this->getHomeUrlFront();
            $login->setReturnUrl($returnUrl);
            
            return $identity;
        }
        return NULL;
    }
    
    public function validatePassword(int $userId, string $password) : int {
        try {
            $validatePassword = FALSE;
            $user = User::findIdentity($userId);
            if ($user) {
                try {
                    $validPassword = (Yii::$app->getSecurity()->validatePassword($password, $user->password));
                } catch (InvalidArgumentException|Exception $e) {
                    $validPassword = FALSE;
                }
                return $validPassword ? 200 : 401;
            }
            return 400;
        } catch (Exception $ex) {
            return 500;
        }
    }
    
    public function findIdentityByAccessToken($token, $type = NULL) {
        try {
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            $tokenService = Yii::$app->getModule('admin-gw')->get('tokenService');
            $ex = explode('/', trim($token));
            $accessToken = $ex[0];
            $nonce = $ex[1] ?? -1;
            $xApiKey = Yii::$app->request->getHeaders()->get('X-Api-Key');
            
            $identity = $tokenService->getUserByToken($xApiKey, Token::PUBLIC_KEY);
            
            if (!$identity) {
                return;
            }
            
            $privateKey = $tokenService->getPrivateKeyUsuarioId($identity->id);
            if (!$privateKey) {
                return;
            }
            
            $master = TRUE; //\Yii::$app->authManager->checkAccess($identity->id, 'grupow');
            $hash = hash('sha256', $privateKey->uid . '/' . $xApiKey . '/' . $nonce);
            $publicKey = $tokenService->getToken($xApiKey, Token::PUBLIC_KEY);
            if (!$master && $nonce < $publicKey->last_nonce) {
                throw new UnauthorizedHttpException("Invalid nonce: must be higher than previous");
                return;
            }
            if ($accessToken !== $hash) {
                throw new UnauthorizedHttpException("Invalid credentials: hash does not match");
                return;
            }
            $tokenService->updateLastNonce($publicKey, $nonce);
            $transaction->commit();
            
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        
        $identity->trigger(User::EVENT_SUCCESS_LOGIN);
        
        return $identity;
    }
    
    public function getHomeUrl($user = NULL) : string {
        
        $homeUrl = Yii::$app->homeUrl;
        $sistemaUrl = getenv('BASE_URL_SISTEMA');
        $siteUrl = getenv('BASE_URL');
        
        if (Yii::$app->user->can('grupow')) {
            $homeUrl = $sistemaUrl . Url::to(['/dashboard']);
        } else {
            $homeUrl = Url::to(['/user/default/painel'], TRUE);
        }
        
        return $homeUrl;
    }
    
    
    public function getHomeUrlFront($user = NULL) : string {
        
        $homeUrl = Yii::$app->homeUrl;
        
        return $homeUrl;
    }
    
    public function getInitialData(int $userId) : array {
        $response = User::find()->select(['user.name']);
        $response->andWhere(['=', 'user.id', $userId]);
        return $response->asArray()->one() ?? [];
    }
    
    public function validateAccessByActiveSessions(int $companyId, int $qtyUsers) : bool {
        $redis = new \Redis();
        $redis->connect($_ENV['AMP_WSK_SRV_REDIS'], $_ENV['AMP_WSK_PORT_REDIS']);
        $redis->select($_ENV['AMP_WSK_DB_REDIS']);
        
        $activeConnections = $redis->hGetAll($_ENV['AMP_WSK_KEY_SESSION']);
        if ($activeConnections) {
            $qty = 0;
            foreach ($activeConnections as $key => $value) {
                if (strstr($key, 'cnx_')) {
                    $data = json_decode($value);
                    if ((int) $data->companyId === $companyId) {
                        $qty++;
                    }
                }
            }
            
            if ($qty >= $qtyUsers) {
                return FALSE;
            }
        }
        return TRUE;
    }
}
