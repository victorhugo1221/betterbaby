<?php

namespace common\modules\adminGw\behaviors;

use common\components\Behavior;
use common\modules\adminGw\models\Token;


class TokenBehavior extends Behavior {
    public       $name;
    public       $token;
    public array $booleanFields = ['status'];

	public function events() {
        return [
            Token::EVENT_BEFORE_INSERT => 'beforeCreate',
            Token::EVENT_BEFORE_UPDATE => 'beforeUpdate',
        ];
    }


	/*    public function afterSave($event) {
		$this->saveLog($this->owner->id, 'DiscountCoupon', $this->_action, $this->_log);
	}
	*/
    public function afterCreate($event){
        parent::afterSave($event);
        
        
    }
}