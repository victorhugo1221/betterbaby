<?php

namespace common\modules\adminGw\behaviors;

use common\components\Behavior;
use common\modules\adminGw\models\AuthAssignment;

class AuthAssignmentBehavior extends Behavior {
    public       $name;
    public       $token;
    public array $booleanFields = ['status'];

	public function events() {
        return [
            AuthAssignment::EVENT_BEFORE_INSERT => 'beforeCreate',
            AuthAssignment::EVENT_BEFORE_UPDATE => 'beforeUpdate'
        ];
    }
}