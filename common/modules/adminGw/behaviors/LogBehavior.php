<?php

namespace common\modules\adminGw\behaviors;

use common\components\Behavior;
use common\modules\adminGw\models\Log;


class LogBehavior extends Behavior {
    public       $name;
    public       $token;
    public array $booleanFields = ['status'];

	public function events() {
        return [
            Log::EVENT_BEFORE_INSERT => 'beforeCreate',
            Log::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            Log::EVENT_AFTER_INSERT  => 'afterCreate',
            Log::EVENT_AFTER_UPDATE  => 'afterSave',
        ];
    }


	/*    public function afterSave($event) {
		$this->saveLog($this->owner->id, 'DiscountCoupon', $this->_action, $this->_log);
	}
	*/
    public function afterCreate($event){
        parent::afterSave($event);
        
        
    }
}