<?php

namespace common\modules\adminGw\behaviors;

use common\components\Behavior;
use common\modules\adminGw\models\AuthItem;


class AuthItemBehavior extends Behavior {
    public       $name;
    public       $token;
    public array $booleanFields = ['status'];

	public function events() {
        return [
            AuthItem::EVENT_BEFORE_INSERT => 'beforeCreate',
            AuthItem::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            AuthItem::EVENT_AFTER_INSERT  => 'afterCreate',
            AuthItem::EVENT_AFTER_UPDATE  => 'afterSave',
        ];
    }


	/*    public function afterSave($event) {
        $this->saveLog($this->owner->id, 'DiscountCoupon', $this->_action, $this->_log);
    }
    */
    public function afterCreate($event){
        parent::afterSave($event);
        
        
    }
}