<?php

namespace common\modules\serviceSpecialty\services;

use common\components\Service;
use common\modules\order\models\Order;
use common\modules\serviceSpecialty\models\ServiceSpecialty;
use common\modules\specialty\models\Specialty;

class ServiceSpecialtyService extends Service {
    public function init() {
        parent::init();
        $this->className = ServiceSpecialty::class;
    }
    
    
    public function ListSpecialty() : array {
        return ServiceSpecialty::find()->select(['name', 'id'])->orderBy(['name' => SORT_ASC])->asArray()->all();
        
    }
}