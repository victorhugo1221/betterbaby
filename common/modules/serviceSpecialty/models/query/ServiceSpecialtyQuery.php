<?php

namespace common\modules\serviceSpecialty\models\query;

use common\components\utils\ModelTrait;
use common\modules\healthPlan\models\HealthPlan;
use common\modules\serviceSpecialty\models\ServiceSpecialty;
use Yii;
use yii\db\ActiveQuery;

class ServiceSpecialtyQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['status' => 1])->andWhere(['IS', ServiceSpecialty::tableName() . '.deleted', NULL]);
    }
    
    public function filter($search) {
        
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}
