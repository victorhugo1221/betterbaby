<?php

namespace common\modules\serviceSpecialty\models;

use Yii;

/**
 * This is the model class for table "service_specialty".
 *
 * @property int $id
 * @property string $name
 * @property string $created
 */
class ServiceSpecialty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_specialty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created','deleted','status'], 'safe'],
            [['name'], 'string', 'max' => 155],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Título',
            'status' => 'status',
            'created' => 'Data de criação',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\serviceSpecialty\models\query\ServiceSpecialtyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\serviceSpecialty\models\query\ServiceSpecialtyQuery(get_called_class());
    }
}
