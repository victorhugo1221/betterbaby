<?php

namespace common\modules\serviceSpecialty;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('serviceSpecialtyService', __NAMESPACE__ . '\services\ServiceSpecialtyService');
    }
}