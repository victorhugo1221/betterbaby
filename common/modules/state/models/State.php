<?php

namespace common\modules\state\models;

use common\components\Model;
use common\modules\city\models\City;
use common\modules\state\models\query\StateQuery;
use yii\db\ActiveQuery;

class State extends Model {
    public static function tableName() {
        return 'state';
    }
    
    public function rules() {
        return [
            [['name', 'code'], 'required'],
            [['status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['name'], 'string', 'max' => 30],
            [['code'], 'string', 'max' => 2],
        ];
    }
    
    public function attributeLabels() {
        return [
            'id'      => 'Id',
            'name'    => 'UF',
            'code'    => 'Sigla',
            'status'  => 'Status',
            'created' => 'Data de Cadastro',
            'updated' => 'Data da última atualização',
            'deleted' => 'Data da exclusão',
        ];
    }
    
    public function getCities(): ActiveQuery {
        return $this->hasMany(City::class, ['state_id' => 'id']);
    }
    
    public static function find(): StateQuery {
        return new StateQuery(get_called_class());
    }
    
}
