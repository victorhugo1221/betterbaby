<?php

namespace common\modules\state\models\query;

use yii\db\ActiveQuery;
use common\modules\state\models\State;
use common\components\utils\ModelTrait;

/**
 * This is the ActiveQuery class for [[\common\modules\state\models\State]].
 *
 * @see \common\modules\state\models\State
 */
class StateQuery extends ActiveQuery {
	use ModelTrait;

	public function init() {
		return $this->andWhere(['IS', State::tableName() . '.deleted', NULL]);
	}

	/**
	 * {@inheritdoc}
	 * @return State[]|array
	 */
	public function all($db = NULL) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return State|array|null
	 */
	public function one($db = NULL) {
		return parent::one($db);
	}
}
