<?php

namespace common\modules\state;

class Module extends \yii\base\Module {

	public $controllerNamespace = __NAMESPACE__ . '\controllers';

	public function init() {
		parent::init();
        $this->set('stateService', ['class' => __NAMESPACE__ . '\services\StateService']);
	}
}
