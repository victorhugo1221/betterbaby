<?php

namespace common\modules\state\services;

use common\modules\state\models\query\StateQuery;
use common\modules\state\models\State;
use common\components\Service;

class StateService extends Service {
    public function init() {
        parent::init();
        $this->className = State::class;
    }

    public function findById(string $id): State {
        return State::findOne($id);
    }
    
    public function findStates(?string $name = NULL) : array {
        $respose = State::find();
        
        if(!empty($name)){
            $respose->andWhere(['LIKE', 'name', $name]);
        }
        
        return $respose->orderBy('name')->asArray()->all();
    }

		public function getByInitials(string $initials) {
			return State::find()->andWhere("code='$initials'")->one();
		}
}
