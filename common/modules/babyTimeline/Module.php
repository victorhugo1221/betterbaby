<?php

namespace common\modules\babyTimeline;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('babyTimelineService', __NAMESPACE__ . '\services\BabyTimelineService');
    }
}