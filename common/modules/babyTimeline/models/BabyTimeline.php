<?php

namespace common\modules\babyTimeline\models;

use Yii;

class BabyTimeline extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'baby_timeline';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'week'], 'required'],
            [['week'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['title'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Título',
            'week' => 'Semana',
            'created' => 'Data Criação',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\babyTimeline\models\query\BabyTimelineQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\babyTimeline\models\query\BabyTimelineQuery(get_called_class());
    }
}
