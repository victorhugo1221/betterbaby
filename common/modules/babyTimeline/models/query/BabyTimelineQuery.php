<?php

namespace common\modules\babyTimeline\models\query;

use common\components\utils\ModelTrait;
use common\modules\babyTimeline\models\BabyTimeline;
use yii\db\ActiveQuery;


class BabyTimelineQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', BabyTimeline::tableName() . '.deleted', null]);
    }
    public function filter($search) {
        $response = $this->defaultFilter($search);
        return $response;
    }
}
