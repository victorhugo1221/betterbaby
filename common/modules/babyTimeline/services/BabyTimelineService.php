<?php

namespace common\modules\babyTimeline\services;

use common\components\Service;
use common\modules\babyName\models\BabyName;
use common\modules\babyTimeline\models\BabyTimeline;
use common\modules\news\models\baseCrm;
use common\modules\publicity\models\Publicity;
use common\modules\solicitation\models\Solicitation;
use Yii;
use Exception;

class BabyTimelineService extends Service {
    public function init() {
        parent::init();
        $this->className = BabyTimeline::class;
    }
    
    public function ListTimeline() : array {
        return BabyTimeline::find()->select(['id', 'title', 'week', 'color'])
            ->orderBy(['week' => SORT_ASC])->asArray()->all();
    }
    
    
}