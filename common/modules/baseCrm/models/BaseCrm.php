<?php

namespace common\modules\baseCrm\models;

use common\components\utils\Text;
use Yii;


class BaseCrm extends \yii\db\ActiveRecord {
    
    public static array $_directory_file = [
        'photo' => ['folder' => 'uploads/news/'],
    ];
    
    public static function tableName() {
        return 'base_crm';
    }
    
    
    public function rules() {
        return [
            [['name', 'crm'], 'required'],
            [['crm', 'state', 'name','type','situation','specialty'], 'safe'],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'        => 'ID',
            'crm'       => 'CRM',
            'state'     => 'Estado',
            'name'      => 'Nome',
            'type'      => 'Tipo',
            'situation' => 'Situação',
            'specialty' => 'Especialidade',
        ];
    }
    
    
    /**
     * {@inheritdoc}
     * @return \common\modules\baseCrm\models\query\BaseCrmQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\modules\baseCrm\models\query\BaseCrmQuery(get_called_class());
    }
}
