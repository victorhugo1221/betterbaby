<?php

namespace common\modules\baseCrm\models\query;

use common\components\utils\ModelTrait;
use common\modules\baseCrm\models\BaseCrm;
use yii\db\ActiveQuery;


class BaseCrmQuery extends  ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', BaseCrm::tableName() . '.deleted', null]);
    }
    public function filter($search) {
        $response = $this->defaultFilter($search);
        return $response;
    }
}