<?php

namespace common\modules\baseCrm\services;

use common\components\Service;
use common\modules\baseCrm\models\BaseCrm;
use Yii;
use Exception;

class BaseCrmService extends Service {
    public function init() {
        parent::init();
        $this->className = BaseCrm::class;
    }
    
    public function SearchCrm($crm) : array {
        return  BaseCrm::find()->select([
            'id',
            'name',
        ])->andWhere(['crm' => $crm])->all();
    }
    
}