<?php

namespace common\modules\baseCrm;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('baseCrmService', __NAMESPACE__ . '\services\BaseCrmService');
    }
}