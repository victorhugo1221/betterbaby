<?php

namespace common\modules\news\models\query;

use common\components\utils\ModelTrait;
use common\modules\news\models\baseCrm;
use yii\db\ActiveQuery;


class NewsQuery extends  ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', baseCrm::tableName() . '.deleted', null]);
    }
    public function filter($search) {
        $response = $this->defaultFilter($search);
        return $response;
    }
}