<?php

namespace common\modules\news\models;

use common\components\utils\Text;
use Yii;


class News extends \yii\db\ActiveRecord {
    
    public static array $_directory_file = [
        'photo' => ['folder' => 'uploads/news/'],
    ];
    
    public static function tableName() {
        return 'news';
    }
    
    
    public function rules() {
        return [
            [['title', 'description'], 'required'],
            [['created', 'updated', 'deleted'], 'safe'],
            [['title'], 'string', 'max' => 150],
            [['source'], 'string', 'max' => 150],
            [['description'], 'string', 'max' => 1000],
            [['photo'], 'file', 'extensions' => 'png,jpg,jpeg,bmp,gif'],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'          => 'ID',
            'title'       => 'Título',
            'description' => 'Descrição',
            'source'      => 'Fonte',
            'photo'       => 'Imagem',
            'created'     => 'Data de Criação',
            'updated'     => 'Updated',
            'deleted'     => 'Deleted',
        ];
    }
    
    public function upload($field) {
        if (empty($this->{$field}->tempName)) {
            return TRUE;
        }
        
        $caminho_pasta = \Yii::$app->basePath . '/web/' . self::$_directory_file[$field]['folder'] . '/';
        
        if (!is_dir($caminho_pasta)) {
            mkdir($caminho_pasta, 0755, TRUE);
        }
        
        $name = Text::slugify($this->{$field}->baseName) . '-' . time();
        $this->{$field}->saveAs($caminho_pasta . $name . '.' . $this->{$field}->extension);
        $this->{$field}->name = $name . '.' . $this->{$field}->extension;
        $this->{$field} = $name . '.' . $this->{$field}->extension;
        
        return TRUE;
    }
    
    /**
     * {@inheritdoc}
     * @return \common\modules\news\models\query\NewsQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\modules\news\models\query\NewsQuery(get_called_class());
    }
}
