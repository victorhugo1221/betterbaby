<?php

namespace common\modules\news;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('newsService', __NAMESPACE__ . '\services\NewsService');
    }
}