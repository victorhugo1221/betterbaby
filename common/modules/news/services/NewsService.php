<?php

namespace common\modules\news\services;

use common\components\Service;
use common\modules\news\models\baseCrm;
use common\modules\news\models\News;
use common\modules\solicitation\models\Solicitation;
use Yii;
use Exception;

class NewsService extends Service {
    public function init() {
        parent::init();
        $this->className = News::class;
    }
    
    public function ListNews() : array {
        $result = [];
        $solicitation = News::find()->all();
        
        foreach ($solicitation as $key => $soli) {
            
            $result[] = [
                'id'          => $soli->id,
                'title'       => $soli->title,
                'description' => $soli->description,
                'source'      => $soli->source,
                'photo'       => $_ENV['BACKENDURL'] . 'uploads/news/' . $soli->photo,
            ];
        }
        return $result;
    }
    
}