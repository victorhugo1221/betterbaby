<?php

namespace common\modules\plan;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('planService', __NAMESPACE__ . '\services\PlanService');
    }
}