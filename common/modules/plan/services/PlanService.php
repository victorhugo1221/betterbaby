<?php

namespace common\modules\plan\services;

use common\components\Service;
use common\modules\plan\models\Plan;

class PlanService extends Service {
    public function init() {
        parent::init();
        $this->className = Plan::class;
    }
}