<?php

namespace common\modules\plan\models\query;

use common\components\utils\ModelTrait;
use common\modules\plan\models\Plan;
use Yii;
use yii\db\ActiveQuery;

class PlanQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', Plan::tableName() . '.deleted', null]);
    }
    
    public function filter($search) {
        
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}
