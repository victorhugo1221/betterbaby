<?php

namespace common\modules\plan\models;
use common\components\Model;
use common\modules\plan\behaviors\HealthPlanBehavior;
use common\modules\plan\models\query\PlanQuery;
use Yii;


class Plan extends Model
{
    const EVENT_SUCCESS_LOGIN = 'eventSuccessLogin';
    const EVENT_NEW_PASSWORD = 'onNewPassword';
    const EVENT_PASSWORD_RECOVERY = 'onPasswordRecovery';
    
    
    public const STATUS_PENDENT = 0;
    public const STATUS_ACTIVE = 1;
    
    public static array $_status = [
        self::STATUS_PENDENT => 'Aguardando ativação',
        self::STATUS_ACTIVE  => 'Ativo',
    ];
    
    
    public static function tableName()
    {
        return 'plan';
    }

    public function rules()
    {
        return [
            [['title', 'price',], 'required'],
            [['status'], 'integer'],
            [['created', 'updated', 'deleted'], 'safe'],
           
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Título',
            'price' => 'Preço',
            'status' => 'Status',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

  
    public function getCompanyPlans()
    {
        return $this->hasMany(CompanyPlan::className(), ['plan_id' => 'id']);
    }
    
    public static function find()
    {
        return new \common\modules\plan\models\query\PlanQuery(get_called_class());
    }
}
