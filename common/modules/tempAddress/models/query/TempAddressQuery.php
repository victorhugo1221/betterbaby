<?php

namespace common\modules\tempAddress\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\tempAddress\models\TempAddress]].
 *
 * @see \common\modules\tempAddress\models\TempAddress
 */
class TempAddressQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\modules\tempAddress\models\TempAddress[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\tempAddress\models\TempAddress|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
