<?php

namespace common\modules\tempAddress\models;

use Yii;


class TempAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temp_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'city', 'district', 'state', 'street', 'zip_code', 'longitude', 'latitude'], 'required'],
            [['user_id'], 'integer'],
            [['created'], 'safe'],
            [['title', 'state', 'number'], 'string', 'max' => 45],
            [['city', 'district', 'street'], 'string', 'max' => 100],
            [['zip_code'], 'string', 'max' => 9],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'city' => 'City',
            'district' => 'District',
            'state' => 'State',
            'street' => 'Street',
            'number' => 'Number',
            'zip_code' => 'Zip Code',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'created' => 'Created',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\tempAddress\models\query\TempAddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\tempAddress\models\query\TempAddressQuery(get_called_class());
    }
}
