<?php

namespace common\modules\tempAddress\services;

use common\components\Service;
use common\modules\order\models\Order;
use common\modules\specialty\models\Specialty;
use common\modules\tempAddress\models\TempAddress;
use Exception;
use Yii;

class TempAddressService extends Service {
    public function init() {
        parent::init();
        $this->className = TempAddress::class;
    }
    
    
    public function ListAddress($id) : array {
        return TempAddress::find()->select([
            'id',
            'title',
            'city',
            'district',
            'state',
            'street',
            'number',
            'longitude',
            'latitude',
        ])
            ->andWhere(['user_id' => $id])
            ->asArray()
            ->all();
    }
    
    public function createAddress(TempAddress $address) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $saveAddress = $this->create($address);
            
            if ($saveAddress) {
                $transaction->commit();
                return $saveAddress;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
}