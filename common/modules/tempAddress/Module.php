<?php

namespace common\modules\tempAddress;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('tempAddressService', __NAMESPACE__ . '\services\TempAddressService');
    }
}