<?php

namespace common\modules\publicityView\models\query;


class PublicityViewQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\modules\publicityView\models\PublicityView[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\publicityView\models\PublicityView|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
