<?php

namespace common\modules\publicityView\models;

use common\components\Model;
use common\modules\publicity\models\Publicity;
use common\modules\user\models\User;
use Yii;


class PublicityView extends Model
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'publicity_view';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['publicity_id', 'user_id'], 'required'],
            [['publicity_id', 'user_id'], 'integer'],
            [['created', 'deleted'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'publicity_id' => 'Publicity ID',
            'user_id' => 'User ID',
            'created' => 'Created',
            'deleted' => 'Deleted',
        ];
    }
    
    public function getUserClientRel() {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
    public function getPublicityRel() {
        return $this->hasOne(Publicity::class, ['id' => 'publicity_id']);
    }
    
    
    /**
     * {@inheritdoc}
     * @return \common\modules\publicityView\models\query\PublicityViewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\publicityView\models\query\PublicityViewQuery(get_called_class());
    }
}
