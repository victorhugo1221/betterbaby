<?php

namespace common\modules\publicityView;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('publicityViewService', __NAMESPACE__ . '\services\PublicityViewService');
    }
}