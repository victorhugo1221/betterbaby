<?php

namespace common\modules\userProvider\services;

use api\components\utils\HttpCode;
use common\components\Service;
use common\modules\address\models\Address;
use common\modules\baseCrm\models\BaseCrm;
use common\modules\favoriteProvider\models\FavoriteProvider;
use common\modules\plan\models\Plan;
use common\modules\solicitation\models\Solicitation;
use common\modules\user\models\User;
use common\modules\user\models\UserClient;
use common\modules\userProvider\models\UserProvider;
use common\modules\userProviderHealthPlan\models\UserProviderHealthPlan;
use common\modules\userProviderPlan\models\UserProviderPlan;
use Yii;
use Exception;
use yii\db\ActiveRecord;

class UserProviderService extends Service {
    public function init() {
        parent::init();
        $this->className = UserProvider::class;
    }
    
    public function createUser(User $user, $password) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user->status = User::STATUS_ACTIVE;
            $user->password = Yii::$app->security->generatePasswordHash($password);
            $saveUser = $this->create($user);
            
            if ($saveUser) {
                $transaction->commit();
                return $saveUser;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function createUserApp(User $user) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user->status = User::STATUS_ACTIVE;
            $saveUser = $this->create($user);
            
            if ($saveUser) {
                $transaction->commit();
                return $saveUser;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function createUserProvider(UserProvider $userProvider, $userId) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userProvider->user_id = $userId;
            $saveUserProvider = $this->create($userProvider);
            
            if ($saveUserProvider) {
                $transaction->commit();
                return $saveUserProvider;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function createUserClient(UserClient $userClient, $userId) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userClient->user_id = $userId;
            $saveUserClient = $this->create($userClient);
            
            if ($saveUserClient) {
                $transaction->commit();
                return $saveUserClient;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    
    public function createAddress(Address $address, $userId) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $address->user_id = $userId;
            $saveAddress = $this->create($address);
            if ($saveAddress) {
                $transaction->commit();
                return $saveAddress;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function createUserProviderPlan(UserProviderPlan $userProviderPlan, $userId) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userProviderPlan->user_id = $userId;
            $saveUserProviderPlan = $this->create($userProviderPlan);
            if ($saveUserProviderPlan) {
                $transaction->commit();
                return $saveUserProviderPlan;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function createUserProviderHealthPlan(UserProviderHealthPlan $userProviderService, $userId) : int {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $userProviderService->user_id = $userId;
            $saveUserProviderService = $this->create($userProviderService);
            if ($saveUserProviderService) {
                $transaction->commit();
                return $saveUserProviderService;
            } else {
                $transaction->rollBack();
                return 0;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    
    public function updateUserProvider(User $user, UserProvider $userProvider, Address $address) : bool {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $saveUser = $this->update($user);
            $saveUserProvider = $this->update($userProvider);
            $saveAddress = $this->update($address);
            
            if ($saveUser && $saveUserProvider && $saveAddress) {
                $transaction->commit();
                return TRUE;
            } else {
                $transaction->rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function updateAvatar(int $id, string $avatar) : bool {
        $user = UserProvider::find()->andWhere(['user_id' => $id])->one();
        if ($user && $user->avatar != $avatar) {
            $user->avatar = $avatar;
            $user->save();
        }
        return TRUE;
    }
    
    public function delete(int $id) : bool {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = User::findOne($id);
            $model->deleted = date('Y-m-d h:m:s');
            $model->status = User::STATUS_ADMIN_INACTIVE;
            $deleteUser = $model->save();
            
            
            if ($deleteUser) {
                $transaction->commit();
                return TRUE;
            } else {
                $transaction->rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            return FALSE;
            throw $e;
        }
    }
    
    
    public function ListAllUserProvider($userId, $latitude, $longitude, $rangeKm, $searchText, $category, $acceptPne, $princeRange, $doctorSpecialty, $healthPlan, $serviceSpecialty) : array {
    
        return Yii::$app->db->createCommand(
            "SELECT
                user.id,
                user.name,
                CONCAT('http://profissional.betterbaby.com.br/', 'uploads/user-provider/', user_provider.avatar) AS avatar,
                user_provider.crm_specialty,
                user_provider.additional_specialty,
                user_provider.profile_text,
                user_provider.average_rating,
                user_provider.price_range,
                user_provider.accept_pne,
                user_provider.health_plan,
                user_provider.category_id,
                user_provider.service_category,
                address.city,
                address.latitude,
                address.longitude,
                favorite_provider.user_provider_id AS favorite,
                FORMAT(( 6371 * acos( cos( radians(c.lat) )
                        * cos( radians( address.latitude ) )
                        * cos( radians( address.longitude ) - radians(c.lng) )
                        + sin( radians(c.lat) )
                        * sin( radians( address.latitude ) ) ) ) ,2, 'pt-br' ) AS distancia
            FROM
            	user AS user
            	INNER JOIN user_provider on (user.id = user_provider.user_id)
            	INNER JOIN address on (user.id = address.user_id)
            	LEFT JOIN favorite_provider ON user.id = favorite_provider.user_provider_id AND '$userId' = favorite_provider.user_client_id
                JOIN (
                    SELECT
                    '$latitude'AS lat,
                  	'$longitude' AS lng
                ) AS c
            WHERE user.name LIKE '%$searchText%'
            AND user.deleted IS NULL
            AND IFNULL(user_provider.category_id,'') LIKE '%$category%'
            AND IFNULL(user_provider.accept_pne,'') LIKE '%$acceptPne%'
            AND IFNULL(user_provider.price_range,'') LIKE '%$princeRange%'
            AND CONCAT(IFNULL(user_provider.crm_specialty,''), ' ', IFNULL(user_provider.additional_specialty,'')) LIKE '%$doctorSpecialty%'
            AND IFNULL(user_provider.health_plan,'') LIKE '%$healthPlan%'
            AND IFNULL(user_provider.service_category,'') LIKE '%$serviceSpecialty%'
            HAVING distancia < $rangeKm
            ORDER BY
            	user_provider.ranking DESC, distancia  ")->queryAll();
        
    }
    
    
    public function ListFavoriteProvider($id, $searchName) : array {
        return FavoriteProvider::find()->select([
            'user.id',
            'user.name',
            'concat (\'' . $_ENV ["BASEURL"] . 'uploads/user-provider/' . '\', user_provider.avatar) as avatar',
            'user_provider.health_plan',
            'user_provider.service_category',
            'user_provider.crm',
            'user_provider.crm_state',
            'user_provider.crm_specialty',
            'user_provider.additional_specialty',
            'user_provider.instagram_url',
            'user_provider.facebook_url',
            'user_provider.linkedin_url',
            'user_provider.profile_text',
            'user_provider.average_rating',
            'user_provider.price_range_health_plan',
            'user_provider.price_range',
            'user_provider.accept_pne',
            'user_provider.category_id',
            'address.city',
            'address.state',
            'address.street',
            'address.number',
            'address.complement',
            'address.district',
            'address.zip_code',
            'address.latitude',
            'address.longitude',
        ])
            ->innerJoin('user', 'favorite_provider.user_provider_id = user.id')
            ->innerJoin('user_provider', 'user.id = user_provider.user_id')
            ->innerJoin('address', 'user.id = address.user_id')
            ->andWhere(['like', 'user.name', $searchName . '%', FALSE])
            ->andWhere(['favorite_provider.user_client_id' => $id])->asArray()->all();
        
        
    }
    
    
    public function ListUserData($id) : array {
        return User::find()->select([
            'user.id',
            'user.name',
            'user.cpf',
            'user.cellphone',
            'user.email',
            'concat (\'' . $_ENV ["BASEURL"] . 'uploads/user-provider/' . '\', user_provider.avatar) as avatar',
            'user_provider.health_plan',
            'user_provider.service_category',
            'user_provider.crm',
            'user_provider.crm_state',
            'user_provider.crm_specialty',
            'user_provider.additional_specialty',
            'user_provider.instagram_url',
            'user_provider.facebook_url',
            'user_provider.linkedin_url',
            'user_provider.profile_text',
            'user_provider.average_rating',
            'user_provider.price_range_health_plan',
            'user_provider.price_range',
            'user_provider.accept_pne',
            'user_provider.category_id',
            'user_provider.start_hour',
            'user_provider.end_hour',
            'user_provider.gap',
            'user_provider.guidelines',
            'address.city',
            'address.state',
            'address.street',
            'address.number',
            'address.complement',
            'address.district',
            'address.zip_code',
            'user_provider_plan.end_date',
            'plan.title',
        ])
            ->innerJoin('user_provider', 'user.id = user_provider.user_id')
            ->innerJoin('address', 'user.id = address.user_id')
            ->innerJoin('user_provider_plan', 'user.id = user_provider_plan.user_id')
            ->innerJoin('plan', 'user_provider_plan.plan_id = plan.id')
            ->andWhere(['user.id' => $id])->asArray()->all();
        
        
    }
    
    public function ListDetailsProvider($id) : array {
        return User::find()->select([
            'user.id',
            'user.name',
            'user.cellphone',
            'concat (\'' . $_ENV ["BASEURL"] . 'uploads/user-provider/' . '\', user_provider.avatar) as avatar',
            'user_provider.crm_specialty',
            'user_provider.service_category',
            'user_provider.health_plan',
            'user_provider.additional_specialty',
            'user_provider.instagram_url',
            'user_provider.facebook_url',
            'user_provider.linkedin_url',
            'user_provider.profile_text',
            'user_provider.average_rating',
            'user_provider.price_range_health_plan',
            'user_provider.price_range',
            'user_provider.accept_pne',
            'user_provider.category_id',
            'address.city',
            'address.state',
            'address.street',
            'address.number',
            'address.complement',
            'address.district',
            'address.zip_code',
            'address.latitude',
            'address.longitude',
        ])
            ->innerJoin('user_provider', 'user.id = user_provider.user_id')
            ->innerJoin('address', 'user.id = address.user_id')
            ->andWhere(['user.id' => $id])->asArray()->all();
        
        
    }
    
    
    public function deleteProvider(int $id) : bool {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = User::find()->andWhere(['id' => $id])->one();
            $model->deleted = date('Y-m-d h:m:s');
            $model->status = User::STATUS_ADMIN_INACTIVE;
            $deleteUser = $model->save();
            
            $modelUserProvider = UserProvider::find()->andWhere(['user_id' => $id])->one();
            $modelUserProvider->deleted = date('Y-m-d h:m:s');
            $deleteUserProvider = $modelUserProvider->save();
            
            $modelAddress = Address::find()->andWhere(['user_id' => $id])->one();
            $modelAddress->deleted = date('Y-m-d h:m:s');
            $deleteAddress = $modelAddress->save();
            
            
            if ($deleteUser && $deleteUserProvider && $deleteAddress) {
                $transaction->commit();
                return TRUE;
            } else {
                $transaction->rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function newFavorite($userId, $providerId) : bool {
        $favorite = new favoriteProvider;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $favorite->user_client_id = $userId;
            $favorite->user_provider_id = $providerId;
            $saveFavorite = $this->create($favorite);
            
            if ($saveFavorite) {
                $transaction->commit();
                return TRUE;
            } else {
                $transaction->rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    public function deleteFavorite($userId, $providerId) : bool {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = FavoriteProvider::find()->andWhere(['user_client_id' => $userId])->andWhere(['user_provider_id' => $providerId])->one();
            $deleteFavorite = $model->delete();
            
            if ($deleteFavorite) {
                $transaction->commit();
                return TRUE;
            } else {
                $transaction->rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
    
    public function ListAvailableTime($id, $date) : array {
        $providerDate = Solicitation::find()->select([
            'TIME(scheduled_date) as start',
            'TIME(scheduled_date_end) as end',
        ])
            ->andWhere(['user_provider' => $id])
            ->andWhere(['DATE(scheduled_date)' => $date])
            ->andWhere(['!=', 'status', 0])
            ->asArray()->all();
        
        
        $user = UserProvider::find()->andWhere(['user_id' => $id])->one();
        
        if ($user->start_hour) {
            $startH = $user->start_hour;
        } else {
            $startH = '07:00:00';
        }
        
        if ($user->end_hour) {
            $endH = $user->end_hour;
        } else {
            $endH = '22:00:00';
        }
        
        if ($user->gap) {
            $gap = $user->gap;
        } else {
            $gap = 30;
        }
        
        $Hours = [
            [$startH, $endH],
        ];
        
        
        $result = [];
        foreach ($Hours as $businessHour) {
            [$start, $end] = [strtotime($businessHour[0]), strtotime($businessHour[1])];
            
            while ($start < $end) {
                $first = $start;
                $start += $gap * 60; // intervalo de tempo entre cada hora
                
                $result[] = ['start' => date('H:i:s', $first), 'end' => date('H:i:s', $start)];
            }
        }
        
        if (!empty($providerDate)) {
            foreach ($providerDate as $appointment) {
                
                foreach ($result as $k => $item) {
                    if (strtotime($item['start']) >= strtotime($appointment['start']) && strtotime($item['end']) <= strtotime($appointment['end'])) {
                        unset($result[$k]);
                    }
                }
            }
        }
        
        return $result;
    }
    
    
    public function SearchCrm($crm) : array {
        return BaseCrm::find()->select([
            'id',
            'crm',
            'name',
            'state',
            'situation',
            'specialty',
        ])->andWhere(['crm' => $crm])->asArray()->all();
    }
    
    
}