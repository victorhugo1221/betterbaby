<?php

namespace common\modules\userProvider;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('userProviderService', __NAMESPACE__ . '\services\UserProviderService');
    }
}