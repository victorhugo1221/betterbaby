<?php

namespace common\modules\userProvider\behaviors;

use common\components\Behavior;
use common\modules\userProvider\models\UserProvider;
use Yii;

class UserProviderBehavior extends Behavior {
    
    public $email;
    
    public function init() {
        $this->email = $this->email ?? Yii::$app->getModule('user')->get('emailService');
        parent::init();
    }
    
    public function events() {
        return [
            UserProvider::EVENT_BEFORE_INSERT     => 'beforeCreate',
            UserProvider::EVENT_BEFORE_UPDATE     => 'beforeUpdate',
            UserProvider::EVENT_AFTER_INSERT      => 'afterSave',
            UserProvider::EVENT_AFTER_UPDATE      => 'afterSave',
        ];
    }
    
    
    public function onNewUser($event) {
        parent::afterSave($event);
        if ($event->sender->is_administrator == 0) {
            $this->email->newUser($event->sender);
        }
        
    }
}