<?php

namespace common\modules\userProvider\models\query;

use common\components\utils\ModelTrait;
use common\modules\userProvider\models\UserProvider;
use Yii;
use yii\db\ActiveQuery;

class UserProviderQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', UserProvider::tableName() . '.deleted', null]);
    }
    
    public function filter($search) {
        
        
        $response = $this->defaultFilter($search);
        return $response;
    }
}
