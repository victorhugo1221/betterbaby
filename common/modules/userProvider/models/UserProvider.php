<?php

namespace common\modules\userProvider\models;

use common\components\utils\Text;
use common\modules\address\models\Address;
use common\modules\city\models\City;
use common\modules\plan\models\Plan;
use common\modules\user\models\User;
use common\modules\userProviderPlan\models\UserProviderPlan;
use PhpOffice\PhpSpreadsheet\Calculation\Category;
use Yii;


class UserProvider extends \yii\db\ActiveRecord {
    
    public static array $_directory_file = [
        'avatar' => ['folder' => 'uploads/user-provider/'],
    ];
    
    
    public const IS_DOCTOR = 1;
    public const IS_CLINICAL = 2;
    public const IS_SERVICE = 3;
    
    public static $_category = [
        self::IS_DOCTOR   => 'Médico',
        self::IS_CLINICAL => 'Clinica',
        self::IS_SERVICE  => 'Serviços',
    ];
    
    
    public const STATUS_PENDENT = 0;
    public const STATUS_ACTIVE = 1;
    
    
    public static $_status = [
        self::STATUS_PENDENT => 'Aguardando ativação',
        self::STATUS_ACTIVE  => 'Ativo',
    
    ];
    /**
     * @var int|mixed|null
     */
    private $category_id;
    
    /**
     * @var int|mixed|null
     */
    
    
    public static function tableName() {
        return 'user_provider';
    }
    
    
    public function rules() {
        return [
            [['user_id'], 'required'],
            [['user_id', 'plan_id', 'category_id', 'price_range', 'accept_pne', 'ranking', 'status'], 'integer'],
            [['price_range_health_plan', 'crm', 'crm_specialty', 'instagram_url', 'facebook_url', 'linkedin_url', 'profile_text', 'guidelines'], 'string'],
            [['average_rating'], 'number'],
            [['health_plan', 'gap', 'service_category', 'created', 'updated', 'deleted'], 'safe'],
            [['additional_specialty'], 'string', 'max' => 100],
            [['start_hour', 'end_hour'], 'string', 'max' => 100],
            [['cnpj'], 'string', 'max' => 30],
            [['others'], 'string', 'max' => 155],
            [['avatar'], 'file', 'extensions' => 'png,jpg,jpeg,bmp,gif'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'id'                      => 'ID',
            'user_id'                 => 'User ID',
            'plan_id'                 => 'Plan ID',
            'price_range_health_plan' => 'Valor médio da consulta com convênio',
            'category_id'             => 'Category ID',
            'crm_specialty'           => 'Especialidade',
            'crm'                     => 'CRM',
            'crm_state'               => 'Estado CRM',
            'cnpj'                    => 'cnpj',
            'price_range'             => 'Valor Médio da consulta',
            'accept_pne'              => 'Realiza Atendimento PNE?',
            'additional_specialty'    => 'Especialidades Adicionais',
            'health_plan'             => 'Convenios',
            'service_category'        => 'Serviços prestados',
            'others'                  => 'Outros',
            'avatar'                  => 'Foto Perfil',
            'average_rating'          => 'Avaliação',
            'instagram_url'           => 'Instagram @',
            'facebook_url'            => 'Facebook @',
            'linkedin_url'            => 'Linkedin @',
            'profile_text'            => 'Descrição',
            'guidelines'              => 'Orirentações do atendimento',
            'ranking'                 => 'Ranking (1-99)',
            'status'                  => 'Status',
            'created'                 => 'Created',
            'updated'                 => 'Updated',
            'deleted'                 => 'Deleted',
        ];
    }
    
    
    public function upload($field) {
        if (empty($this->{$field}->tempName)) {
            return TRUE;
        }
        
        $caminho_pasta = \Yii::$app->basePath . '/web/' . self::$_directory_file[$field]['folder'] . '/';
        
        if (!is_dir($caminho_pasta)) {
            mkdir($caminho_pasta, 0755, TRUE);
        }
        
        $name = Text::slugify($this->{$field}->baseName) . '-' . time();
        $this->{$field}->saveAs($caminho_pasta . $name . '.' . $this->{$field}->extension);
        $this->{$field}->name = $name . '.' . $this->{$field}->extension;
        $this->{$field} = $name . '.' . $this->{$field}->extension;
        
        return TRUE;
    }
    
    
    public function getCategory() {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }
    
    
    public function getAddressRel() {
        return $this->hasOne(Address::class, ['user_id' => 'user_id']);
    }
    
    public function getProviderPlanRel() {
        return $this->hasOne(UserProviderPlan::class, ['user_id' => 'user_id']);
    }
    
    
    public function getUserRel() {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
    
    public static function find() {
        return new \common\modules\userProvider\models\query\UserProviderQuery(get_called_class());
    }
}
