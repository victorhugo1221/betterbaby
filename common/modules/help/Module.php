<?php

namespace common\modules\help;

class Module extends \yii\base\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
        $this->set('helpService', __NAMESPACE__ . '\services\HelpService');
    }
}