<?php

namespace common\modules\help\services;

use common\components\Service;
use common\modules\help\models\Help;
use common\modules\news\models\baseCrm;
use common\modules\solicitation\models\Solicitation;
use Yii;
use Exception;

class HelpService extends Service {
    public function init() {
        parent::init();
        $this->className = Help::class;
    }
    
   
    public function ListHelp() : array {
        return Help::find()->select(['id','title','text'])
            ->andWhere(['type' => 0])->asArray()->all();
    }
    
    public function ListHelpClient() : array {
        return Help::find()->select(['id','title','text'])
            ->andWhere(['type' => 1])->asArray()->all();
    }
    
    public function ListHelpWebSite() : array {
        return Help::find()->select(['id','title','text'])
            ->andWhere(['type' => 2])->asArray()->all();
    }
    
    public function ListHelpWebProfissional() : array {
        return Help::find()->select(['id','title','text'])
            ->andWhere(['type' => 3])->asArray()->all();
    }
    
}