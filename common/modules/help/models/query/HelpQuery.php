<?php

namespace common\modules\help\models\query;

use common\components\utils\ModelTrait;
use common\modules\help\models\Help;
use common\modules\news\models\baseCrm;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\help\models\Help]].
 *
 * @see \common\modules\help\models\Help
 */
class HelpQuery extends ActiveQuery {
    use ModelTrait;
    
    public function init() {
        return $this->andWhere(['IS', Help::tableName() . '.deleted', null]);
    }
    public function filter($search) {
        $response = $this->defaultFilter($search);
        return $response;
    }

}
