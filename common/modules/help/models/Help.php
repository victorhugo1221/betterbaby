<?php

namespace common\modules\help\models;

use Yii;


class Help extends \yii\db\ActiveRecord {
    public const TYPE_PROVIDER = 0;
    public const TYPE_CLIENT = 1;
    public const TYPE_WEBSITE = 2;
    
    
    public static $_type = [
        self::TYPE_PROVIDER => 'Faq Profissionais',
        self::TYPE_CLIENT   => 'Faq Mães',
        self::TYPE_WEBSITE  => 'Faq Site',
    
    ];
    
    public static function tableName() {
        return 'help';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['title', 'type', 'text'], 'required'],
            [['created', 'updated', 'type', 'deleted'], 'safe'],
            [['title'], 'string', 'max' => 155],
            [['text'], 'string', 'max' => 255],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'      => 'ID',
            'title'   => 'Título',
            'text'    => 'Texto',
            'type'    => 'Tipo',
            'created' => 'Data de criação',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }
    
    /**
     * {@inheritdoc}
     * @return \common\modules\help\models\query\HelpQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\modules\help\models\query\HelpQuery(get_called_class());
    }
}
