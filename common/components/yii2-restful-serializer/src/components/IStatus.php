<?php
namespace grupow\rest\components;
interface IStatus {

	public function getMessage();
	public function setMessage($message);
	public function getErrorCode();
	public function setErrorCode($errorCode);

}