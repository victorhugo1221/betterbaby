<?php

namespace grupow\rest;

use Yii;
use yii\base\InvalidValueException;
use yii\helpers\FileHelper;
use JMS\Serializer\Context;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use yii\base\Component;
use yii\base\Model;
use yii\data\DataProviderInterface;
use yii\data\Pagination;
use yii\web\Request;
use yii\web\Response;
use yii\helpers\Html;
use grupow\rest\components\IStatus;

class Serializer extends \yii\rest\Serializer
{

    public $metadirs;
    public $defaultMetadirs = [
        'grupow\rest\components' => '@grupowrest/components/meta',
    ];

    public $cachePrefix = 'active_serializer_';
    public $enableCache = false;
    public $groupsParam = 'groups';
    public $defaultContext = 'todos_campos';
    public $validationFormats = [
        self::VALIDATION_REST,
        self::VALIDATION_FORM
    ];
    public $defaultValidationFormat = self::VALIDATION_REST;
    public $validationFormatParam = '_validation-format';
    public $collectionEnvelope = 'data';
    public $requestedGroups;

    private $serializer;

    const VALIDATION_REST = 'rest';
    const VALIDATION_FORM = 'form';

    public function init()
    {

        parent::init();
        $this->serializer = $this->buildSerializer();

        $metadataFactory = $this->getMetadataFactory();
        // print_r($metadataFactory->getMetadataForClass('api\modules\segmento\models\Fisica'));
        // exit;
        if ($this->enableCache)
            $metadataFactory->setCache($this->getMetadataCache());

        return $this;
    }

    protected function getMetadataCache(){
        return new SerializerCache($this->cachePrefix, Yii::$app->cache);
    }

    protected function parseMetaDirs(){

        foreach ($this->defaultMetadirs as $key => $dir) {
            $this->defaultMetadirs[$key] = Yii::getAlias($dir);
        }

        return array_merge($this->metadirs, $this->defaultMetadirs);
    }

    protected function buildSerializer(){

        $metadirs = $this->parseMetaDirs();

        $defaultContext = $this->defaultContext;

        // var_dump($metadirs);
        // var_dump($defaultContext);
        // exit;
        return SerializerBuilder::create()
            ->setDebug(YII_DEBUG)
            ->addMetadataDirs($metadirs)
            ->setSerializationContextFactory(function () use ($defaultContext) {
                return SerializationContext::create()
                    ->setSerializeNull(true)
                    ->setGroups([$defaultContext]);
            })
            ->build();
    }

    public function getMetadataFactory(){
        return $this->serializer->getMetadataFactory();
    }

    protected function getRequestedGroups()
    {

        if(is_null($this->requestedGroups)){
            $groups = $this->request->get($this->groupsParam);
            $this->requestedGroups = is_string($groups) ? preg_split('/\s*,\s*/', $groups, -1, PREG_SPLIT_NO_EMPTY) : [];
        }

        return $this->requestedGroups;
    }

    public function setRequestedGroups($groups){
        $this->requestedGroups = $groups;
        return $this;
    }

    /**
     * Serializes the given data to the specified output format.
     *
     * @param object|array|scalar $data
     * @param string $format
     * @param Context $context
     *
     * @return string
     */
    public function serialize($data)
    {

        $context = $this->getContext();

        if ($data instanceof Model && $data->hasErrors()) {
            $data = $this->serializeModelErrors($data);
        } elseif ($data instanceof Arrayable) {
            $data = $this->serializeModel($data);
        } elseif ($data instanceof DataProviderInterface) {
            $data = $this->serializeDataProvider($data);
        } elseif ($data instanceof IStatus) {
            $data = $this->serializeIStatus($data);
        }

        if($data !== null){
            return $this->serializer->toArray($data, $context);
        }


        return null;
    }

    public function serializeContext($data, $context){
        return $this->serializer->toArray($data, $context);
    }

    /**
     * Deserializes the given data to the specified type.
     *
     * @param string $data
     * @param string $type
     * @param string $format
     * @param Context $context
     *
     * @return object|array|scalar
     */
    public function deserialize($data, $type, $format = 'json', DeserializationContext $context = null)
    {
        return $this->serializer->deserialize($data, $type, $format, $context);
    }

    public function fromArray($data, $type, DeserializationContext $context = null)
    {
        return $this->serializer->fromArray($data, $type, $context);
    }

    public function getContext(){
        $groups = $this->getRequestedGroups();
        $context = null;
        if(!empty($groups))
            $context = SerializationContext::create()->setGroups($groups);
        return $context;
    }

    public function serializeIStatus($status){
        if(!is_null($status->getErrorCode())){
            $this->response->setStatusCode($status->getErrorCode());
        }
        return $this->serializer->toArray($status, $this->getContext());
    }

    /**
     * Serializes a set of models.
     * @param array $models
     * @return array the array representation of the models
     */
    protected function serializeModels(array $models)
    {

        return $this->serializer->toArray($models, $this->getContext());
    }

    /**
     * Serializes a model object.
     * @param Arrayable $model
     * @return array the array representation of the model
     */
    protected function serializeModel($model)
    {
        if ($this->request->getIsHead()) {
            return null;
        }

        return $model; //$this->serializer->toArray($models, $this->getContext());
    }

    /**
     * Serializes the validation errors in a model.
     * @param Model $model
     * @return array the array representation of the errors
     */
    protected function serializeModelErrors($model)
    {

        $format = $this->getValidationFormat();
        $method = "serializeErrors".ucfirst($format);

        if(!in_array($format, $this->validationFormats) || !$this->hasMethod($method))
            throw new InvalidValueException(Yii::t('app.validation','invalid.format'), 500);

        $result = $this->$method($model);

        return $result;
    }

    /**
    * Serializa em formato REST
    */
    protected function serializeErrorsRest($model){

        $this->response->setStatusCode(400, 'Data Validation Failed.');

        $namespace = get_class($model);
        $result = [
            'namespace'=>$namespace,
            'model'=>$model->formName(),
            'errors'=>[]
        ];

        foreach ($model->getFirstErrors() as $name => $message) {
            $result['errors'][] = [
                'field'   => $name,
                'message' => $message,
            ];
        }
        return $result;
    }

    /**
    * Serializa em formato FORM
    */
    protected function serializeErrorsForm($model){

        $this->response->setStatusCode(400, 'Data Validation Failed.');

        $result = [];

        foreach ($model->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($model, $attribute)] = $errors;
        }

        return $result;
    }

    public function getValidationFormat(){

        $format = $this->request->get($this->validationFormatParam, $this->defaultValidationFormat);

        return $format;
    }


    /**
     * Serializes a data provider.
     * @param DataProviderInterface $dataProvider
     * @return array the array representation of the data provider.
     */
    protected function serializeDataProvider($dataProvider)
    {

        if ($this->preserveKeys) {
            $models = $dataProvider->getModels();
        } else {
            $models = array_values($dataProvider->getModels());
        }
        $models = $this->serializeModels($models);

        if (($pagination = $dataProvider->getPagination()) !== false) {
            $this->addPaginationHeaders($pagination);
        }

        // var_dump($pagination);
        // exit;
        if ($this->request->getIsHead()) {
            return null;
        } elseif ($this->collectionEnvelope === null) {
            return $models;
        }

        $result = [
            $this->collectionEnvelope => $models,
        ];
        if ($pagination !== false) {
            $result = array_merge($result, $this->serializePagination($pagination));
        }
        if(property_exists($dataProvider, 'getExtraMeta')){
            $extraMeta = $dataProvider->getExtraMeta();
            if($extraMeta){
                $result = $this->serializerExtraMeta($result, $extraMeta);
            }
        }
        return $this->serializer->toArray($result, $this->getContext());

    }

    protected function serializerExtraMeta($result, $meta){
        if(!is_array($result[$this->metaEnvelope])){
            $result[$this->metaEnvelope]=[];
        }

        $result[$this->metaEnvelope] = array_merge($result[$this->metaEnvelope], $meta);

        return $result;
    }

}
