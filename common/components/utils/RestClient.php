<?php

namespace common\components\utils;

use Codeception\Util\HttpCode;
use common\modules\adminGw\logs\GrupoWLog;
use Exception;
use GuzzleHttp\Client;
use Yii;

class RestClient {
	private Client $restClient;
	private string $method;

	public function __construct() {
		$this->restClient = new Client();
		$this->method     = 'post';
	}

	private function getClientToken(): string {
		try {
			$token = Yii::$app->cache->get($_ENV['KEY_NAME_TOKEN']);
			if (empty($token)) {
				$response = $this->restClient->post("{$_ENV['DEFAULT_URL_API']}oauth/token", [
					'auth'        => [$_ENV['API_USER'], $_ENV['API_PASSWORD']],
					'headers'     => ['Content-Type' => 'application/x-www-form-urlencoded'],
					'form_params' => ['grant_type' => 'client_credentials'],
				]);

				if ($response->getStatusCode() === HttpCode::OK) {
					$content = json_decode($response->getBody()->getContents());

					Yii::$app->cache->set($_ENV['KEY_NAME_TOKEN'], $content->access_token, (int)$content->expires_in);

					$token = $content->access_token;
				}
			}
			return $token;
		} catch (Exception $e) {
			GrupoWLog::sendMessageErrorLogToSqs(Yii::$app->request, $e, GrupoWLog::TYPE_API);
			// Todo: send emails to inform fails
			return 'ERRO AO GERAR TOKEN';
		}
	}

	public function getData(string $endPoint, ?array $params = null, ?array $header = null): array {
		try {
			$requestHeader = ['Content-Type' => 'application/json', 'Authorization' => "Bearer {$this->getClientToken()}"];
			if (!empty($header)) {
				$requestHeader = array_merge($requestHeader, $header);
			}
			$response = $this->restClient->request('get', "{$_ENV['DEFAULT_URL_API']}{$endPoint}",
			                                       ['headers' => $requestHeader,
			                                        'query'   => $params]);
			if ($response->getStatusCode() === HttpCode::OK) {
				$content  = json_decode($response->getBody()->getContents());
				$response = [
					'result'  => $content->Result,
					'message' => !empty($content->Message) ? $content->Message : null,
					'data'    => !empty($content->Data) ? $content->Data : null,
				];
				return $response;
			}
			throw new Exception("Error on getData endPoint:{$endPoint} | params: {$params} | code: {$response->getStatusCode()} | reason: {$response->getReasonPhrase()} | body: {$response->getBody()->getContents()}");
		} catch (Exception $e) {
			GrupoWLog::sendMessageErrorLogToSqs(Yii::$app->request, $e, GrupoWLog::TYPE_API);
			return ['result' => 0, 'message' => $e->getMessage()];
		}
	}

	public function postData(string $endPoint, string $data, ?array $params = null, ?array $header = null): array {
		try {
			$requestHeader = ['Content-Type' => 'application/json', 'Authorization' => "Bearer {$this->getClientToken()}"];
			if (!empty($header)) {
				$requestHeader = array_merge($requestHeader, $header);
			}
			$response = $this->restClient->request($this->method, "{$_ENV['DEFAULT_URL_API']}{$endPoint}",
			                                       ['headers' => $requestHeader,
			                                        'query'   => $params,
			                                        'body'    => $data]);
			if ($response->getStatusCode() === HttpCode::OK) {
				$content  = json_decode($response->getBody()->getContents());
				$response = [
					'result'  => $content->Result,
					'message' => !empty($content->Message) ? $content->Message : null,
					'data'    => !empty($content->Data) ? $content->Data : null,
				];
				return $response;
			}
			throw new Exception("Error on getData endPoint:{$endPoint} | params: {$params} | code: {$response->getStatusCode()} | reason: {$response->getReasonPhrase()} | body: {$response->getBody()->getContents()}");
		} catch (Exception $e) {
			GrupoWLog::sendMessageErrorLogToSqs(Yii::$app->request, $e, GrupoWLog::TYPE_API);
			return ['result' => 0, 'message' => $e->getMessage()];
		}
	}

	public function deleteData(string $endPoint, ?string $data = '', ?array $params = null, ?array $header = null): array {
		$this->method = 'delete';
		return $this->postData($endPoint, $data, $params, $header);
	}

	public function putData(string $endPoint, string $data, ?array $params = null, ?array $header = null): array {
		$this->method = 'put';
		return $this->postData($endPoint, $data, $params, $header);
	}

}