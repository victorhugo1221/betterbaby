<?php

namespace common\components\utils;

trait ModelTrait {
	public function defaultFilter($search) {
		if (!empty($search['status']) || (isset($search['status']) && $search['status'] === "0")) {
			$this->where(['=', 'status', $search['status']]);
		}

		if (!empty($search['term']) && !empty($search['field']) && !empty($search['operation'])) {
		    if($search['field'] != "null") {
                $operation = strtolower(trim($search['operation']));
                $operations = ['equal'       => '=',
                               'diferent'    => '<>',
                               'greaterthen' => '>',
                               'lowerthen'   => '<',
                               'startwith'   => 'LIKE',
                               'contain'     => 'LIKE',
                               'endwith'     => 'LIKE',];
                $beggining = $operation === 'endwith' || $operation === 'contain' ? '%' : '';
                $ending = $operation === 'startwith' || $operation === 'contain' ? '%' : '';

                if ($operation === 'startwith' || $operation === 'endwith' || $operation === 'contain') {
                    $this->andWhere([$operations[$operation], $search['field'], "{$beggining}{$search['term']}{$ending}", false]);
                } else {
                    $this->andWhere([$operations[$operation], $search['field'], $search['term']]);
                }
            }
		}

		return $this;
	}

	public function filter($search) {
		$response = $this->defaultFilter($search);
		return $response;
	}

	public function all($db = null) {
		return parent::all($db);
	}

	public function one($db = null) {
		return parent::one($db);
	}
}