<?php


namespace common\components\utils;


class Validation {
    public static function validateCpf(string $cpf): bool {
        $cpf = preg_replace('/[^[0-9]]/', '', $cpf);
        
        if (strlen($cpf) != 11) {
            return FALSE;
        }
        
        $isRepeatedNumbers = preg_match('/(\d)\1{10}/', $cpf);
        if ($isRepeatedNumbers) {
            return FALSE;
        }
        
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return FALSE;
            }
        }
        return TRUE;
        
    }
}