<?php
namespace common\components;
//use phpoffice\phpspreadsheet\PhpSpreadsheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

use Yii;
use moonland\phpexcel\Excel;

class Excelutil extends Excel
{
    public function run()
	{
		if ($this->mode == 'export') 
		{
			$sheet = new Spreadsheet();
			
                      
	    	if (!isset($this->models))
	    		throw new InvalidConfigException('Config models must be set');
	    	
	    	if (isset($this->properties))
	    	{
	    		$this->properties($sheet, $this->properties);
	    	}
	    	
	    	// if ($this->isMultipleSheet) {
	    	// 	$index = 0;
	    	// 	$worksheet = [];
	    	// 	foreach ($this->models as $title => $models) {
	    	// 		$sheet->createSheet($index);
	    	// 		$sheet->getSheet($index)->setTitle($title);
	    	// 		$worksheet[$index] = $sheet->getSheet($index);
	    	// 		$columns = isset($this->columns[$title]) ? $this->columns[$title] : [];
	    	// 		$headers = isset($this->headers[$title]) ? $this->headers[$title] : [];
	    	// 		$this->executeColumns($worksheet[$index], $models, $this->populateColumns($columns), $headers);
	    	// 		$index++;
	    	// 	}
	    	// } else {
                $worksheet = $sheet->getActiveSheet();
				//$worksheet = $this->protectedKeyColumnAndFirstRow($worksheet);
				

	    		$this->executeColumns($worksheet, $this->models, isset($this->columns) ? $this->populateColumns($this->columns) : [], isset($this->headers) ? $this->headers : []);
	    	// }
	    	
	    	if ($this->asAttachment) {
	    		$this->setHeaders();
			}
	    	$this->writeFile($sheet);
		} 
		elseif ($this->mode == 'import') 
		{
			if (is_array($this->fileName)) {
				$datas = [];
				foreach ($this->fileName as $key => $filename) {
					$datas[$key] = $this->readFile($filename);
				}
				return $datas;
			} else {
				return $this->readFile($this->fileName);
			}
		}
    }
	
	/**
	 * @param array $config
	 * @return string
	 */
	public static function widget($config = [])
	{
		if ($config['mode'] == 'import' && !isset($config['asArray'])) {
			$config['asArray'] = true;
		}
		
		if (isset($config['asArray']) && $config['asArray']==true)
		{
	        $config['class'] = get_called_class();
	        $widget = \Yii::createObject($config);
	        return $widget->run();
		} else {
			return parent::widget($config);
		}
    }

    protected function protectedKeyColumnAndFirstRow($sheet) {
        $sheet->getProtection()->setSheet(true);

        // Trava de chave
        $sheet->getStyle('A1:A5000')
        ->getProtection()
        ->setLocked('protected');
        
        // Trava de entidades
        $sheet->getStyle('B1:B5000')
        ->getProtection()
        ->setLocked('protected');

        // Trava de Índices Superiores
        $sheet->getStyle('A1:Z1')
        ->getProtection()
        ->setLocked('protected');
        
        $trava_idiomas = -2; // Número negativo de colunas à travar (Chave e Entidade), já travados
        if (sizeof($this->models) > 0) {
            $model_row = $this->models[0];
            $cols = sizeof((Array) $model_row);
            $trava_idiomas += $cols;
        }

        // Protege as colunas dos idiomas
        $alphabet = ['0', 'A', 'B', 'C', 'D', 'E', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        for ($i = $trava_idiomas; $i < ($trava_idiomas + $trava_idiomas); $i++) {
            $letter = $alphabet[$i];
            $used[] = $letter;
            $sheet->getStyle("{$letter}2:{$letter}5000")
            ->getProtection()
            ->setLocked('unprotected');
        }
        return $sheet;
    }
}