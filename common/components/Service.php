<?php
namespace common\components;

use DateTimeImmutable;
use grupow\base\exceptions\EntityNotFoundException;
use grupow\base\exceptions\ValidationException;
use JMS\Serializer\Context;
use SqlFormatter;
use Yii;
use yii\base\Component;
use yii\helpers\Html;
use yii\base\Model;
use yii\base\StaticInstanceTrait;

class Service extends Component {
    
    use StaticInstanceTrait;
    
    private   $validationExceptionMessage = "The model has errors";
    private   $notFoundExceptionMessage   = "The entity was not found";
    public    $module;
    protected $currentDateTime;
    protected $className;
    
    public function __construct($config = []) {
        parent::__construct($config);
        $this->currentDateTime = new DateTimeImmutable();
    }
    
    public function validate($model) {
        if (!$model->validate()) {
            $this->triggerValidation($model);
        } else {
            return $model;
        }
    }
    
    public function triggerValidation($model) {
        $e         = new ValidationException($this->validationExceptionMessage, 422, NULL, $model, $model->getFirstErrors());
        $e->model  = &$model;
        $e->errors = $model->getFirstErrors();
        throw $e;
    }
    
    public function create($model): int {
        if ($model->hasAttribute('user_created')) {
            $model->user_created = Yii::$app->user->identity->id;
        }
        if ($model->hasAttribute('created')) {
            $model->created = $this->currentDateTime->format('Y-m-d h:m:s');
        }
        if ($model->save()) {
            return $model->id;
        }
        return 0;
    }
    
    public function update($model): bool {
        if ($model->hasAttribute('updated')) {
            $model->updated = $this->currentDateTime->format('Y-m-d h:m:s');
        }
        return $model->save();
    }
    
    public function delete(int $id): bool {
        $model = $this->className::findOne($id);
        if ($model && $model->hasAttribute('deleted')) {
            $model->deleted = $this->currentDateTime->format('Y-m-d h:i:s');
            $model->validate();
            $save = $model->save(FALSE);
            return $save;
        }
        return TRUE;
    }
    
    public function findModel($query, $method = 'one') {
        $model = $query->$method();
        if (is_null($model) || empty($model)) {
            throw new EntityNotFoundException($this->notFoundExceptionMessage);
        }
        return $model;
    }
    
    public function findOne($ids) {
        $keys       = $this->className::primaryKey();
        $query      = $this->className::find();
        $ids        = is_array($ids) ? $ids : [$ids];
        $conditions = ["AND"];
        foreach ($keys as $indexKey => $value) {
            if (isset($ids[ $indexKey ])) {
                $conditions[] = [$value => $ids[ $indexKey ]];
            }
        }
        $query->andWhere($conditions);
        return $this->findModel($query);
    }
    
    public function findByUid($uid, string $uidColumn = 'uid') {
        $query = $this->className::find();
        $query->andWhere([$uidColumn => $uid]);
        return $this->findModel($query);
    }
    
    public function copyData($dbObj, $novoObj, $context = NULL) {
        
        $serializerContext = Yii::$app->serializer->getContext();
        $defaultContext    = $serializerContext instanceof Context ? $serializerContext->get('groups')->get() : [Yii::$app->serializer->defaultContext];
        $context           = $context instanceof Context ? $context->attributes->get('groups')->get() : $defaultContext;
        $metadataFactory = Yii::$app->serializer->getMetadataFactory();
        $data            = $metadataFactory->getMetadataForClass($dbObj::className());
        foreach ($data->propertyMetadata as $key => $metadata) {
            if ($key == "_default") {
                continue;
            }
            if (is_array($metadata->groups)) {
                $isInContext = !empty(array_intersect($context, $metadata->groups));
                if ($isInContext && !empty($metadata->setter)) {
                    $value = call_user_func([$novoObj, $metadata->getter]);
                    if ($metadata->skipWhenEmpty && $value === NULL) {
                        continue;
                    }
                    $dbObj->{$metadata->setter}($value);
                }
            }
        }
        return $novoObj;
    }
    
    public function formatErrors($model, $index = NULL) {
        $result = [];
        $models = func_get_args();
        foreach ($models as $i => $model) {
            if (is_array($model)) {
                foreach ($model as $mi => $one) {
                    if ($model[ $mi ] instanceof Model) {
                        $result = array_merge($result, $this->formatErrors($model[ $mi ], $mi));
                    }
                }
                continue;
            }
            if (!($model instanceof Model)) {
                continue;
            }
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $attribute => $errors) {
                    $name                                      = !is_numeric($index) ? $attribute : "[$index]" . $attribute;
                    $result[ Html::getInputId($model, $name) ] = $errors;
                }
            }
        }
        return $result;
    }
}
