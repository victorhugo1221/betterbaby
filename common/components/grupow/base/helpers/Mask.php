<?php
namespace grupow\base\helpers;


class Mask
{
	/**
	* Insira aqui as macaras padrão para a sua aplicação
	*/
    const MASK_CNPJ = "##.###.###/####-##";
    const MASK_TEL10 = "(##) ####-####";
    const MASK_TEL11 = "(##) #####-####";
    const MASK_CPF = "###.###.###-##";
    const MASK_CEP = "#####-###";
    const MASK_DATA = "##/##/####";

    /**
     * @param mixed $val
     * @param string $mask
     * @return string
     */
    public static function create($val, $mask)
    {
        $val = (string) $val;
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++)
        {
            if($mask[$i] == '#')
            {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else
            {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
}