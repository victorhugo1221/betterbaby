<?php

namespace grupow\base\rbac;

use yii\rbac\DbManager as RbacDbManager;

class DbManager extends RbacDbManager {
	
	const AFTER_INVALIDATE_CACHE = 'after_invalidate_cache';

	public function invalidateCache(){
		parent::invalidateCache();
		$this->trigger(self::AFTER_INVALIDATE_CACHE);
	}

}