<div class="tabs-container">
    <ul class="nav nav-tabs">
        <?php for($i=0;$i<count($tabs);$i++):
            $active = $i==0?'active':'';
            $expanded = $i==0?'aria-expanded="true"':'aria-expanded="false"';
        ?>
        <li class="<?=$active;?>"><a data-toggle="tab" href="#wctab-<?=($i+1);?>" <?=$expanded;?>> <?=$tabs[$i];?></a></li>
        <?php endfor; ?>
    </ul>
    <div class="tab-content">
        <?php
        // var_dump(count($widgets));
        // exit;
        ?>
        <?php for($i=0; $i < count($widgets); $i++): 
        // var_dump($i);
            $active = $i==0?'active':'';?>
        <div id="wctab-<?=($i+1);?>" class="tab-pane <?=$active;?>">
            <div class="panel-body">
                <?php
                echo $widgets[$i];
                ?>
            </div>
        </div>
        <?php 
        endfor; ?>
    </div>


</div>