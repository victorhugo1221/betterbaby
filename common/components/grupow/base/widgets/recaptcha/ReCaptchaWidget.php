<?php

namespace grupow\base\widgets\recaptcha;

use himiklab\yii2\recaptcha\ReCaptcha;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\widgets\InputWidget;

class ReCaptchaWidget extends ReCaptcha {

    public function run()
    {
        if (empty($this->siteKey)) {
            /** @var ReCaptcha $reCaptcha */
            $reCaptcha = Yii::$app->reCaptcha;
            if (!empty($reCaptcha->siteKey)) {
                $this->siteKey = $reCaptcha->siteKey;
            } else {
                throw new InvalidConfigException('Required `siteKey` param isn\'t set.');
            }
        }

        if (self::$firstWidget) {
            $view = $this->view;
            $arguments = http_build_query([
                'hl' => $this->getLanguageSuffix(),
                'render' => 'explicit',
                'onload' => 'recaptchaOnloadCallback',
            ]);
            $view->registerJsFile(
                self::JS_API_URL . '?' . $arguments,
                ['position' => $view::POS_END, 'async' => true, 'defer' => true]
            );
            $view->registerJs($this->render('onload'), $view::POS_END);

            self::$firstWidget = false;
        }

        $this->customFieldPrepare();
        echo Html::tag('div', '', $this->buildDivOptions());
    }

    protected function customFieldPrepare()
    {
        $view = $this->view;
        if ($this->hasModel()) {
            $inputName = Html::getInputName($this->model, $this->attribute);
        } else {
            $inputName = $this->name;
        }

        $inputId = $this->getReCaptchaId();
        $verifyCallbackName = lcfirst(Inflector::id2camel($inputId)) . 'Callback';
        $jsCode = $this->render('verify', [
            'verifyCallbackName' => $verifyCallbackName,
            'jsCallback' => $this->jsCallback,
            'inputId' => $inputId,
        ]);
        $this->jsCallback = $verifyCallbackName;

        if (empty($this->jsExpiredCallback)) {
            $jsExpCode = "var recaptchaExpiredCallback = function(){jQuery('#{$inputId}').val('');};";
        } else {
            $jsExpCode = "var recaptchaExpiredCallback = function(){jQuery('#{$inputId}').val(''); " .
                "{$this->jsExpiredCallback}();};";
        }
        $this->jsExpiredCallback = 'recaptchaExpiredCallback';

        $view->registerJs($jsCode, $view::POS_END);
        $view->registerJs($jsExpCode, $view::POS_END);

        echo Html::input('hidden', $inputName, null, ['id' => $inputId]);
    }

}