<?php

namespace grupow\base\components;

use XLSXWriter as BaseXLSXWriter;
use XLSXWriter_BuffererWriter;

class XLSXWriter extends BaseXLSXWriter
{
    public function __construct()
    {

        // date_default_timezone_set('America/Sao_Paulo');

        $this->addCellStyle($number_format='GENERAL', $style_string=null);
        $this->addCellStyle($number_format='GENERAL', $style_string=null);
        $this->addCellStyle($number_format='GENERAL', $style_string=null);
        $this->addCellStyle($number_format='GENERAL', $style_string=null);
    }

    protected function writeCell(XLSXWriter_BuffererWriter &$file, $row_number, $column_number, $value, $num_format_type, $cell_style_idx)
    {
        $cell_name = self::xlsCell($row_number, $column_number);

        if(strpos($value, ':string:') !== false) {
            $value = str_replace(':string:', '', $value);
            $file->write('<c r="'.$cell_name.'" s="'.$cell_style_idx.'" t="inlineStr"><is><t>'.self::xmlspecialchars($value).'</t></is></c>');
        } else  {
            parent::writeCell($file, $row_number, $column_number, $value, $num_format_type, $cell_style_idx);
        }
    }

    protected function addCellStyle($number_format, $cell_style_string)
    {
        $number_format_idx = self::add_to_list_get_index($this->number_formats, $number_format);
        $lookup_string = $number_format_idx.";".$cell_style_string;
        $cell_style_idx = self::add_to_list_get_index($this->cell_styles, $lookup_string);
        return $cell_style_idx;
    }
}
