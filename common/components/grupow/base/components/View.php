<?php

namespace grupow\base\components;

use alexandernst\devicedetect\DeviceDetect;
use Yii;
use yii\helpers\{ArrayHelper, FileHelper, Html};

class View extends \yii\web\View
{

    public $ampDir = 'amp';
    public $mobileDir = 'mobile';
    public $layout = null;
    public $deviceDetect;

    public function __construct(DeviceDetect $deviceDetect){
        $this->deviceDetect = $deviceDetect;
    }

    public function render($view, $params = array(), $context = null)
    {
        if ($context === null) {
            $context = $this->context;
        }

        return parent::render($view, $params, $context);
    }

    protected function findViewFile($view, $context = null)
    {

        $layout = Yii::$app->controller->findLayoutFile($this);
        $viewFile = parent::findViewFile($view, $context);
        
        $dir = false;
        if($this->isMobile() && $this->hasModView($this->mobileDir, $viewFile)){
            $dir = $this->mobileDir;
        } else if($this->isAmpEnabled() && $this->hasModView($this->ampDir, $viewFile)){
            $dir = $this->ampDir;
        }

        if($dir){
            $view = $dir.DIRECTORY_SEPARATOR.$view;
            if($layout && $this->hasModView($dir, $layout)){
                Yii::$app->controller->layout = $this->getLayoutView($dir, $layout);
            }
        }

        return parent::findViewFile($view, $context);
    }

    protected function isAmpEnabled(){
        return Yii::$app->amp->isEnabled();
    }

    protected function isMobile(){
        return $this->deviceDetect->isMobile();
    }

    protected function getLayoutView($type, $layout){
        $layout = $this->getModViewFilePath($type, $layout);
        return str_replace(Yii::getAlias('@app'), '@app', $layout);
    }

    protected function getModViewFilePath($type, $view){
        $info = pathinfo($view);
        extract($info);
        /*
        $dirname = '/Applications/MAMP/htdocs/yii2-test-drive/frontend/views/site';
        $basename = 'index.php';
        $extension = 'php';
        $filename = 'index';
        */
        $file = $dirname.DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR.$basename;
        return $file;
    }


    protected function hasModView($type, $view){
        $file = $this->getModViewFilePath($type, $view);
        return file_exists($file);
    }

}
