<?php
namespace grupow\base\components;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Response;
class Pjax extends \yii\widgets\Pjax {

	public static $clearScripts = false;
	public $timeout = 5000;

	public function init(){
		if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        if ($this->requiresPjax()) {
            ob_start();
            ob_implicit_flush(false);
            $view = $this->getView();
            if(self::$clearScripts)
            	$view->clear();
            $view->beginPage();
            $view->head();
            $view->beginBody();
            if ($view->title !== null) {
                echo Html::tag('title', Html::encode($view->title));
            }
        } else {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'div');
            echo Html::beginTag($tag, array_merge([
                'data-pjax-container' => '',
                'data-pjax-push-state' => $this->enablePushState,
                'data-pjax-replace-state' => $this->enableReplaceState,
                'data-pjax-timeout' => $this->timeout,
                'data-pjax-scrollto' => $this->scrollTo,
            ], $options));
        }
	}

    public static function end(){
        parent::end();
        if(isset(Yii::$app->assetManager->bundles['yii\web\JqueryAsset'])){
            Yii::$app->assetManager->bundles['yii\web\JqueryAsset']->js = [];
        }
    }
}