<?php
namespace grupow\base\components;
use Yii;
use yii\base\ActionFilter;

class AmazonSqsFilter extends AmazonFilter {

	public function afterAction($action, $result){

		if(!in_array($action->id, $this->actions) || !self::isSqsRequest($this->queue_url, isset($this->headers['x-aws-sqsd-receipthandle'])))
			return parent::afterAction($action, $result);

		$client = Yii::$app->queue->getClient();

		Yii::trace(Yii::$app->request->getRawBody(), 'rotina');

		// $receiptHandle = $this->params->ReceiptHandle ?? null;
		$receiptHandle = $this->getReceiptHandle();
		if($receiptHandle) {
			$res = $client->deleteMessage([
				'QueueUrl' => $this->queue_url, // REQUIRED
	    		'ReceiptHandle' => $receiptHandle, // REQUIRED
			]);
		}

		return parent::afterAction($action, $result);
	}

}