<?php
namespace grupow\base\components;

class MenuItem {
	//
	public static function Item($array){

		$array['visible'] = $array['visible'] ?? true;
		if(isset($array['items']) && is_array($array['items'])) {
			$array['visible'] = false;
			foreach ($array['items'] as $key => $value) {
				// $array['items'][$key]
				$visible = isset($array['items'][$key]['visible']) ? $array['items'][$key]['visible'] : true;
				if($visible){
					$array['visible'] = true;
					break;
				}
			}
		}
		return $array;
	}
}