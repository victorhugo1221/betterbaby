<?php
namespace grupow\base\components;
use bscheshirwork\redis\Session;
use Exception;
use Yii;

/**
* requires: "bscheshirwork/yii2-redis-session": "*@dev"
*/
class RedisSession extends Session {

	/**
	* Apenas um wrapper para evitar bug de sessão ao tentar fechar a sessão
	*/
	public function close()
    {
        if ($this->getIsActive()) {
        	try {
            	session_write_close();
        	} catch (Exception $e) {
        		Yii::error($e->getMessage());
        	}
        }
    }

}