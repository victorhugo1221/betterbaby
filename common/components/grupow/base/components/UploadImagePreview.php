<?php

namespace grupow\base\components;

use yii\helpers\Html;
use yii\helpers\Url;

class UploadImagePreview {

    public $bannerUrlToSave       = NULL;
    public $bannerPathToSave      = '/images';
    public $maxFileSize           = '2MB';
    public $maxFileSizeInKB       = 2000;
    public $fileSizeDesktop       = 'Desktop: 1280px x 288px (Ratio: 40:9)';
    public $fileSizeMobile        = 'Mobile: 512px x 130px (Ratio: 40:9)';
    public $componentTitle        = 'Imagens';
    public $noImage               = '';
    public $extension             = '.jpg, .jpeg, .png';
    public $inputName             = 'Banner';
    public $showDesktop           = true;
    public $showMobile            = true;
    public $titleDesktop          = 'Desktop';
    public $titleMobile           = 'Mobile';
    public $inputAttributeDesktop = 'desktop_image';
    public $inputAttributeMobile  = 'mobile_image';

    public function setNoImage(string $noImage = NULL): void {
        $this->noImage = !isset($noImage) ? $_ENV['BACKENDURL'] . "images/no-image.png" : $noImage;
    }

    public function showUploadDesktopImage(string $image = NULL): string {
        $html = '';
        $modelImage = (empty($image) ? '' : $image);
        $previewImage = (empty($image) ? $this->noImage : $image);

        $divSize = '12';
        if (!empty($this->showDesktop) && !empty($this->showMobile)) {
            $divSize = '6';
        }

        $html .= "<div class='col-xs-12 col-sm-12 col-md-{$divSize}'>
                      <label class='image-preview' data-component='image-preview' data-upload-ext-accept='{$this->extension}' data-upload-url='{$this->bannerUrlToSave}' data-path='{$this->bannerPathToSave}' data-size-message='{$this->maxFileSize}' data-size-kb='{$this->maxFileSizeInKB}'>
                          <h5>" . $this->titleDesktop . "</h5>
                          <div class='image-preview_image' style=\"background-image:url('{$previewImage}')\">
                              <div class='image-preview_caption'>
                                  <div class='image-preview_caption-text'>
                                      Adicione uma imagem
                                  </div>
                              </div>
                          </div>
                          <input type='hidden' name='{$this->inputName}[{$this->inputAttributeDesktop}]' class='image-save' value='{$modelImage}'>
                          <input type='hidden' name='imagemAtual' value='{$previewImage}'>
                          <input type='file' name='slideimg' id='desktop_image' class='image-preview_input' value='{$previewImage}' accept='{$this->extension}'>
                      </label>
                  </div>";
        return $html;
    }

    public function showUploadMobileImage(string $image = NULL): string {
        $html = '';
        $modelImage = (empty($image) ? '' : $image);
        $previewImage = (empty($image) ? $this->noImage : $image);

        $divSize = '12';
        if (!empty($this->showDesktop) && !empty($this->showMobile)) {
            $divSize = '6';
        }

        $html .= "<div class='col-xs-12 col-sm-12 col-md-{$divSize}'>
                      <label class='image-preview' data-component='image-preview' data-upload-ext-accept='{$this->extension}' data-upload-url='{$this->bannerUrlToSave}' data-path='{$this->bannerPathToSave}' data-size-message='{$this->maxFileSize}' data-size-kb='{$this->maxFileSizeInKB}'>
                            <h5>" . $this->titleMobile . "</h5>
                            <div class='image-preview_image' style=\"background-image:url('{$previewImage}')\">
                                <div class='image-preview_caption'>
                                    <div class='image-preview_caption-text'>
                                        Adicione uma imagem
                                    </div>
                                </div>
                            </div>
                            <input type='hidden' name='{$this->inputName}[{$this->inputAttributeMobile}]' class='image-save' value='{$modelImage}'>
                            <input type='hidden' name='imagemAtual' value='{$previewImage}'>
                            <input type='file' name='slideimg' id='mobile_image' class='image-preview_input' value='{$previewImage}'>
                      </label>
                  </div>   ";
        return $html;
    }

    public function orientationAndSize(): string {
        $html = '';
        $html .= '<div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <b>Orientações: </b>
                        <ol>
                            <li>Imagens nos formatos ' . $this->extension . '</li>
                            <li>
                                Imagens de no máximo ' . $this->maxFileSize . '.
                                <br>
                                Recomendamos a compressão das imagens através de plataformas como:
                                <ul>
                                    <li><a href="https://compressjpeg.com/pt/" target="_blank">Compress JPEG</a></li>
                                    <li><a href="https://tinypng.com/" target="_blank">Tiny PNG</a></li>
                                </ul>
                            </li>
                            <li>Remover acento e caracteres especiais do nome do arquivo</li>
                        </ol>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <b>Tamanhos: </b>
                        <ul>';

        if ($this->showDesktop) {
            $html .= '<li>' . $this->fileSizeDesktop . '</li>';
        }

        if ($this->showMobile) {
            $html .= '<li>' . $this->fileSizeMobile . '</li>';
        }
        $html .= '
                </ul>
            </div>
        </div>';


        return $html;
    }

    public function printImageComponent(string $desktopImage = NULL, string $mobileImage = NULL, string $noImage = NULL): string {
        $this->setNoImage($noImage);
        $html = '';
        $html .= '
                    <div class="col-12 p-0">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>' . $this->componentTitle . '</h5>
                            </div>
                            <div class="ibox-content border-left-right p-sm">';
        $html .= $this->orientationAndSize();
        $html .= '                <div class="row pt-3">';

        if ($this->showDesktop) {
            $html .= $this->showUploadDesktopImage($desktopImage);
        }

        if ($this->showMobile) {
            $html .= $this->showUploadMobileImage($mobileImage);
        }

        $html .= '     </div>
                           </div>
                       </div>
                    </div>
                ';
        return $html;
    }
}