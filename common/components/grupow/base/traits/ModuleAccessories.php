<?php
namespace grupow\base\traits;

use Yii;
use yii\web\Response;
use grupow\rest\components\ActionStatus;
use common\components\Service;
use yii\web\View;

trait ModuleAccessories {
    
    public $currentModule;
    
    public function getComponent($component){
        $component = explode(':', $component);
        $moduleName = $this->currentModule;
        $componentName = $component[0];
        if(count($component) > 1){
            $moduleName = $component[0];
            $componentName = $component[1];
        }
        $module = Yii::$app->getModule($moduleName)->get($componentName);
        return $module;
    }

}