<?php
namespace grupow\rest\components;
interface IFriendlyApiError {

	public function getApiMessage();
	public function getApiErrorCode();

}