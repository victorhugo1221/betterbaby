<?php

namespace common\components;

use common\modules\adminGw\models\Page;
use yii\db\ActiveRecord;

abstract class Model extends ActiveRecord {
	public $logRelatedOld;
	public $logRelatedNew;
    public $relatedName   = null;

	public const EVENT_STATUS_CHANGE = 'eventStatusChange';

	public static function classTitle(?string $identifier = null): string {
		$page = Page::find()->andWhere(['identifier' => $identifier ?? static::tableName()])->one();
		return $page->title ?? '';
	}

	public static function getPageName(?string $identifier = null): string {
		$page = Page::find()->andWhere(['identifier' => $identifier ?? static::tableName()])->one();
		return $page->title;
	}

	public static function getPageUrl(?string $identifier = null): string {
		$page = Page::find()->andWhere(['identifier' => $identifier ?? static::tableName()])->one();
		return $page->url;
	}
}