<?php

namespace common\components\web;

class View extends \yii\web\View {
    public $breadcrumbs;
    public $description;
    public $current;
    public $url;
}