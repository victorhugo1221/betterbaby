<?php

namespace common\components\web;

use common\modules\adminGw\models\Page;
use yii\helpers\Url;
use yii\web\Response;
use Yii;

class Controller extends \yii\web\Controller {
    protected $pageTitle;
    protected $pageIdentifier;

    const ACTION_CREATE = 1;
    const ACTION_UPDATE = 2;

    public $enableCsrfValidation = FALSE;

    protected $category;


    public function beforeAction($ev) {
        $this->view->url = new \common\components\Url;

        $url = Url::current();
        $url = explode('?', $url);
        $url = $url[0];
        $page = Page::find()->where(['url' => $url]);
        $page = $page->asArray()->one();


        if (!empty($page)) {

            if (!empty($page['permissions']) && Yii::$app->user->isGuest) {
                $this->redirect(['/user/default/login']);
                return FALSE;
            }

            if (!empty($page['permissions']) && !Yii::$app->user->can($page['permissions'])) {
                return $this->redirect('/403');
            }

            if (empty($page['permissions']) && !Yii::$app->user->isGuest) {
                $this->redirect(['/user/default/login']);
                return FALSE;
            }

            $this->view->title = $page['title'];
            $this->view->current = $page['id'];
        }

        $headers = Yii::$app->request->headers;
        if ($headers->has('x-ajax-form')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }


        return parent::beforeAction($ev);
    }

    public function redirectAfterSave($button, $action, $modelName = '', $modelId = 0): string {
        if (!empty($button) && ($button == 'continue')) {
            if ($action == self::ACTION_CREATE) {
                return 'create';
            }
            if ($action == self::ACTION_UPDATE) {
                return $modelName . '/update/' . $modelId;
            }
        }
        return 'index';
    }

    public function manipulatingTheFilterInSession($filter): ?array {
        if ($this->checkIfTheUserIsEnteringThePageWithoutFilter($filter)) {
            $filter = $this->getFilterOnSession($this->pageIdentifier) ?? null;
        } else {
            $this->destroyFilterOnSession($this->pageIdentifier);
            $this->setFilterOnSession($this->pageIdentifier, $filter);
        }
        return $filter;
    }

    public function setFilterOnSession(string $pageIdentifier, array $filter): void {
        Yii::$app->session->set('Filter-' . $pageIdentifier, $filter);
    }

    public function getFilterOnSession(string $pageIdentifier): ?array {
        return Yii::$app->session->get('Filter-' . $pageIdentifier);
    }

    public function destroyFilterOnSession(string $pageIdentifier): void {
        Yii::$app->session->remove('Filter-' . $pageIdentifier);
    }

    public function checkIfTheUserIsEnteringThePageWithoutFilter(array $pageFilter): bool {
        $response = true;
        foreach ($pageFilter as $item) {
            $response = $response && empty($item);
        }
        return $response;
    }

    public function actionClearFilter() {
        $this->destroyFilterOnSession($this->pageIdentifier);
        return $this->actionIndex();
    }

    public function getPageName(string $class, ?string $identifier = null): string {
        $page = Page::find()->andWhere(['identifier' => $identifier ?? $class::tableName()])->one();
        return $page->title;
    }

    public function getPageUrl(string $class, ?string $identifier = null): string {
        $page = Page::find()->andWhere(['identifier' => $identifier ?? $class::tableName()])->one();
        return $page->url;
    }
}