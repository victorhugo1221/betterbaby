<?php

use yii\db\Migration;

class m210915_123748_startBetterDatabase extends Migration {
    //region Create tables
    
    private function _createAddress() : void {
        $this->createTable('address', [
            'id'         => $this->integer()->append('auto_increment')->notNull(),
            'user_id'    => $this->integer()->notNull(),
            'city'       => $this->string(100)->notNull(),
            'state'      => $this->string(100)->notNull(),
            'street'     => $this->string(100)->notNull(),
            'number'     => $this->string(100)->notNull(),
            'complement' => $this->string(100)->null(),
            'district'   => $this->string(100)->notNull(),
            'zip_code'   => $this->integer(9)->notNull(),
            'latitude'   => $this->string(20)->null(),
            'longitude'  => $this->string(20)->null(),
            'status'     => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'    => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'    => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'    => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_address-user_id', 'address', 'user_id');
    }
    
    private function _createHelp() : void {
        $this->createTable('help', [
            'id'      => $this->integer()->append('auto_increment')->notNull(),
            'title'   => $this->string(100)->notNull(),
            'text'   => $this->string(255)->notNull(),
            'created' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted' => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        
    }
    
    private function _createUserProvider() : void {
        $this->createTable('user_provider', [
            'id'                      => $this->integer()->append('auto_increment')->notNull(),
            'user_id'                 => $this->integer()->notNull(),
            'plan_id'                 => $this->integer()->Null(),
            'price_range'             => $this->tinyInteger()->defaultValue(0)->notNull(),
            'price_range_health_plan' => $this->string(45)->null(),
            'additional_specialty'    => $this->string(100)->null(),
            'document_photo'          => $this->text()->null(),
            'avatar'                  => $this->text()->null(),
            'accept_pne'              => $this->string(45)->null(),
            'instagram_url'           => $this->string(45)->null(),
            'facebook_url'            => $this->string(45)->null(),
            'linkedin_url'            => $this->string(45)->null(),
            'cnpj'                    => $this->string(14)->null(),
            'crm'                     => $this->string(10)->null(),
            'crm_state'               => $this->string(45)->null(),
            'crm_specialty'           => $this->string(100)->null(),
            'average_rating'          => $this->decimal(2, 1)->null(),
            'status'                  => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'                 => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'                 => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'                 => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_user_provider-company_id', 'user_provider', 'user_id');
        
    }
    
    private function _createHealthPlan() : void {
        $this->createTable('health_plan', [
            'id'      => $this->integer()->append('auto_increment')->notNull(),
            'title'   => $this->string(100)->notNull(),
            'status'  => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted' => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createUserProviderHealthPlan() : void {
        $this->createTable('user_provider_health_plan', [
            'id'             => $this->integer()->append('auto_increment')->notNull(),
            'user_id'        => $this->integer()->notNull(),
            'health_plan_id' => $this->integer()->notNull(),
            'created'        => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'        => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'        => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_user_provider_health_plan-user_id', 'user_provider_health_plan', 'user_id');
        $this->createIndex('idx_user_provider_health_plan-health_plan_id', 'user_provider_health_plan', 'health_plan_id');
        
    }
    
    private function _createUserProviderPlan() : void {
        $this->createTable('user_provider_plan', [
            'id'       => $this->integer()->append('auto_increment')->notNull(),
            'user_id'  => $this->integer()->notNull(),
            'plan_id'  => $this->integer()->notNull(),
            'end_date' => $this->integer()->null(),
            'created'  => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'  => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'  => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_user_provider_plan-user_id', 'user_provider_plan', 'user_id');
        $this->createIndex('idx_user_provider_plan-plan_id', 'user_provider_plan', 'plan_id');
        
    }
    
    
    private function _createPlan() : void {
        $this->createTable('plan', [
            'id'          => $this->integer()->append('auto_increment')->notNull(),
            'category_id' => $this->integer()->notNull(),
            'title'       => $this->string(100)->notNull(),
            'price'       => $this->string(10)->notNull(),
            'status'      => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'     => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_plan-category_id', 'plan', 'category_id');
        
    }
    
    private function _createSolicitation() : void {
        $this->createTable('solicitation', [
            'id'                => $this->integer()->append('auto_increment')->notNull(),
            'user_client'       => $this->integer()->null(),
            'user_provider'     => $this->integer()->null(),
            'price'             => $this->string(10)->notNull(),
            'cancellation_date' => $this->dateTime()->null(),
            'scheduled_date'    => $this->dateTime()->notNull(),
            'rating_title'      => $this->string(100)->null(),
            'rating_text'       => $this->string(255)->null(),
            'rating_stars'      => $this->decimal(2, 1)->null(),
            'status'            => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'           => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'           => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'           => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_solicitation-user_client', 'solicitation', 'user_client');
        $this->createIndex('idx_solicitation-user_provider', 'solicitation', 'user_provider');
    }
    
    private function _createOrder() : void {
        $this->createTable('order', [
            'id'                => $this->integer()->append('auto_increment')->notNull(),
            'user_id'           => $this->integer()->null(),
            'price'             => $this->string(10)->notNull(),
            'cancellation_date' => $this->dateTime()->null(),
            'status'            => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'           => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'           => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'           => $this->dateTime()->null(),
            'primary key (id)',
        ]);
        $this->createIndex('idx_order-user_id', 'order', 'user_id');
    }
    
    private function _createNotification() : void {
        $this->createTable('notification', [
            'id'               => $this->integer()->append('auto_increment')->notNull(),
            'solicitation_id'  => $this->integer()->notNull(),
            'title'            => $this->string(50)->notNull(),
            'text'             => $this->string(1000)->notNull(),
            'send_push'        => $this->boolean()->defaultValue(0)->notNull(),
            'required_reading' => $this->boolean()->defaultValue(0)->notNull(),
            'reading_date'     => $this->dateTime()->null(),
            'status'           => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'          => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'          => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'          => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createNews() : void {
        $this->createTable('news', [
            'id'          => $this->integer()->append('auto_increment')->notNull(),
            'title'       => $this->string(50)->notNull(),
            'description' => $this->string(1000)->notNull(),
            'photo'       => $this->string(255)->notNull(),
            'created'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'     => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createPublicity() : void {
        $this->createTable('publicity', [
            'id'          => $this->integer()->append('auto_increment')->notNull(),
            'title'       => $this->string(50)->notNull(),
            'description' => $this->string(255)->null(),
            'phone'       => $this->string(50)->null(),
            'instagram'   => $this->string(50)->null(),
            'facebook'    => $this->string(50)->null(),
            'photo'       => $this->string(255)->notNull(),
            'status'      => $this->tinyInteger()->defaultValue(1)->notNull(),
            'created'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'     => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createBabyName() : void {
        $this->createTable('baby_name', [
            'id'      => $this->integer()->append('auto_increment')->notNull(),
            'name'    => $this->string(55)->notNull(),
            'gender'  => $this->string(10)->notNull(),
            'origin'  => $this->string(50)->null(),
            'meaning' => $this->string(155)->null(),
            'created' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted' => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    
    private function _createFavoriteProvider() : void {
        $this->createTable('favorite_provider', [
            'id'               => $this->integer()->append('auto_increment')->notNull(),
            'user_client_id'   => $this->integer()->notNull(),
            'user_provider_id' => $this->integer()->notNull(),
            'created'          => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'          => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'          => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createUserClientPhoto() : void {
        $this->createTable('user_client_photo', [
            'id'      => $this->integer()->append('auto_increment')->notNull(),
            'user_id' => $this->integer()->notNull(),
            'title'   => $this->string(50)->notNull(),
            'photo'   => $this->string(255)->notNull(),
            'created' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated' => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted' => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    
    private function _createAppSetting() : void {
        $this->createTable('app_setting', [
            'id'                          => $this->integer()->append('auto_increment')->notNull(),
            'maximum_travel_distance'     => $this->decimal(9, 2)->notNull(),
            'max_minutes_to_cancel'       => $this->integer()->notNull(),
            'cancellation_fee_percentage' => $this->decimal(9, 2)->notNull(),
            'cancellation_fee_amount'     => $this->decimal(9, 2)->notNull(),
            'created'                     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'                     => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'                     => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    private function _createUserAdministrator() : void {
        $this->createTable('user_administrator', [
            'id'                      => $this->integer()->append('auto_increment')->notNull(),
            'receive_complaint_email' => $this->boolean()->defaultValue(0)->notNull(),
            'user_id'                 => $this->integer()->notNull(),
            'created'                 => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'updated'                 => $this->dateTime()->defaultExpression('current_timestamp()')->notNull(),
            'deleted'                 => $this->dateTime()->null(),
            'primary key (id)',
        ]);
    }
    
    
    //region Insert default data
    
    private function _insertHealthPlans() : void {
        $this->insert('health_plan', ['title' => 'Unimed', 'status' => '1',]);
        $this->insert('health_plan', ['title' => 'Amil', 'status' => '1',]);
        $this->insert('health_plan', ['title' => 'Agemed', 'status' => '1',]);
        $this->insert('health_plan', ['title' => 'SulAmérica', 'status' => '1',]);
        $this->insert('health_plan', ['title' => 'Bradesco Saúde', 'status' => '1',]);
    }
    
    private function _insertCategory() : void {
        $this->insert('category', ['id' => '1', 'title' => 'Medicos', 'status' => '1',]);
        $this->insert('category', ['id' => '2', 'title' => 'Clinicas', 'status' => '1',]);
        $this->insert('category', ['id' => '3', 'title' => 'Serviços', 'status' => '1',]);
        
    }
    
    private function _insertPlans() : void {
        $this->insert('plan', ['category_id' => '1', 'title' => 'Free médico', 'price' => '00', 'status' => '1',]);
        $this->insert('plan', ['category_id' => '1', 'title' => 'Mensal médico', 'price' => '00', 'status' => '1',]);
        $this->insert('plan', ['category_id' => '2', 'title' => 'Free Clinica', 'price' => '00', 'status' => '1',]);
        $this->insert('plan', ['category_id' => '2', 'title' => 'Mensal Clinica', 'price' => '00', 'status' => '1',]);
        $this->insert('plan', ['category_id' => '3', 'title' => 'Free Serviço', 'price' => '00', 'status' => '1',]);
        $this->insert('plan', ['category_id' => '3', 'title' => 'Mensal Serviço', 'price' => '00', 'status' => '1',]);
        
    }
    
    //endregion
    
    private function _createForeignKeys() : void {
        $this->addForeignKey('FK_address-user_id_user-id', 'address', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_user_provider-user_id_user-id', 'user_provider', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_user_provider-plan_id_plan-id', 'user_provider', 'plan_id', 'plan', 'id');
        $this->addForeignKey('FK_user_provider_health_plan-user_id_user-id', 'user_provider_health_plan', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_user_provider_health_plan-health_plan_id_health_plan-id', 'user_provider_health_plan', 'health_plan_id', 'health_plan', 'id');
        $this->addForeignKey('FK_plan-category_id_category_id', 'plan', 'category_id', 'category', 'id');
        $this->addForeignKey('FK_solicitation-user_client_user_id', 'solicitation', 'user_client', 'user', 'id');
        $this->addForeignKey('FK_solicitation-user_user_provider_id', 'solicitation', 'user_provider', 'user', 'id');
        $this->addForeignKey('FK_order-user_id_user-id', 'order', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_user_provider_plan-user_id_user-id', 'user_provider_plan', 'user_id', 'user', 'id');
        $this->addForeignKey('FK_user_provider_plan-plan_id_plan-id', 'user_provider_plan', 'plan_id', 'plan', 'id');
        
    }
    
    //endregion
    
    //region Rollback
    private function _dropIndex() : void {
        $this->dropIndex('idx_address-company_id', 'address');
        $this->dropIndex('idx_address-user_id', 'address');
        $this->dropIndex('idx_user_provider-company_id', 'user_provider');
        $this->dropIndex('idx_user_provider-plan_id', 'user_provider');
        $this->dropIndex('idx_user_provider_health_plan-user_id', 'user_provider_health_plan');
        $this->dropIndex('idx_user_provider_health_plan-health_plan_id', 'user_provider_health_plan');
        $this->dropIndex('idx_plan-category_id', 'plan');
        $this->dropIndex('idx_solicitation-user_client', 'solicitation');
        $this->dropIndex('idx_solicitation-user_provider', 'solicitation');
        $this->dropIndex('idx_order-user_id', 'order');
        $this->dropIndex('idx_user_provider_plan-user_id', 'user_provider_plan');
        $this->dropIndex('idx_user_provider_plan-plan_id', 'user_provider_plan');
        
    }
    
    private function _dropForeignKeys() {
        $this->dropForeignKey('FK_address-user_id_user-id', 'address');
        $this->dropForeignKey('FK_user_provider-user_id_user-id', 'user_provider');
        $this->dropForeignKey('FK_user_provider-plan_id_plan-id', 'user_provider');
        $this->dropForeignKey('FK_user_provider_health_plan-user_id_user-id', 'user_provider_health_plan');
        $this->dropForeignKey('FK_user_provider_health_plan-health_plan_id_health_plan-id', 'user_provider_health_plan');
        $this->dropForeignKey('FK_plan-category_id_category_id', 'plan');
        $this->dropForeignKey('FK_solicitation-user_client_user_id', 'solicitation');
        $this->dropForeignKey('FK_solicitation-user_user_provider_id', 'solicitation');
        $this->dropForeignKey('FK_order-user_id_user-id', 'order');
        $this->dropForeignKey('FK_user_provider_plan-user_id_user-id', 'user_provider_plan');
        $this->dropForeignKey('FK_user_provider_plan-plan_id_plan-id', 'user_provider_plan');
        
        
    }
    
    private function _dropTables() : void {
        $this->dropTable('category');
        $this->dropTable('complaint');
        $this->dropTable('order');
        $this->dropTable('user_provider');
        $this->dropTable('menu');
        $this->dropTable('menu_option');
        $this->dropTable('plan');
        $this->dropTable('notification');
        $this->dropTable('app_setting');
        $this->dropTable('user_administrator');
        $this->dropTable('user_provider_plan');
    }
    
    //endregion
    
    public function safeUp() {
        $this->_createAddress();
        $this->_createSolicitation();
        $this->_createOrder();
        $this->_createUserProviderHealthPlan();
        $this->_createHealthPlan();
        $this->_createHelp();
        $this->_createUserProvider();
        $this->_createPlan();
        $this->_createNotification();
        $this->_createAppSetting();
        $this->_createUserAdministrator();
        $this->_createUserProviderPlan();
        $this->_createNews();
        $this->_createPublicity();
        $this->_createBabyName();
        $this->_createFavoriteProvider();
        $this->_createUserClientPhoto();
        $this->_createForeignKeys();
        
        
        $this->_insertHealthPlans();
        $this->_insertCategory();
        $this->_insertPlans();
    }
    
    public function safeDown() {
        if ($_ENV['DB_HOST'] === 'localhost' || $_ENV['DB_HOST'] === '192.168.2.3') {
            $this->_dropForeignKeys();
            $this->_dropIndex();
            $this->_dropTables();
            $tableName = $this->db->tablePrefix . 'users';
            if (!$this->db->getTableSchema($tableName, TRUE) === NULL) {
                $this->dropTable('migration');
            }
        } else {
            echo "===================== ATTENTION ======================";
            echo " Rollback is not applicable in production environment.";
            echo "======================================================";
        }
    }
}
