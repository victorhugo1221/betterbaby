<?php

use api\tests\ApiTester;
use api\tests\utils\BaseTest;
use Codeception\Util\HttpCode;

class AccountCest extends BaseTest
{

    public function tryRegisterWithoutToken(ApiTester $I)
    {
        $I->sendPost('account/register', []);
        $I->seeResponseCodeIs(HttpCode::SERVICE_UNAVAILABLE);
    }

    public function tryChangePasswordInvalidToken(ApiTester $I)
    {
        $this->_generatePrimaryToken($I);
        $I->sendPost('account/register', []);
        $this->_jsonResponseIsUnauthorized($I);
    }

    public function tryRegisterSmallPassword(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['password' => '23e32']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Senha precisa ter pelo menos 8 caracteres.']);
    }

    public function tryRegisterDifferentPasswordConfirmation(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['password' => '23e32', 'confirmpassword' => '324d3']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'A confirmação precisa ser igual a senha.']);
    }

    public function tryRegisterWithoutName(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Nome precisa ser informado.']);
    }

    public function tryRegisterWithoutPhone(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Telefone precisa ser informado.']);
    }

    public function tryRegisterWithoutRG(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'RG precisa ser informado.']);
    }

    public function tryRegisterWithoutCPF(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'CPF precisa ser informado.']);
    }

    public function tryRegisterWithoutNationality(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Nacionalidade precisa ser informada.']);
    }

    public function tryRegisterWithoutMaritalStatus(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Estado civil precisa ser informado.']);
    }

    public function tryRegisterWithoutOccupation(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Profissão precisa ser informada.']);
    }

    public function tryRegisterWithoutAddress(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Endereço precisa ser informado.']);
    }

    public function tryRegisterWithoutState(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Estado precisa ser informado.']);
    }

    public function tryRegisterWithoutCity(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => 'i', 'city' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Cidade precisa ser informada.']);
    }

    public function tryRegisterWithoutDistrict(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => 'i', 'city' => 'j', 'district' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Bairro precisa ser informado.']);
    }

    public function tryRegisterWithoutStreetAddress(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => 'i', 'city' => 'j', 'district' => 'k', 'streetaddress' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Logradouro precisa ser informado.']);
    }

    public function tryRegisterWithoutComplement(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => 'i', 'city' => 'j', 'district' => 'k', 'streetaddress' => 'l', 'complement' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Complemento precisa ser informado.']);
    }

    public function tryRegisterWithoutNumber(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => 'i', 'city' => 'j', 'district' => 'k', 'streetaddress' => 'l', 'complement' => 'm', 'number' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Número precisa ser informado.']);
    }

    public function tryRegisterWithoutEmail(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => 'i', 'city' => 'j', 'district' => 'k', 'streetaddress' => 'l', 'complement' => 'm', 'number' => 'n', 'email' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'E-mail precisa ser informado.']);
    }

    public function tryRegisterWithoutCompanyProfessionalName(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => 'i', 'city' => 'j', 'district' => 'k', 'streetaddress' => 'l', 'complement' => 'm', 'number' => 'n', 'email' => 'o', 'companyprofessionalname' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Nome da empresa ou profissional precisa ser informado.']);
    }

    public function tryRegisterWithoutPassword(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => 'i', 'city' => 'j', 'district' => 'k', 'streetaddress' => 'l', 'complement' => 'm', 'number' => 'n', 'email' => 'o', 'companyprofessionalname' => 'p', 'password' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Senha precisa ser informada.']);
    }

    public function tryRegisterWithoutConfirmPassword(ApiTester $I)
    {
        $this->_generateToken($I);
        $I->sendPost('account/register', ['name' => 'a', 'phone' => 'b', 'rg' => 'c', 'cpf' => 'd', 'nationality' => 'e', 'maritalstatus' => 'f', 'occupation' => 'g', 'address' => 'h', 'state' => 'i', 'city' => 'j', 'district' => 'k', 'streetaddress' => 'l', 'complement' => 'm', 'number' => 'n', 'email' => 'o', 'companyprofessionalname' => 'p', 'password' => 'q', 'confirmpassword' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Confirmação da senha precisa ser informada.']);
    }

}
