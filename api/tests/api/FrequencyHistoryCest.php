<?php

use api\tests\ApiTester;
use api\tests\utils\BaseTest;
use Codeception\Util\HttpCode;

class FrequencyHistoryCest extends BaseTest {

    public function tryGuideListWithPrimaryToken(ApiTester $I): void {
        $this->_generatePrimaryToken($I);
        $I->sendGet('frequency-history/list-by-user', []);
        $I->seeResponseCodeIs(HttpCode::UNAUTHORIZED);
        $I->seeResponseMatchesJsonType(["message" => "string"]);
    }
    
    public function tryFrequencyHistoryListByUserId(ApiTester $I): void {
        $this->_generateToken($I);
        $I->sendGet('frequency-history/list-by-user', ['user_id' => 1]);
        $this->_jsonResponseIsOk($I);
    }
    
    public function tryFrequencyHistoryListWithoutUserId(ApiTester $I): void {
        $this->_generateToken($I);
        $I->sendGet('frequency-history/list-by-user');
        $this->_jsonResponseIsOk($I);
    }
    
    
    
    public function tryRegisterHistoryFrequencyHistoryWithPrimaryToken(ApiTester $I): void {
        $this->_generatePrimaryToken($I);
        $time = time();
        $I->sendPost('frequency-history/register-history', ['userId' => 4, 'history' => "Codeception test {$time}"]);
        $this->_jsonResponseIsErrorOrWarning($I, HttpCode::UNAUTHORIZED);
        $I->seeResponseMatchesJsonType(["message" => "string"]);
    }
    
    public function tryRegisterHistoryFrequencyHistory(ApiTester $I): void {
        $this->_generateToken($I);
        $time = time();
        $I->sendPost('frequency-history/register-history', ['userId' => 4, 'history' => "Codeception test {$time}"]);
        $this->_jsonResponseIsOk($I);
        $I->seeResponseMatchesJsonType(["message" => "string"]);
    }
    
    public function tryRegisterHistoryFrequencyHistoryWithoutUserId(ApiTester $I): void {
        $this->_generateToken($I);
        $time = time();
        $I->sendPost('frequency-history/register-history', ['history' => "Codeception test {$time}"]);
        $this->_jsonResponseIsOk($I);
        $I->seeResponseMatchesJsonType(["message" => "string"]);
    }
}
