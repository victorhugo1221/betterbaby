<?php

namespace api\tests\api;

use api\tests\ApiTester;
use api\tests\utils\BaseTest;
use Codeception\Util\HttpCode;
use http\Message\Body;


class UserCest extends BaseTest {
    
    public function tryChangePasswordOK(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendPost('/user/change-password', ['actual_password' => 'x', 'password' => 'x']);
        $this->_jsonResponseIsOk($I);
    }
    
    public function tryChangePasswordInvalidPassword(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendPost('/user/change-password', ['actual_password' => 'aaa111', 'password' => '2']);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryChangePasswordNoNewPassword(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendPost('/user/change-password', ['actual_password' => 'x', 'password' => '']);
        $this->_jsonResponseIsUnauthorized($I);
        
    }
    
    public function tryChangePasswordNoOldPassword(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendPost('/user/change-password', ['actual_password' => '', 'password' => 'y']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Senha antiga precisa ser informada']);
        
    }
    
    public function tryChangePasswordInvalidToken(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->sendPost('/user/change-password', ['actual_password' => 'x', 'password' => 'y']);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryChangePasswordWithoutToken(ApiTester $I) {
        $I->sendPost('/user/change-password', ['actual_password' => 'x', 'password' => 'y']);
        $I->seeResponseCodeIs(HttpCode::SERVICE_UNAVAILABLE);
    }
    
    //Login//
    
    public function tryLoginOK(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseMatchesJsonType(["content" => ["token" => "string", "refreshToken" => "string"]]);
    }
    
    public function tryLoginWithInvalidApiKey(ApiTester $I) {
        $I->sendPost('user/generate-primary-token', ['apiKey' => 'YXBpIGtW4']);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryLoginWithoutApiKey(ApiTester $I) {
        $I->sendPost('auth/generate-primary-token', ['apiKey' => '']);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryLoginWithoutEmail(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->sendPost('user/authentication', ['email' => '', 'password' => 'x']);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryLoginWithoutPassword(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->sendPost('user/authentication', ['email' => 'atendimento@grupow.com.br', 'password' => '']);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryLoginWithoutEmailAndPassword(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->sendPost('user/authentication', ['email' => '', 'password' => '']);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    //get data//
    
    public function tryGetUSerDataOK(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendGet('/user/get-data');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseMatchesJsonType(["content" => ["token" => "string", "refreshToken" => "string"]]);
    }
    
    public function tryGetUSerDataInvalidToken(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->sendGet('/user/get-data');
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryGetUSerDataWithoutToken(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->sendGet('/user/get-data');
        $I->seeResponseCodeIs(HttpCode::SERVICE_UNAVAILABLE);
    }
    
    //update user//
    
    public function tryUpdateUserOK(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendPut('/user/update', [
            'cpf'             => '012345678910',
            'phone'           => '4799254664',
            'rg'              => '1234567',
            'nationality'     => 'brasileiro',
            'profession'      => 'programador',
            'maritalStatusId' => '1',
            'cityId'          => '2054',
            'zipCode'         => '88220000',
            'neighborhood'    => 'meia praia',
            'street'          => '302',
            'complement'      => 'ap',
            'number'          => '5222',
        ]);
        $this->_jsonResponseIsOk($I);
        $I->seeResponseMatchesJsonType(["message" => "string"]);
    }
    
    public function tryUpdateUserInvalidToken(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->sendPut('/user/update', [
            'cpf'             => '012345678910',
            'phone'           => '4799254664',
            'rg'              => '1234567',
            'nationality'     => 'brasileiro',
            'profession'      => 'programador',
            'maritalStatusId' => '1',
            'cityId'          => '2054',
            'zipCode'         => '88220000',
            'neighborhood'    => 'meia praia',
            'street'          => '302',
            'complement'      => 'ap',
            'number'          => '5222',
        ]);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryUpdateUserWithoutToken(ApiTester $I) {
        $I->sendPut('/user/update', [
            'cpf'             => '012345678910',
            'phone'           => '4799254664',
            'rg'              => '1234567',
            'nationality'     => 'brasileiro',
            'profession'      => 'programador',
            'maritalStatusId' => '1',
            'cityId'          => '2054',
            'zipCode'         => '88220000',
            'neighborhood'    => 'meia praia',
            'street'          => '302',
            'complement'      => 'ap',
            'number'          => '5222',
        ]);
        $I->seeResponseCodeIs(HttpCode::SERVICE_UNAVAILABLE);
    }
    
    
    
}