<?php

use api\tests\ApiTester;
use api\tests\utils\BaseTest;
use Codeception\Util\HttpCode;

class EditPasswordCest extends BaseTest {
    
    public function tryChangePasswordWithoutToken(ApiTester $I) {
        $I->sendPost('user/change-password', ['actual_password' => 'x', 'password' => 'y']);
        $I->seeResponseCodeIs(HttpCode::SERVICE_UNAVAILABLE);
    }
    
    public function tryChangePasswordInvalidToken(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->sendPost('user/change-password', ['actual_password' => 'x', 'password' => 'y']);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryChangePasswordNoOldPassword(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendPost('user/change-password', ['actual_password' => '', 'password' => 'y']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Senha antiga precisa ser informada']);
    }
    
    public function tryChangePasswordNoNewPassword(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendPost('user/change-password', ['actual_password' => 'x', 'newPassword' => '']);
        $this->_jsonResponseIsErrorOrWarning($I);
        $I->seeResponseContainsJson(['message' => 'Nova senha precisa ser informada']);
    }
    
    public function tryChangePasswordInvalidPassword(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendPost('user/change-password', ['actual_password' => 'aaa111', 'password' => '2']);
        $this->_jsonResponseIsUnauthorized($I);
    }
    
    public function tryChangePasswordOK(ApiTester $I) {
        $this->_generateToken($I);
        $I->sendPost('user/change-password', ['actual_password' => 'x', 'password' => 'x']);
        $this->_jsonResponseIsOk($I);
    }
}
