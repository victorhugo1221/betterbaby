<?php

use api\tests\ApiTester;
use api\tests\utils\BaseTest;
use Codeception\Util\HttpCode;

class CompanyCest extends BaseTest {

    public function tryCreateNewCompanySucess(ApiTester $I): void {
        $this->_generateToken($I);
        $I->sendPost('/company/new-company',  ['name' => 'x company', 'logo' => 'logoexemplo', 'watermark' => 'watermarkexemplo']);
        $this->_jsonResponseIsOk($I);
        $I->seeResponseMatchesJsonType(["message" => "string"]);
        
        }
        
    public function tryCreateNewCompanyWithPrimaryToken(ApiTester $I): void {
        $this->_generatePrimaryToken($I);
        $I->sendPost('/company/new-company',  ['name' => 'x company', 'logo' => 'logoexemplo', 'watermark' => 'watermarkexemplo']);
        $this->_jsonResponseIsErrorOrWarning($I, HttpCode::UNAUTHORIZED);
        $I->seeResponseMatchesJsonType(["message" => "string"]);
    }
    
    public function tryGetDataSucess(ApiTester $I): void {
        $this->_generateToken($I);
        $I->sendGet('/company/get-data',  ['name' => 'x company', 'logo' => 'logoexemplo', 'watermark' => 'watermarkexemplo']);
        $this->_jsonResponseIsOk($I);
        $I->seeResponseMatchesJsonType(["message" => "string"]);
        
    }
    
    



}