<?php

use api\tests\ApiTester;
use api\tests\utils\BaseTest;
use Codeception\Util\HttpCode;

class EmailCest extends BaseTest {
    
    public function trySendAContactFormWithoutToken(ApiTester $I): void {
        $I->sendPost('email/send-contact-form', [
            'name' => "joão pedro",
            'email' => "grupow@grupow.com.br",
            'subject' => "Testes",
            'message' => "Lorem Ipsum is simply dummy text of the printing and typesetting"
        ]);
        $I->seeResponseCodeIs(HttpCode::SERVICE_UNAVAILABLE);
        $I->seeResponseMatchesJsonType(["message" => "string"]);
    }
    
    public function trySendAContactFormWithPrimaryToken(ApiTester $I): void {
        $this->_generatePrimaryToken($I);
        $I->sendPost('email/send-contact-form', [
            'name' => "joão pedro",
            'email' => "grupow@grupow.com.br",
            'subject' => "Testes",
            'message' => "Lorem Ipsum is simply dummy text of the printing and typesetting"
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
    }
}
