<?php

namespace api\tests\utils;

use api\tests\ApiTester;
use Codeception\Util\HttpCode;


abstract class BaseTest {
    protected string $token        = "";
    protected string $refreshToken = "";
    
    protected function _before(ApiTester $I) {
        $response           = $I->generateToken($this->token, $this->refreshToken);
        $this->token        = $response['token'];
        $this->refreshToken = $response['refreshToken'];
        $I->haveHttpHeader('Content-Type', 'application/json');
    }
    
    protected function _jsonResponseIsUnauthorized(ApiTester $I): void {
        $I->seeResponseCodeIs(HttpCode::UNAUTHORIZED);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType(["message" => "string"]);
    }
    
    protected function _jsonResponseIsOk(ApiTester $I, int $code = HttpCode::OK): void {
        $I->seeResponseCodeIs($code);
        $I->seeResponseIsJson();
    }
    
    protected function _jsonResponseIsError(ApiTester $I, int $code = HttpCode::INTERNAL_SERVER_ERROR): void {
        $I->seeResponseCodeIs($code);
        $I->seeResponseIsJson();
    }
    
    protected function _jsonResponseIsErrorOrWarning(ApiTester $I, int $code = HttpCode::BAD_REQUEST): void {
        $I->seeResponseCodeIs($code);
        $I->seeResponseIsJson();
    }
    
    protected function _generatePrimaryToken(ApiTester $I) {
        $I->sendPost('/auth/generate-primary-token', ['apiKey' => 'YXBpIGtleSB0byBnZW5lcmF0ZSBwcmltYXJ5IHRva2VuIGZvciBhIE1pTmhhIFByb1Bvc3Rh']);
        $returnToken = $I->grabResponse();
        $token       = json_decode($returnToken);
        $I->amBearerAuthenticated($token->content->token);
        return $token;
    }
    
    protected function _generateToken(ApiTester $I) {
        $this->_generatePrimaryToken($I);
        $I->sendPost('/auth/authentication', ['login' => '1633378965@grupow.com.br', 'password' => 'x']);
        $returnToken = $I->grabResponse();
        $token       = json_decode($returnToken);
        $I->amBearerAuthenticated($token->content->token);
        return $token;
    }
}