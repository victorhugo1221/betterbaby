<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');
defined('YII_APP_BASE_PATH') or define('YII_APP_BASE_PATH', __DIR__.'/../../');

require_once YII_APP_BASE_PATH . '/vendor/autoload.php';
require_once YII_APP_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php';
require_once YII_APP_BASE_PATH . '/common/config/bootstrap.php';
require_once __DIR__ . '/../config/bootstrap.php';

// clear old test fail reports
if ($handle = opendir(__DIR__ . './_output')) {
	while (false !== ($file = readdir($handle))) {
		if (substr($file, -5) === '.html' || substr($file, -4) === '.png' || substr($file, -5) === '.json') {
			unlink(__DIR__ . './_output/' . $file);
		}
	}
	closedir($handle);
}

ob_start();