<?php

namespace api\tests;

use api\components\utils\Auth;
use common\modules\user\models\User;
use common\modules\user\models\UserClient;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 */
class ApiTester extends \Codeception\Actor {
    use _generated\ApiTesterActions;
    
    /**
     * Define custom actions here
     */
    
    public function generateToken(string $user_id, ?bool $force = false): array {
        if ($force === true || empty($token) || empty($refreshToken)) {
            return [
                "token"        => Auth::generateJWT(),
                "refreshToken" => Auth::refreshJWT($user_id)
            ];
        }
        return [
            "token"        => $token,
            "refreshToken" => $refreshToken
        ];
    }
}
