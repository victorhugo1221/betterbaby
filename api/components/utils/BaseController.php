<?php

namespace api\components\utils;

use Yii;
use Exception;
use yii\filters\Cors;
use yii\helpers\Json;
use yii\rest\ActiveController;
use common\components\utils\Text;
use Firebase\JWT\ExpiredException;
use yii\base\InvalidArgumentException;
use Dersonsena\JWTTools\JWTSignatureBehavior;

/**
 * @SWG\Swagger(schemes={ "https"}, host="api.betterbaby.com.br", basePath="/", @SWG\Info(version="1.0.0", title="Better Baby - Rest API", termsOfService="")),
 * @SWG\Parameter(parameter="primary_bearer", name="Authorization", in="header", required=true, type="string", default="Bearer PRIMARY_TOKEN", description="Can be a primary or final Bearer token"),
 * @SWG\Parameter(parameter="final_bearer", name="Authorization", in="header", required=true, type="string", default="Bearer TOKEN_FINAL", description="Must be a final Bearer token")
 */
class BaseController extends ActiveController {
    use Auth;
    
    protected $currentUser;
    protected $jsonData;
    protected $post;
    protected $get;
    protected $lang;
    
    public function init() {
        parent::init();
        $token = str_replace('Bearer ', '', Yii::$app->request->getHeaders()['authorization']);
        if (!empty($token)) {
            $this->currentUser = static::decryptToken($token);
        }
        $this->lang = $this->request->headers['Accept-Language'] ?? 'pt';
    }
    
    public function actions() {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        return $actions;
    }
    
    public static function allowedDomains() {
        return [
            trim($_ENV['BACKENDURL'], '/'),
            trim($_ENV['BASEURL'], '/'),
            trim($_ENV['APIURL'], '/'),
            trim($_ENV['BACKENDURL'], '/'),
            trim($_ENV['BASEURL'], '/'),
            trim($_ENV['APIURL'], '/'),
            'http://localhost:8100',
        ];
    }
    
    public function behaviors() {
        $behaviors = parent::behaviors();
        
        // This process is done because the browser needs CORS before authentication
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors'  => [
                'Origin'                           => self::allowedDomains(),
                'Access-Control-Request-Method'    => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers'   => ['Origin', 'X-Requested-With', 'Access-Control-Allow-Headers', 'Content-Range', 'Content-Disposition', 'Content-Type', 'Authorization', 'accept', 'Accept'],
                'Access-Control-Allow-Credentials' => TRUE,
                'Access-Control-Max-Age'           => 3600,
                'Access-Control-Expose-Headers'    => ['X-Pagination-Current-Page'],
            ],
        ];
        
        $behaviors['authenticator']           = $auth;
        $behaviors['authenticator']['except'] = ['options'];
        
        $behaviors['jwtValidator'] = [
            'class'     => JWTSignatureBehavior::class,
            'secretKey' => $_ENV['JWT_TOKEN_SECRET'],
            'except'    => ['generate-primary-token'],
        ];
        
        return $behaviors;
    }
    
    public function beforeAction($action) {
        try {
            if (Yii::$app->request->isPost || Yii::$app->request->isPut) {
                $data = file_get_contents("php://input");
                if (!empty($data)) {
                    try {
                        $this->post = Json::decode($data);
                    } catch (InvalidArgumentException $e) {
                        Yii::$app->response->setStatusCode(HttpCode::BAD_REQUEST_400);
                        $this->asJson('Malformed JSON');
                        return FALSE;
                    }
                } else {
                    $this->post = Yii::$app->request->getBodyParams();
                }
            }
            
            $this->get = Yii::$app->request->get();
            return parent::beforeAction($action);
        } catch (ExpiredException $ex) {
            Yii::$app->response->setStatusCode(HttpCode::UNAUTHORIZED_401);
            $this->asJson(['message' => $ex->getMessage()]);
            return FALSE;
        } catch (Exception $ex) {
            Yii::$app->response->setStatusCode(HttpCode::SERVICE_UNAVAILABLE_503);
            $this->asJson(['message' => $ex->getMessage()]);
            return FALSE;
        }
    }
    
    protected function response(?array $responseData = [], ?int $code = HttpCode::OK_200, ?int $totalCount = NULL): array {
        $countData = 0;
        
        if (isset($responseData['message'])) {
            $message = empty($responseData['message']) ? NULL : $responseData['message'];
        }
        
        if (isset($responseData['data']) && (count($responseData['data']) > 0 || isset($responseData['message']))) {
            $content   = empty($responseData['data']) ? NULL : $this->convertSnakeCaseToCamelCase($responseData['data']);
            $countData = count($responseData['data']);
        } elseif (isset($responseData) && count($responseData) > 0) {
            unset($responseData['message']);
            $content   = empty($responseData) ? NULL : $this->convertSnakeCaseToCamelCase($responseData);
            $countData = count($responseData);
        } else {
            $code    = HttpCode::NO_CONTENT_204;
            $content = [];
        }
        
        $response = [];
        if (!empty($content)) {
            $response['content'] = $content;
        }
        
        if (!empty($message)) {
            $response['message'] = $message;
        }
        
        if (!empty($totalCount)) {
            $response['totalCount'] = $totalCount;
        } elseif (!empty($countData)) {
            $response['totalCount'] = $countData;
        }
        
        if (isset($responseData['pageCount']) && !empty($responseData['pageCount'])) {
            $response['pageCount'] = $responseData['pageCount'];
        }
        
        Yii::$app->response->setStatusCode($code);
        return $response;
    }
    
    public function convertSnakeCaseToCamelCase(array $content): array {
        if (!empty($content["token"]) && !empty($content["refreshToken"])) {
            return $content;
        }
        
        $response = NULL;
        foreach ($content as $item) {
            $data       = is_array($item) ? $item : $content;
            $newContent = [];
            foreach ($data as $keyContent => $value) {
                $keyContent              = lcfirst(str_replace('_', '', ucwords($keyContent, '_')));
                $newContent[$keyContent] = $value;
            }
            $response[] = $newContent;
        }
        
        
        return $response;
    }
    
    public function theTokenSentIsNotThePrimaryToken(): bool {
        if (!$this->currentUser->id > 0) {
            Yii::$app->response->setStatusCode(HttpCode::UNAUTHORIZED_401);
            return FALSE;
        }
        return TRUE;
    }
    
    protected function convertFileToBase64(string $file, string $mimeType): string {
        $data   = file_get_contents($file);
        $base64 = base64_encode($data);
        return $base64;
    }
    
    protected function compressFilesToBase64(?string $zipFileName): string {
        $tempFileName = $zipFileName ?? Text::slugify(microtime()) . '.zip';
        $tempFileName = "../runtime/cache/{$tempFileName}";
        $zipFile      = new ZipArchive();
        if ($zipFile->open($tempFileName, ZipArchive::CREATE)) {
            
            foreach ($_FILES as $file) {
                if (is_array($file['name'])) {
                    $qtty = count($file['name']);
                    for ($i = 0; $i < $qtty; $i++) {
                        $zipFile->addFile($file['tmp_name'][$i], $file['name'][$i]);
                    }
                } else {
                    $zipFile->addFile($file['tmp_name'], $file['name']);
                }
            }
        }
        $zipFile->close();
        $data = file_get_contents($tempFileName);
        
        unlink($tempFileName);
        return base64_encode($data);
    }
    
    protected function validadeReceivedParams(array $params): string {
        $message = 'Dados Obrigatórios não encontrados: ';
        $fail    = FALSE;
        foreach ($params as $key => $value) {
            if (empty($value)) {
                $fail    = TRUE;
                $message .= "{$key} é obrigatório, ";
                Yii::$app->response->setStatusCode(HttpCode::BAD_REQUEST_400);
            }
        }
        return $fail ? $message : '';
    }
}