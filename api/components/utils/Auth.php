<?php

namespace api\components\utils;

use common\modules\user\models\User;
use Dersonsena\JWTTools\JWTTools;
use common\modules\user\models\UserClient;
use Exception;
use yii\db\ActiveRecord;

trait Auth {
    
    public static function generateJWT(?array $data = [], ActiveRecord $model = NULL, ?array $paramModel = NULL, ?bool $primary = FALSE): string {
        $token = JWTTools::build($_ENV['JWT_TOKEN_SECRET'],
            [
                'iss'        => $_ENV['JWT_ISSUER'],
                'aud'        => $_ENV['JWT_AUDIENCE'],
                'expiration' => $primary ? $_ENV['JWT_DURATION_SECONDS_PRIMARY'] : $_ENV['JWT_DURATION_SECONDS'],
            ]
        );
        
        if (isset($model) && count($paramModel) > 0) {
            $token->withModel($model, $paramModel);
        }
        
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $token->getPayload()->addExtraAttribute($key, $value);
            }
        }
        
        return $token->getJWT();
    }
    
    public static function refreshJWT(?array $data = []): string {
        $token = JWTTools::build($_ENV['JWT_TOKEN_SECRET'],
            [
                'iss'        => $_ENV['JWT_ISSUER'],
                'aud'        => $_ENV['JWT_AUDIENCE'],
                'expiration' => $_ENV['JWT_REFRESH_DURATION_SECONDS'],
            ]
        );
        
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $token->getPayload()->addExtraAttribute($key, $value);
            }
        }
        return $token->getJWT();
    }
    
    
    public static function decryptToken(string $token) {
        try {
            $decodedToken = JWTTools::build($_ENV['JWT_TOKEN_SECRET'])->decodeToken($token);
            return $decodedToken;
        } catch (Exception $e) {
            return FALSE;
        }
    }
}
