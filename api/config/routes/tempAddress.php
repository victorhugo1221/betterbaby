<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'temp-address' => '/temp-address/temp-address',
    ],
    'extraPatterns' => [
        'OPTIONS new-address' => 'new-address',
        'POST new-address' => 'new-address',
        
        'OPTIONS list-address' => 'list-address',
        'GET list-address' => 'list-address',

    ],
];
