<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'publicity' => '/publicity/publicity',
    ],
    'extraPatterns' => [
        'OPTIONS list-publicity' => 'list-publicity',
        'GET list-publicity' => 'list-publicity',
        
        'OPTIONS publicity-details' => 'publicity-details',
        'GET publicity-details' => 'publicity-details',
        
        'OPTIONS new-view' => 'new-view',
        'POST new-view' => 'new-view',

    ],
];
