<?php
return [
    'class'         => 'yii\rest\UrlRule',
    'prefix'        => '/',
    'pluralize'     => FALSE,
    'ruleConfig'    => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller'    => [
        'auth' => '/auth/authentication',
    ],
    'extraPatterns' => [
        'OPTIONS generate-primary-token' => 'generate-primary-token',
        'POST generate-primary-token'    => 'generate-primary-token',
        
        'OPTIONS authentication' => 'authentication',
        'POST authentication' => 'authentication',
        
        'OPTIONS authentication-professional' => 'authentication-professional',
        'POST authentication-professional' => 'authentication-professional',
        
        'OPTIONS refresh-token' => 'refresh-token',
        'GET refresh-token' => 'refresh-token',
    ],
];
