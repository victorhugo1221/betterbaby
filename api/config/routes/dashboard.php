<?php
return [
	'class'         => 'yii\rest\UrlRule',
	'prefix'        => '/',
	'pluralize'     => FALSE,
	'ruleConfig'    => [
		'class' => 'yii\web\UrlRule',
	],
	'controller'    => [
		'dashboard' => '/dashboard/dashboard',
	],
	'extraPatterns' => [
        'OPTIONS get-data' => 'get-data',
        'GET get-data'     => 'get-data',
	],
];
