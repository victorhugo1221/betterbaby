<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'health-plan' => '/health-plan/health-plan',
    ],
    'extraPatterns' => [
        'OPTIONS list-plans' => 'list-plans',
        'GET list-plans' => 'list-plans',

    ],
];
