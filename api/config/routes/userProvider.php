<?php
return [
    'class'         => 'yii\rest\UrlRule',
    'prefix'        => '/',
    'pluralize'     => FALSE,
    'ruleConfig'    => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller'    => [
        'user-provider' => '/user-provider/user-provider',
    ],
    'extraPatterns' => [
        'OPTIONS get-data' => 'get-data',
        'GET get-data'     => 'get-data',
        
        'OPTIONS get-user-plan' => 'get-user-plan',
        'GET get-user-plan'     => 'get-user-plan',
        
        'OPTIONS list-all' => 'list-all',
        'GET list-all'     => 'list-all',
        
        'OPTIONS list-favorite' => 'list-favorite',
        'GET list-favorite'     => 'list-favorite',
        
        'OPTIONS list-details' => 'list-details',
        'GET list-details'     => 'list-details',
        
        'OPTIONS list-available-time' => 'list-available-time',
        'GET list-available-time'     => 'list-available-time',
        
        'OPTIONS search-crm' => 'search-crm',
        'GET search-crm'     => 'search-crm',
        
        'OPTIONS new-doctor' => 'new-doctor',
        'POST new-doctor'    => 'new-doctor',
        
        'OPTIONS new-clinic' => 'new-clinic',
        'POST new-clinic'    => 'new-clinic',
        
        'OPTIONS new-service' => 'new-service',
        'POST new-service'    => 'new-service',
        
        'OPTIONS update-user-provider' => 'update-user-provider',
        'PUT update-user-provider'    => 'update-user-provider',
        
        'OPTIONS delete-user-provider' => 'delete-user-provider',
        'PUT delete-user-provider'     => 'delete-user-provider',
        
        'OPTIONS upload-avatar' => 'upload-avatar',
        'POST upload-avatar'     => 'upload-avatar',
        
        'OPTIONS new-favorite' => 'new-favorite',
        'POST new-favorite'     => 'new-favorite',
    ],
];
