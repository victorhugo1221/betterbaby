<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'help' => '/help/help',
    ],
    'extraPatterns' => [
        'OPTIONS list-help' => 'list-help',
        'GET list-help' => 'list-help',
        
        'OPTIONS list-help-client' => 'list-help-client',
        'GET list-help-client' => 'list-help-client',
        
        'OPTIONS list-help-website' => 'list-help-website',
        'GET list-help-website' => 'list-help-website',

        'OPTIONS list-help-website' => 'list-help-web',
        'GET list-help-website' => 'list-help-website',
    ],
];
