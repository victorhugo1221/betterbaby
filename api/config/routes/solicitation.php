<?php
return [
    'class'         => 'yii\rest\UrlRule',
    'prefix'        => '/',
    'pluralize'     => FALSE,
    'ruleConfig'    => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller'    => [
        'solicitation' => '/solicitation/solicitation',
    ],
    'extraPatterns' => [
        'OPTIONS get-data' => 'get-data',
        'GET get-data'     => 'get-data',
        
        'OPTIONS dashboard-revenue' => 'dashboard-revenue',
        'GET dashboard-revenue'     => 'dashboard-revenue',
        
        'OPTIONS dashboard-solicitation' => 'dashboard-solicitation',
        'GET dashboard-solicitation'     => 'dashboard-solicitation',
        
        'OPTIONS new-solicitation' => 'new-solicitation',
        'POST new-solicitation'    => 'new-solicitation',
        
        'OPTIONS update-data' => 'update-data',
        'PUT update-data'    => 'update-data',
        
        'OPTIONS get-details' => 'get-details',
        'GET get-details'     => 'get-details',
        
        'OPTIONS list-data-by-client' => 'list-data-by-client',
        'GET list-data-by-client'     => 'list-data-by-client',
        
        'OPTIONS list-rating' => 'list-rating',
        'GET list-rating'     => 'list-rating',
        
        'OPTIONS list-rating-client' => 'list-rating-client',
        'GET list-rating-client'     => 'list-rating-client',
        
        'OPTIONS list-solicitation-pending' => 'list-solicitation-pending',
        'GET list-solicitation-pending'     => 'list-solicitation-pending',
        
        'OPTIONS list-schedule' => 'list-schedule',
        'GET list-schedule'     => 'list-schedule',
        
        'OPTIONS confirm-solicitation' => 'confirm-solicitation',
        'PUT confirm-solicitation'     => 'confirm-solicitation',
        
        'OPTIONS cancel-solicitation' => 'cancel-solicitation',
        'PUT cancel-solicitation'     => 'cancel-solicitation',
        
        'OPTIONS expire-solicitation' => 'expire-solicitation',
        'PUT expire-solicitation'     => 'expire-solicitation',
    
    ],
];
