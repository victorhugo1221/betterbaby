<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'email' => '/email/email',
    ],
    'extraPatterns' => [
        'OPTIONS send-contact-form' => 'send-contact-form',
        'POST send-contact-form' => 'send-contact-form',
    ],
];
