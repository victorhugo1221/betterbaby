<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'news' => '/news/news',
    ],
    'extraPatterns' => [
        'OPTIONS list-news' => 'list-news',
        'GET list-news' => 'list-news',

    ],
];
