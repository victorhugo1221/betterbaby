<?php
return [
	'class'         => 'yii\rest\UrlRule',
	'prefix'        => '/',
	'pluralize'     => FALSE,
	'ruleConfig'    => [
		'class' => 'yii\web\UrlRule',
	],
	'controller'    => [
		'user' => '/user/user',
	],
	'extraPatterns' => [
		'OPTIONS update-user' => 'update-user',
		'PUT update-user'    => 'update-user',

		'OPTIONS get-data' => 'get-data',
		'GET get-data'     => 'get-data',
        
        'OPTIONS request-new-password' => 'request-new-password',
		'GET request-new-password'     => 'request-new-password',

        'OPTIONS new-user' => 'new-user',
        'POST new-user'    => 'new-user',
        
        'OPTIONS upload-avatar' => 'upload-avatar',
        'POST upload-avatar'     => 'upload-avatar',
        
        'OPTIONS update-user-client' => 'update-user-client',
        'PUT update-user-client'     => 'update-user-client',
        
        'OPTIONS delete-user-client' => 'delete-user-client',
        'PUT delete-user-client'     => 'delete-user-client',
	],
];
