<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'baby-week-data' => '/baby-week-data/baby-week-data',
    ],
    'extraPatterns' => [
        'OPTIONS list-size' => 'list-size',
        'GET list-size' => 'list-size',
        
        'OPTIONS list-week' => 'list-week',
        'GET list-week' => 'list-week',

    ],
];
