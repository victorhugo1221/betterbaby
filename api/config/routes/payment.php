<?php
return [
	'class'         => 'yii\rest\UrlRule',
	'prefix'        => '/',
	'pluralize'     => FALSE,
	'ruleConfig'    => [
		'class' => 'yii\web\UrlRule',
	],
	'controller'    => [
		'payment' => '/payment/payment',
	],
	'extraPatterns' => [
		'OPTIONS update' => 'update',
		'POST update'    => 'update',

		'OPTIONS cancel' => 'cancel',
		'PUT cancel'    => 'cancel',

		'OPTIONS subscribe' => 'subscribe',
		'POST subscribe'    => 'subscribe',

		'OPTIONS generate-session' => 'generate-session',
		'GET generate-session'     => 'generate-session',

		'OPTIONS verify' => 'verify',
		'GET verify'     => 'verify',

		'OPTIONS list' => 'list',
		'GET list'     => 'list',
	],
];
