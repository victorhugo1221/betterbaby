<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'baby-name' => '/baby-name/baby-name',
    ],
    'extraPatterns' => [
        'OPTIONS list-name' => 'list-name',
        'GET list-name' => 'list-name',

    ],
];
