<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'plan' => '/plan/plan',
    ],
    'extraPatterns' => [
        'OPTIONS get-plan-data' => 'get-plan-data',
        'GET get-plan-data' => 'get-plan-data',

        'OPTIONS get-all-plan-data' => 'get-all-plan-data',
        'GET get-all-plan-data' => 'get-all-plan-data',
    ],
];
