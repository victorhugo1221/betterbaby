<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'baby-timeline' => '/baby-timeline/baby-timeline',
    ],
    'extraPatterns' => [
        'OPTIONS list-data' => 'list-data',
        'GET list-data' => 'list-data',

    ],
];
