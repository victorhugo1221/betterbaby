<?php
return [
    'class' => 'yii\rest\UrlRule',
    'prefix' => '/',
    'pluralize' => FALSE,
    'ruleConfig' => [
        'class' => 'yii\web\UrlRule',
    ],
    'controller' => [
        'service-specialty' => '/service-specialty/service-specialty',
    ],
    'extraPatterns' => [
        'OPTIONS list-specialty' => 'list-specialty',
        'GET list-specialty' => 'list-specialty',

    ],
];
