<?php
return [
    'auth'              => [
        'class' => 'api\modules\auth\Module',
    ],
    'user'              => [
        'class' => 'api\modules\user\Module',
    ],
    'user-provider'     => [
        'class' => 'api\modules\userProvider\Module',
    ],
    'health-plan'       => [
        'class' => 'api\modules\healthPlan\Module',
    ],
    'plan'              => [
        'class' => 'api\modules\plan\Module',
    ],
    'specialty'         => [
        'class' => 'api\modules\specialty\Module',
    ],
    'service-specialty' => [
        'class' => 'api\modules\serviceSpecialty\Module',
    ],
    'help'              => [
        'class' => 'api\modules\help\Module',
    ],
    'email'             => [
        'class' => 'api\modules\email\Module',
    ],
    'dashboard'         => [
        'class' => 'api\modules\dashboard\Module',
    ],
    'solicitation'      => [
        'class' => 'api\modules\solicitation\Module',
    ],
    'baby-name'         => [
        'class' => 'api\modules\babyName\Module',
    ],
    'baby-week-data'    => [
        'class' => 'api\modules\babyWeekData\Module',
    ],
    'news'              => [
        'class' => 'api\modules\news\Module',
    ],
    'publicity'         => [
        'class' => 'api\modules\publicity\Module',
    ],
    'payment'           => [
        'class' => 'api\modules\payment\Module',
    ],
    'temp-address'      => [
        'class' => 'api\modules\tempAddress\Module',
    ],
    'baby-timeline'     => [
        'class' => 'api\modules\babyTimeline\Module',
    ],
];
