<?php

namespace api\modules\auth\controllers;

use api\components\utils\Auth;
use api\components\utils\BaseController;
use api\components\utils\HttpCode;
use common\modules\adminGw\logs\GrupoWLog;
use common\modules\user\models\LoginForm;
use common\modules\user\models\User;
use common\modules\user\models\UserClient;
use common\modules\userProvider\models\UserProvider;
use Exception;
use Yii;
use yii\web\UnauthorizedHttpException;
use common\modules\adminGw\services\AuthenticationService;

class AuthenticationController extends BaseController {
    use Auth;
    
    public $modelClass = '';
    
    /**
     * @SWG\Post(
     *    path = "/auth/generate-primary-token",
     *    tags = {"Auth"},
     *    operationId = "generatePrimaryToken",
     *    summary="Generates a valid token with no user data to be used on endpoints without authentication",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	@SWG\Parameter(in = "body", name = "body", required = true, type = "string", @SWG\Schema(type="object", @SWG\Property(property="apiKey", type="string", example="bm90IHZhbGlkIGFwaSBrZXk"))),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionGeneratePrimaryToken() : array {
        $response = [];
        try {
            $code = new UnauthorizedHttpException();
            $httpCode = $code->statusCode;
            $response = ['message' => $code->getName()];
            
            if (!isset($this->post['apiKey']) || (isset($this->post['apiKey']) && empty($this->post['apiKey']))) {
                $response = $this->response(['message' => "Incorrect syntax. The param 'apiKey' is empty."], HttpCode::BAD_REQUEST_400);
                GrupoWLog::sendMessageLogToSqs(Yii::$app->request, json_encode($response), GrupoWLog::TYPE_API, HttpCode::BAD_REQUEST_400);
                return $response;
            }
            
            if ($this->post['apiKey'] === $_ENV['JWT_API_KEY']) {
                $dataToken = ['id' => 0];
                
                $token = Auth::generateJWT($dataToken, NULL, NULL, TRUE);
                $refreshToken = Auth::refreshJWT($dataToken);
                
                if (!empty($token) && !empty($refreshToken)) {
                    unset($response['message']);
                    $httpCode = HttpCode::OK_200;
                    $response = $this->response(['token' => $token, 'refreshToken' => $refreshToken]);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        Yii::$app->response->setStatusCode($httpCode);
        return $response;
    }
    
    /**
     * @SWG\Get(
     *    path = "/auth/refresh-token",
     *    tags = {"Auth"},
     *    operationId = "refreshToken",
     *    summary="Send the old token to receive a new token.",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	  @SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    
    public function actionRefreshToken() : array {
        try {
            $token = str_replace('Bearer ', '', Yii::$app->request->getHeaders()['authorization']);
            $tokenDecrypt = self::decryptToken($token);
            
            $response = [];
            if ($tokenDecrypt->id > 0) {
                $user = User::find()->andWhere(['id' => $tokenDecrypt->id])->one();
                $token = Auth::generateJWT(['id' => $user->id], $user, ['id', 'name']);
                $refreshToken = Auth::refreshJWT(['id' => $user->id]);
            } else {
                $dataToken = ['id' => 0];
                $token = Auth::generateJWT($dataToken, NULL, NULL, TRUE);
                $refreshToken = Auth::refreshJWT($dataToken);
            }
            
            if (!empty($token) && !empty($refreshToken)) {
                unset($response['message']);
                $httpCode = HttpCode::OK_200;
            } else {
                $httpCode = HttpCode::INTERNAL_SERVER_ERROR_500;
            }
            
            $response = $this->response(['token' => $token, 'refreshToken' => $refreshToken]);
            GrupoWLog::sendMessageLogToSqs(Yii::$app->request, json_encode($response), GrupoWLog::TYPE_API, $httpCode);
        } catch (\Exception $e) {
            $httpCode = HttpCode::INTERNAL_SERVER_ERROR_500;
            GrupoWLog::sendMessageErrorLogToSqs(Yii::$app->request, $e, GrupoWLog::TYPE_API);
        }
        Yii::$app->response->setStatusCode($httpCode);
        return $response;
    }
    
    /**
     * @SWG\Post(
     *    path = "/auth/authentication",
     *    tags = {"Auth"},
     *    operationId = "generateToken",
     *    summary="Send the parameters to receive a token. This token is used on all other endpoints.",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *	@SWG\Parameter(in = "body",name = "body",required = true,type = "string",
     *       @SWG\Schema(type="object",
     *              @SWG\Property(property="email", type="string", example="Send an e-mail useremail@mail.com"),
     *              @SWG\Property(property="password", type="string", example="!CW@4d31bI"),
     *          )
     *    ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionAuthentication() : array {
        $code = new UnauthorizedHttpException();
        $httpCode = $code->statusCode;
        $response = ['message' => $code->getName()];
        try {
            $post = Yii::$app->request->post();
            $model = new LoginForm();
            $model->email = $post['email'];
            $model->password = $post['password'];
            if ($model->validate()) {
                
                $authenticationService = Yii::$app->getModule('admin-gw')->get('authenticationService');
                $user = $authenticationService->loginClient($model);
                if ($user) {
                    $token = Auth::generateJWT(['id' => $user->id], $user, ['id', 'name']);
                    $refreshToken = Auth::refreshJWT(['id' => $user->id]);
                    if (!empty($token) && !empty($refreshToken)) {
                        unset($response['message']);
                        $httpCode = HttpCode::OK_200;
                        $response = $this->response([
                            'token'        => $token,
                            'refreshToken' => $refreshToken,
                        ]);
                    }
                }
            } else {
                $httpCode = HttpCode::UNAUTHORIZED_401;
                $response = $this->response(['message' => "Usuário ou senha incorretos"]);
            }
            
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        Yii::$app->response->setStatusCode($httpCode);
        return $response;
    }
    
    
    
    
    
    /**
     * @SWG\Post(
     *    path = "/auth/authentication-professional",
     *    tags = {"Auth"},
     *    operationId = "generateToken",
     *    summary="Send the parameters to receive a token. This token is used on all other endpoints.",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *	@SWG\Parameter(in = "body",name = "body",required = true,type = "string",
     *       @SWG\Schema(type="object",
     *              @SWG\Property(property="email", type="string", example="Send an e-mail useremail@mail.com"),
     *              @SWG\Property(property="password", type="string", example="!CW@4d31bI"),
     *          )
     *    ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionAuthenticationProfessional() : array {
        $code = new UnauthorizedHttpException();
        $httpCode = $code->statusCode;
        $response = ['message' => $code->getName()];
        try {
            $post = Yii::$app->request->post();
            $model = new LoginForm();
            $model->email = $post['email'];
            $model->password = $post['password'];
            if ($model->validate()) {
                
                $authenticationService = Yii::$app->getModule('admin-gw')->get('authenticationService');
                $user = $authenticationService->loginAppProfessional($model);
                if ($user) {
                    $token = Auth::generateJWT(['id' => $user->id], $user, ['id', 'name']);
                    $refreshToken = Auth::refreshJWT(['id' => $user->id]);
                    if (!empty($token) && !empty($refreshToken)) {
                        unset($response['message']);
                        $httpCode = HttpCode::OK_200;
                        $response = $this->response([
                            'token'        => $token,
                            'refreshToken' => $refreshToken,
                        ]);
                    }
                }
            } else {
                $httpCode = HttpCode::UNAUTHORIZED_401;
                $response = $this->response(['message' => "Usuário ou senha incorretos"]);
            }
            
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        Yii::$app->response->setStatusCode($httpCode);
        return $response;
    }
    
    
    
    
    
}
