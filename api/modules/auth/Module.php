<?php

namespace api\modules\auth;

class Module extends \yii\base\Module
{
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    public $defaultRoute='default/index';
    
    public function init()
    {
        parent::init();
    }
}
