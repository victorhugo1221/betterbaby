<?php

namespace api\modules\healthPlan\controllers;

use api\components\utils\HttpCode;
use common\modules\plan\services\HealthPlanService;
use Yii;
use api\components\utils\BaseController;
use Exception;


class HealthPlanController extends BaseController {
    public $modelClass = '';
    private $service;
    
    /**
     * @SWG\Get(
     *  path = "/health-plan/list-plans",
     *  tags = {"HealthPlan"},
     *  operationId = "ListPlans",
     *  summary="plan data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListPlans() : array {
        try {
            $service = Yii::$app->getModule('health-plan')->get('healthPlanService');
            $user = $service->ListPlans($this->currentUser->id);
            
            if (!empty($user)) {
                $httpCode = HttpCode::OK_200;
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
            
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
}