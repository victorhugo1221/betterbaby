<?php

namespace api\modules\healthPlan;

class Module extends \common\modules\healthPlan\Module {
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    public $defaultRoute        = 'default/index';
    
    public function init() {
        parent::init();
    }
}
