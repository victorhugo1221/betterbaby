<?php

namespace api\modules\user;
use common\modules\user\models\User;

class Module extends \common\modules\user\Module
{
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    public $defaultRoute = 'user/index';
    
    public function init()
    {
        parent::init();
    }
}
