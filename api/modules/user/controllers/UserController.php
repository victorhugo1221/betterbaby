<?php

namespace api\modules\user\controllers;

use common\modules\address\models\Address;
use common\modules\adminGw\models\Token;
use common\modules\company\models\Company;
use common\modules\companyPlan\models\CompanyPlan;
use common\modules\group\models\Group;
use common\modules\groupUser\models\GroupUser;
use common\modules\user\models\User;
use api\components\utils\BaseController;
use api\components\utils\HttpCode;
use api\components\utils\Response;
use common\modules\adminGw\logs\GrupoWLog;
use common\modules\user\models\UserClient;
use common\modules\userProvider\models\UserProvider;
use common\modules\userProvider\services\UserProviderService;
use Exception;
use Ramsey\Uuid\Uuid;
use Yii;
use common\modules\user\services\UserService;


class UserController extends BaseController {
    public $modelClass = 'common\modules\user\models\User';
    private $userService;
    
    public function init() {
        $this->userService = Yii::$app->getModule('user')->get('userService');
        parent::init();
    }
    
    
    /**
     * @SWG\Get(
     *    path = "/user/get-data",
     *    tags = {"User"},
     *    operationId = "GetData",
     *    summary="Get user data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionGetData() : array {
        try {
            $service = Yii::$app->getModule('user')->get('userService');
            $user = $service->userGetData($this->currentUser->id);
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "não autorizado"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Put(
     *    path = "/user/update-user",
     *    tags = {"User"},
     *    operationId = "update",
     *    summary="Update user",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	  @SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *       @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="AYLA JULIANNA SARMENTO PEREIRA"),
     *              @SWG\Property(property="email", type="string", example="thiagodiasbelem@hotmail.com"),
     *              @SWG\Property(property="phone", type="string", example="(91) 992494985"),
     *              @SWG\Property(property="rg", type="string", example="0123456"),
     *              @SWG\Property(property="cpf", type="string", example="04168567214"),
     *              @SWG\Property(property="nationality", type="string", example="Brasileiro"),
     *              @SWG\Property(property="maritalStatusId", type="integer", example="1"),
     *              @SWG\Property(property="profession", type="string", example="programador"),
     *              @SWG\Property(property="ibgeCityCode", type="string", example="3929"),
     *              @SWG\Property(property="cityName", type="string", example="itajai"),
     *              @SWG\Property(property="stateInitials", type="string", example="4"),
     *              @SWG\Property(property="zipCode", type="string", example="88220000"),
     *              @SWG\Property(property="neighborhood", type="string", example="meia praia"),
     *              @SWG\Property(property="street", type="string", example="Rua 222"),
     *              @SWG\Property(property="complement", type="string", example="ap 402"),
     *              @SWG\Property(property="number", type="string", example="5042"),
     *        ),
     *     ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionUpdate() : array {
        if ($this->theTokenSentIsNotThePrimaryToken()) {
            try {
                if (empty($this->post['name'])) {
                    $response = $this->response(['message' => 'Incorrect sintaxe. The param "Name" is empty.'], HttpCode::BAD_REQUEST_400);
                    return $response;
                }
                
                /** @var UserService $service */
                $service = Yii::$app->getModule('user')->get('userService');
                /** @var User $user */
                $user = $service->findById($this->currentUser->id);
                
                if (empty($user)) {
                    $response = $this->response(['message' => 'Usuário não encontrado'], HttpCode::BAD_REQUEST_400);
                    return $response;
                }
                
                $db = Yii::$app->db;
                $transaction = $db->beginTransaction();
                $userClient = UserClient::find()->andWhere(['user_id' => $this->currentUser->id])->one();
                
                if (!empty($userClient) && !empty($userClient->user_id) && $userClient->user_id != $this->currentUser->id) {
                    $response = $this->response(['message' => 'This "CPF" was used by another user'], HttpCode::BAD_REQUEST_400);
                    return $response;
                }
                
                if ($user->name != $this->post['name']) {
                    $user->name = $this->post['name'];
                    $user->save();
                }
                
                if (!empty($this->post['ibgeCityCode']) && !empty($this->post['stateInitials'])) {
                    $cityService = Yii::$app->getModule('city')->get('cityService');
                    $stateService = Yii::$app->getModule('state')->get('stateService');
                    
                    $state = $stateService->getByInitials($this->post['stateInitials']);
                    $city = $cityService->getCityByCode($state->id, $this->post['ibgeCityCode'], $this->post['cityName']);
                }
                
                $userClient->cpf = preg_replace('/[^[:alnum:]]/', '', $this->post['cpf']);
                $userClient->phone = preg_replace('/[^[:alnum:]]/', '', $this->post['phone']);
                $userClient->rg = $this->post['rg'];
                $userClient->nationality = $this->post['nationality'];
                $userClient->profession = $this->post['profession'];
                $userClient->marital_status_id = $this->post['maritalStatusId'];
                $userClient->city_id = $city->id;
                $userClient->zip_code = $this->post['zipCode'];
                $userClient->neighborhood = $this->post['neighborhood'];
                $userClient->street = $this->post['street'];
                $userClient->complement = $this->post['complement'];
                $userClient->number = $this->post['number'];
                
                if (!$userClient->validate()) {
                    $erros = $userClient->errors;
                    foreach ($erros as $er) {
                        $response = $this->response(['message' => $er[0]], HttpCode::BAD_REQUEST_400);
                        return $response;
                    }
                }
                
                $userClient->save();
                $transaction->commit();
                
                $response = $this->response(['message' => "Usuário atualizado com sucesso."]);
            } catch (\Exception $e) {
                $transaction->rollBack();
                $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
            }
            return $response;
            
        } else {
            return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
        }
    }
    
    
    /**
     * @SWG\Post(
     *    path = "/user/new-user",
     *    tags = {"User"},
     *    operationId = "NewUser",
     *    summary="Create a new user ",
     *    produces = {"application/json"},
     *     consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *        @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="fulano de souza"),
     *              @SWG\Property(property="email", type="string", example="teste@edfy.com.br"),
     *              @SWG\Property(property="cellphone", type="string", example="479999999"),
     *              @SWG\Property(property="password", type="string", example="teste123"),
     *              @SWG\Property(property="babyName", type="string", example="pedro"),
     *              @SWG\Property(property="babyDate", type="string", example="02-03-2022"),
     *              @SWG\Property(property="referralCode", type="string", example="99999999"),
     *              @SWG\Property(property="city", type="string", example="são paulo"),
     *              @SWG\Property(property="state", type="string", example="SC"),
     *              @SWG\Property(property="street", type="string", example="rua 222"),
     *              @SWG\Property(property="number", type="string", example="123"),
     *              @SWG\Property(property="complement", type="string", example="ap 202"),
     *              @SWG\Property(property="district", type="string", example="centro"),
     *              @SWG\Property(property="zipCode", type="string", example="01001000"),
     *              @SWG\Property(property="pushToken", type="string", example="xxxxx"),
     *          ),
     *    ),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     * @throws Exception
     */
    public function actionNewUser() : array {
        
        $validadeMessage = $this->validadeReceivedParams([
            'name'      => $this->post['name'],
            'email'     => $this->post['email'],
            'cellphone' => $this->post['cellphone'],
            'password'  => $this->post['password'],
            'city'      => $this->post['city'],
            'state'     => $this->post['state'],
            'street'    => $this->post['street'],
            'number'    => $this->post['number'],
            'district'  => $this->post['district'],
            'zip_code'  => $this->post['zipCode'],
        ]);
        if (!empty($validadeMessage)) {
            $response = $this->response(['message' => 'Campo obrigatório não preenchido'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        /** @var UserService $service */
        $service = Yii::$app->getModule('user')->get('userService');
        /** @var User $UserProvider */
        
        // Validar unicidade E-mail
        if (!empty($this->post['email'])) {
            $user = User::find()->andWhere(['email' => $this->post['email']])->one();
            if (!empty($user)) {
                $response = $this->response(['message' => 'Este E-mail já está sendo utilizado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        $Nstreet = str_replace([' ', "\t", "\n"], '', $this->post['street']);
        $Ndistrict = str_replace([' ', "\t", "\n"], '', $this->post['district']);
        $Ncity = str_replace([' ', "\t", "\n"], '', $this->post['city']);
        $Nzipcode = str_replace([' ', "\t", "\n"], '', $this->post['zipCode']);
        $Nzipcode = substr($Nzipcode, 0, 5);
        
        $curl = curl_init();
        
        curl_setopt_array($curl, [
            CURLOPT_URL            => 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . $Nstreet . ',' . $Ndistrict . ',' . $Ncity . ',' . $Nzipcode . '.json?country=br&types=address%2Cpostcode&access_token=pk.eyJ1IjoiZWRmeWl0MjIiLCJhIjoiY2w4YnduamdoMDE0ZzN2bXdldzBnMml1NSJ9.iDgduwdNkY7SfJuja9zqpQ',
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'GET',
        ]);
        
        $response = curl_exec($curl);
        $jsonData = json_decode($response, TRUE);
        
        curl_close($curl);
        
        if (!empty($jsonData['features'][0]['center'][0])) {
            $longitude = $jsonData['features'][0]['center'][0];
            $latitude = $jsonData['features'][0]['center'][1];
        }
        
        $user = new User(
            [
                'name'       => $this->post['name'],
                'email'      => $this->post['email'],
                'cellphone'  => $this->post['cellphone'],
                'push_token' => $this->post['pushToken'],
                'password'   => Yii::$app->security->generatePasswordHash($this->post['password']),
                'status'     => 1,
            ]);
        if ($this->post['babyDate'] !== date('Y-m-d')) {
            $start_date = $this->post['babyDate'];
            $date = strtotime($start_date);
            $date = strtotime("+280 day", $date);
            $date = date('Y/m/d', $date);
        } else {
            $date = "0000-00-00";
        }
        
        $userClient = new UserClient(
            [
                'baby_date'     => $date,
                'referral_code' => $this->post['referralCode'],
                'status'        => 1,
            ]);
        $address = new Address(
            [
                'city'       => $this->post['city'],
                'state'      => $this->post['state'],
                'street'     => $this->post['street'],
                'number'     => $this->post['number'],
                'complement' => $this->post['complement'],
                'district'   => $this->post['district'],
                'zip_code'   => $this->post['zipCode'],
                'latitude'   => $latitude,
                'longitude'  => $longitude,
                'status'     => 1,
            ]);
        
        if (!$user->validate()) {
            $erros = $user->errors;
            foreach ($erros as $er) {
                $response = $this->response(['message' => $er[0]], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        if ($latitude == NULL && $longitude == NULL) {
            $response = $this->response(['message' => 'Não foi possivel encontrar o endereço'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        $transaction = Yii::$app->db->beginTransaction();
        $userId = $service->createUserApp($user);
        
        if ($userId > 0) {
            $clientId = $service->createUserClient($userClient, $userId);
            $addressId = $service->createAddress($address, $userId);
        }
        
        if ($userId && $addressId && $clientId) {
            $this->actionSendNewClientEmail($this->post['email'], $this->post['name']);
            if (!empty($this->post['referralCode'])) {
                $referralUser = User::find()->andWhere(['cellphone' => $this->post['referralCode']])->one();
                $this->actionReferralAlertEmail($referralUser->email, $this->post['name']);
            }
            $transaction->commit();
            $response = $this->response(['message' => 'Cadastrado realizado com sucesso!']);
            return $response;
            
        } else {
            $transaction->rollBack();
            throw new \Exception("Ocorreu um erro, por favor, contate o TI.");
        }
        
    }
    
    
    public function actionSendNewClientEmail($email, $name) {
        $template = 'layouts/newUserClient';
        try {
            $subject = 'Bem-vindo ao Better Baby';
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name' => $name,
                ]
            )
                ->setFrom([$_ENV['SENDER_EMAIL'] => $_ENV['SENDER_NAME']])
                ->setTo($email)
                ->setSubject($subject)
                ->send();
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch (Exception $e) {
            throw $e;
        }
        return TRUE;
    }
    
    
    public function actionReferralAlertEmail($email, $name) {
        $template = 'layouts/referralAlert';
        try {
            $subject = 'Nova indicação Better Baby!';
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name' => $name,
                ]
            )
                ->setFrom([$_ENV['SENDER_EMAIL'] => $_ENV['SENDER_NAME']])
                ->setTo($email)
                ->setSubject($subject)
                ->send();
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch (Exception $e) {
            throw $e;
        }
        return TRUE;
    }
    
    
    /**
     * @SWG\Get(
     *    path = "/user/request-new-password",
     *    tags = {"User"},
     *    operationId = "RequestNewPassword",
     *    summary="Forgot password route to get a link",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Parameter(name="email", in="query", type="string", description="user's email", required=true),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionRequestNewPassword() : array {
        try {
            /** @var UserService $service */
            $service = Yii::$app->getModule('user')->get('userService');
            $user = $service->findByEmail($this->get['email']);
            if (empty($user)) {
                $response = $this->response(['message' => 'E-mail não encontrado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
            
            $user->hash = Uuid::uuid1()->toString();
            $success = $user->save();
            
            if (!$success) {
                $response = $this->response(['message' => 'Erro ao gerar o HASH'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
            $createToken = new Token(
                [
                    'user_id'    => $user->id,
                    'uid'        => $user->hash,
                    'active'     => Token::RECOVER,
                    'type'       => 1,
                    'last_nonce' => 1,
                    'status'     => 1,
                ]);
            
            $tokenId = $service->createToken($createToken);
            
            $token = Token::find()->where(['user_id' => $user->id, 'active' => Token::RECOVER])->orderBy(['id' => SORT_DESC])->one();
            $success = $this->actionSendRecoveryPasswordEmail($this->get['email'], $user->name, $token->uid);
            
            if ($success) {
                $response = $this->response(['message' => 'E-mail enviado com sucesso']);
            } else {
                $response = $this->response(['message' => 'Ocorreu um erro ao enviar o e-mail'], HttpCode::BAD_REQUEST_400);
            }
            return $response;
        } catch (\Exception $e) {
            $response = $this->response(['message' => 'Ocorreu um erro ao enviar o e-mail'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
    }
    
    public function actionSendRecoveryPasswordEmail($email, $name, $token) {
        $template = 'layouts/recoverPassword';
        try {
            $subject = 'Better Baby - Esqueci minha senha';
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name'  => $name,
                    'token' => $token,
                ]
            )
                ->setFrom([$_ENV['SENDER_EMAIL'] => $_ENV['SENDER_NAME']])
                ->setTo($email)
                ->setSubject($subject)
                ->send();
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch (Exception $e) {
            throw $e;
        }
        return TRUE;
    }
    
    
    /**
     * @SWG\Post(
     *    path = "/user/upload-avatar",
     *    tags = {"User"},
     *    operationId = "uploadAvatar",
     *    summary="Upload an image to user avatar",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Parameter(name="avatar", type="file", format="binary", description="Avatar image", in="formData"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionUploadAvatar() {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $avatar = $this->post['avatar'] ?? NULL;
                
                if (!$avatar) {
                    return $this->response(['message' => 'Arquivo de imagem para o avatar não foi informado'], HttpCode::BAD_REQUEST_400);
                } else {
                    $nome_arquivo = 'avatar-' . $this->currentUser->id . '.' . rand(999, 99999) . ".jpg";
                    $avatar = str_replace("data:image/jpeg;base64", "", $avatar);
                    $avatar = str_replace("data:image/jpg;base64", "", $avatar);
                    $avatar = str_replace(" ", "+", $avatar);
                    $avatar = base64_decode($avatar);
                    
                    if (file_put_contents('../../backend/web/uploads/user-client/' . $nome_arquivo, $avatar)) {
                        /** @var UserService $service */
                        $service = Yii::$app->getModule('user')->get('userService');
                        /** @var User $user */
                        if ($service->updateAvatarUser($this->currentUser->id, $nome_arquivo)) {
                            return $this->response(['message' => 'Avatar atualizado com sucesso'], HttpCode::OK_200);
                        }
                    } else {
                        return $this->response(['message' => 'Ocorreu um erro ao atualizar o avatar'], HttpCode::INTERNAL_SERVER_ERROR_500);
                    }
                }
            } else {
                return $this->response(['message' => 'Unauthorized'], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::INTERNAL_SERVER_ERROR_500);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Put(
     *    path = "/user/update-user-client",
     *    tags = {"User"},
     *    operationId = "UpdateUserClient",
     *    summary="Update user",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	  @SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *       @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="fulano de souza"),
     *              @SWG\Property(property="email", type="string", example="teste@edfy.com.br"),
     *              @SWG\Property(property="cellphone", type="string", example="479999999"),
     *              @SWG\Property(property="pushToken", type="string", example="0000000000000"),
     *              @SWG\Property(property="city", type="string", example="são paulo"),
     *              @SWG\Property(property="state", type="string", example="SC"),
     *              @SWG\Property(property="street", type="string", example="rua 222"),
     *              @SWG\Property(property="number", type="string", example="123"),
     *              @SWG\Property(property="complement", type="string", example="ap 202"),
     *              @SWG\Property(property="district", type="string", example="centro"),
     *              @SWG\Property(property="zipCode", type="string", example="01001000"),
     *        ),
     *     ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionUpdateUserClient() : array {
        if ($this->theTokenSentIsNotThePrimaryToken()) {
            /** @var UserService $service */
            $service = Yii::$app->getModule('user')->get('userService');
            /** @var User $user */
            $user = $service->findById($this->currentUser->id);
            
            if (empty($user)) {
                $response = $this->response(['message' => 'Usuário não encontrado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
            
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            $user = User::find()->andWhere(['id' => $this->currentUser->id])->one();
            $oldUser = User::find()->andWhere(['id' => $this->currentUser->id])->one();
            $userClient = UserClient::find()->andWhere(['user_id' => $this->currentUser->id])->one();
            $userAddress = Address::find()->andWhere(['user_id' => $this->currentUser->id])->one();
            
            
            $curl = curl_init();
            
            curl_setopt_array($curl, [
                CURLOPT_URL            => 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . $this->post['city'] . ',' . $this->post['state'] . '.json?types=address&access_token=pk.eyJ1IjoiZWRmeWl0MjIiLCJhIjoiY2w4YnduamdoMDE0ZzN2bXdldzBnMml1NSJ9.iDgduwdNkY7SfJuja9zqpQ',
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_ENCODING       => '',
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 0,
                CURLOPT_FOLLOWLOCATION => TRUE,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => 'GET',
            ]);
            
            $response = curl_exec($curl);
            $jsonData = json_decode($response, TRUE);
            
            curl_close($curl);
            
            if (!empty($jsonData['features'][0]['center'][0])) {
                $userAddress->longitude = $jsonData['features'][0]['center'][0];
                $userAddress->latitude = $jsonData['features'][0]['center'][1];
            }
            
            if (!empty($this->post['name'])) {
                $user->name = $this->post['name'];
            }
            if (!empty($this->post['email'])) {
                $user->email = $this->post['email'];
            }
            if (!empty($this->post['cellphone'])) {
                $user->cellphone = $this->post['cellphone'];
            }
            if (!empty($this->post['pushToken'])) {
                $user->push_token = $this->post['pushToken'];
            }
            //address//
            if (!empty($this->post['city'])) {
                $userAddress->city = $this->post['city'];
            }
            if (!empty($this->post['state'])) {
                $userAddress->state = $this->post['state'];
            }
            if (!empty($this->post['street'])) {
                $userAddress->street = $this->post['street'];
            }
            if (!empty($this->post['number'])) {
                $userAddress->number = $this->post['number'];
            }
            if (!empty($this->post['complement'])) {
                $userAddress->complement = $this->post['complement'];
            }
            if (!empty($this->post['district'])) {
                $userAddress->district = $this->post['district'];
            }
            if (!empty($this->post['zipCode'])) {
                if (strlen($this->post['zipCode']) == 7) {
                    $userAddress->zip_code = sprintf("%'.08d\n", $this->post['zipCode']);
                } else {
                    $userAddress->zip_code = $this->post['zipCode'];
                }
            }
            
            $user->save();
            $userClient->save();
            $userAddress->save();
            
            $transaction->commit();
            
            $response = $this->response(['message' => "Usuário atualizado com sucesso."]);
            
            return $response;
            
        } else {
            return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
        }
    }
    
    /**
     * @SWG\Put(
     *    path = "/user/delete-user-client",
     *    tags = {"User"},
     *    operationId = "deleteUserClient",
     *    summary="delete user",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	  @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Parameter(name="userId", in="query", type="integer", description="user's id", required=false),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionDeleteUserClient() : array {
        if ($this->theTokenSentIsNotThePrimaryToken()) {
            try {
                
                /** @var UserService $service */
                $service = Yii::$app->getModule('user')->get('userService');
                /** @var User $user */
                $user = $service->deleteClient($this->currentUser->id);
                
                if (empty($user)) {
                    $response = $this->response(['message' => 'Ocorreu um erro ao deletar o Usuário'], HttpCode::BAD_REQUEST_400);
                    return $response;
                }
                $response = $this->response(['message' => "Usuário deletado com sucesso."]);
            } catch (\Exception $e) {
                $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
            }
            return $response;
            
        } else {
            return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
        }
    }
    
    
}









