<?php

namespace api\modules\dashboard\controllers;

use common\modules\proposal\models\Proposal;
use api\components\utils\BaseController;
use api\components\utils\HttpCode;
use common\modules\adminGw\logs\GrupoWLog;
use common\modules\solicitation\models\Solicitation;
use Exception;
use Yii;
use common\modules\adminGw\services\AuthenticationService;

class DashboardController extends BaseController {
    public $modelClass = '';
    
   
    
    /**
     * @SWG\Get(
     *    path = "/dashboard/get-data",
     *    tags = {"Dashboard"},
     *  operationId = "getData",
     *  summary="get all data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/final_bearer"),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionGetData(): array{
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
    
                $solicitation = Solicitation:: find()->andWhere(['user_provider' => $this->currentUser->id])->all();
    
                if (!empty($solicitation)) {
                    $response = $solicitation;
                    return $this->response($response);
                } else {
                    return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
}
