<?php

namespace api\modules\babyWeekData;

class Module extends \common\modules\babyWeekData\Module {
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    public $defaultRoute        = 'default/index';
    
    public function init() {
        parent::init();
    }
}
