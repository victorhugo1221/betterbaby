<?php

namespace api\modules\babyWeekData\controllers;

use api\components\utils\HttpCode;
use common\modules\plan\services\HealthPlanService;
use Yii;
use api\components\utils\BaseController;
use Exception;


class BabyWeekDataController extends BaseController {
    public $modelClass = '';
    private $service;
    
    /**
     * @SWG\Get(
     *  path = "/baby-week-data/list-size",
     *  tags = {"BabyWeekData"},
     *  operationId = "ListSize",
     *  summary="name data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListSize() : array {
        try {
            $service = Yii::$app->getModule('baby-week-data')->get('babyWeekDataService');
            $user = $service->ListSize($this->currentUser->id);
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    
    
    /**
     * @SWG\Get(
     *  path = "/baby-week-data/list-week",
     *  tags = {"BabyWeekData"},
     *  operationId = "ListSize",
     *  summary="name data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListWeek() : array {
        try {
            $service = Yii::$app->getModule('baby-week-data')->get('babyWeekDataService');
            $user = $service->ListWeek($this->currentUser->id);
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
}