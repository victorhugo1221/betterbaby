<?php

namespace api\modules\solicitation\controllers;

use api\components\utils\HttpCode;
use common\modules\solicitation\models\Solicitation;
use common\modules\solicitation\services\SolicitationService;
use common\modules\user\models\User;
use common\modules\userProvider\models\UserProvider;
use Yii;
use api\components\utils\BaseController;
use common\modules\adminGw\logs\GrupoWLog;
use Exception;
use Ramsey\Uuid\Uuid;
use yii\helpers\ArrayHelper;
use yii\web\UnauthorizedHttpException;
use ExpoSDK\Expo;
use ExpoSDK\ExpoMessage;

class SolicitationController extends BaseController {
    public $modelClass = '';
    private $service;
    
    
    /**
     * @SWG\Get(
     *    path = "/solicitation/get-data",
     *    tags = {"Solicitation"},
     *    operationId = "GetData",
     *    summary="Get data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Parameter(name="searchName", in="query", type="string", description="search by name", required=false),
     *    @SWG\Parameter(name="status", in="query", type="string", description="status", required=false),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionGetData() : array {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $httpCode = HttpCode::UNAUTHORIZED_401;
                $service = Yii::$app->getModule('solicitation')->get('solicitationService');
                $user = $service->ListSolicitationByUserProvider($this->currentUser->id, $this->get['searchName'], $this->get['status']);
                
                if (!empty($user)) {
                    $response = $user;
                    return $this->response($response);
                } else {
                    return $this->response(['message' => "Nenhum dado encontrado"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Get(
     *    path = "/solicitation/list-data-by-client",
     *    tags = {"Solicitation"},
     *    operationId = "ListDataByClient",
     *    summary="List data by user client",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListDataByClient() : array {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $httpCode = HttpCode::UNAUTHORIZED_401;
                $service = Yii::$app->getModule('solicitation')->get('solicitationService');
                $user = $service->ListSolicitationByUserClient($this->currentUser->id);
                
                if (!empty($user)) {
                    $response = $user;
                    return $this->response($response);
                } else {
                    return $this->response(['message' => "Nenhum dado encontrado"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Get(
     *    path = "/solicitation/get-details",
     *    tags = {"Solicitation"},
     *    operationId = "GetDetails",
     *    summary="Get solicitation details",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Parameter(name="solicitationId", in="query", type="integer", description="solicitation's id", required=true),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionGetDetails() : array {
        try {
            $service = Yii::$app->getModule('solicitation')->get('solicitationService');
            $user = $service->ListDetailsSolicitation($this->get['solicitationId']);
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Nenhum dado encontrado"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Get(
     *    path = "/solicitation/list-rating",
     *    tags = {"Solicitation"},
     *    operationId = "ListRating",
     *    summary="Get data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListRating() : array {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $httpCode = HttpCode::UNAUTHORIZED_401;
                $service = Yii::$app->getModule('solicitation')->get('solicitationService');
                $user = $service->ListRatingByUser($this->currentUser->id);
                
                if (!empty($user)) {
                    $response = $user;
                    return $this->response($response);
                } else {
                    return $this->response(['message' => "Nenhum dado encontrado"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    /**
     * @SWG\Get(
     *    path = "/solicitation/list-rating-client",
     *    tags = {"Solicitation"},
     *    operationId = "ListRatingClient",
     *    summary="Get data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListRatingClient() : array {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $httpCode = HttpCode::UNAUTHORIZED_401;
                $service = Yii::$app->getModule('solicitation')->get('solicitationService');
                $user = $service->ListRatingByClient($this->currentUser->id);
                
                if (!empty($user)) {
                    $response = $user;
                    return $this->response($response);
                } else {
                    return $this->response(['message' => "Nenhum dado encontrado"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    /**
     * @SWG\Get(
     *    path = "/solicitation/list-solicitation-pending",
     *    tags = {"Solicitation"},
     *    operationId = "ListSolicitationPending",
     *    summary="Get data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListSolicitationPending() : array {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $httpCode = HttpCode::UNAUTHORIZED_401;
                $service = Yii::$app->getModule('solicitation')->get('solicitationService');
                $user = $service->ListSolicitationPending($this->currentUser->id);
                
                if (!empty($user)) {
                    $response = $user;
                    return $this->response($response);
                } else {
                    return $this->response(['message' => "Nenhum dado encontrado"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Get(
     *    path = "/solicitation/list-schedule",
     *    tags = {"Solicitation"},
     *    operationId = "ListSchedule",
     *    summary="Get data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListSchedule() : array {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $httpCode = HttpCode::UNAUTHORIZED_401;
                $service = Yii::$app->getModule('solicitation')->get('solicitationService');
                $user = $service->ListSchedule($this->currentUser->id);
                
                if (!empty($user)) {
                    $response = $user;
                    return $response;
                } else {
                    return $this->response(['message' => "Nenhum dado encontrado"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    /**
     * @SWG\Get(
     *    path = "/solicitation/dashboard-revenue",
     *    tags = {"Solicitation"},
     *    operationId = "DashboardRevenue",
     *    summary="Dashboard Revenue",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionDashboardRevenue() : array {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $service = Yii::$app->getModule('solicitation')->get('solicitationService');
                $data = $service->DashboardRevenueDataNew($this->currentUser->id);
                
                if (!empty($data)) {
                    $response = $data;
                    return $response;
                } else {
                    return $this->response(['message' => "Nenhum dado encontrado"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Get(
     *    path = "/solicitation/dashboard-solicitation",
     *    tags = {"Solicitation"},
     *    operationId = "DashboardSolicitation",
     *    summary="Dashboard Solicitation",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionDashboardSolicitation() : array {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $service = Yii::$app->getModule('solicitation')->get('solicitationService');
                $data = $service->DashboardSolicitationDataNew($this->currentUser->id);
                
                if (!empty($data)) {
                    $response = $data;
                    return $response;
                } else {
                    return $this->response(['message' => "Nenhum dado encontrado"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Put(
     *   path="/solicitation/cancel-solicitation",
     *   tags={"Solicitation"},
     *   operationId="CancelSolicitation",
     *   summary="Cancel Solicitation",
     *   @SWG\Parameter(ref="#/parameters/primary_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *       @SWG\Schema(
     *            type="object",
     *            @SWG\Property(property="solicitationId", type="string", example="solicitation Id"),
     *            @SWG\Property(property="cancellationReason", type="string", example="muito tarde"),
     *         ),
     *    ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionCancelSolicitation() : array {
        try {
            $service = Yii::$app->getModule('solicitation')->get('solicitationService');
            $response = $service->cancelSolicitation($this->post['solicitationId'], $this->post['cancellationReason']);
            
            
            $solicitation = Solicitation::find()->andWhere(['id' => $this->post['solicitationId']])->one();
            $user = User::find()->andWhere(['id' => $solicitation->user_client])->one();
            
            if ($response) {
                $response = $this->response(['message' => 'Atendimento cancelado com sucesso.']);
                $success = $this->actionSendCancelEmail($user->email, $user->name, $solicitation->userProviderRel->name, $solicitation->cancellation_reason, $solicitation->scheduled_date);
            } else {
                $response = $this->response(['message' => 'Ocorreu um erro ao cancelar o atendimento'], HttpCode::BAD_REQUEST_400);
            }
        } catch (\Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
        
    }
    
    
    public function actionSendCancelEmail($email, $name, $providerName, $cancellationReason, $date) {
        $template = 'layouts/cancelSolicitation';
        $date = date_create($date);
        try {
            $subject = 'Atendimento cancelado - Better Baby';
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name'               => $name,
                    'providerName'       => $providerName,
                    'cancellationReason' => $cancellationReason,
                    'date'               => date_format($date, "d/m/Y H:i"),
                ]
            )
                ->setFrom([$_ENV['SENDER_EMAIL'] => $_ENV['SENDER_NAME']])
                ->setTo($email)
                ->setSubject($subject)
                ->send();
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch (Exception $e) {
            throw $e;
        }
        return TRUE;
    }
    
    
    /**
     * @SWG\Put(
     *   path="/solicitation/confirm-solicitation",
     *   tags={"Solicitation"},
     *   operationId="ConfirmSolicitation",
     *   summary="Confirm Solicitation",
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *       @SWG\Schema(
     *            type="object",
     *            @SWG\Property(property="solicitationId", type="string", example="solicitation Id"),
     *         ),
     *    ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionConfirmSolicitation() : array {
        try {
            $service = Yii::$app->getModule('solicitation')->get('solicitationService');
            $response = $service->activeSolicitation($this->post['solicitationId']);
            
            $solicitation = Solicitation::find()->andWhere(['id' => $this->post['solicitationId']])->one();
            $user = User::find()->andWhere(['id' => $solicitation->user_client])->one();
            
            if ($response) {
                $response = $this->response(['message' => 'Atendimento confirmado com sucesso.']);
                if ($solicitation->online_service == 1) {
                    $online = 'Online';
                } else {
                    $online = 'Presencial';
                }
                
                $success = $this->actionSendConfirmEmail($user->email, $user->name, $solicitation->userProviderRel->name, $solicitation->userProviderRel->providerRel->guidelines, $online, $solicitation->scheduled_date);
            } else {
                $response = $this->response(['message' => 'Ocorreu um erro ao confirmar o atendimento'], HttpCode::BAD_REQUEST_400);
            }
            
        } catch (\Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    public function actionSendConfirmEmail($email, $name, $providerName, $guidelines, $type, $date) {
        $template = 'layouts/confirmSolicitation';
        $date = date_create($date);
        try {
            $subject = 'Atendimento confirmado - Better Baby';
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name'         => $name,
                    'providerName' => $providerName,
                    'guidelines'   => $guidelines,
                    'type'         => $type,
                    'date'         => date_format($date, "d/m/Y H:i"),
                ]
            )
                ->setFrom([$_ENV['SENDER_EMAIL'] => $_ENV['SENDER_NAME']])
                ->setTo($email)
                ->setSubject($subject)
                ->send();
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch
        (Exception $e) {
            throw $e;
        }
        return TRUE;
    }
    
    
    /**
     * @SWG\Put(
     *   path="/solicitation/expire-solicitation",
     *   tags={"Solicitation"},
     *   operationId="ExpireSolicitation",
     *   summary="expire Solicitation",
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *       @SWG\Schema(
     *            type="object",
     *            @SWG\Property(property="solicitationId", type="string", example="solicitation Id"),
     *         ),
     *    ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionExpireSolicitation() : array {
        try {
            $service = Yii::$app->getModule('solicitation')->get('solicitationService');
            $response = $service->expireSolicitation($this->post['solicitationId']);
            
            $solicitation = Solicitation::find()->andWhere(['id' => $this->post['solicitationId']])->one();
            $user = User::find()->andWhere(['id' => $solicitation->user_client])->one();
            
            if ($response) {
                $response = $this->response(['message' => 'Atendimento expirado com sucesso.']);
                $success = $this->actionSendExpireEmail($user->email, $user->name, $solicitation->userProviderRel->name, $solicitation->cancellation_reason, $solicitation->scheduled_date);
            } else {
                $response = $this->response(['message' => 'Ocorreu um erro ao confirmar o atendimento'], HttpCode::BAD_REQUEST_400);
            }
            
        } catch (\Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o TI.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    public function actionSendExpireEmail($email, $name, $providerName, $cancellationReason, $date) {
        $template = 'layouts/cancelSolicitation';
        $date = date_create($date);
        try {
            $subject = 'Atendimento cancelado - Better Baby';
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name'               => $name,
                    'providerName'       => $providerName,
                    'cancellationReason' => $cancellationReason,
                    'date'               => date_format($date, "d/m/Y H:i"),
                ]
            )
                ->setFrom([$_ENV['SENDER_EMAIL'] => $_ENV['SENDER_NAME']])
                ->setTo($email)
                ->setSubject($subject)
                ->send();
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch (Exception $e) {
            throw $e;
        }
        return TRUE;
    }
    
    
    /**
     * @SWG\Put(
     *   path="/solicitation/update-data",
     *   tags={"Solicitation"},
     *   operationId="UpdateData",
     *   summary="update data",
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *       @SWG\Schema(
     *            type="object",
     *            @SWG\Property(property="solicitationId", type="string", example="solicitation Id"),
     *            @SWG\Property(property="callUrl", type="string", example="call url"),
     *            @SWG\Property(property="ratingText", type="string", example="muito bom"),
     *            @SWG\Property(property="ratingStars", type="string", example="4"),
     *         ),
     *    ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionUpdateData() : array {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        $solicitation = Solicitation::find()->andWhere(['id' => $this->post['solicitationId']])->one();
        
        if (!empty($this->post['callUrl'])) {
            if (strpos($this->post['callUrl'], 'https://') !== 0) {
                $this->post['callUrl'] = 'https://' . $this->post['callUrl'];
            }
            $solicitation->call_url = $this->post['callUrl'];
        }
        if (!empty($this->post['ratingText'])) {
            $solicitation->rating_text = $this->post['ratingText'];
        }
        if ($this->post['ratingStars'] !== NULL) {
            $solicitation->rating_stars = $this->post['ratingStars'];
        }
        
        
        $solicitation->save();
        $transaction->commit();
        
        $response = $this->response(['message' => 'Atendimento atualizado com sucesso.']);
        
        if ($this->post['ratingStars'] !== '' && $this->post['ratingStars'] !== NULL) {
            $transaction = $db->beginTransaction();
            $providerId = $solicitation->user_provider;
            
            $allSolitation = Solicitation::find()->andWhere(['user_provider' => $providerId])->andWhere(['>=', 'rating_stars', 0.0])->count();
            $sumSolicitation = Solicitation::find()->andWhere(['user_provider' => $providerId])->andWhere(['>=', 'rating_stars', 0.0])->sum('rating_stars');
            
            $average = ($sumSolicitation / $allSolitation);
            
            $userProvider = UserProvider::find()->andWhere(['user_id' => $providerId])->one();
            $userProvider->average_rating = strval($average);
            
            $userProvider->save();
            $transaction->commit();
        }
        
        return $response;
        
    }
    
    
    /**
     * @SWG\Post(
     *    path = "/solicitation/new-solicitation",
     *    tags = {"Solicitation"},
     *    operationId = "NewSolicitationr",
     *    summary="Create a new Solicitation",
     *    produces = {"application/json"},
     *     consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *        @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="userProviderId", type="string", example="4"),
     *              @SWG\Property(property="scheduledDate", type="string", example="2022-06-10 10:30:00"),
     *              @SWG\Property(property="onlineService", type="string", example="1"),
     *              @SWG\Property(property="note", type="string", example="teste123"),
     *          ),
     *    ),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     * @throws Exception
     */
    public function actionNewSolicitation() : array {
        
        $validadeMessage = $this->validadeReceivedParams([
            'user_provider'  => $this->post['userProviderId'],
            'scheduled_date' => $this->post['scheduledDate'],
            'online_service' => $this->post['onlineService'],
        ]);
        if (!empty($validadeMessage)) {
            $response = $this->response(['message' => $validadeMessage], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        /** @var SolicitationService $service */
        $service = Yii::$app->getModule('solicitation')->get('solicitationService');
        /** @var Solicitation $Solicitation */
        $userClient = User::find()->andWhere(['id' => $this->currentUser->id])->one();
        $userProvider = User::find()->andWhere(['id' => $this->post['userProviderId']])->one();
        
        $timestamp = strtotime($this->post['scheduledDate']) + 60 * 60;
        $time = date('Y-m-d H:i:s', $timestamp);
        
        $solicitation = new Solicitation(
            [
                'user_client'        => $this->currentUser->id,
                'user_provider'      => $this->post['userProviderId'],
                'scheduled_date'     => $this->post['scheduledDate'],
                'scheduled_date_end' => $time,
                'online_service'     => $this->post['onlineService'],
                'note'               => $this->post['note'],
                'price'              => $userProvider->providerRel->price_range_health_plan,
            ]);
        
        $transaction = Yii::$app->db->beginTransaction();
        $solicitationId = $service->createSolicitationApp($solicitation);
        
        
        if ($solicitationId > 0) {
            $transaction->commit();
            $response = $this->response(['message' => 'Atendimento criado com sucesso!']);
            // send push notification //
            if ($userProvider->push_token != NULL) {
                $messages = [
                    new ExpoMessage([
                        'title' => 'Novo Atendimento!',
                        'body'  => $userClient->name . ' agendou para ' . date('d/m/Y H:i', strtotime($this->post['scheduledDate'])) . ' clique para confirmar ou cancelar.',
                    ]),
                ];
                
                $defaultRecipients = [
                    $userProvider->push_token,
                ];
                
                (new Expo)->send($messages)->to($defaultRecipients)->push();
                //end push//
            }
            return $response;
        } else {
            $transaction->rollBack();
            throw new \Exception("Ocorreu um erro, por favor, contate o TI.");
        }
        
    }
    
}