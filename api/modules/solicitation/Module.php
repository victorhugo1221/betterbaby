<?php

namespace api\modules\solicitation;

class Module extends \common\modules\solicitation\Module {
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    public $defaultRoute        = 'default/index';
    
    public function init() {
        parent::init();
    }
}
