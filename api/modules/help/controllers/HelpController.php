<?php

namespace api\modules\help\controllers;

use api\components\utils\HttpCode;
use common\modules\plan\services\HealthPlanService;
use Yii;
use api\components\utils\BaseController;
use Exception;


class HelpController extends BaseController {
    public $modelClass = '';
    private $service;
    
    /**
     * @SWG\Get(
     *  path = "/help/list-help",
     *  tags = {"Help"},
     *  operationId = "ListHelp",
     *  summary="help data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListHelp() : array {
        try {
            $service = Yii::$app->getModule('help')->get('helpService');
            $user = $service->ListHelp($this->currentUser->id);
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    /**
     * @SWG\Get(
     *  path = "/help/list-help-client",
     *  tags = {"Help"},
     *  operationId = "ListHelpClient",
     *  summary="help data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListHelpClient() : array {
        try {
            $service = Yii::$app->getModule('help')->get('helpService');
            $user = $service->ListHelpClient($this->currentUser->id);
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    
    
    /**
     * @SWG\Get(
     *  path = "
     *
     *     ",
     *  tags = {"Help"},
     *  operationId = "ListHelpClient",
     *  summary="help data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListHelpWebsite() : array {
        try {
            $service = Yii::$app->getModule('help')->get('helpService');
            $user = $service->ListHelpWebSite($this->currentUser->id);
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
}