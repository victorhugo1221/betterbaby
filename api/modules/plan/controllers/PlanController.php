<?php

namespace api\modules\plan\controllers;

use api\components\utils\HttpCode;
use common\modules\plan\models\Plan;
use common\modules\plan\services\HealthPlanService;
use Yii;
use api\components\utils\BaseController;


class PlanController extends BaseController {
	public  $modelClass = '';
	private $service;

	/**
	 * @SWG\Get(
	 *  path = "/plan/get-plan-data",
	 *  tags = {"Plan"},
	 *  operationId = "plandata",
	 *  summary="plan data",
	 *  produces = {"application/json"},
	 *  consumes = {"application/json"},
	 *  @SWG\Property(property="data", type="string", example="21/11/2021"),
	 *  @SWG\Parameter(ref="#/parameters/final_bearer"),
	 *  @SWG\Parameter(name="id",in="query",type="string",required=true),
	 *
	 *  @SWG\Response(response = 200, description = "Success"),
	 *  @SWG\Response(response = 400, description = "Bad Request"),
	 *  @SWG\Response(response = 401, description = "Unauthorized"),
	 *  @SWG\Response(response = 500, description = "Internal Server Error")
	 *)
	 */
	public function actionGetPlanData(): array {
		if ($this->theTokenSentIsNotThePrimaryToken()) {
			$validateMessage = $this->validadeReceivedParams([$this->get['id']]);
			if (!empty($validateMessage)) {
				return $this->response(['message' => $validateMessage], HttpCode::BAD_REQUEST_400);
			}

			/** @var SolicitationService $service */
			$this->service = Yii::$app->getModule('plan')->get('planService');
			/** @var Plan $plan */
			$plan             = $this->service->findById((int)$this->get['id']);
			$return           = ['result' => 1, 'data' => []];
			$return['data'][] = ['id'                                => $plan['id'],
			                     'name'                              => $plan['name'],
			                     'price'                             => $plan['price'],
			                     'maximumNumberOfUsers'              => $plan['maximum_number_of_users'],
			                     'maximumNumberOfProposalPerMonth'   => $plan['maximum_number_of_proposal_per_month'],
			                     'maximumNumberOfMyProposalHelpsYou' => $plan['maximum_number_of_my_proposal_helps_you'],
			                     'allowFipe'                         => $plan['allow_fipe'],
			                     'allowsCustomizeLogoAndHeader'      => $plan['allows_customize_logo_and_header'],
			                     'enablesLegalValidityInTheProposal' => $plan['enables_legal_validity_in_the_proposal'],
			                     'enableDashboard'                   => $plan['enable_dashboard'],
			                     'enablesPreContracts'               => $plan['enables_pre_contracts']];
			return $this->response($return);
		} else {
			return $this->response(['message' => 'This endpoint require a final token'], HttpCode::UNAUTHORIZED_401);
		}
	}

	/**
	 * @SWG\Get(
	 *  path = "/plan/get-all-plan-data",
	 *  tags = {"Plan"},
	 *  operationId = "allplandata",
	 *  summary="get all plan data",
	 *  produces = {"application/json"},
	 *  consumes = {"application/json"},
	 *  @SWG\Property(property="data", type="string", example="21/11/2021"),
	 *  @SWG\Parameter(ref="#/parameters/primary_bearer"),
	 *
	 *  @SWG\Response(response = 200, description = "Success"),
	 *  @SWG\Response(response = 400, description = "Bad Request"),
	 *  @SWG\Response(response = 401, description = "Unauthorized"),
	 *  @SWG\Response(response = 500, description = "Internal Server Error")
	 *)
	 */
	public function actionGetAllPlanData(): array {
		$response = [];
		try {
			$this->service = Yii::$app->getModule('plan')->get('planService');
			$data          = $this->service->findAll();
			$return        = ['result' => 1, 'data' => []];
			foreach ($data as $plan) {
				if ($plan['id']==5) {
					continue;
				}
				$return['data'][] = ['id'                                => $plan['id'],
				                     'name'                              => $plan['name'],
				                     'price'                             => $plan['price'],
				                     'maximumNumberOfUsers'              => $plan['maximum_number_of_users'],
				                     'maximumNumberOfProposalPerMonth'   => $plan['maximum_number_of_proposal_per_month'],
				                     'maximumNumberOfMyProposalHelpsYou' => $plan['maximum_number_of_my_proposal_helps_you'],
				                     'allowFipe'                         => $plan['allow_fipe'],
				                     'allowsCustomizeLogoAndHeader'      => $plan['allows_customize_logo_and_header'],
				                     'enablesLegalValidityInTheProposal' => $plan['enables_legal_validity_in_the_proposal'],
				                     'enableDashboard'                   => $plan['enable_dashboard'],
				                     'enablesPreContracts'               => $plan['enables_pre_contracts'],
				                     'code'                              => $plan['plan_code'],
				                     'code_5'                            => $plan['discount_code_5'],
				                     'code_10'                           => $plan['discount_code_10'],
				                     'code_15'                           => $plan['discount_code_15'],
				];
			}
			//return $return;
			$response = $this->response($return);
		} catch (\Exception $e) {

		}
		return $response;
	}


	/**
	 * @SWG\Get(
	 *  path = "/plan/get-user-plan-data",
	 *  tags = {"Plan"},
	 *  operationId = "userplandata",
	 *  summary="user plan data",
	 *  produces = {"application/json"},
	 *  consumes = {"application/json"},
	 *  @SWG\Property(property="data", type="string", example="21/11/2021"),
	 *  @SWG\Parameter(ref="#/parameters/primary_bearer"),
	 *
	 *  @SWG\Response(response = 200, description = "Success"),
	 *  @SWG\Response(response = 400, description = "Bad Request"),
	 *  @SWG\Response(response = 401, description = "Unauthorized"),
	 *  @SWG\Response(response = 500, description = "Internal Server Error")
	 *)
	 */
	public function actionGetUserPlanData(): array {
		if (empty($this->get['id'])) {
			$httpCode            = HttpCode::BAD_REQUEST_400;
			$response['message'] = 'id do plano não informado';
			Yii::$app->response->setStatusCode($httpCode);
			return $response;
		}

		if ($this->get ['id'] > 0) {
			/** @var SolicitationService $service */
			$service = Yii::$app->getModule('plan')->get('planService');
			/** @var Plan $plan */
			$plan = $service->findById($this->get->id);


			if (!empty($plan)) {
				$httpCode = HttpCode::OK_200;
				$response = [
					'name'  => $plan->name,
					'price' => $plan->group_name,

				];
			}
		} else {
			$httpCode = HttpCode::UNAUTHORIZED_401;
		}

		Yii::$app->response->setStatusCode($httpCode);
		if (empty($response['message'])) {
			$response['message'] = $httpCode == HttpCode::OK_200 ? 'OK' : 'Fail';
		}
		return $response;
	}


}