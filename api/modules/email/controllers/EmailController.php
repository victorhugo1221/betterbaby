<?php

namespace api\modules\email\controllers;

use api\components\utils\BaseController;
use api\components\utils\HttpCode;
use common\modules\solicitation\models\Solicitation;
use common\modules\user\models\User;
use common\modules\user\services\UserService;
use Exception;
use Yii;
use common\modules\adminGw\services\AuthenticationService;

class EmailController extends BaseController {
    public $modelClass = '';
    
    
    /**
     * @SWG\Post(
     *  path = "/email/send-contact-form",
     *  tags = {"Email"},
     *  operationId = "sendContactForm",
     *  summary="send Contact Form",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/final_bearer"),
     *   @SWG\Parameter(in="body",name="body",required=true,type="string",
     *        @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="interestArea", type="string", example="1"),
     *              @SWG\Property(property="text", type="string", example="bla bla bla"),
     *          ),
     *    ),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionSendContactForm() : array {
        try {
            /** @var UserService $service */
            $service = Yii::$app->getModule('user')->get('userService');
            /** @var User $user */
            $user = $service->findById($this->currentUser->id);
            
            $email = "";
            if ($this->post['interestArea'] == 1) {
                $email = "financeiro@betterbaby.com.br";
            }
            if ($this->post['interestArea'] == 2) {
                $email = "marketing@betterbaby.com.br";
            }
            if ($this->post['interestArea'] == 3) {
                $email = "suporte@betterbaby.com.br";
            }
            if ($this->post['interestArea'] == 4) {
                $email = "comunicacao@betterbaby.com.br";
            }
            if ($this->post['interestArea'] == 5) {
                $email = "marketing@betterbaby.com.br";
            }
            
            $success = $this->actionSendEmail($email, $user->name, $user->id, $this->post['text'],$user->email);
            
            if ($success) {
                $response = $this->response(['message' => 'E-mail enviado com sucesso']);
            } else {
                $response = $this->response(['message' => 'Ocorreu um erro ao enviar o e-mail'], HttpCode::BAD_REQUEST_400);
            }
            return $response;
        } catch (\Exception $e) {
            $response = $this->response(['message' => 'Ocorreu um erro ao enviar o e-mail'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
    }
    
    
    public function actionSendEmail($email, $name, $userId, $text, $clientEmail) {
        $template = 'layouts/contactForm';
        try {
            $subject = 'Novo contato';
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name'        => $name,
                    'userId'      => $userId,
                    'text'        => $text,
                    'clientEmail' => $clientEmail,
                ]
            )
                ->setFrom([$_ENV['SENDER_EMAIL'] => $_ENV['SENDER_NAME']])
                ->setTo($email)
                ->setSubject($subject)
                ->send();
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch (Exception $e) {
            throw $e;
        }
        return TRUE;
    }
}
