<?php

namespace api\modules\publicity\controllers;

use api\components\utils\HttpCode;
use common\modules\plan\services\HealthPlanService;
use common\modules\publicityView\models\PublicityView;
use common\modules\publicityView\services\PublicityViewService;
use Yii;
use api\components\utils\BaseController;
use Exception;


class PublicityController extends BaseController {
    public $modelClass = '';
    private $service;
    
    /**
     * @SWG\Get(
     *  path = "/publicity/list-publicity",
     *  tags = {"Publicity"},
     *  operationId = "ListPublicity",
     *  summary="news data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListPublicity() : array {
        try {
            $service = Yii::$app->getModule('publicity')->get('publicityService');
            $user = $service->ListPublicity($this->currentUser->id);
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    /**
     * @SWG\Get(
     *  path = "/publicity/publicity-details",
     *  tags = {"Publicity"},
     *  operationId = "PublicityDetails",
     *  summary="news data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *   @SWG\Parameter(name="publicityId", in="query", type="integer", description="publicity's id", required=true),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionPublicityDetails() : array {
        try {
            $service = Yii::$app->getModule('publicity')->get('publicityService');
            $user = $service->PublicityDetails($this->get['publicityId']);
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Post(
     *    path = "/publicity/new-view",
     *    tags = {"Publicity"},
     *    operationId = "NewView",
     *    summary="Create a new View",
     *    produces = {"application/json"},
     *     consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *        @SWG\Schema(
     *              type="object",
     *               @SWG\Property(property="publicityId", type="string", example="1"),
     *          ),
     *    ),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     * @throws Exception
     */
    public function actionNewView() : array {
        
        $validadeMessage = $this->validadeReceivedParams([
            'publicity_id' => $this->post['publicityId'],
        ]);
        if (!empty($validadeMessage)) {
            $response = $this->response(['message' => $validadeMessage], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        /** @var PublicityViewService $service */
        $service = Yii::$app->getModule('publicity')->get('publicityService');
        /** @var PublicityView $Solicitation */
        
        $publicity = new PublicityView(
            [
                'user_id'      => $this->currentUser->id,
                'publicity_id' => $this->post['publicityId'],
            ]);
        
        if (!$publicity->validate()) {
            $erros = $publicity->errors;
            foreach ($erros as $er) {
                $response = $this->response(['message' => $er[0]], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        $transaction = Yii::$app->db->beginTransaction();
        $solicitationId = $service->createPublicityView($publicity);
        
        
        if ($solicitationId > 0) {
            $transaction->commit();
            $response = $this->response(['message' => 'View adicionada com sucesso!']);
            return $response;
        } else {
            $transaction->rollBack();
            throw new \Exception("Ocorreu um erro, por favor, contate o TI.");
        }
        
    }
    
    
}