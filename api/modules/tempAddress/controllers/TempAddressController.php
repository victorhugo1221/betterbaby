<?php

namespace api\modules\tempAddress\controllers;

use api\components\utils\HttpCode;
use common\modules\plan\services\HealthPlanService;
use common\modules\tempAddress\models\TempAddress;
use common\modules\tempAddress\services\TempAddressService;
use Yii;
use api\components\utils\BaseController;
use Exception;


class TempAddressController extends BaseController {
    public $modelClass = '';
    private $service;
    
    /**
     * @SWG\Get(
     *  path = "/temp-address/list-address",
     *  tags = {"TempAddress"},
     *  operationId = "ListAddress",
     *  summary="specialty data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListAddress() : array {
        try {
            $service = Yii::$app->getModule('temp-address')->get('tempAddressService');
            $user = $service->ListAddress($this->currentUser->id);
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Post(
     *    path = "/temp-address/new-address",
     *    tags = {"TempAddress"},
     *    operationId = "NewAddress",
     *    summary="Create a new Addres",
     *    produces = {"application/json"},
     *     consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *        @SWG\Schema(
     *              type="object",
     *               @SWG\Property(property="title", type="string", example="casa"),
     *               @SWG\Property(property="city", type="string", example="são paulo"),
     *              @SWG\Property(property="state", type="string", example="SC"),
     *              @SWG\Property(property="street", type="string", example="rua 222"),
     *              @SWG\Property(property="number", type="string", example="123"),
     *              @SWG\Property(property="district", type="string", example="centro"),
     *              @SWG\Property(property="zipCode", type="string", example="01001000"),
     *          ),
     *    ),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     * @throws Exception
     */
    public function actionNewAddress() : array {
        
        $validadeMessage = $this->validadeReceivedParams([
            'state' => $this->post['state'],
            'city'  => $this->post['city'],
        ]);
        if (!empty($validadeMessage)) {
            $response = $this->response(['message' => $validadeMessage], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        /** @var TempAddressService $service */
        $service = Yii::$app->getModule('temp-address')->get('tempAddressService');
        /** @var TempAddress $Solicitation */
    
    
        $Nstreet = str_replace([' ', "\t", "\n"], '', $this->post['street']);
        $Ndistrict = str_replace([' ', "\t", "\n"], '', $this->post['district']);
        $Ncity = str_replace([' ', "\t", "\n"], '', $this->post['city']);
        $Nzipcode = str_replace([' ', "\t", "\n"], '', $this->post['zipCode']);
        $Nzipcode = substr($Nzipcode, 0, 5);
    
    
        $curl = curl_init();
    
        curl_setopt_array($curl, [
            CURLOPT_URL            => 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . $Nstreet . ',' . $Ndistrict . ',' . $Ncity . ',' . $Nzipcode . '.json?country=br&types=address%2Cpostcode&access_token=pk.eyJ1IjoiZWRmeWl0MjIiLCJhIjoiY2w4YnduamdoMDE0ZzN2bXdldzBnMml1NSJ9.iDgduwdNkY7SfJuja9zqpQ',
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'GET',
        ]);
    
    
        $response = curl_exec($curl);
        $jsonData = json_decode($response, TRUE);
    
        curl_close($curl);
    
        if (!empty($jsonData['features'][0]['center'][0])) {
            $longitude = $jsonData['features'][0]['center'][0];
            $latitude = $jsonData['features'][0]['center'][1];
        }
        
        
        $address = new TempAddress(
            [
                'user_id'   => $this->currentUser->id,
                'title'     => $this->post['title'],
                'city'      => $this->post['city'],
                'state'     => $this->post['state'],
                'street'    => $this->post['street'],
                'number'    => $this->post['number'],
                'district'  => $this->post['district'],
                'zip_code'  => $this->post['zipCode'],
                'latitude'  => $latitude,
                'longitude' => $longitude,
            ]);
        
        if (!$address->validate()) {
            $erros = $address->errors;
            foreach ($erros as $er) {
                $response = $this->response(['message' => $er[0]], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        $transaction = Yii::$app->db->beginTransaction();
        $solicitationId = $service->createAddress($address);
        
        
        if ($solicitationId > 0) {
            $transaction->commit();
            $response = $this->response(['message' => 'Endereço criado com sucesso!']);
            return $response;
        } else {
            $transaction->rollBack();
            throw new \Exception("Ocorreu um erro, por favor, contate o TI.");
        }
        
    }
    
    
}