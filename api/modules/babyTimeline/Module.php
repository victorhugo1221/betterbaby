<?php

namespace api\modules\babyTimeline;

class Module extends \common\modules\babyTimeline\Module {
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    public $defaultRoute        = 'default/index';
    
    public function init() {
        parent::init();
    }
}
