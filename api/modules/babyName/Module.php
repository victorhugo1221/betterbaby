<?php

namespace api\modules\babyName;

class Module extends \common\modules\babyName\Module {
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    public $defaultRoute        = 'default/index';
    
    public function init() {
        parent::init();
    }
}
