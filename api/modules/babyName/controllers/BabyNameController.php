<?php

namespace api\modules\babyName\controllers;

use api\components\utils\HttpCode;
use common\modules\plan\services\HealthPlanService;
use Yii;
use api\components\utils\BaseController;
use Exception;


class BabyNameController extends BaseController {
    public $modelClass = '';
    private $service;
    
    /**
     * @SWG\Get(
     *  path = "/baby-name/list-name",
     *  tags = {"BabyName"},
     *  operationId = "ListName",
     *  summary="name data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *  @SWG\Parameter(name="searchName", in="query", type="string", description="search by name", required=false),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListName() : array {
        try {
            $service = Yii::$app->getModule('baby-name')->get('babyNameService');
            $user = $service->ListName($this->get['searchName']);
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
}