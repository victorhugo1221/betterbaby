<?php

namespace api\modules\serviceSpecialty;

class Module extends \common\modules\serviceSpecialty\Module {
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    public $defaultRoute        = 'default/index';
    
    public function init() {
        parent::init();
    }
}
