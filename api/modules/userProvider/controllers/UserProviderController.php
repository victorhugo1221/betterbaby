<?php

namespace api\modules\userProvider\controllers;

use common\modules\address\models\Address;
use common\modules\favoriteProvider\models\FavoriteProvider;
use common\modules\user\models\User;
use api\components\utils\BaseController;
use api\components\utils\HttpCode;
use api\components\utils\Response;
use common\modules\adminGw\logs\GrupoWLog;
use common\modules\user\models\UserClient;
use common\modules\userProvider\models\UserProvider;
use common\modules\userProvider\services\UserProviderService;
use common\modules\userProviderHealthPlan\models\UserProviderHealthPlan;
use common\modules\userProviderPlan\models\UserProviderPlan;
use Exception;
use Ramsey\Uuid\Uuid;
use Yii;
use common\modules\user\services\UserService;
use yii\web\UploadedFile;


class UserProviderController extends BaseController {
    public $modelClass = 'common\modules\userProvider\models\UserProvider';
    
    
    /**
     * @SWG\Get(
     *    path = "/user-provider/get-data",
     *    tags = {"UserProvider"},
     *    operationId = "GetData",
     *    summary="Get user data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionGetData() : array {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $httpCode = HttpCode::UNAUTHORIZED_401;
                $service = Yii::$app->getModule('user-provider')->get('userProviderService');
                $user = $service->ListUserData($this->currentUser->id);
                
                if (!empty($user)) {
                    $response = $user;
                    return $this->response($response);
                } else {
                    return $this->response(['message' => "Não autorizado"], HttpCode::UNAUTHORIZED_401);
                }
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Get(
     *    path = "/user-provider/list-all",
     *    tags = {"UserProvider"},
     *    operationId = "ListAll",
     *    summary="Get all data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Parameter(name="latitude", in="query", type="string", description="latitude", required=true),
     *    @SWG\Parameter(name="longitude", in="query", type="string", description="latitude", required=true),
     *    @SWG\Parameter(name="rangeKm", in="query", type="string", description="range", required=false),
     *    @SWG\Parameter(name="searchText", in="query", type="string", description="search text name or speciality", required=false),
     *    @SWG\Parameter(name="categoryStatus", in="query", type="string", description="category Doctor", required=false),
     *    @SWG\Parameter(name="acceptPne", in="query", type="string", description="accept  pne", required=false),
     *    @SWG\Parameter(name="princeRange", in="query", type="string", description="price Range", required=false),
     *    @SWG\Parameter(name="doctorSpecialty", in="query", type="string", description="doctor Specialty", required=false),
     *    @SWG\Parameter(name="healthPlan", in="query", type="string", description="health Plan", required=false),
     *    @SWG\Parameter(name="serviceSpecialty", in="query", type="string", description="service Category", required=false),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListAll() : array {
        try {
            $service = Yii::$app->getModule('user-provider')->get('userProviderService');
            
            $userAddres = Address::find()->andWhere(['user_id' => $this->currentUser->id])->one();
            
            if ((!isset($this->get['rangeKm']))) {
                $this->get['rangeKm'] = 100;
            }
            
            if ($this->get['latitude'] == NULL) {
                $latitude = $userAddres->latitude;
            } else {
                $latitude = $this->get['latitude'];
            }
            if ($this->get['longitude'] == NULL) {
                $longitude = $userAddres->longitude;
            } else {
                $longitude = $this->get['longitude'];
            }
            
            $user = $service->ListAllUserProvider(
                $this->currentUser->id,
                $latitude,
                $longitude,
                $this->get['rangeKm'],
                $this->get['searchText'],
                $this->get['categoryStatus'],
                $this->get['acceptPne'],
                $this->get['princeRange'],
                $this->get['doctorSpecialty'],
                $this->get['healthPlan'],
                $this->get['serviceSpecialty'],
            );
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "ainda não há profissionais na sua região"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    /**
     * @SWG\Get(
     *    path = "/user-provider/list-favorite",
     *    tags = {"UserProvider"},
     *    operationId = "ListFavorite",
     *    summary="Get all data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Parameter(name="searchName", in="query", type="string", description="search by name", required=false),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListFavorite() : array {
        try {
            $service = Yii::$app->getModule('user-provider')->get('userProviderService');
            
            $user = $service->ListFavoriteProvider($this->currentUser->id, $this->get['searchName']);
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "ainda não há profissionais na sua região"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Get(
     *    path = "/user-provider/list-details",
     *    tags = {"UserProvider"},
     *    operationId = "ListDetails",
     *    summary="Get user provide data",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Parameter(name="userId", in="query", type="integer", description="user's id", required=true),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListDetails() : array {
        try {
            $service = Yii::$app->getModule('user-provider')->get('userProviderService');
            $user = $service->ListDetailsProvider($this->get['userId']);
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    /**
     * @SWG\Post(
     *    path = "/user-provider/new-doctor",
     *    tags = {"UserProvider"},
     *    operationId = "NewDoctor",
     *    summary="Create a new doctor",
     *    produces = {"application/json"},
     *     consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *        @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="fulano de souza"),
     *              @SWG\Property(property="cpf", type="string", example="12345678901"),
     *              @SWG\Property(property="email", type="string", example="teste@edfy.com.br"),
     *              @SWG\Property(property="cellphone", type="string", example="479999999"),
     *              @SWG\Property(property="password", type="string", example="teste123"),
     *              @SWG\Property(property="crm", type="string", example="12234"),
     *              @SWG\Property(property="crmState", type="string", example="SC"),
     *              @SWG\Property(property="crmSpecialty", type="string", example="DERMATOLOGIA - RQE Nº 9540"),
     *              @SWG\Property(property="priceRange", type="string", example="200"),
     *              @SWG\Property(property="priceRangeHealthPlan", type="string", example="100"),
     *              @SWG\Property(property="additionalSpecialty", type="string", example="xxx"),
     *              @SWG\Property(property="acceptPne", type="string", example="1"),
     *              @SWG\Property(property="city", type="string", example="são paulo"),
     *              @SWG\Property(property="state", type="string", example="SC"),
     *              @SWG\Property(property="street", type="string", example="rua 222"),
     *              @SWG\Property(property="number", type="string", example="123"),
     *              @SWG\Property(property="complement", type="string", example="ap 202"),
     *              @SWG\Property(property="district", type="string", example="centro"),
     *              @SWG\Property(property="zipCode", type="string", example="01001000"),
     *              @SWG\Property(property="healthPlan", type="string", example="1"),
     *              @SWG\Property(property="pushToken", type="string", example="xxxxx"),
     *              @SWG\Property(property="startHour", type="string", example="10:10"),
     *              @SWG\Property(property="endHour", type="string", example="16:30"),
     *              @SWG\Property(property="gap", type="string", example="15"),
     *          ),
     *    ),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     * @throws Exception
     */
    public function actionNewDoctor() : array {
        
        $validadeMessage = $this->validadeReceivedParams([
            'name'        => $this->post['name'],
            'cpf'         => $this->post['cpf'],
            'email'       => $this->post['email'],
            'cellphone'   => $this->post['cellphone'],
            'password'    => $this->post['password'],
            'crm'         => $this->post['crm'],
            'crm_state'   => $this->post['crmState'],
            'price_range' => $this->post['priceRange'],
            'accept_pne'  => $this->post['acceptPne'],
            'city'        => $this->post['city'],
            'state'       => $this->post['state'],
            'street'      => $this->post['street'],
            'number'      => $this->post['number'],
            'district'    => $this->post['district'],
            'zip_code'    => $this->post['zipCode'],
        ]);
        if (!empty($validadeMessage)) {
            $response = $this->response(['message' => 'Campo obrigatório não preenchido'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        /** @var UserProviderService $service */
        $service = Yii::$app->getModule('user-provider')->get('userProviderService');
        /** @var UserProvider $UserProvider */
        
        // Validar unicidade E-mail
        if (!empty($this->post['email'])) {
            $user = User::find()->andWhere(['email' => $this->post['email']])->one();
            if (!empty($user)) {
                $response = $this->response(['message' => 'Este E-mail já está sendo utilizado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        // Validar formato E-mail
        if (filter_var($this->post['email'], FILTER_VALIDATE_EMAIL)) {
            $test = 0;
        } else {
            $response = $this->response(['message' => 'Este E-mail é invalido'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        // Validar unicidade CPF
        if (!empty($this->post['cpf'])) {
            $user = User::find()->andWhere(['cpf' => $this->post['cpf']])->one();
            if (!empty($user)) {
                $response = $this->response(['message' => 'Este CPF já está sendo utilizado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        $Nstreet = str_replace([' ', "\t", "\n"], '', $this->post['street']);
        $Ndistrict = str_replace([' ', "\t", "\n"], '', $this->post['district']);
        $Ncity = str_replace([' ', "\t", "\n"], '', $this->post['city']);
        $Nzipcode = str_replace([' ', "\t", "\n"], '', $this->post['zipCode']);
        $Nzipcode = substr($Nzipcode, 0, 5);
        
        $curl = curl_init();
        
        curl_setopt_array($curl, [
            CURLOPT_URL            => 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . $Nstreet . ',' . $Ndistrict . ',' . $Ncity . ',' . $Nzipcode . '.json?country=br&types=address%2Cpostcode&access_token=pk.eyJ1IjoiZWRmeWl0MjIiLCJhIjoiY2w4YnduamdoMDE0ZzN2bXdldzBnMml1NSJ9.iDgduwdNkY7SfJuja9zqpQ',
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'GET',
        ]);
        
        $response = curl_exec($curl);
        $jsonData = json_decode($response, TRUE);
        
        curl_close($curl);
        
        if (!empty($jsonData['features'][0]['center'][0])) {
            $longitude = $jsonData['features'][0]['center'][0];
            $latitude = $jsonData['features'][0]['center'][1];
        }
        
        $user = new User(
            [
                'name'       => $this->post['name'],
                'cpf'        => $this->post['cpf'],
                'email'      => $this->post['email'],
                'cellphone'  => $this->post['cellphone'],
                'push_token' => $this->post['pushToken'],
                'password'   => Yii::$app->security->generatePasswordHash($this->post['password']),
                'status'     => 1,
            ]);
        
        
        $userProvider = new UserProvider(
            [
                'category_id'             => 1,
                'crm'                     => $this->post['crm'],
                'crm_state'               => $this->post['crmState'],
                'crm_specialty'           => $this->post['crmSpecialty'],
                'price_range'             => $this->post['priceRange'],
                'price_range_health_plan' => $this->post['priceRangeHealthPlan'],
                'additional_specialty'    => json_encode($this->post['additionalSpecialty'], JSON_UNESCAPED_UNICODE),
                'accept_pne'              => $this->post['acceptPne'],
                'health_plan'             => json_encode($this->post['healthPlan'], JSON_UNESCAPED_UNICODE),
                'start_hour'              => $this->post['startHour'],
                'end_hour'                => $this->post['endHour'],
                'gap'                     => $this->post['gap'],
                'status'                  => 1,
            ]);
        $address = new Address(
            [
                'city'       => $this->post['city'],
                'state'      => $this->post['state'],
                'street'     => $this->post['street'],
                'number'     => $this->post['number'],
                'complement' => $this->post['complement'],
                'district'   => $this->post['district'],
                'zip_code'   => $this->post['zipCode'],
                'latitude'   => $latitude,
                'longitude'  => $longitude,
                'status'     => 1,
            ]);
        $userProviderPlan = new UserProviderPlan(
            [
                'plan_id'  => 1,
                'end_date' => date("Y-m-d H:i:m", strtotime("+60 day")),
            ]);
        
        if (!$user->validate()) {
            $erros = $user->errors;
            foreach ($erros as $er) {
                $response = $this->response(['message' => $er[0]], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        if ($latitude == NULL && $longitude == NULL) {
            $response = $this->response(['message' => 'Não foi possivel encontrar o endereço'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        $transaction = Yii::$app->db->beginTransaction();
        $userId = $service->createUserApp($user);
        
        if ($userId > 0) {
            $providerId = $service->createUserProvider($userProvider, $userId);
            $addressId = $service->createAddress($address, $userId);
            $userProviderPlanId = $service->createUserProviderPlan($userProviderPlan, $userId);
        }
        
        if ($userId && $addressId && $providerId && $userProviderPlanId) {
            if ($this->actionSendNewProviderEmail($this->post['email'], $this->post['name']) == FALSE) {
                $response = $this->response(['message' => 'Email inválido! Ocorreu um erro ao enviar o email!']);
                return $response;
            }
            $transaction->commit();
            $response = $this->response(['message' => 'Cadastrado realizado com sucesso!']);
            return $response;
        } else {
            $transaction->rollBack();
            throw new \Exception("Ocorreu um erro, por favor, contate o suporte.");
        }
        
    }
    
    
    /**
     * @SWG\Post(
     *    path = "/user-provider/new-clinic",
     *    tags = {"UserProvider"},
     *    operationId = "NewClinic",
     *    summary="Create a new clinic",
     *    produces = {"application/json"},
     *     consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *        @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="fulano de souza"),
     *              @SWG\Property(property="cnpj", type="string", example="12345678901"),
     *              @SWG\Property(property="email", type="string", example="teste@edfy.com.br"),
     *              @SWG\Property(property="cellphone", type="string", example="479999999"),
     *              @SWG\Property(property="password", type="string", example="teste123"),
     *              @SWG\Property(property="additionalSpecialty", type="string", example="CIRURGIA DE CABEÇA E PESCOÇO"),
     *              @SWG\Property(property="acceptPne", type="string", example="1"),
     *              @SWG\Property(property="city", type="string", example="são paulo"),
     *              @SWG\Property(property="state", type="string", example="SC"),
     *              @SWG\Property(property="street", type="string", example="rua 222"),
     *              @SWG\Property(property="number", type="string", example="123"),
     *              @SWG\Property(property="complement", type="string", example="ap 202"),
     *              @SWG\Property(property="district", type="string", example="centro"),
     *              @SWG\Property(property="zipCode", type="string", example="01001000"),
     *              @SWG\Property(property="healthPlan", type="string", example="1"),
     *              @SWG\Property(property="pushToken", type="string", example="xxxxx"),
     *          ),
     *    ),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     * @throws Exception
     */
    public function actionNewClinic() : array {
        
        $validadeMessage = $this->validadeReceivedParams([
            'name'                 => $this->post['name'],
            'cnpj'                 => $this->post['cnpj'],
            'email'                => $this->post['email'],
            'cellphone'            => $this->post['cellphone'],
            'password'             => $this->post['password'],
            'additional_specialty' => $this->post['additionalSpecialty'],
            'accept_pne'           => $this->post['acceptPne'],
            'city'                 => $this->post['city'],
            'state'                => $this->post['state'],
            'street'               => $this->post['street'],
            'number'               => $this->post['number'],
            'district'             => $this->post['district'],
            'zip_code'             => $this->post['zipCode'],
        ]);
        if (!empty($validadeMessage)) {
            $response = $this->response(['message' => 'Campo obrigatório não preenchido'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        /** @var UserProviderService $service */
        $service = Yii::$app->getModule('user-provider')->get('userProviderService');
        /** @var UserProvider $UserProvider */
        
        // Validar unicidade E-mail
        if (!empty($this->post['email'])) {
            $user = User::find()->andWhere(['email' => $this->post['email']])->one();
            if (!empty($user)) {
                $response = $this->response(['message' => 'Este E-mail já está sendo utilizado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        // Validar unicidade CPF
        if (!empty($this->post['cnpj'])) {
            $userProvider = UserProvider::find()->andWhere(['cnpj' => $this->post['cnpj']])->one();
            if (!empty($userProvider)) {
                $response = $this->response(['message' => 'Este cnpj já está sendo utilizado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        $Nstreet = str_replace([' ', "\t", "\n"], '', $this->post['street']);
        $Ndistrict = str_replace([' ', "\t", "\n"], '', $this->post['district']);
        $Ncity = str_replace([' ', "\t", "\n"], '', $this->post['city']);
        $Nzipcode = str_replace([' ', "\t", "\n"], '', $this->post['zipCode']);
        $Nzipcode = substr($Nzipcode, 0, 5);
        
        $curl = curl_init();
        
        curl_setopt_array($curl, [
            CURLOPT_URL            => 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . $Nstreet . ',' . $Ndistrict . ',' . $Ncity . ',' . $Nzipcode . '.json?country=br&types=address%2Cpostcode&access_token=pk.eyJ1IjoiZWRmeWl0MjIiLCJhIjoiY2w4YnduamdoMDE0ZzN2bXdldzBnMml1NSJ9.iDgduwdNkY7SfJuja9zqpQ',
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'GET',
        ]);
        
        $response = curl_exec($curl);
        $jsonData = json_decode($response, TRUE);
        
        curl_close($curl);
        
        if (!empty($jsonData['features'][0]['center'][0])) {
            $longitude = $jsonData['features'][0]['center'][0];
            $latitude = $jsonData['features'][0]['center'][1];
        }
        
        
        $user = new User(
            [
                'name'       => $this->post['name'],
                'email'      => $this->post['email'],
                'cellphone'  => $this->post['cellphone'],
                'push_token' => $this->post['pushToken'],
                'password'   => Yii::$app->security->generatePasswordHash($this->post['password']),
                'status'     => 1,
            ]);
        
        $userProvider = new UserProvider(
            [
                'category_id'          => 2,
                'health_plan'          => json_encode($this->post['healthPlan'], JSON_UNESCAPED_UNICODE),
                'additional_specialty' => json_encode($this->post['additionalSpecialty'], JSON_UNESCAPED_UNICODE),
                'accept_pne'           => $this->post['acceptPne'],
                'cnpj'                 => $this->post['cnpj'],
                'status'               => 1,
            ]);
        $address = new Address(
            [
                'city'       => $this->post['city'],
                'state'      => $this->post['state'],
                'street'     => $this->post['street'],
                'number'     => $this->post['number'],
                'complement' => $this->post['complement'],
                'district'   => $this->post['district'],
                'zip_code'   => $this->post['zipCode'],
                'latitude'   => $latitude,
                'longitude'  => $longitude,
                'status'     => 1,
            ]);
        $userProviderPlan = new UserProviderPlan(
            [
                'plan_id'  => 3,
                'end_date' => date("Y-m-d H:i:m", strtotime("+60 day")),
            ]);
        
        if (!$user->validate()) {
            $erros = $user->errors;
            foreach ($erros as $er) {
                $response = $this->response(['message' => $er[0]], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        if ($latitude == NULL && $longitude == NULL) {
            $response = $this->response(['message' => 'Não foi possivel encontrar o endereço'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        $transaction = Yii::$app->db->beginTransaction();
        $userId = $service->createUserApp($user);
        
        if ($userId > 0) {
            $providerId = $service->createUserProvider($userProvider, $userId);
            $addressId = $service->createAddress($address, $userId);
            $userProviderPlanId = $service->createUserProviderPlan($userProviderPlan, $userId);
        }
        
        if ($userId && $addressId && $providerId && $userProviderPlanId) {
            $this->actionSendNewProviderEmail($this->post['email'], $this->post['name']);
            $transaction->commit();
            $response = $this->response(['message' => 'Cadastrado realizado com sucesso!']);
            return $response;
        } else {
            $transaction->rollBack();
            throw new \Exception("Ocorreu um erro, por favor, contate o suporte.");
        }
        
    }
    
    
    /**
     * @SWG\Post(
     *    path = "/user-provider/new-service",
     *    tags = {"UserProvider"},
     *    operationId = "NewService",
     *    summary="Create a new user service",
     *    produces = {"application/json"},
     *     consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *        @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="fulano de souza"),
     *              @SWG\Property(property="cpf", type="string", example="12345678901"),
     *              @SWG\Property(property="email", type="string", example="teste@edfy.com.br"),
     *              @SWG\Property(property="cellphone", type="string", example="479999999"),
     *              @SWG\Property(property="password", type="string", example="teste123"),
     *              @SWG\Property(property="serviceCategory", type="string", example="fotografo"),
     *              @SWG\Property(property="others", type="string", example="manicure"),
     *              @SWG\Property(property="city", type="string", example="são paulo"),
     *              @SWG\Property(property="state", type="string", example="SC"),
     *              @SWG\Property(property="street", type="string", example="rua 222"),
     *              @SWG\Property(property="number", type="string", example="123"),
     *              @SWG\Property(property="complement", type="string", example="ap 202"),
     *              @SWG\Property(property="district", type="string", example="centro"),
     *              @SWG\Property(property="zipCode", type="string", example="01001000"),
     *              @SWG\Property(property="pushToken", type="string", example="xxxxx"),
     *              @SWG\Property(property="startHour", type="string", example="10:10"),
     *              @SWG\Property(property="endHour", type="string", example="16:30"),
     *              @SWG\Property(property="gap", type="string", example="15"),
     *          ),
     *    ),
     *  @SWG\Response(response = 200, description = "Success"),
     *  @SWG\Response(response = 400, description = "Bad Request"),
     *  @SWG\Response(response = 401, description = "Unauthorized"),
     *  @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     * @throws Exception
     */
    public function actionNewService() : array {
        
        $validadeMessage = $this->validadeReceivedParams([
            'name'             => $this->post['name'],
            'cpf'              => $this->post['cpf'],
            'email'            => $this->post['email'],
            'cellphone'        => $this->post['cellphone'],
            'password'         => $this->post['password'],
            'service_category' => $this->post['serviceCategory'],
            'city'             => $this->post['city'],
            'state'            => $this->post['state'],
            'street'           => $this->post['street'],
            'number'           => $this->post['number'],
            'district'         => $this->post['district'],
            'zip_code'         => $this->post['zipCode'],
        ]);
        
        
        if (!empty($validadeMessage)) {
            $response = $this->response(['message' => 'Campo obrigatório não preenchido'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        /** @var UserProviderService $service */
        $service = Yii::$app->getModule('user-provider')->get('userProviderService');
        /** @var UserProvider $UserProvider */
        
        // Validar unicidade E-mail
        if (!empty($this->post['email'])) {
            $user = User::find()->andWhere(['email' => $this->post['email']])->one();
            if (!empty($user)) {
                $response = $this->response(['message' => 'Este E-mail já está sendo utilizado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        // Validar unicidade CPF
        if (!empty($this->post['cpf'])) {
            $user = User::find()->andWhere(['cpf' => $this->post['cpf']])->one();
            if (!empty($user)) {
                $response = $this->response(['message' => 'Este cpf já está sendo utilizado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        $Nstreet = str_replace([' ', "\t", "\n"], '', $this->post['street']);
        $Ndistrict = str_replace([' ', "\t", "\n"], '', $this->post['district']);
        $Ncity = str_replace([' ', "\t", "\n"], '', $this->post['city']);
        $Nzipcode = str_replace([' ', "\t", "\n"], '', $this->post['zipCode']);
        $Nzipcode = substr($Nzipcode, 0, 5);
        
        
        $curl = curl_init();
        
        curl_setopt_array($curl, [
            CURLOPT_URL            => 'https://api.mapbox.com/geocoding/v5/mapbox.places/' . $Nstreet . ',' . $Ndistrict . ',' . $Ncity . ',' . $Nzipcode . '.json?country=br&types=address%2Cpostcode&access_token=pk.eyJ1IjoiZWRmeWl0MjIiLCJhIjoiY2w4YnduamdoMDE0ZzN2bXdldzBnMml1NSJ9.iDgduwdNkY7SfJuja9zqpQ',
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'GET',
        ]);
        
        
        $response = curl_exec($curl);
        $jsonData = json_decode($response, TRUE);
        
        curl_close($curl);
        
        if (!empty($jsonData['features'][0]['center'][0])) {
            $longitude = $jsonData['features'][0]['center'][0];
            $latitude = $jsonData['features'][0]['center'][1];
        }
        
        
        $user = new User(
            [
                'name'       => $this->post['name'],
                'cpf'        => $this->post['cpf'],
                'email'      => $this->post['email'],
                'cellphone'  => $this->post['cellphone'],
                'push_token' => $this->post['pushToken'],
                'password'   => Yii::$app->security->generatePasswordHash($this->post['password']),
                'status'     => 1,
            ]);
        
        $userProvider = new UserProvider(
            [
                'category_id'      => 3,
                'service_category' => json_encode($this->post['serviceCategory'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
                'others'           => $this->post['others'],
                'start_hour'       => $this->post['startHour'],
                'end_hour'         => $this->post['endHour'],
                'gap'              => $this->post['gap'],
                'status'           => 1,
            ]);
        
        $userProviderPlan = new UserProviderPlan(
            [
                'plan_id'  => 5,
                'end_date' => date("Y-m-d H:i:m", strtotime("+60 day")),
            ]);
        
        $address = new Address(
            [
                'city'       => $this->post['city'],
                'state'      => $this->post['state'],
                'street'     => $this->post['street'],
                'number'     => $this->post['number'],
                'complement' => $this->post['complement'],
                'district'   => $this->post['district'],
                'zip_code'   => $this->post['zipCode'],
                'latitude'   => $latitude,
                'longitude'  => $longitude,
                'status'     => 1,
            ]);
        
        if (!$user->validate()) {
            $erros = $user->errors;
            foreach ($erros as $er) {
                $response = $this->response(['message' => $er[0]], HttpCode::BAD_REQUEST_400);
                return $response;
            }
        }
        
        if ($latitude === NULL && $longitude === NULL) {
            $response = $this->response(['message' => 'Não foi possivel encontrar o endereço.'], HttpCode::BAD_REQUEST_400);
            return $response;
        }
        
        
        $transaction = Yii::$app->db->beginTransaction();
        $userId = $service->createUserApp($user);
        
        if ($userId > 0) {
            $providerId = $service->createUserProvider($userProvider, $userId);
            $addressId = $service->createAddress($address, $userId);
            $userProviderPlanId = $service->createUserProviderPlan($userProviderPlan, $userId);
        }
        
        if ($userId && $addressId && $providerId && $userProviderPlanId) {
            $this->actionSendNewProviderEmail($this->post['email'], $this->post['name']);
            $transaction->commit();
            $response = $this->response(['message' => 'Cadastrado realizado com sucesso!']);
            return $response;
        } else {
            $transaction->rollBack();
            throw new \Exception("Ocorreu um erro, por favor, contate o suporte.");
        }
        
    }
    
    
    /**
     * @SWG\Put(
     *    path = "/user-provider/update-user-provider",
     *    tags = {"UserProvider"},
     *    operationId = "updateUserProvider",
     *    summary="Update user",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	  @SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *       @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="fulano de souza"),
     *              @SWG\Property(property="email", type="string", example="teste@edfy.com.br"),
     *              @SWG\Property(property="cellphone", type="string", example="479999999"),
     *              @SWG\Property(property="pushToken", type="string", example="0000000000000"),
     *              @SWG\Property(property="priceRange", type="string", example="200"),
     *              @SWG\Property(property="priceRangeHealthPlan", type="string", example="100"),
     *              @SWG\Property(property="additionalSpecialty", type="string", example="CIRURGIA DE CABEÇA E PESCOÇO - RQE Nº 9472"),
     *              @SWG\Property(property="acceptPne", type="string", example="1"),
     *              @SWG\Property(property="instagramUrl", type="string", example="better baby"),
     *              @SWG\Property(property="facebookUrl", type="string", example="better baby"),
     *              @SWG\Property(property="linkedinUrl", type="string", example="better baby"),
     *              @SWG\Property(property="profileText", type="string", example="texto de perfil"),
     *              @SWG\Property(property="city", type="string", example="são paulo"),
     *              @SWG\Property(property="state", type="string", example="SC"),
     *              @SWG\Property(property="street", type="string", example="rua 222"),
     *              @SWG\Property(property="number", type="string", example="123"),
     *              @SWG\Property(property="complement", type="string", example="ap 202"),
     *              @SWG\Property(property="district", type="string", example="centro"),
     *              @SWG\Property(property="zipCode", type="string", example="01001000"),
     *              @SWG\Property(property="healthPlan", type="string", example="1"),
     *              @SWG\Property(property="serviceCategory", type="string", example="1"),
     *              @SWG\Property(property="others", type="string", example="yoga"),
     *              @SWG\Property(property="startHour", type="string", example="10:10"),
     *              @SWG\Property(property="endHour", type="string", example="16:30"),
     *              @SWG\Property(property="gap", type="string", example="15"),
     *              @SWG\Property(property="guidelines", type="string", example="venha com roupa confortavel"),
     *        ),
     *     ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionUpdateUserProvider() : array {
        if ($this->theTokenSentIsNotThePrimaryToken()) {
            /** @var UserService $service */
            $service = Yii::$app->getModule('user')->get('userService');
            /** @var User $user */
            $user = $service->findById($this->currentUser->id);
            
            if (empty($user)) {
                $response = $this->response(['message' => 'Usuário não encontrado'], HttpCode::BAD_REQUEST_400);
                return $response;
            }
            
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            $user = User::find()->andWhere(['id' => $this->currentUser->id])->one();
            $oldUser = User::find()->andWhere(['id' => $this->currentUser->id])->one();
            $userProvider = UserProvider::find()->andWhere(['user_id' => $this->currentUser->id])->one();
            $userAddress = Address::find()->andWhere(['user_id' => $this->currentUser->id])->one();
            
            
            if (!empty($this->post['name'])) {
                $user->name = $this->post['name'];
            }
            if (!empty($this->post['email'])) {
                $user->email = $this->post['email'];
            }
            if (!empty($this->post['cellphone'])) {
                $user->cellphone = $this->post['cellphone'];
            }
            if (!empty($this->post['pushToken'])) {
                $user->push_token = $this->post['pushToken'];
            }
            
            //userProvider//
            if (!empty($this->post['serviceCategory'] && json_decode($this->post['serviceCategory']) !== $userProvider->service_category) && is_array($this->post['serviceCategory'])) {
                $userProvider->service_category = json_encode($this->post['serviceCategory'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            }
            
            if (!empty($this->post['healthPlan'] && json_decode($this->post['healthPlan']) !== $userProvider->health_plan) && is_array($this->post['healthPlan'])) {
                $userProvider->health_plan = json_encode($this->post['healthPlan'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            }
            
            if (!empty($this->post['additionalSpecialty'])) {
                $userProvider->additional_specialty = json_encode($this->post['additionalSpecialty'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            }
            
            if (!empty($this->post['acceptPne'])) {
                $userProvider->accept_pne = $this->post['acceptPne'];
            }
            if (!empty($this->post['others'])) {
                $userProvider->others = $this->post['others'];
            }
            if (!empty($this->post['cnpj'])) {
                $userProvider->cnpj = $this->post['cnpj'];
            }
            if (!empty($this->post['priceRange'])) {
                $userProvider->price_range = $this->post['priceRange'];
            }
            if (!empty($this->post['priceRangeHealthPlan'])) {
                $userProvider->price_range_health_plan = $this->post['priceRangeHealthPlan'];
            }
            if (!empty($this->post['instagramUrl'])) {
                $userProvider->instagram_url = $this->post['instagramUrl'];
            }
            if (!empty($this->post['facebookUrl'])) {
                $userProvider->facebook_url = $this->post['facebookUrl'];
            }
            if (!empty($this->post['linkedinUrl'])) {
                $userProvider->linkedin_url = $this->post['linkedinUrl'];
            }
            if (!empty($this->post['profileText'])) {
                $userProvider->profile_text = $this->post['profileText'];
            }
            if (!empty($this->post['startHour'])) {
                $userProvider->start_hour = $this->post['startHour'];
            }
            if (!empty($this->post['endHour'])) {
                $userProvider->end_hour = $this->post['endHour'];
            }
            if (!empty($this->post['gap'])) {
                $userProvider->gap = $this->post['gap'];
            }
            if (!empty($this->post['guidelines'])) {
                $userProvider->guidelines = $this->post['guidelines'];
            }
            //address//
            if (!empty($this->post['city'])) {
                $userAddress->city = $this->post['city'];
            }
            if (!empty($this->post['state'])) {
                $userAddress->state = $this->post['state'];
            }
            if (!empty($this->post['street'])) {
                $userAddress->street = $this->post['street'];
            }
            if (!empty($this->post['number'])) {
                $userAddress->number = $this->post['number'];
            }
            if (!empty($this->post['complement'])) {
                $userAddress->complement = $this->post['complement'];
            }
            if (!empty($this->post['district'])) {
                $userAddress->district = $this->post['district'];
            }
            if (!empty($this->post['zipCode'])) {
                if (strlen($this->post['zipCode']) == 7) {
                    $userAddress->zip_code = sprintf("%'.08d\n", $this->post['zipCode']);
                } else {
                    $userAddress->zip_code = $this->post['zipCode'];
                }
            }
            
            $user->save();
            $userProvider->save();
            $userAddress->save();
            
            $transaction->commit();
            
            $response = $this->response(['message' => "Usuário atualizado com sucesso."]);
            
            return $response;
            
        } else {
            return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
        }
    }
    
    
    /**
     * @SWG\Put(
     *    path = "/user-provider/delete-user-provider",
     *    tags = {"UserProvider"},
     *    operationId = "deleteUserProvider",
     *    summary="delete user",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *	  @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionDeleteUserProvider() : array {
        if ($this->theTokenSentIsNotThePrimaryToken()) {
            try {
                /** @var UserProviderService $service */
                $service = Yii::$app->getModule('user-provider')->get('userProviderService');
                /** @var UserProvider $UserProvider */
                $user = $service->deleteProvider($this->currentUser->id);
                
                if (empty($user)) {
                    $response = $this->response(['message' => 'Ocorreu um erro ao deletar o Usuário'], HttpCode::BAD_REQUEST_400);
                    return $response;
                }
                $response = $this->response(['message' => "Usuário deletado com sucesso."]);
            } catch (\Exception $e) {
                $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
            }
            return $response;
            
        } else {
            return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
        }
    }
    
    
    /**
     * @SWG\Post(
     *    path = "/user-provider/upload-avatar",
     *    tags = {"UserProvider"},
     *    operationId = "uploadAvatar",
     *    summary="Upload an image to user avatar",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *    @SWG\Parameter(name="avatar", type="file", format="binary", description="Avatar image", in="formData"),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionUploadAvatar() {
        try {
            if ($this->theTokenSentIsNotThePrimaryToken()) {
                $avatar = $this->post['avatar'] ?? NULL;
                
                if (!$avatar) {
                    return $this->response(['message' => 'Arquivo de imagem para o avatar não foi informado'], HttpCode::BAD_REQUEST_400);
                } else {
                    $nome_arquivo = 'avatar-' . $this->currentUser->id . '.' . rand(999, 99999) . ".jpg";
                    $avatar = str_replace("data:image/jpeg;base64", "", $avatar);
                    $avatar = str_replace("data:image/jpg;base64", "", $avatar);
                    $avatar = str_replace(" ", "+", $avatar);
                    $avatar = base64_decode($avatar);
                    
                    if (file_put_contents('../../frontend/web/uploads/user-provider/' . $nome_arquivo, $avatar)) {
                        /** @var UserService $service */
                        $service = Yii::$app->getModule('user-provider')->get('userProviderService');
                        /** @var User $user */
                        if ($service->updateAvatar($this->currentUser->id, $nome_arquivo)) {
                            return $this->response(['message' => 'Avatar atualizado com sucesso'], HttpCode::OK_200);
                        }
                    } else {
                        return $this->response(['message' => 'Ocorreu um erro ao atualizar o avatar'], HttpCode::INTERNAL_SERVER_ERROR_500);
                        
                    }
                }
            } else {
                return $this->response(['message' => 'Unauthorized'], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        
    }
    
    
    /**
     * @SWG\Post(
     *    path = "/user-provider/new-favorite",
     *    tags = {"UserProvider"},
     *    operationId = "newFavorite",
     *    summary="add a favorite provider",
     *    produces = {"application/json"},
     *    consumes = {"application/json"},
     *    @SWG\Parameter(ref="#/parameters/final_bearer"),
     *	  @SWG\Parameter(in="body",name="body",required=true,type="string",
     *       @SWG\Schema(
     *            type="object",
     *            @SWG\Property(property="providerId", type="string", example="provider Id"),
     *         ),
     *    ),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     *)
     */
    public function actionNewFavorite() : array {
        try {
            /** @var UserProviderService $service */
            $service = Yii::$app->getModule('user-provider')->get('userProviderService');
            /** @var UserProvider $UserProvider */
            $user = FavoriteProvider::find()->andWhere(['user_client_id' => $this->currentUser->id])->andWhere(['user_provider_id' => $this->post['providerId']])->one();
            if (!$user) {
                $response = $service->newFavorite($this->currentUser->id, $this->post['providerId']);
            } else {
                $response = $service->deleteFavorite($this->currentUser->id, $this->post['providerId']);
            }
            if ($response) {
                $response = $this->response(['message' => 'Adicionado aos favoritos com sucesso.']);
            } else {
                $response = $this->response(['message' => 'Ocorreu um erro ao adicionar aos favoritos'], HttpCode::BAD_REQUEST_400);
            }
            
        } catch (\Exception $e) {
            $response = $this->response(['message' => "Ocorreu um erro, por favor, contate o suporte.: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
        
    }
    
    
    /**
     * @SWG\Get(
     *  path = "/user-provider/list-available-time",
     *  tags = {"UserProvider"},
     *  operationId = "ListAvailableTime",
     *  summary="name data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *    @SWG\Parameter(name="providerId", in="query", type="string", description="user provider id", required=true),
     *    @SWG\Parameter(name="date", in="query", type="string", description="date", required=true),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionListAvailableTime() : array {
        try {
            $service = Yii::$app->getModule('user-provider')->get('userProviderService');
            $time = $service->ListAvailableTime($this->get['providerId'], $this->get['date']);
            
            if ($this->get['providerId'] === NULL) {
                return $this->response(['message' => "nenhum id enviado"], HttpCode::UNAUTHORIZED_401);
            }
            
            if (!empty($time)) {
                $response = $time;
                return $this->response($response);
            } else {
                return $this->response(['message' => "nenhum horario agendado"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    /**
     * @SWG\Get(
     *  path = "/user-provider/search-crm",
     *  tags = {"UserProvider"},
     *  operationId = "SearchCrm",
     *  summary="news data",
     *  produces = {"application/json"},
     *  consumes = {"application/json"},
     *	@SWG\Parameter(ref="#/parameters/primary_bearer"),
     *  @SWG\Parameter(name="crm", in="query", type="string", description="search by crm", required=false),
     *    @SWG\Response(response = 200, description = "Success"),
     *    @SWG\Response(response = 400, description = "Bad Request"),
     *    @SWG\Response(response = 401, description = "Unauthorized"),
     *    @SWG\Response(response = 500, description = "Internal Server Error")
     * )
     */
    public function actionSearchCrm() : array {
        try {
            $service = Yii::$app->getModule('user-provider')->get('userProviderService');
            $user = $service->SearchCrm($this->get['crm']);
            
            
            if (!empty($user)) {
                $response = $user;
                return $this->response($response);
            } else {
                return $this->response(['message' => "Unauthorized"], HttpCode::UNAUTHORIZED_401);
            }
        } catch (Exception $e) {
            $response = $this->response(['message' => "An exception was occurred, please, contact the IT. Error: {$e->getMessage()}"], HttpCode::SERVICE_UNAVAILABLE_503);
        }
        return $response;
    }
    
    
    public function actionSendNewProviderEmail($email, $name) {
        $template = 'layouts/activateUser';
        try {
            $subject = 'Bem-vindo ao Better Baby';
            $mailer = Yii::$app->mailer;
            $emailSend = $mailer->compose($template,
                [
                    'name' => $name,
                ]
            )
                ->setFrom([$_ENV['SENDER_EMAIL'] => $_ENV['SENDER_NAME']])
                ->setTo($email)
                ->setSubject($subject)
                ->send();
            if (!$emailSend) {
                return FALSE;
            }
            
        } catch (Exception $e) {
            return FALSE;
        }
        return TRUE;
    }
    
    
}









