<?php

namespace api\modules\userProvider;
use common\modules\user\models\User;

class Module extends \common\modules\userProvider\Module
{
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    public $defaultRoute = 'userProvider/index';
    
    public function init()
    {
        parent::init();
    }
}
