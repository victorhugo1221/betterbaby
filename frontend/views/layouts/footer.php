<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class=" container-fluid ">
    <nav>
        <ul>
            <li>
                <a href="https://www.betterbaby.com.br">
                    Better Baby
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/politica-de-privacidade'], TRUE) ?>">
                    Politica de Privacidade
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/ajuda'], TRUE) ?>">
                    Ajuda
                </a>
            </li>
        </ul>
    </nav>
    <div class="copyright" id="copyright">
        &copy;
        <script>
            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
        </script>
         <a href="https://www.betterbaby.com.br" target="_blank">Better Baby</a> | <a href="https://www.edfy.com.br" target="_blank">Edfy</a>.
    </div>
</div>