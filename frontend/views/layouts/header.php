<?php

use yii\helpers\Html;
use common\components\Url;

?>
<?= Html::csrfMetaTags() ?>
<?php $this->beginPage() ?>

<nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-toggle">
                <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <?php
            $name = Yii::$app->user->identity->name;
            ?>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">


                <a class="navbar-brand">Bem-vindo, <?php echo $name ?></a>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="now-ui-icons users_single-02"></i>
                        <p>
                            <span class="d-lg-none d-md-block">Minha conta</span>
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="<?= Url::to(['/minha-conta'], TRUE) ?>">Minha Conta</a>
                        <a class="dropdown-item" href="<?= Url::to(['/planos'], TRUE) ?>">Planos</a>
                        <a class="dropdown-item" href="<?= Url::to(['/ajuda'], TRUE) ?>">Ajuda</a>
                        <a class="dropdown-item" href="<?= Url::to(['/sair'], TRUE) ?>">Sair</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>