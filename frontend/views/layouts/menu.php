<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>

<body class="">
<div class="wrapper ">
    <div class="sidebar" data-color="orange">
        <div class="logo">
            <a href="<?= Url::to(['/dashboard'], true) ?>" class="simple-text logo-normal">
                <img src="./images/logo.png" style="width: 50%; justify-content: center; margin-right: auto; margin-left: auto; display: block;margin-top: 15px;" alt="logo">
            </a>
        </div>
        <div class="sidebar-wrapper" id="sidebar-wrapper">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?= Url::to(['/dashboard'], true) ?>">
                        <i class="fas fa-chart-line"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/agenda'], true) ?>">
                        <i class="far fa-calendar-alt"></i>
                        <p>Agenda</p>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/atendimentos'], true) ?>">
                        <i class="fas fa-users"></i>
                        <p>Atendimentos</p>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/avaliacoes'], true) ?>">
                        <i class="far fa-star"></i>
                        <p>Avaliações</p>
                    </a>
                </li>

                <li>
                    <a  href="<?= Url::to(['/planos'], TRUE) ?>">
                        <i class="fas fa-user-tie"></i>
                        <p>Planos</p>
                    </a>
                </li>
               
                <li>
                    <a href="<?= Url::to(['/minha-conta'], true) ?>">
                        <i class="fas fa-user-tie"></i>
                        <p>Meus Dados</p>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/configuracoes'], true) ?>">
                        <i class="fas fa-cogs"></i>
                        <p>Dados da Conta</p>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/logout'], true) ?>">
                        <i class="fas fa-sign-out-alt"></i>
                        <p>Sair</p>
                    </a>
                </li>
            </ul>
        </div>
       
<!--        <div class="main-panel" id="main-panel">-->
<!---->
<!--        <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">-->
<!--            <div class="container-fluid">-->
<!--                <div class="navbar-wrapper">-->
<!--                    <div class="navbar-toggle">-->
<!--                        <button type="button" class="navbar-toggler">-->
<!--                            <span class="navbar-toggler-bar bar1"></span>-->
<!--                            <span class="navbar-toggler-bar bar2"></span>-->
<!--                            <span class="navbar-toggler-bar bar3"></span>-->
<!--                        </button>-->
<!--                    </div>-->
<!--                    <a class="navbar-brand" href="#pablo">Dashboard</a>-->
<!--                </div>-->
<!--                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">-->
<!--                    <span class="navbar-toggler-bar navbar-kebab"></span>-->
<!--                    <span class="navbar-toggler-bar navbar-kebab"></span>-->
<!--                    <span class="navbar-toggler-bar navbar-kebab"></span>-->
<!--                </button>-->
    </div>
    