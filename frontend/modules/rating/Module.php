<?php

namespace frontend\modules\rating;

class Module extends \common\modules\solicitation\Module {
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';

    public function init()
    {
        parent::init();
    }
}
