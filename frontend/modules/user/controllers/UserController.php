<?php

namespace frontend\modules\user\controllers;

use common\components\Service;
use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\address\models\Address;
use common\modules\adminGw\exceptions\TokenException;
use common\modules\adminGw\models\Page;
use common\modules\healthPlan\models\HealthPlan;
use common\modules\user\models\ForgotMyPasswordForm;
use common\modules\user\models\LoginForm;
use common\modules\user\models\NewPasswordForm;
use common\modules\user\models\User;
use common\modules\user\models\UserClient;
use common\modules\userProvider\models\UserProvider;
use common\modules\userProviderHealthPlan\models\UserProviderHealthPlan;
use grupow\base\exceptions\EntityNotFoundException;
use grupow\base\exceptions\ValidationException;
use grupow\rest\components\ActionStatus;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use common\models\Pais;
use common\components\Utils;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;


class UserController extends Controller {
    
    
    const ACTION_CREATE = 1;
    const ACTION_UPDATE = 2;
    
    private $userProviderService;
    private ?object $service;
    
    
    public function init() {
        parent::init();
        Yii::$app->errorHandler->errorAction = 'user/default/error';
        $this->service = Yii::$app->getModule('user')->get('userService');
        $this->userProviderService = Yii::$app->getModule('user-provider')->get('userProviderService');
        return $this;
    }
    
    
    public function actionIndex() {
        return $this->render('index');
    }
    
    public function actionPasswordChanged() {
        return $this->render('password-changed');
    }
    
    
    public function actionCreate() {
        $model = new User();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            $errors = [];
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors = array_merge($errors, ActiveForm::validate($model));
                return $errors;
            }
            
            $id = $this->userProviderService->createProvider($model);
            
            if ($id) {
//                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnInsert($this->pageTitle, 'o'));
                $permissions = !empty($post['User']['new_permissions']) ? $post['User']['new_permissions'] : [];
                $this->savePermissions($id, $permissions);
                
                $newPermissions = $this->permissionService->getPermissionsByUserListbox($model->getId());
                if (!empty($newPermissions)) {
                    $this->permissionService->saveLog($id, 1, NULL, $newPermissions);
                }
                
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_CREATE);
                return $this->redirect([$redirect]);
            } else {
//                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsert($this->pageTitle, 'o'));
            }
        }
        return $this->render('create', ['model' => $model, 'pageTitle' => $this->pageTitle]);
    }
    
    
    public function actionUpdate($id) {
        $errors = [];
        $modelUser = User::find()->where(['IS', User::tableName() . '.deleted', NULL])->andWhere(['id' => $id])->one();
        if ($modelUser == NULL) {
            Yii::$app->session->setFlash('error', DefaultMessage::registerNotFound($this->pageTitle, 'o'));
            return $this->redirect(['index']);
        }
        $modelUserClient = UserClient::find()->andWhere(['>', 'user_client.status', -1])->all();
        
        $newPassword = new NewPasswordForm();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $modelUser->load($post);
            $modelUserClient->load($post);
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors = array_merge($errors, ActiveForm::validate($modelUser));
                $errors = array_merge($errors, ActiveForm::validate($modelUserClient));
                return $errors;
            }
            
            if ($this->dataService->updateClient($modelUser, $modelUserClient)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle, 'o'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'user', $modelUser->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle, 'o'));
                return $this->redirect(['user/update/' . $modelUser->id]);
            }
        } else {
            return $this->render('update', ['model' => $modelUser, 'modelUserClient' => $modelUserClient, 'newPassword' => $newPassword, 'pageTitle' => $this->pageTitle]);
        }
    }
    
    public function actionDelete($id) {
        if ($this->dataService->deleteClient($id)) {
            Yii::$app->session->setFlash('success', DefaultMessage::sucessOnDelete($this->pageTitle, 'o'));
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', DefaultMessage::errorOnDelete($this->pageTitle, 'o'));
            return $this->redirect(['user/update/' . $id]);
        }
    }
    
    public function formatErrors() {
        $service = new Service();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return call_user_func_array([$service, 'formatErrors'], func_get_args());
    }
    
    
    public function actionForgotMyPassword() {
        $service = $this->service;
        $form = new ForgotMyPasswordForm();
        if (Yii::$app->request->isPost) {
            $form->load(Yii::$app->request->post());
            try {
                if (Yii::$app->request->get('validation', TRUE)) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $service->validate($form);
                    return [];
                }
                $service->requestNewPassword($form);
                $message = 'Solicitação de troca de senha enviada com sucesso.';
                if (Yii::$app->request->isAjax) {
                    return $this->actionStatus($message);
                }
                Yii::$app->session->setFlash('success', $message);
                return $this->refresh();
            } catch (ValidationException $e) {
                if (Yii::$app->request->isAjax) {
                    return $this->formatErrors($form);
                }
            }
        }
        
        return $this->render('forgot-my-password', ['model' => $form]);
    }
    
    public function actionStatus($mensagem, $status = 200, $meta = []) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->serializer->serialize(new ActionStatus($mensagem, $status, $meta));
    }
    
    
    public function actionChangePassword($token) {
        $service = $this->service;
        $tokenService = Yii::$app->getModule('admin-gw')->get('tokenService');
        
        $status = NULL;
        
        try {
            $token = $tokenService->findByUid($token);
        } catch (EntityNotFoundException $e) {
            $status = new ActionStatus('Token inválido', 400);
        }
        
        $form = new NewPasswordForm();
        
        if (Yii::$app->request->isPost) {
            $form->load(Yii::$app->request->post());
            
            try {
                if (Yii::$app->request->get('validation', FALSE)) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $service->validate($form);
                    return [];
                }
                
                $service->recoveryPassword($form, $token);
                Yii::$app->session->setFlash('success', 'Sua senha foi definida com successo!');
                return $this->redirect(['/password-changed']);
                
            } catch (ValidationException $e) {
                if (Yii::$app->request->isAjax) {
                    return $this->formatErrors($form);
                }
            } catch (TokenException $e) {
                $status = new ActionStatus($e->getMessage(), $e->getError());
            }
        }
        return $this->render('change-password', ['model' => $form, 'token' => $token, 'status' => $status]);
    }
    
    
    public function actionRegisterhome() : string {
        return $this->render('registerHome');
    }
    
    public function actionRegisterdoctor() {
        
        $model = new User();
        $modelUserProvider = new UserProvider();
        $modelAddress = new Address();
        $modelUserProviderHealthPlan = new UserProviderHealthPlan();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            $modelUserProvider->load($post);
            $modelAddress->load($post);
            $modelUserProviderHealthPlan->load($post);
            
            $errors = [];
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors = array_merge($errors, ActiveForm::validate($model));
                $errors = array_merge($errors, ActiveForm::validate($modelUserProvider));
                $errors = array_merge($errors, ActiveForm::validate($modelAddress));
                $errors = array_merge($errors, ActiveForm::validate($modelUserProviderHealthPlan));
                return $errors;
            }
            $password = $model->password;
            $userId = $this->userProviderService->createUser($model, $password);
            if ($userId > 0) {
                $providerId = $this->userProviderService->createUserProvider($modelUserProvider, $userId);
                $addressId = $this->userProviderService->createAddress($modelAddress, $userId);
                $userProviderHealthPlanId = $this->userProviderService->createUserProviderHealthPlan($modelUserProviderHealthPlan, $userId);
            }
            
            if ($userId && $addressId && $providerId && $userProviderHealthPlanId) {
                return $this->redirect(['/login']);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsertFront());
            }
        }
        return $this->render('registerDoctor', ['model' => $model, 'modelUserProvider' => $modelUserProvider, 'modelAddress' => $modelAddress, 'modelUserProviderHealthPlan' => $modelUserProviderHealthPlan]);
    }
    
    
    public function actionRegisterservice() {
        $model = new User();
        $modelUserProvider = new UserProvider();
        $modelAddress = new Address();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            $modelUserProvider->load($post);
            $modelAddress->load($post);
            
            $errors = [];
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors = array_merge($errors, ActiveForm::validate($model));
                $errors = array_merge($errors, ActiveForm::validate($modelUserProvider));
                $errors = array_merge($errors, ActiveForm::validate($modelAddress));
                return $errors;
            }
            $password = $model->password;
            $userId = $this->userProviderService->createUser($model, $password);
            if ($userId > 0) {
                $providerId = $this->userProviderService->createUserProvider($modelUserProvider, $userId);
                $addressId = $this->userProviderService->createAddress($modelAddress, $userId);
            }
            
            if ($userId && $addressId && $providerId) {
                return $this->redirect(['/login']);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsertFront());
            }
        }
        return $this->render('registerService', ['model' => $model, 'modelUserProvider' => $modelUserProvider, 'modelAddress' => $modelAddress]);
    }
    
    
    public function actionRegisterclinic() {
        $model = new User();
        $modelUserProvider = new UserProvider();
        $modelAddress = new Address();
        $modelUserProviderHealthPlan = new UserProviderHealthPlan();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            $modelUserProvider->load($post);
            $modelAddress->load($post);
            $modelUserProviderHealthPlan->load($post);
            
            $errors = [];
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors = array_merge($errors, ActiveForm::validate($model));
                $errors = array_merge($errors, ActiveForm::validate($modelUserProvider));
                $errors = array_merge($errors, ActiveForm::validate($modelAddress));
                $errors = array_merge($errors, ActiveForm::validate($modelUserProviderHealthPlan));
                return $errors;
            }
            
            $id = $this->userProviderService->createDoctor($model);
            
            if ($id) {
                return $this->redirect(['/login']);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsertFront());
            }
        }
        return $this->render('registerClinic', ['model' => $model, 'modelUserProvider' => $modelUserProvider, 'modelAddress' => $modelAddress, 'modelUserProviderHealthPlan' => $modelUserProviderHealthPlan]);
    }
    
    
    //login
    
    public function actionPainel() : Response {
        return $this->redirect(['/dashboard']);
    }
    
    public function actionLogout() : Response {
        $this->service->logout();
        return $this->goHome();
    }
    
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $authenticationService = Yii::$app->getModule('admin-gw')->get('authenticationService');
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $authenticationService->loginClientWeb($model);
            if ($user) {
                return $this->redirect('/dashboard');
            } else {
                $this->returnsUserOrPasswordIncorrect($model);
            }
        }
        return $this->render('login', ['model' => $model]);
    }
    
    private function returnsUserOrPasswordIncorrect($model) {
        $model->addError('login', 'Os dados informados não conferem');
    }
    
    
    //profiles
    
    public function actionProfiledoctor() {
        $id = Yii::$app->user->identity->id;
        $errors = [];
        $model = User::find()->where(['IS', User::tableName() . '.deleted', NULL])->andWhere(['id' => $id])->one();
        $modelUserProvider = UserProvider::find()->where(['user_id' => $model->id])->one();
        $modelAddress = Address::find()->andWhere(['user_id' => $model->id])->one();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            $modelUserProvider->load($post);
            $modelAddress->load($post);
            
            foreach (UserProvider::$_directory_file as $key => $file) {
                $this->uploadedFile($key, $modelUserProvider);
            }
            
            $errors = [];
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors = array_merge($errors, ActiveForm::validate($model));
                $errors = array_merge($errors, ActiveForm::validate($modelUserProvider));
                $errors = array_merge($errors, ActiveForm::validate($modelAddress));
                return $errors;
            }
            
            if ($this->userProviderService->updateUserProvider($model, $modelUserProvider, $modelAddress)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdateFront());
                return $this->redirect(["/minha-conta"]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdateFront());
                return $this->redirect(['/minha-conta']);
            }
        } else {
            return $this->render('profileDoctor', ['model' => $model, 'modelUserProvider' => $modelUserProvider, 'modelAddress' => $modelAddress,]);
        }
    }
    
    
    public function uploadedFile($field, $modelUserProvider) {
        
        if (!empty(UploadedFile::getInstance($modelUserProvider, $field))) {
            $modelUserProvider->{$field} = UploadedFile::getInstance($modelUserProvider, $field);
            $modelUserProvider->validate();
            if (!isset($modelUserProvider->errors[$field])) {
                $modelUserProvider->upload($field);
            }
        } else {
            $modelUserProvider->{$field} = $modelUserProvider->getOldAttribute($field);
        }
    }
    
    
}
