<?php

use common\modules\healthPlan\models\HealthPlan;
use common\modules\specialty\models\Specialty;
use common\modules\state\models\State;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Cadastro</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet"/>
    <link href="<?= Url::base() ?>./css/demo.css" rel="stylesheet"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>

<style>
    .bodylogin {
        background-image: linear-gradient(to right, #5ec0d0, #d49ec7);
    }
</style>
<?php
$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['/dashboard'], TRUE),
    ],
    'enableAjaxValidation'   => TRUE,
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>
<body class="bodylogin">
<div class="container">
    <form>
    <div class="row">
        <div class="col-sm-10 col-md-10 col-lg-10 mx-auto">
            <div class="card border-0 shadow rounded-3 my-5">
                <div class="card-body p-4 p-sm-5">
                    <h5 class="card-title text-center mb-5 fw-light fs-5">Cadastre-se</h5>
                    <div class="row">
                        <div class=" col-md-4">
                            <div class="form-group">
                                <?= $form->field($modelUserProvider, 'crm')->textInput(['id'=>"crm",'class' => 'form-control form_field-input', 'maxlength' => 8, 'required' => true]); ?>
                                <span style="color:red;margin-top: 0px;display: inline-block;" data-crm-validator></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" style="margin-top: 0px">
                                <label for="exampleInputEmail1">Estado</label>
                                <select class="form-control" id="uf">
                                    <option></option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                    <option value="EX">Estrangeiro</option>
                                </select>
                            </div>
                        </div>
                        <div class=" col-md-5">
                            <?= $form->field($modelUserProvider, 'crm_specialty')->textInput(['id'=>'especialidade','class' => 'form-control form_field-input', 'maxlength' => TRUE, 'disabled' => TRUE,]); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'name')->textInput(['id'=>'nome','class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?php echo $form->field($model, 'cpf')->widget(MaskedInput::class, ['mask' => '999.999.999-99']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'email')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'cellphone')->textInput(['maxlength' => TRUE, 'data-mask' => '(99) 99999-9999']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php
                                echo $form->field($modelUserProvider, 'additional_specialty')->widget(Select2::classname(), [
                                    'data'     => ArrayHelper::map(Specialty::find()->all(), 'id', 'name'),
                                    'language' => 'pt',
                                    'options'  => [
                                        'id'          => 'especialidades',
                                        'name'        => 'especialidades',
                                        'multiple'    => TRUE,
                                        'placeholder' => 'Selecione uma especialidade...',
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-4">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'zip_code')->textInput(['id' => 'cep', 'class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'state')->textInput(['id' => 'estado', 'class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'city')->textInput(['id' => 'cidade', 'class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'district')->textInput(['id' => 'bairro', 'class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'street')->textInput(['id' => 'logradouro', 'class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'number')->textInput(['id' => 'numero', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'complement')->textInput(['id' => 'complemento','class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-12 d-flex justify-content-center">
                            <h5 class="card-title text-center mb-3 fw-light fs-5">Selecione o tipo de atendimento</h5>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <section class="app">
                            <article class="feature1">
                                <input type="checkbox" name="particular" id="particular"/>
                                <div>
                                    <span>Particular</span>
                                </div>
                            </article>

                            <article class="feature2">
                                <input type="checkbox" name="comconvenio" id="comconvenio"/>
                                <div>
                                    <span>Convênio</span>
                                </div>
                            </article>
                        </section>
                    </div>
                    <div class="row">
                        <div class=" col-md-4 " id="precomedio" name="precomedio">
                            <div class="form-group">
                                <?php
                                echo $form->field($modelUserProvider, 'price_range')->widget(Select2::class, [
                                    'name'    => 'kv-type-01',
                                    'data'    => [1 => "Até R$150", 2 => "De R$150 Até R$300", 3 => "De R$300 Até R$500", 4 => "Acima de R$500"],
                                    'options' => [
                                        'placeholder' => 'Selecione uma faixa de valor ...',
                                        'options'     => [
                                            1 => ['data-level' => '1'],
                                            2 => ['data-level' => '2'],
                                            3 => ['data-level' => '3'],
                                            4 => ['data-level' => '4'],
                                        ],
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4 " id="convenio" name="convenio">
                            <div class="form-group">
                                <?php
                                echo $form->field($modelUserProviderHealthPlan, 'health_plan_id')->widget(Select2::classname(), [
                                    'data'     => ArrayHelper::map(healthPlan::find()->all(), 'id', 'title'),
                                    'language' => 'pt',
                                    'options'  => [
                                        'id'          => 'convenios',
                                        'name'        => 'convenios',
                                        'multiple'    => TRUE,
                                        'placeholder' => 'Selecione um convenio ...',
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="form-group">
                                <?php
                                echo $form->field($modelUserProvider, 'accept_pne')->widget(Select2::classname(), [
                                    'name'    => 'accept-pne',
                                    'data'    => [1 => "Sim", 2 => "Não"],
                                    'options' => [
                                        'placeholder' => 'Selecione uma opção ...',
                                        'options'     => [
                                            1 => ['data-level' => '1'],
                                            2 => ['data-level' => '2'],
                                        ],
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'password')->textInput(['class' => 'form-control form_field-input', 'minlength' => '6', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                    </div>
                    <div class="container">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" required>
                        <label class="form-check-label" for="inlineCheckbox1">Li e aceito a <a href="<?= Url::to(['/politica-de-privacidade'], TRUE) ?>">Politica de Privacidade</a></label>
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-center">
                            <?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary btn-lg', 'name' => 'entrar']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= Url::base() ?>./js/main.js"></script>
<?php ActiveForm::end(); ?>
<script>
    $(function () {
        $('div[name="precomedio"]').hide();

        //show it when the checkbox is clicked
        $('input[name="particular"]').on('click', function () {
            if ($(this).prop('checked')) {
                $('div[name="precomedio"]').fadeIn();
            } else {
                $('div[name="precomedio"]').hide();
            }
        });
    });
    $(function () {
        $('div[name="convenio"]').hide();

        //show it when the checkbox is clicked
        $('input[name="comconvenio"]').on('click', function () {
            if ($(this).prop('checked')) {
                $('div[name="convenio"]').fadeIn();
            } else {
                $('div[name="convenio"]').hide();
            }
        });
    });
</script>
<script type="text/javascript">
    $("#cep").focusout(function () {
        //Início do Comando AJAX
        $.ajax({
            //O campo URL diz o caminho de onde virá os dados
            //É importante concatenar o valor digitado no CEP
            url: 'https://viacep.com.br/ws/' + $(this).val() + '/json/',
            //Aqui você deve preencher o tipo de dados que será lido,
            //no caso, estamos lendo JSON.
            dataType: 'json',
            //SUCESS é referente a função que será executada caso
            //ele consiga ler a fonte de dados com sucesso.
            //O parâmetro dentro da função se refere ao nome da variável
            //que você vai dar para ler esse objeto.
            success: function (resposta) {
                //Agora basta definir os valores que você deseja preencher
                //automaticamente nos campos acima.
                $("#logradouro").val(resposta.logradouro);
                $("#complemento").val(resposta.complemento);
                $("#bairro").val(resposta.bairro);
                $("#cidade").val(resposta.localidade);
                $("#estado").val(resposta.uf);
                //Vamos incluir para que o Número seja focado automaticamente
                //melhorando a experiência do usuário
                $("#numero").focus();
            }
        });
    });
    </script>
</body>
</html>


