<?php

use common\widgets\Alert;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Planos
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet" />
</head>

<body class="">
<?php $url = Yii::$app->basePath.'./views/layouts/menu.php';
include $url; ?>
<div class="main-panel" id="main-panel">
    <?php $url = Yii::$app->basePath.'./views/layouts/header.php';
    include $url; ?>
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto">
                <div class="card card-upgrade">
                    <div class="card-header text-center">
                        <h4 class="card-title">Adquira um plano e otimize seu trabalho</h4>
                            <p class="card-category">Seu plano <strong>Free</strong>  vence em <strong>12</strong>  dias</p>
                    </div>
                    <div class="card-body">


                        <div class="container">
                            <div class="card-deck mb-3 text-center">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-header">
                                        <h4 class="my-0 font-weight-normal">Mensal</h4>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="card-title pricing-card-title">R$59,89 <small class="text-muted">/ mes</small></h3>
                                        <ul class="list-unstyled mt-3 mb-4">
                                            <li>Pagamento recorrente mensal</li>
                                        </ul>
                                        <a href="https://www.mercadopago.com.br/subscriptions/checkout?preapproval_plan_id=2c9380847cc4c817017cc7ec18af0277" target="_blank"><button type="button" class="btn btn-lg btn-block btn-light">Comprar</button></a>
                                    </div>
                                </div>
                                <div class="card mb-4 box-shadow">
                                    <div class="card-header">
                                        <h4 class="my-0 font-weight-normal">Semestral</h4>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="card-title pricing-card-title">R$47,90 <small class="text-muted">/ mes</small></h3>
                                        <ul class="list-unstyled mt-3 mb-4">
                                            <li>Pagamento unico válido por 6 meses</li>

                                        </ul>
                                        <button type="button" class="btn btn-lg btn-block btn-light">Comprar</button>
                                    </div>
                                </div>
                                <div class="card mb-4 box-shadow">
                                    <div class="card-header">
                                        <h4 class="my-0 font-weight-normal">Anual</h4>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="card-title pricing-card-title">R$44,90 <small class="text-muted">/ mes</small></h3>
                                        <ul class="list-unstyled mt-3 mb-4">
                                            <li>Pagamento unico válido por 12 meses</li>
                                        </ul>
                                        <button type="button" class="btn btn-lg btn-block btn-light">Comprar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?include "footer.php";?>
    </div>
</div>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
</body>

</html>