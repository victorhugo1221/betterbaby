<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = $_ENV['PROJECT_NAME'] . ' - Esqueci minha senha';
$this->registerJsFile('js/require.js', ['data-main' => 'js/forgot-my-password.js']);
if (Yii::$app->session->hasFlash('forgot-my-password')) {
    $this->registerJs();
}
?>

    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    </head>

    <style>
        .bodylogin {
            background-image: linear-gradient(to right, #5ec0d0, #d49ec7);
        }
    </style>
    
    <?php $form = ActiveForm::begin([
        'id'                   => 'forgot-my-password',
        'fieldConfig'          => [
            'template' => "{label}\n{input}\n{error}",
        ],
        'method'               => 'POST',
        'validationUrl'        => Url::to(['/esqueci-minha-senha', 'validation' => TRUE], TRUE),
        'options'              => ['class' => 'm-t ajax-form'],
        'enableAjaxValidation' => TRUE,
        'validateOnChange'     => FALSE,
        'validateOnBlur'       => FALSE,
    ]); ?>

    <body class="bodylogin">
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-9 col-lg-9 mx-auto">
                <div class="card border-0 shadow rounded-3 my-5">
                    <div class="card-body p-4 p-sm-5">
                        <h5 class="card-title text-center mb-5 fw-light fs-5">Redefinir Senha</h5>
                        <form>
                            <div class="form-group">
                                <?= $form->field($model, 'email')->textInput(['class' => 'form-control form-control-lg', 'autofocus' => TRUE, 'placeholder' => 'Digite seu e-mail cadastrado...']) ?>
                                <small id="emailHelp" class="form-text text-muted"><p>será enviado um link no email acima para redefinir a senha.</p></small>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 p-0 mt-2 pt-1 forgot-password">
                                <a href="<?= Url::to(['/login'], TRUE) ?>" class="forgot-password">Voltar para o Login</a>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                                <?= Html::submitButton('Solicitar nova senha', ['class' => 'btn btn-primary', 'name' => 'entrar']) ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>

    </html>

<?php ActiveForm::end(); ?>