<?php

use common\widgets\Alert;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Better baby</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>

<body class="user-profile">

<?php $url = Yii::$app->basePath . '/views/layouts/menu.php';
include $url; ?>
<?php $url = Yii::$app->basePath . '/views/layouts/header.php';
include $url; ?>

<?php
$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['/'], TRUE),
    ],
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>
<div class="main-panel" id="main-panel">
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Editar Perfil</h5>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label>Plano</label>
                                        <input type="text" class="form-control" disabled="" placeholder="Company" value="Medico Mensal">
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <?= $form->field($modelUserProvider, 'crm')->textInput(['class' => 'form-control form_field-input','disabled' => TRUE]); ?>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <?= $form->field($model, 'cpf')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'disabled' => TRUE]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <?= $form->field($model, 'email')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <?= $form->field($model, 'name')->textInput(['class' => 'form-control form_field-input', 'data-mask' => '999.999.999-99', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'cellphone')->widget(MaskedInput::class, ['mask' => '(99) 99999-9999']) ?>
                                    </div>
                                </div>
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($modelAddress, 'zip_code')->textInput(['id' => 'cep', 'class' => 'form-control form_field-input', 'maxlength' => "8"]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($modelAddress, 'state')->textInput(['id' => 'uf', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($modelAddress, 'city')->textInput(['id' => 'cidade', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($modelAddress, 'district')->textInput(['id' => 'bairro', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($modelAddress, 'street')->textInput(['id' => 'logradouro', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($modelAddress, 'number')->textInput(['id' => 'numero', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($modelAddress, 'complement')->textInput(['id' => 'complemento', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <?= $form->field($modelUserProvider, 'instagram_url')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <?= $form->field($modelUserProvider, 'facebook_url')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <?= $form->field($modelUserProvider, 'linkedin_url')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 pr-1">
                                    <div class="form-group">
                                        <?= $form->field($modelUserProvider, 'profile_text')->textarea(['class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                                    </div>
                                </div>
                            </div>
                            <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary btn-lg', 'name' => 'salvar']) ?>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="image">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <?php if ($modelUserProvider->avatar == NULL) { ?>
                                <img class="avatar border-gray" src="./images/default-avatar.png" alt="avatar">
                            <?php } else { ?>
                                <img class="avatar border-gray" src="<?= '/uploads/user-provider/' . $modelUserProvider->avatar; ?>" alt="avatar">
                            <?php } ?>
                            <div class="row d-flex justify-content-center">
                                <div class="col-12">
                                    <?= $form->field($modelUserProvider, 'avatar')->fileInput(['class' => 'btn btn-light', 'maxlength' => TRUE,])->label('<i class="fas fa-upload"></i> Imagem Perfil') ?>
                                    <?php if ($modelUserProvider->avatar && !isset($modelUserProvider->errors['avatar'])) { ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <h5 class="title">
                                <?php echo $model->name; ?>
                            </h5>
                            <h6><?php echo $modelUserProvider->crm_specialty; ?></h6>
                            <span class="fa fa-star checked" style="color: yellow"></span>
                            <span class="fa fa-star checked" style="color: yellow"></span>
                            <span class="fa fa-star checked" style="color: yellow"></span>
                            <span class="fa fa-star checked" style="color: yellow"></span>
                            <span class="fa fa-star checked" style="color: gray"></span>

                        </div>
                        <p class="description text-center">
                            <?= Html::encode($modelUserProvider->profile_text) ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <footer class="footer">
            <?php $url = Yii::$app->basePath . '/views/layouts/footer.php';
            include $url; ?>
        </footer>
    </div>
</div>
</div>
<script type="text/javascript">
    $("#cep").focusout(function () {
        //Início do Comando AJAX
        $.ajax({
            //O campo URL diz o caminho de onde virá os dados
            //É importante concatenar o valor digitado no CEP
            url: 'https://viacep.com.br/ws/' + $(this).val() + '/json/unicode/',
            //Aqui você deve preencher o tipo de dados que será lido,
            //no caso, estamos lendo JSON.
            dataType: 'json',
            //SUCESS é referente a função que será executada caso
            //ele consiga ler a fonte de dados com sucesso.
            //O parâmetro dentro da função se refere ao nome da variável
            //que você vai dar para ler esse objeto.
            success: function (resposta) {
                //Agora basta definir os valores que você deseja preencher
                //automaticamente nos campos acima.
                $("#logradouro").val(resposta.logradouro);
                $("#complemento").val(resposta.complemento);
                $("#bairro").val(resposta.bairro);
                $("#cidade").val(resposta.localidade);
                $("#uf").val(resposta.uf);
                //Vamos incluir para que o Número seja focado automaticamente
                //melhorando a experiência do usuário
                $("#numero").focus();
            }
        });
    });


</script>
<!--   Core JS Files   -->
<script src="<?= Url::base() ?>./js/core/jquery.min.js"></script>
<script src="<?= Url::base() ?>./js/core/popper.min.js"></script>
<script src="<?= Url::base() ?>./js/core/bootstrap.min.js"></script>
<script src="<?= Url::base() ?>./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="<?= Url::base() ?>./js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= Url::base() ?>./js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= Url::base() ?>./js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= Url::base() ?>./js/demo.js"></script>
</body>

</html>