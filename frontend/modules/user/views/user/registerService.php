<?php

use common\modules\healthPlan\models\HealthPlan;
use common\modules\state\models\State;
use kartik\base\InputWidget;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        Cadastro
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

</head>

<style>
    .bodylogin {
        background-image: linear-gradient(to right, #5ec0d0, #d49ec7);
    }
</style>
<?php
$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => '',
        'data-redirect-success' => Url::to(['/dashboard'], TRUE),
    ],
    'enableAjaxValidation'   => FALSE,
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>
<body class="bodylogin">
<div class="container">
    <div class="row">
        <div class="col-sm-10 col-md-10 col-lg-10 mx-auto">
            <div class="card border-0 shadow rounded-3 my-5">
                <div class="card-body p-4 p-sm-5">
                    <h5 class="card-title text-center mb-5 fw-light fs-5">Cadastre-se</h5>
                    <div class="row">
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'name')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?php echo $form->field($model, 'cpf')->widget(MaskedInput::class, ['mask' => '999.999.999-99']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'email')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $form->field($model, 'cellphone')->widget(MaskedInput::class, ['mask' => '(99) 99999-9999']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $form->field($model, 'additional_specialty')->widget(MultipleInput::class, [
                                'max'               => 5,
                                'min'               => 1, // should be at least 2 rows
                                'allowEmptyList'    => FALSE,
                                'enableGuessTitle'  => FALSE,
                                'iconSource' =>    MultipleInput::ICONS_SOURCE_FONTAWESOME,
                                'addButtonPosition' => MultipleInput::POS_ROW, // show add button in the header
                               
                                
                            ])
                                ->label('Tipos de Serviço Prestados');
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-4">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'zip_code')->textInput(['id' => 'cep', 'class' => 'form-control form_field-input', 'maxlength' => "8"]); ?>
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'state')->textInput(['id' => 'uf', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                            </div>
                        </div>
                        <div class=" col-md-4">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'city')->textInput(['id' => 'cidade', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'district')->textInput(['id' => 'bairro', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'street')->textInput(['id' => 'logradouro', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'number')->textInput(['id' => 'numero', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                            </div>
                        </div>
                        <div class=" col-md-6">
                            <div class="form-group">
                                <?= $form->field($modelAddress, 'complement')->textInput(['id' => 'complemento', 'class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'password')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                    </div>
                    <div class="container">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" required>
                        <label class="form-check-label" for="inlineCheckbox1">Li e aceito a <a href="<?= Url::to(['/politica-de-privacidade'], TRUE) ?>">Politica de Privacidade</a></label>
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-center">
                            <?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary btn-lg', 'name' => 'entrar']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#cep").focusout(function () {
        //Início do Comando AJAX
        $.ajax({
            //O campo URL diz o caminho de onde virá os dados
            //É importante concatenar o valor digitado no CEP
            url: 'https://viacep.com.br/ws/' + $(this).val() + '/json/',
            //Aqui você deve preencher o tipo de dados que será lido,
            //no caso, estamos lendo JSON.
            dataType: 'json',
            //SUCESS é referente a função que será executada caso
            //ele consiga ler a fonte de dados com sucesso.
            //O parâmetro dentro da função se refere ao nome da variável
            //que você vai dar para ler esse objeto.
            success: function (resposta) {
                //Agora basta definir os valores que você deseja preencher
                //automaticamente nos campos acima.
                $("#logradouro").val(resposta.logradouro);
                $("#complemento").val(resposta.complemento);
                $("#bairro").val(resposta.bairro);
                $("#cidade").val(resposta.localidade);
                $("#uf").val(resposta.uf);
                //Vamos incluir para que o Número seja focado automaticamente
                //melhorando a experiência do usuário
                $("#numero").focus();
            }
        });
    });
</script>
<?php ActiveForm::end(); ?>
</body>
</html>


