<?php

use common\widgets\Alert;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


echo Alert::widget([
    'alertTypes' => [
        'sucess'   => 'alert-success',
        'error'    => 'alert-danger',
        'facebook' => 'alert-warning',
    ],
    'options'    => [
        'class' => '-msg col-sm-12 text-center',
    ],
]);
$message = Yii::$app->session->getFlash('loginerror', FALSE);
?>

<?php if ($message) { ?>
    <div class="alert alert-danger text-center"> <?= $message; ?></div>
<?php } ?>

<?php if ($model->hasErrors()) { ?>
    <div class="alert alert-danger -msg">
        <?php foreach ($model->getFirstErrors() as $error):
            echo $error;
            break;
        endforeach; ?>
    </div>
<?php } ?>
<?php $form = ActiveForm::begin([
    'id'          => 'form-validation',
    'options'     => ['data-component' => 'form', 'class' => 'ajax-form'],
    'fieldConfig' => [
        'template' => "{label}\n{input}",
    ],
]); ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        Better Baby
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?= Url::base() ?>./css/demo.css" rel="stylesheet"/>
</head>

<style>
    .bodylogin {
        background-image: linear-gradient(to right, #5ec0d0, #d49ec7);
    }

    .field-icon {
        float: right;
        margin-right: 10px;
        margin-top: -35px;
        position: relative;
        z-index: 2;
    }
</style>

<body class="bodylogin">
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-8 col-lg-8 mx-auto">
            <div class="card border-0 shadow rounded-3 my-5">
                <div class="card-body p-4 p-sm-5">
                    <div class="d-flex justify-content-center mb-5">
                        <img src="./images/logo.png" alt="logo" width="180px">
                    </div>
                    <form>
                        <div class="form-group">
                            <?= $form->field($model, 'email')->textInput(['autofocus' => TRUE, 'placeholder' => 'E-mail'])->label("Usuário") ?>
                            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Senha', 'id' => 'show_password'])->label("Senha") ?>
                            <span onclick="showPass()"><i class="fa fa-eye-slash field-icon" style="color: #9a9a9a" aria-hidden="true"></i></span>

                        </div>
                        <div class="form-group">
                            <div class="d-flex justify-content-between">
                                <a href="<?= Url::to(['/esqueci-minha-senha'], TRUE) ?>"><p>Esqueci minha senha</p></a>
                                <a href="<?= Url::to(['/cadastro'], TRUE) ?>"><p>Cadastre-se</p></a>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <?= Html::submitButton('Entrar', ['class' => 'btn btn-primary btn-lg', 'name' => 'entrar']) ?>
                        </div>
                    </form>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    function showPass() {
        var x = document.getElementById("show_password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>

</html>