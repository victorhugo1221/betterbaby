<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        Cadastro
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
</head>

<style>
    .bodylogin {
        background-image: linear-gradient(to right, #5ec0d0, #d49ec7);
    }


    .cardcontent {
        text-align: center;
        justify-content: center;
        color: #eeeeee;
        margin-right: 10px;
        font-size: 25px;
    }

    span {
        font-size: 15px;
    }

    .box {
        padding: 60px 0px;
    }

    .box-part {
        background-color: #5fbfd0;
        padding: 50px 10px;
        margin: 30px 10px;
        min-height: 200px;
        border-radius: 5px;
    }

    .box-part:hover {
        background-color: #5fbfd0;
        box-shadow: inset 0 0 10px #82bfc9, 0 0 10px #7cb3cb;
    }

    .cardcontent:hover {
        color: white;
        text-decoration: none;
    }
</style>

<body class="bodylogin">

<div class="container">
    <div class="row">
        <div class="col-sm-11 col-md-11 col-lg-11 mx-auto">
            <div class="card border-0 shadow rounded-3 my-5">
                <div class=" p-4 p-sm-5">
                    <h5 class="card-title text-center mb-5 fw-light fs-5">Selecione um tipo de cadastro</h5>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="box-part text-center">
                                    <div class="title my-5">
                                        <a class="cardcontent stretched-link" href="<?= Url::to(['/cadastro-medico'], TRUE) ?>"><i style="margin-right: 10px;" class="fas fa-user-md"></i>Médico</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="box-part text-center">
                                    <div class="title my-5">
                                        <a class="cardcontent stretched-link" href="<?= Url::to(['/cadastro-clinica'], TRUE) ?>"> <i style="margin-right: 10px;" class="fas fa-clinic-medical"></i>Clínica</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="box-part text-center">
                                    <div class="title my-5">
                                        <a class="cardcontent stretched-link" href="<?= Url::to(['/cadastro-servico'], TRUE) ?>"><i style="margin-right: 10px;" class="fas fa-user-tag"> </i>Serviço</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</body>

</html>