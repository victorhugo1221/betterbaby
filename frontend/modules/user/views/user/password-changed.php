<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <title>
            Better Baby
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="<?= Url::base() ?>../css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= Url::base() ?>../css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    </head>

    <style>
        .bodylogin {
            background-image: linear-gradient(to right, #5ec0d0, #d49ec7);
        }
    </style>

<body class="bodylogin">
<div class="container">
    <div class="row">
    <div class="col-sm-9 col-md-9 col-lg-9 mx-auto">
        <div class="card border-0 shadow rounded-3 my-5">
            <div class="card-body p-4 p-sm-5">
                <h5 class="card-title text-center mb-5 fw-light fs-5">Senha alterada com sucesso!</h5>
                
            </div>
        </div>
    </div>

