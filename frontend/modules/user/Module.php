<?php

namespace frontend\modules\user;

class Module extends \common\modules\user\Module {
    public $controllerNamespace = __NAMESPACE__ . '\controllers';

    public function init()
    {
        parent::init();
    }
}
