<?php

use common\widgets\Alert;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Planos</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
  
</head>

<body class="">
<?php $url = Yii::$app->basePath.'/views/layouts/menu.php';
include $url; ?>
<?php $url = Yii::$app->basePath.'/views/layouts/header.php';
include $url; ?>
<div class="main-panel" id="main-panel">
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12 ml-auto mr-auto">
                <div class="card card-upgrade">
                    <div class="card-header text-center">
                        <h4 class="card-title">Adquira um plano e otimize seu trabalho</h4>
                            <p class="card-category">Seu plano <strong><?php echo $model->planRel->title; ?></strong>  vence: <strong> <?php echo  date('d/m/Y H:i:m', strtotime($model->end_date)); ?> </strong> </p>
                    </div>
                    <div class="card-body">
                        <div class="">
                            <div class="card-deck mb-3 text-center">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-header">
                                        <h4 class="my-0 font-weight-normal"><?php echo $modelMonthly->title; ?></h4>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="card-title pricing-card-title">R$<?php echo $modelMonthly->price; ?></h3>
                                        <ul class="list-unstyled mt-3 mb-4">
                                            <li>Pagamento recorrente mensal</li>
                                        </ul>
                                        <a class="btn btn-lg btn-block btn-primary stretched-link" href="https://mpago.la/1Rz8WfY">Comprar</a>

                                        <!--                                        <button type="button" class="btn btn-lg btn-block btn-light" data-toggle="modal" data-target="#eventModal">Cancelar</button>-->
                                    </div>
                                </div>
                                <div class="card mb-4 box-shadow">
                                    <div class="card-header">
                                        <h4 class="my-0 font-weight-normal"><?php echo $modelSemiannual->title; ?></h4>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="card-title pricing-card-title">R$<?php echo $modelSemiannual->price; ?> <small class="text-muted"> 5% desconto</small></h3>
                                        <ul class="list-unstyled mt-3 mb-4">
                                            <li>Pagamento único válido por 6 meses</li>
                                        </ul>
                                        <a class="btn btn-lg btn-block btn-primary stretched-link" href="https://www.mercadopago.com.br/subscriptions/checkout?preapproval_plan_id=2c9380847cc4c817017cc7ec18af0277">Comprar</a>
                                    </div>
                                </div>
                                <div class="card mb-4 box-shadow">
                                    <div class="card-header">
                                        <h4 class="my-0 font-weight-normal"><?php echo $modelYearly->title; ?></h4>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="card-title pricing-card-title">R$<?php echo $modelYearly->price; ?> <small class="text-muted">10% desconto</small></h3>
                                        <ul class="list-unstyled mt-3 mb-4">
                                            <li>Pagamento único válido por 12 meses</li>
                                        </ul>
                                        <button type="button" class="btn btn-lg btn-block btn-primary">Comprar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <?php $url = Yii::$app->basePath . '/views/layouts/footer.php';
            include $url; ?>
        </footer>
    </div>
</div>


<!--  add Event Modal   -->
<div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="eventModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="eventModalLabel">Motivo do cancelamento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Motivo</label>
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>Escolha um motivo</option>
                            <option>Valor muito alto</option>
                            <option>Não estou usando a plataforma</option>
                            <option>Não consegui clientes</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Observação:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body input').val(recipient)
    })
</script>


<!--   Core JS Files   -->
<script src="<?= Url::base() ?>./js/core/jquery.min.js"></script>
<script src="<?= Url::base() ?>./js/core/popper.min.js"></script>
<script src="<?= Url::base() ?>./js/core/bootstrap.min.js"></script>
<script src="<?= Url::base() ?>./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= Url::base() ?>./js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= Url::base() ?>./js/demo.js"></script>
</body>

</html>