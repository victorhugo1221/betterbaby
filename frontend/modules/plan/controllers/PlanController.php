<?php

namespace frontend\modules\plan\controllers;

use common\modules\plan\models\Plan;
use common\modules\user\models\LoginForm;
use common\modules\userProvider\models\UserProvider;
use common\modules\userProviderPlan\models\UserProviderPlan;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use common\models\Pais;
use common\components\Utils;
use yii\helpers\Url;
use yii\web\Response;


class PlanController extends Controller {
    public function actionIndex() {
        $id = Yii::$app->user->identity->id;
        $modelMonthly = Plan::find()->andWhere(['system_title' => 'monthly_doctor'])->one();;
        $modelSemiannual = Plan::find()->andWhere(['system_title' => 'semiannual_doctor'])->one();;
        $modelYearly = Plan::find()->andWhere(['system_title' => 'yearly_doctor'])->one();;
        $model= UserProviderPlan::find()->andWhere(['user_id' => $id])->one();;
        return $this->render('index', ['model' => $model,'modelMonthly' => $modelMonthly,'modelSemiannual' => $modelSemiannual,'modelYearly' => $modelYearly,]);
    }
    
    
}
