<?php

namespace frontend\modules\plan;

class Module extends \yii\base\Module
{
    public $controllerNamespace = __NAMESPACE__ . '\controllers';

    public function init()
    {
        parent::init();
    }
}
