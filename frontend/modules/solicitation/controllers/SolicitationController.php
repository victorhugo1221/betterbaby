<?php

namespace frontend\modules\solicitation\controllers;

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\adminGw\models\Page;
use common\modules\solicitation\models\Solicitation;
use common\modules\user\models\NewPasswordForm;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use common\components\web\Controller;
use common\modules\user\models\User;
use function Webmozart\Assert\Tests\StaticAnalysis\isArray;

class SolicitationController extends Controller {
    private $dataService;
    
    
    
    public function init() {
        $this->pageTitle = $this->getPageName(Solicitation::class, 'Solicitation');
        $this->pageIdentifier = $this->getPageUrl(Solicitation::class, 'Solicitation');
        $this->dataService = $this->dataService ?? Yii::$app->getModule('solicitation')->get('solicitationService');
        parent::init();
    }
    
    
    
    public function actionIndex() {
        $id = Yii::$app->user->identity->id;
        $model = Solicitation::find()->andWhere(['user_provider' => $id])->innerJoin('user', 'solicitation.user_client = user.id');
        $filter = Yii::$app->request->get();
        
        $filter = $this->manipulatingTheFilterInSession($filter);
        
        $dataProvider = new ActiveDataProvider([
            'query'      => $model->orderBy('scheduled_date DESC')->filter($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);
        
        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'model' => $model]);
    }
    
    
    
    public function actionClearfilter()
    {
        $this->destroyFilterOnSession($this->pageIdentifier);
        return $this->redirect(['index']);
    }
    
    
    
    
    
    public function actionCreate() {
        $model = new Solicitation();
        $providerId = Yii::$app->user->identity->id;
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            
            
            if (Yii::$app->request->get('validation', FALSE)) {
                $erros = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $erros = array_merge($erros, ActiveForm::validate($model));
                return $erros;
            }
            
            if ($this->dataService->createSolicitation($model, $providerId)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnInsert($this->pageTitle, 'a'));
                return $this->redirect(['/atendimentos']);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsertFront());
            }
        }
        return $this->render('create', ['model' => $model, 'pageTitle' => $this->pageTitle]);
    }
    
    
    public function actionConfirm($id) {
        if ($this->dataService->confirmsolicitation($id)) {
            Yii::$app->session->setFlash('success', DefaultMessage::sucessOnDelete($this->pageTitle, 'o'));
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', DefaultMessage::errorOnDelete($this->pageTitle, 'o'));
            return $this->redirect(['index']);
        }
    }
    
    
    public function actionCancel($id) {
        if ($this->dataService->cancelsolicitation($id)) {
            Yii::$app->session->setFlash('success', DefaultMessage::sucessOnDelete($this->pageTitle, 'o'));
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', DefaultMessage::errorOnDelete($this->pageTitle, 'o'));
            return $this->redirect(['index']);
        }
    }
    
    
}