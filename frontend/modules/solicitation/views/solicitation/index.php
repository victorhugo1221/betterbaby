<?php

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\healthPlan\models\HealthPlan;
use common\modules\solicitation\models\Solicitation;
use common\modules\specialty\models\Specialty;
use common\widgets\Alert;
use common\components\utils\Filter;
use kartik\select2\Select2;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;

$filterdButton = new Filter();
$formName = substr($dataProvider->query->modelClass, strrpos($dataProvider->query->modelClass, '\\') + 1);
$formName = strtolower($formName);
$urlDefault = '/atendimentos';

$fields = [
    'user.name' => 'Nome',
    'price'     => 'celular',
];


$filter = empty($filter) ? [] : $filter;


?>

<?php $this->beginPage() ?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        Atendimentos
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?= Url::base() ?>./css/demo.css" rel="stylesheet"/>
</head>

<body class="">
<?php $url = Yii::$app->basePath . '/views/layouts/menu.php';
include $url; ?>
<?php $url = Yii::$app->basePath . '/views/layouts/header.php';
include $url; ?>
<div class="main-panel" id="main-panel">
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Atendimentos</h5>
                        <a href="<?= Url::to(['/atendimentos-novo'], TRUE) ?>">
                            <button type="button" class="btn btn-primary"><i class="fas fa-plus" style="margin-right: 10px"></i>Novo atendimento</button>
                        </a>
                        <div class="row">
                            <div class="col-12">
                                <form id="options-form-<?= $formName ?>" method="GET" action="">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6 input-group">
                                            <?= $filterdButton->showFilterField($fields, $filter) ?>
                                        </div>

                                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6 dv-filter d-flex justify-content-start ">
                                            <?= $filterdButton->showFieldButtons($urlDefault) ?>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php
                        Pjax::begin([
                            'enablePushState'    => FALSE,
                            'enableReplaceState' => FALSE,
                            'formSelector'       => '#options-form-' . $formName,
                            'submitEvent'        => 'submit',
                            'timeout'            => 5000,
                            'clientOptions'      => ['maxCacheLength' => 0],
                        ]);
                        
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'tableOptions' => [
                                'id'    => $formName . '-table',
                                'class' => 'table table-striped table-hover',
                            ],
                            'emptyText'    => '<div class="text-center">' . DefaultMessage::theSearchDidNotReturnData() . '</div>',
                            'layout'       => '{items}{pager}',
                            'options'      => [
                                'class'     => 'table-responsive',
                                'data-pjax' => TRUE,
                            ],
                            'columns'      => [
                                [
                                    'attribute'      => 'name',
                                    'label'          => 'Nome',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        
                                        return '<a>' . ($data->name_client !== NULL ? $data->name_client : $data->userClientRel->name) . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'online_service',
                                    'label'          => 'Atendimento',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . ($data->online_service == 1 ? 'Online' : 'Presencial') . '</a>';
                                        
                                    },
                                ],
                                [
                                    'attribute'      => 'cellphone',
                                    'label'          => 'Celular',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . $data->userClientRel->cellphone . '</a>';
                                        
                                    },
                                ],
                                [
                                    'attribute'      => 'created',
                                    'label'          => 'Data Agendada',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a>' . date('d/m/Y H:i', strtotime($data->scheduled_date)) . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'status',
                                    'label'          => 'Status',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) {
                                        if ($data->status == Solicitation::STATUS_PENDENT) {
                                            return '<i class="fas fa-user-clock"></i> ' . Solicitation::$_status[$data->status];
                                        } else if ($data->status == Solicitation::STATUS_ACTIVE) {
                                            return '<i class="fas fa-user-check"></i> ' . Solicitation::$_status[$data->status];
                                        } else {
                                            return '<i class="fas fa-user-lock"></i> ' . Solicitation::$_status[$data->status];
                                        }
                                    },
                                ],
                                [
                                    'attribute'      => 'price',
                                    'label'          => 'Valor',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a>' . 'R$ ' . number_format(floatval($data->price), 2, ',', '.') . '</a>';
                                    },
                                ],
                                [
                                    'class'    => 'yii\grid\ActionColumn',
                                    'header'   => 'Ação',
                                    'template' => '{confirm} {cancel} ',
                                    'buttons'  => [
                                        'confirm' => function ($data) use ($urlDefault) {
                                            return Html::a('<i class="fas fa-check-circle" style="color: #68bd7c"></i> ', $urlDefault . "/confirm/", [
                                                'title'        => Yii::t('app', 'confirm'),
                                                'data-confirm' => 'tem certeza que deseja confirmar?',
                                                'data-method'  => 'POST',
                                            ]);
                                        },
                                        'cancel'  => function ($data) use ($urlDefault) {
                                            return Html::a('<i class="fas fa-ban" style="color: #963b3b"></i> ', $urlDefault, [
                                                'title'        => Yii::t('app', 'cancel'),
                                                'data-confirm' => 'tem certeza que deseja cancelar?',
                                                'data-method'  => 'POST',
                                            ]);
                                        },
                                    ],
                                ],
                            
                            ],
                        ]);
                        
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <?php $url = Yii::$app->basePath . '/views/layouts/footer.php';
        include $url; ?>
    </footer>
</div>


<!--   Confirm Modal   -->
<div class="modal fade" id="createSolicitation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmar o atendimento?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Após confirmado, avisaremos o cliente</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <?php '<a href="' . Url::to(["'/atendimentos/cancelar/"], TRUE) . '" class="client-link">' . $data->title . '</a>'; ?>
            </div>
        </div>
    </div>
</div>
<!--   Cancel Modal   -->
<div class="modal fade" id="cancelSolicitation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cancelar o atendimento?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Após cancelado, avisaremos o cliente</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<script>

</script>

<!--   Core JS Files   -->
<script src="<?= Url::base() ?>./js/core/jquery.min.js"></script>
<script src="<?= Url::base() ?>./js/core/popper.min.js"></script>
<script src="<?= Url::base() ?>./js/core/bootstrap.min.js"></script>
<script src="<?= Url::base() ?>./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= Url::base() ?>./js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= Url::base() ?>./js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= Url::base() ?>./js/demo.js"></script>
</body>

</html>