<?php

use common\modules\healthPlan\models\HealthPlan;
use common\widgets\Alert;
use kartik\datetime\DateTimePicker;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Novo</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?= Url::base() ?>./css/demo.css" rel="stylesheet"/>
</head>

<body class="">
<?php $url = Yii::$app->basePath . '/views/layouts/menu.php';
include $url; ?>
<?php $url = Yii::$app->basePath . '/views/layouts/header.php';
include $url; ?>

<?php
$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
    ],
    'enableAjaxValidation'   => TRUE,
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>
<div class="main-panel" id="main-panel">
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="row">
                        <div class=" col-md-12 d-flex justify-content-center">
                            <h5 class="card-title text-center mb-3 fw-light fs-5">Selecione o tipo de atendimento</h5>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <section class="app">
                            <article class="feature1">
                                <input type="checkbox" name="particular" id="particular"/>
                                <div>
                                    <span>Particular</span>
                                </div>
                            </article>

                            <article class="feature2">
                                <input type="checkbox" name="comconvenio" id="comconvenio"/>
                                <div>
                                    <span>Convênio</span>
                                </div>
                            </article>
                        </section>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <?= $form->field($model, 'name_client')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'required' => TRUE]); ?>
                            </div>
                        </div>
                        <div class="col-md-12 " id="convenio" name="convenio">
                            <div class="form-group">
                                <?php
                                echo $form->field($model, 'health_plan')->widget(Select2::class, [
                                    'data'     => ArrayHelper::map(healthPlan::find()->all(), 'title', 'title'),
                                    'language' => 'pt',
                                    'options'  => [
                                        'id'          => 'especialidades',
                                        'name'        => 'especialidades',
                                        'placeholder' => 'Selecione uma especialidade...',
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <?= $form->field($model, 'scheduled_date')->widget(
                                    
                                    DateTimePicker::class, [
                                    'name'          => 'datetime',
                                    'options'       => ['placeholder' => 'Selecione uma data ...'],
                                    'convertFormat' => TRUE,
                                    'removeButton'  => FALSE,
                                    'pickerButton'  => ['icon' => 'time'],
                                    'pluginOptions' => [
                                        'language'       => 'pt',
                                        'format'         => 'yyyy-MM-dd HH:i:00',
                                        'startDate'      => '01-Jan-2021 12:00 AM',
                                        'todayHighlight' => TRUE,
                                    ],
                                
                                
                                ]); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <?php
                                echo $form->field($model, 'online_service')->widget(Select2::class, [
                                    'name'    => 'online_service',
                                    'data'    => ["1" => "Sim", "2" => "Não"],
                                    'options' => [
                                        'placeholder' => 'Selecione uma opção ...',
                                        'options'     => [
                                            "sim" => ['data-level' => '1'],
                                            'nao' => ['data-level' => '2'],
                                        ],
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" col-md-12 " id="precomedio" name="precomedio">
                                <?= $form->field($model, 'price')->widget(MaskMoney ::class, [
                                    'pluginOptions' => [
                                        'prefix'        => 'R$',
                                        'allowNegative' => FALSE,
                                    ],
                                    'options'       => [
                                        'maxlength' => 10,
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <?= $form->field($model, 'note')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE,]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary btn-lg', 'name' => 'cadastrar']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <?php $url = Yii::$app->basePath . '/views/layouts/footer.php';
        include $url; ?>
    </footer>
</div>
<!--   Core JS Files   -->
<script src="<?= Url::base() ?>./js/core/jquery.min.js"></script>
<script src="<?= Url::base() ?>./js/core/popper.min.js"></script>
<script src="<?= Url::base() ?>./js/core/bootstrap.min.js"></script>
<script src="<?= Url::base() ?>./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Chart JS -->
<script src="<?= Url::base() ?>./js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= Url::base() ?>./js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= Url::base() ?>./js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= Url::base() ?>./js/demo.js"></script>
</body>

</html>
<script>
    $(function () {
        $('div[name="precomedio"]').hide();

        //show it when the checkbox is clicked
        $('input[name="particular"]').on('click', function () {
            if ($(this).prop('checked')) {
                $('div[name="precomedio"]').fadeIn();
            } else {
                $('div[name="precomedio"]').hide();
            }
        });
    });
    $(function () {
        $('div[name="convenio"]').hide();

        //show it when the checkbox is clicked
        $('input[name="comconvenio"]').on('click', function () {
            if ($(this).prop('checked')) {
                $('div[name="convenio"]').fadeIn();
            } else {
                $('div[name="convenio"]').hide();
            }
        });
    });
</script>