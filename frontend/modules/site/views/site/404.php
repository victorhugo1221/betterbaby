
<style type="text/css">
    .box{
        background-color: #fff;
        width: 500px;
        margin: 50px auto;
    }
    #title {
        font-size: 30px;
        margin-top: 25px;
        padding-top: 100px;
    }
    .backgroundbody {
        background-image: linear-gradient(to right, #5ec0d0, #d49ec7);
        font-family: 'Lato', sans-serif;
        color: #ffffff;
    }
    #main{
        display: table;
        width: 100%;
        height: 100vh;
        text-align: center;
    }

    .fof{
        display: table-cell;
        vertical-align: middle;
    }

    .fof h1{
        font-size: 50px;
        display: inline-block;
        padding-right: 12px;
        animation: type .5s alternate infinite;
    }

    @keyframes type{
        from{box-shadow: inset -3px 0px 0px #bebebe;}
        to{box-shadow: inset -3px 0px 0px transparent;}
    }
</style>
<body class="backgroundbody">
<div id="main">
    <div class="fof">
        <h1>Error 404</h1>
        <h4>Página não encontrada</h4>
    </div>
</div>
</body>