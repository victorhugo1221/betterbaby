<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Better Baby</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
</head>


<body>

<div class="container">

    

    <h4> Termos de uso e Políticas de Privacidade
        Termos de uso</h4>

    <p>
        Estes Termos de Uso (“Termos de Uso”) estabelecem os termos e condições de acesso e uso do aplicativo Better Baby para cadastro de médicos, clinicas e serviços oferecido por DARIO BORTOLETTO PEREIRA., situada em São Caetano do Sul, situado à Alameda Conde de Porto Alegre, 411, Santa Maria, São Paulo/SP, CEP 09561-000, inscrita no CNPJ/MF nº 37.661.562/0001-96.
    </p>
    <p>1. OBJETO</p>
    <p>1.1 Estes Termos de Uso regulam o acesso e uso do Serviço pelo correspondente usuário (“Usuário”) quando o aplicativo de cadastro, pesquisa, contato (“Aplicativo”) e/ou o website (“Website”) da Better Baby e/ou outros serviços, plataformas, conteúdos e/ou recursos correlatos são instalados, acessados e/ou utilizados.</p>
    <p> 1.2 O USUÁRIO REALIZARÁ ELETRONICAMENTE SUA ACEITAÇÃO ÀS CONDIÇÕES DESTES TERMOS DE USO E DA CORRESPONDENTE POLÍTICA DE PRIVACIDADE E SEGURANÇA DE DADOS AO SELECIONAR A OPÇÃO “LI E CONCORDO COM OS TERMOS DE USO DESSES SERVIÇOS” DISPONÍVEL NAS PÁGINAS DE CADASTRO DO APLICATIVO (“ACEITE ELETRÔNICO”).</p>
    <p> 1.3 A aposição do Aceite Eletrônico pelo Usuário implica em sua aceitação expressa, automática, plena, sem reservas ou ressalvas, de todas as disposições destes Termos de Uso e da Política de Privacidade e Segurança de Dados aplicável ao Serviço, conforme eventualmente alterados.</p>
    <p> 1.4 Os Termos de Uso e a correspondente Política de Privacidade e Segurança de Dados aplicável ao Serviço estarão disponíveis nos canais eletrônicos de acesso e uso do Aplicativo, inclusive via celular e Website.</p>
    <p> 1.5 Estes Termos de Uso, além de regularem contratualmente a relação entre as partes, devem ser entendidos também como licença de uso das informações vinculados ao Serviço, os quais são regidos pelo direito autoral e por demais leis aplicáveis. Para a utilização de qualquer forma do Serviço, é obrigatório aceitar estes Termos de Uso e a respectiva Política de Privacidade e Segurança de Dados. O acesso ou uso do Serviço sem esta aceitação ou mediante infração constitui não apenas violação destes Termos de Uso, como também violação do direito autoral relacionado ao Serviço.</p>
    <p> 1.6 A Better Baby reserva-se o direito de, a seu exclusivo critério e sem aviso prévio, modificar os Termos de Uso e a Política de Privacidade e Segurança de Dados, incluindo novas condições e/ou restrições, devendo comunicar isto ao Usuário através do Aplicativo e/ou do e-mail. A continuidade do acesso e/ou uso do Serviço pelo Usuário implicará em automática aceitação de eventuais atualizações destes.</p>
    <p> 1.7 A versão válida e eficaz destes Termos de Uso é aquela publicada atualmente pelo Aplicativo e/ou pelo Website, respeitados direitos adquiridos, atos jurídicos perfeitos e coisas julgadas.</p>
    <p> 1.8 O Usuário deverá sempre ler atentamente estes Termos de Uso e a Política de Privacidade e Segurança de Dados e não poderá escusar-se deles alegando ignorância sobre ao seu teor, inclusive quanto a eventuais modificações.</p>
    <p> 1.9 Certos serviços e conteúdos oferecidos com relação ao Serviço podem ser objeto de termos de uso específicos. Neste caso, os termos de uso específico podem substituir, complementar ou modificar estes Termos de Uso. No caso de conflito entre estes Termos de Uso e termos de uso específicos aplicáveis à prestação do Serviço, as condições especiais deverão prevalecer sobre as gerais. As condições especiais também deverão sempre ser lidas pelo Usuário. Outros avisos e comunicados poderão ser realizados ao longo da prestação do Serviço, os quais serão considerados condições especiais e prevalecerão sobre estes Termos de Uso.</p>
    <p> 2. DISPONIBILIDADE DOS SERVIÇOS</p>
    <p> 2.1 A Better Baby envidará seus melhores esforços para assegurar e desenvolver a qualidade dos Serviços.</p>
    <p> 2.2 A Better Baby se compromete a empregar seus melhores esforços, dentro de razoabilidade técnica e econômica, para manter os Serviços disponíveis aos Usuários ininterruptamente e realizar paradas programadas para manutenção fora do horário comercial. Entretanto, poderão existir situações fora do controle razoável da Better Baby que venham a determinar de forma imprevista a suspensão da disponibilidade dos Serviços, tais como casos fortuitos ou de força maior.</p>
    <p> 2.3 Para uso do Serviço, os Usuários deverão providenciar, por sua própria iniciativa e custo, acesso à Internet e telefones celulares compatíveis.</p>
    <p> 3. RESPONSABILIDADES DO USUÁRIO PELO USO DOS SERVIÇOS</p>
    <p> 3.1 Ao se cadastrar, o Usuário procederá à abertura de uma conta para sua identificação perante a Better Baby (“Conta”). O Usuário deverá ser maior de idade e/ou emancipado, sendo que a responsabilidade por qualquer falsidade, inexatidão ou omissão de dados na abertura e manutenção da Conta é exclusivamente do Usuário.</p>
    <p> 3.2 O Usuário compreende e reconhece que:</p>
    <p> 3.2.1 Possui capacidade jurídica para celebrar estes Termos de Uso e utilizar o Serviço, sendo que menores de idade deverão ser assistidos ou representados por seus pais, tutores ou curadores, na forma da lei, os quais serão considerados responsáveis por todos os atos praticados pelos menores;</p>
    <p> 3.2.2 Toda informação oferecida pelo Usuário para acessar e usar o Serviço deverá ser verdadeira;</p>
    <p> 3.2.3 O Usuário garante a autenticidade de todos os dados que informar através do preenchimento dos formulários disponibilizados e é de sua exclusiva responsabilidade manter qualquer informação fornecida à Better Baby permanentemente atualizada de forma a sempre refletir os dados reais do Usuário;</p>
    <p> 3.2.4 O fornecimento de declarações falsas ou inexatas constitui violação destes Termos de Uso, bem como revogação da licença de utilização do Serviço, além de poder configurar em crimes de identidade falsa (artigo 307 do Código Penal) ou falsidade ideológica (artigo 299 do Código Penal);</p>
    <p> 3.2.5 O Usuário será sempre o único e exclusivo responsável pela sua conduta e por danos por ela causados no âmbito da prestação do Serviço, quanto a ele e quanto a terceiros, sendo também o único responsável pelo uso de seu celular para acesso à Conta e ao Serviço, sendo esta ação pessoal e intransferível, devendo o Usuário indenizar aqueles que sofrerem danos e/ou prejuízos pela utilização incorreta e/ou fraudulenta dos seus aparelhos;</p>
    <p> 3.2.6 A Better Baby poderá averiguar as informações prestadas pelo Usuário e, caso detecte qualquer irregularidade, poderá suspender o seu acesso até que tal irregularidade seja corrigida e/ou expedir aviso de suspensão de acesso;</p>
    <p> 3.2.7 Ao utilizar o Serviço, tanto o Usuário quanto os terceiros envolvidos na assinatura digital do aplicativo e/ou outras informações enviarão à Better Baby informações (incluindo, sem limitação, dados e fotos pessoais), autorizando a Better Baby a coletar e usar as informações fornecidas para a administração de sua base de dados, bem como para registrá-las e armazená-las em suporte físico ou intangível, incluindo, mas não se limitando a arquivamento magnético, eletrônico, digital, e/ou qualquer outro meio possível de armazenamento de informação;</p>
    <p> 3.2.8 Todos os direitos, evidências, indícios, atestados, registros, provas técnicas e demais resultados decorrentes da utilização dos Serviços são estendidos aos Signatários dos contratos e/ou outros documentos;</p>
    <p> 3.2.9 As comunicações e transações eletrônicas entre Usuários e Better Baby poderão se constituir em evidências probantes e materializadas dos atos perpetrados com utilização dos Serviços;</p>
    <p> 3.2.10 Os presentes Termos de Uso vinculam Usuários a Better Baby;</p>
    <p> 3.2.11 Leu, entendeu e está de pleno acordo com todas as condições destes Termo de Uso.</p>
    <p> 3.3 Usuário poderá utilizar os Serviços enquanto houver o cadastro e os médicos, clínicas e serviços mediante ao pagamento mensal.</p>
    <p> 3.4 O Usuário se compromete a utilizar o Serviço de boa-fé e em conformidade com todos os preceitos legais, regulamentares e contratuais porventura aplicáveis, bem como com a ordem pública, a moral e os bons costumes e as normas de conduta geralmente aceitas. Qualquer violação a este item poderá sujeitar o Usuário à exclusão da lista de usuários do Serviço, sem prejuízo de outras sanções administrativas, civis e/ou penais eventualmente aplicáveis.</p>
    <p> 4. SEGURANÇA E PRIVACIDADE DE DADOS E DOCUMENTOS</p>
    <p> 4.1 A Better Baby se obriga a, por meio de mecanismos físicos e tecnológicos, não divulgar os dados cadastrais dos Usuários. Tais mecanismos atendem padrões razoáveis de cuidado, considerando-se as possibilidades técnicas e economicamente razoáveis da tecnologia aplicável à Internet, como o uso de firewall para proteção contra acessos indevidos. No entanto, a Better Baby recomenda fortemente que os Usuários adotem medidas de segurança em seu celular, tais como a instalação de programa antivírus e de firewall contra invasões.</p>
    <p> 4.2 Para a prestação do Serviço a Better Baby adota níveis de segurança dentro dos requisitos legais para a proteção dos dados pessoais. Entretanto, é importante que o usuário tenha ciência de que pela própria natureza e características técnicas da Internet, essa proteção não é infalível e encontra-se sujeita à violação pelas mais diversas práticas maliciosas. Os tratamentos de dados pessoais conduzidos pela Better Baby são regidos por sua política de privacidade. Os tratamentos de dados pessoais conduzidos pela Better Baby para a prestação do Serviço são regidos por sua Política de Privacidade e Segurança de Dados.</p>
    <p> 5. LIMITAÇÕES E EXCLUSÃO DE GARANTIAS E DE RESPONSABILIDADES</p>
    <p> 5.1 Por razões técnicas e de operação, não é possível garantir a disponibilidade e continuidade das plataformas de prestação do Serviço, bem como evitar sua falibilidade.</p>
    <p> 5.2 A Better Baby não será responsável, em nenhuma hipótese, por danos ocorridos aos Usuários e/os profissionais decorrentes de qualquer causa fora do controle razoável da Better Baby, tais como, todo serviço exclusivo e relacionados à saúde; atendimentos, orientações, prescrições e procedimentos médicos; todo serviço oriundo à atividade médica, sendo realizada dentro ou fora da clínica e/ou hospitais; danos estéticos ou morais ocorridos nas dependências das clínicas e / ou hospitais ou fora dela, não se limitando a:</p>
    <p> 5.2.1 Divergência de dados dos Usuários e/ou profissionais que impossibilite o uso dos Serviços;</p>
    <p> 5.2.2 Utilização de celulares que impeçam a realização de cadastros digitais mediante uso dos Serviços;</p>
    <p> 5.2.3 Falha de comunicação com a Internet por parte dos Usuários e/ou profissionais;</p>
    <p> 5.2.4 Problemas no processamento de pagamento dos Serviços;</p>
    <p> 5.2.5 Casos fortuitos ou de força maior.</p>
    <p> 5.3 A Better Baby não se responsabiliza por quaisquer custos, prejuízos ou danos que sejam causados aos Usuários, profissionais ou quaisquer terceiros em decorrência da utilização dos Serviços e/ou pelo conteúdo e finalidade dos cadastros eletrônicos que sejam assinados através do Serviço e, por esta razão, não controla, verifica nem atesta o teor ou a legalidade de tal conteúdo, que é de responsabilidade total e exclusiva do Usuário e dos profissionais.</p>
    <p> 5.4 A Better Baby não se responsabiliza por quaisquer custos, prejuízos ou danos de qualquer natureza que possam decorrer da incorreta identidade do Usuário, nos casos de utilização indevida e/ou fraudulenta dos celulares com os quais será feita a assinatura e/ou cadastro.</p>
    <p> 5.5 A Better Baby não tem obrigação de controlar e não controla, autoriza nem se responsabiliza caso o Usuário venha a utilizar os Serviços: (i) para quaisquer fins ou meios ilegais, difamatórios, discriminatórios, ofensivos, obscenos, injuriosos, caluniosos, violentos, ou de assédio, ameaça ou uso de falsa identidade; (ii) cujo conteúdo seja ilegal ou em violação da moral, bons costumes ou da ordem pública.</p>
    <p> 5.6 A Better Baby igualmente não se responsabiliza pelos resultados pretendidos ou verificados com a utilização dos Serviços.</p>
    <p> 5.7 O Usuário não poderá de nenhuma forma empregar mecanismos técnicos que de qualquer forma subvertam a regular utilização do Serviço, sem a prévia consulta aos gestores da Better Baby. Esses mecanismos incluem a utilização de robôs (robots, ou bots), spiders, scripts ou qualquer outra forma de acesso automatizado que de alguma forma sirva para desvirtuar suas finalidades e propósitos. O emprego desses recursos sem autorização prévia implica violação destes Termos de Uso e sujeita a parte infringente ao pagamento de danos emergentes e lucros cessantes, bem como de multa punitiva que se aplicará mesmo que não tenha sido produzido qualquer dano, a ser arbitrada pelo juízo que for responsável pelo julgamento da causa.</p>
    <p> 5.8 O Usuário garante e certifica que, ao acessar ou usar o Serviço, não está infringindo quaisquer direitos legais, contratuais ou de terceiros, bem como quaisquer outros. Quaisquer reclamações de terceiros serão de responsabilidade do Usuário, que deverá assumir todas e quaisquer responsabilidades legais e processuais com relação à reclamação. O Usuário fica ciente que a Better Baby denunciará todas e quaisquer lides, litígios e disputas no qual for envolvida por conta de conteúdos e condutas submetidos ou perpetradas por Usuários ou terceiros, na medida permitida pela lei, de modo a não mais ser parte da lide, litígio ou disputa. A Better Baby também se faculta o direito de chamar ao processo qualquer Usuário, em razão de quaisquer ações originárias de sua conduta e/ou conteúdo submetido. Faculta-se também o direito de responsabilizar diretamente o Usuário perpetrador da
        conduta abusiva, valendo-se para isso de todas os recursos legalmente possíveis, incluindo direito de regresso, dentre outros. Para tanto, a Better Baby irá valer-se dos dados de registro bem como quaisquer outros dados técnicos que permitam identificar o Usuário, ficando desde já ciente disso o Usuário ou terceiros afetados, que não poderão alegar qualquer violação de privacidade nesses casos. A Better Baby reserva-se também o direito de cancelar o registro a qualquer tempo de qualquer Usuário, por sua única e exclusiva discricionariedade, sem qualquer aviso prévio, sempre que for inobservado pelo Usuário o disposto nestes Termos de Uso.</p>
    <p> 5.9 Todas as marcas, nomes comerciais e sinais distintivos de qualquer espécie presentes nas plataformas de acesso e uso do Serviço, incluindo sua própria marca e designação, são pertencentes a seus respectivos titulares de direito. Para a utilização de quaisquer destas marcas, nomes e sinais, é necessário a obtenção da respectiva autorização dos seus titulares.</p>
    <p> 6. COMUNICAÇÃO COM A BETTER BABY</p>
    <p> 6.1 Todas as notificações entre Usuário e Better Baby deverão ser feitas única e exclusivamente através do canal de contato com a equipe de administração do site, pelo email contato@betterbaby.com.br</p>
    <p> 6.2 Todas as notificações e comunicações por parte da Better Baby ao Usuário serão consideradas válidas e eficazes, para todos os efeitos, quando se derem através de qualquer das seguintes formas:</p>
    <p> a. Envio de mensagem por correio eletrônico a qualquer endereço eletrônico fornecido pelo Usuário;</p>
    <p> b. Envio de carta ao domicílio do Usuário quando este tiver fornecido um endereço;</p>
    <p> c. Comunicação telefônica ao número fornecido pelo Usuário; e/ou</p>
    <p> d. Através de mensagens postadas nas plataformas de acesso ao Serviço.</p>
    <p> 6.2.1 Neste sentido, todas as notificações que a Better Baby realizar serão consideradas válidas quando efetuadas empregando os dados e através de qualquer dos meios anteriormente destacados. Para estes efeitos, o Usuário declara que todos os dados fornecidos são válidos e corretos, e como tais serão considerados, comprometendo-se a comunicar e atualizar junto à plataforma Better Baby todas as mudanças relativas a seus dados pessoais.</p>
    <p> 7. VIGÊNCIA</p>
    <p> 7.1 O presente Termo tem vigência por prazo indeterminado, tendo seu início a partir da data do Aceite Eletrônico por parte do Usuário.</p>
    <p> 8. ENCERRAMENTO DOS SERVIÇOS</p>
    <p> 8.1 A Better Baby poderá alterar, adicionar, excluir, interromper ou suspender a prestação dos Serviços a qualquer momento desde que comunique o Usuário mediante aviso eletrônico, respeitado o direito de utilização dos créditos existentes na Conta do Usuário à época do cancelamento ou a restituição de tais créditos ao Usuário.</p>
    <p> 8.2 Será considerado motivo justo para a rescisão imediata e unilateral destes Termos de Uso, por parte da Better Baby, se a utilização dos Serviços ocorrer em violação às disposições do presente instrumento, ou ainda, se o Usuário ou os profissionais infringirem o ordenamento jurídico vigente, incluindo, mas não se limitando, às condutas previstas na cláusula 5.5 deste Termo, sem direito a qualquer restituição de créditos do Usuário.</p>
    <p> 9. CONSIDERAÇÕES GERAIS</p>
    <p> 9.1 Estes Termos de Uso representam a totalidade da avença entre Usuários, profissionais e Better Baby, revogando todo e qualquer acordo oral ou escrito, anterior ou contemporâneo, firmado entre estes com relação ao objeto dos presentes Termos de Uso.</p>
    <p> 9.2 Usuários, profissionais e Better Baby aceitam a força probante, validade e eficácia de comunicações eletrônicas para todos os fins e efeitos destes Termos de Uso.</p>
    <p> 9.3 Estes Termos de Uso obrigam Usuários, profissionais e Better Baby e seus sucessores. Sem prejuízo da disposição anterior, o Usuário não poderá ceder a terceiros quaisquer direitos decorrentes destes Termos de Uso, em nenhuma hipótese. A Better Baby poderá, a qualquer tempo e a seu exclusivo critério, ceder ou transferir, total ou parcialmente, os direitos e obrigações decorrentes do presente Contrato.</p>
    <p> 9.4 Nenhuma alteração ou renúncia de direitos relativa aos presentes Termos de Uso será válida exceto se expressamente negociada e assinada entre Usuários e Better Baby. A omissão, por qualquer destes, em exigir do outro o cumprimento de qualquer obrigação prevista neste instrumento não implica em renúncia ao respectivo direito.</p>
    <p> 9.5 A nulidade de quaisquer disposições dos presentes Termos de Uso não prejudicará a validade das demais. Caso qualquer disposição destes Termos de Uso seja anulada, declarada nula ou inexigível, no todo ou em parte, este documento será considerado modificado com a exclusão ou modificação, na extensão necessária, da disposição nula, anulada ou inexigível, de modo que se preserve os Termos de Uso como válidos e, na medida do possível, de forma consistente com a intenção original dos Usuários e da Better Baby.</p>
    <p> 9.6 As Partes elegem o foro Comarca da cidade de São Caetano do Sul, Estado de São Paulo, como o único competente para dirimir as questões decorrentes destes Termos de Uso com renúncia expressa a qualquer outro, por mais privilegiado que seja.</p>
    <p>  Política de Privacidade e Segurança de Dados
    A Better Baby coleta dados pessoais com a finalidade de prestar seus serviços. Somos comprometidos a preservar a privacidade e segurança de nossos usuários, com tal processamento de dados sendo feito em estrita conformidade às leis e regulamentos aplicáveis, em particular com o Regulamento Geral de Proteção de Dados da União Europeia (GDPR) e a Lei Geral de Proteção de Dados (LGPD). É altamente recomendado que os usuários leiam com atenção a presente Política de Privacidade.</p>
    <p>   1. SOBRE A ORIGINAL E A POLÍTICA DE PRIVACIDADE
    Esta Política de Privacidade e Segurança de Dados (“Política”) define as diretrizes para o tratamento e proteção das informações pessoais coletadas durante o acesso e uso do serviço de assinatura digital de contratos, prova de autenticidade de conteúdo WEB outros documentos eletrônicos oferecido por Better Baby., situada em São Caetano do Sul, à Alameda Conde de porto alegre, 411, Santa Maria, São Paulo/SP, CEP 09560-000, inscrita no CNPJ/MF nº 37.661.562/0001-96.
    Esta Política é complementar aos Termos de Uso que regulam o acesso e uso do Serviço pelo correspondente usuário (“Usuário”) quando o aplicativo de cadastro, pesquisa e comunicação (“Aplicativo”) da Better Baby e/ou outros, serviços e/ou recursos correlatos são instalados, acessados e/ou utilizados (“Termos de Uso”), bem como, estabelece o tratamento que a Better Baby concede às informações dos Usuários e dos terceiros envolvidos na assinatura digital de cadastro que são capturados e/ou armazenadas pela Better Baby.</p>
    <p> 2. ALTERAÇÕES DA POLÍTICA DE PRIVACIDADE
    A Better Baby reserva-se o direito de, a seu exclusivo critério, modificar esta Política, incluindo novas regras, condições e/ou restrições, devendo comunicar isto ao Usuário através das plataformas de uso do Serviço, inclusive o Aplicativo e/ou o Website. A continuidade do acesso e/ou uso do Serviço pelo Usuário implicará em automática aceitação de eventuais atualizações desta.</p>
    <p>  3. DADOS COLETADOS E SUA FINALIDADE
    Ao acessar o Aplicativo e/ou o Website, independentemente de cadastro, certas informações (dados sobre o navegador utilizado, tipo de dispositivo, tempo gasto, endereço de IP, sistema operacional, navegador, idioma do navegador, fuso horário, horário local e geolocalização) serão armazenados em servidores utilizados pela Better Baby.
    Estas e outras informações pessoais capturadas dos Usuários e profissionais são necessárias para sua identificação no contexto do acesso e utilização dos Serviços.
    A fim de prestar seus serviços de coleta de provas sobre conteúdo online por meio do aplicativo, a Better Baby coleta os seguintes dados: nome completo, CRM, CPF, endereço de e-mail, geolocalização, fuso horário, horário local, endereço de IP, sistema operacional, navegador e idioma do navegador. Tais dados são coletados para a identificação da autoria do relatório gerado, fornecendo assim maior grau de segurança e autenticidade para a prova coletada.
    Além disso, a Better Baby também utiliza dados para fins de marketing e comunicação, incluindo a divulgação de produtos, serviços, atividades, promoções, campanhas e eventos nos quais a Better Baby tenha parte, bem como informações de suporte ao usuário, por e-mail e/ou SMS.</p>
    <p>  4. ARMAZENAMENTO DOS DADOS
    As informações capturadas ficarão armazenadas e obedecem a padrões rígidos de confidencialidade e segurança, tais como a criptrografía. Entretanto, é importante que o usuário tenha ciência de que pela própria natureza e características técnicas da Internet, essa proteção não é infalível e encontra-se sujeita à violação pelas mais diversas práticas maliciosas.
    Tais informações serão utilizadas internamente para fins operacionais e estratégicos, envolvendo a administração do Website e do Serviço, incluindo, dentre outras hipóteses, realização de estatísticas e estudos, análise de tráfego, administração, gestão, ampliação e melhoramento das funcionalidades do Serviço, para customização.
    A Better Baby armazenará os relatórios gerados pelo aplicativo, a fim de otimizar a prestação do seu serviços de coleta de provas sobre conteúdo online.
    Os números de cartões de crédito ou débito fornecido pelos profissionais são utilizados somente no processamento dos pagamentos dos Serviços prestados, não sendo armazenados em banco de dados.</p>
    <p> 5. PRAZO DO ARMAZENAMENTO DOS DADOS
    No que tange ao serviço de coleta de provas sobre conteúdo online por meio do aplicativo, a Better Baby armazenará criptograficamente os dados referidos no tópico 3 em servidor próprio, de modo seguro e protegido contra perdas, má utilização e acessos não autorizados. Estes dados serão utilizados somente para a finalidade específica das quais foram coletados e autorizados.
    Os demais dados coletados serão armazenados enquanto forem necessários para a atividade dos usuários</p>
    <p> 5. RELAÇÃO COM TERCEIROS
    A Better Baby poderá compartilhar os dados pessoais coletados com seus parceiros, para que este faça a autenticação notarial do relatório gerado. Note-se que esses websites possuem suas próprias políticas de privacidade, pelas quais a Better Baby não é responsável.
    Nenhum documento e/ou informação pessoal será divulgado e/ou compartilhado em nenhuma hipótese, exceto se expressamente autorizado pelo Usuário e/ou profissional ou mediante ordem judicial ou por determinação legal.
    Nenhum documento e/ou informação pessoal será vendido e as informações pessoais dos Usuários não serão expostas individualmente a quaisquer terceiros, exceto conforme estabelecido neste instrumento ou na forma da Lei e mediante ordem judicial.</p>
    <p> 6. DIREITOS E RESPONSABILIDADES DO USUÁRIO
    O Usuário e/ou profissionais garante e se responsabilizar pela veracidade, exatidão, vigência e autenticidade das fotos pessoais e outras informações que venha a fornecer para uso do Serviço, comprometendo-se a mantê-los atualizados. A Better Baby não tem qualquer responsabilidade no caso de inserção de dados falsos ou de sua inexatidão, podendo, a seu exclusivo critério, suspender e/ou cancelar o cadastro do Usuário, a qualquer momento, caso seja detectada qualquer inexatidão.
    Os usuários da Better Baby, em cumprimento ao Regulamento Geral de Proteção de Dados, possuem garantidos os direitos à: - Transparência, informação, acesso e notificação, de modo a estarem cientes do tratamento dado a seus dados; - Retificação de dados incorretos e preenchimento de dados incompletos; - Apagamento, que pode ser solicitado pelo usuário, além de efetuado automaticamente nos casos previstos em lei, como na eventualidade de terem deixado de ser necessários para a finalidade pela qual foram coletados; - Oposição quanto à possibilidade de recebimento de comunicações diretas via e-mail e/ou SMS; - Retirada de consentimento para a coleta e utilização de dados, a qualquer tempo; - Não sujeição a decisões automatizadas, de modo a solicitar intervenção humana sempre que julgarem necessário; - Portabilidade, solicitando a transferência de seus dados a entidades terceiras.
    <p> 7. CADASTRAMENTO E ACEITE DOS TERMOS E CONDIÇÕES
        O cadastramento como usuário e/ou profissional para utilização do Serviço (“Usuário”) implica em aceitação plena e irrevogável de todos os termos e condições vigentes e publicados pela Better Baby nos canais eletrônicos para aquisição do Aplicativo, inclusive aceitação desta Política, conforme eventualmente alterados.
        Uma vez cadastrado, o Usuário e/ou profissional poderá, a qualquer tempo, por meio de ferramenta oferecida no Aplicativo e/ou no Website, revisar e alterar suas informações de cadastro.
        O cadastramento do usuário e/ou profissional também autoriza a Better Baby a elaborar relatórios sobre as informações e disponibilizar estes relatórios ao Usuário e/ou aos demais profissionais.</p>
    <p> 8. PUBLICIDADE
        A Better Baby poderá enviar comunicados e mensagens publicitárias ao Usuário fazendo uso de todas as tecnologias e os meios de comunicação disponíveis, seja por e-mail, SMS, MMS, mala-direta e outros. Todos os boletins eletrônicos e mensagens publicitárias enviadas por e-mail sempre trarão opção de cancelamento do envio daquele tipo de mensagem. O cancelamento será realizado no tempo mínimo necessário. As mensagens e notificações relacionadas ao Serviço não poderão ser canceladas, exceto se houver o cancelamento do próprio cadastro do Usuário.
        Caso haja alguma dúvida sobre a Política, entre em contato conosco.
        BETTER BABY - CNPJ/MF nº 37.661.562/0001-96</p>


    </p>
    <a class="py-4" onclick="goBack()"><i class="fas fa-chevron-left" style="margin-right: 10px"></i> Voltar </a>
</div>

<script>
    function goBack() {
        window.history.back()
    }
</script>

</body>

</html>