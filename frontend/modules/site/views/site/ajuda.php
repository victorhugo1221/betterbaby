<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        Ajuda Better baby
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-OgVRvuATP1z7JjHLkuOUXw704+h835Lr+6QL9UvYjZE2QaFf/xz9yi7k6UwwzimP"
          crossorigin="anonymous">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
</head>

<style>
    body {
        font-family: sans-serif;
    }

    .card-header {
        background-color: #f5f5f5;
    }

    .card-body {
        background-color: #fff;
    }

    .card {
        margin-bottom: 20px;
    }

    .collapse {
        margin-top: 10px;
        padding: 10px;
        border: 1px solid #ddd;
        border-radius: 5px;
        background-color: #f5f5f5;
    }

    .collapse p:last-child {
        margin-bottom: 0;
    }

    .collapse.show {
        margin-top: 20px;
    }

    .btn-link {
        color: #212529;
        text-decoration: none;
    }
</style>
</head>
<body>
<div class="container">
    <h1>Perguntas frequentes</h1>
    <div id="accordion">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                            aria-controls="collapseOne">
                        Como faço para me cadastrar no site?
                    </button>
                </h5>
            </div>

            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <p>Para se cadastrar no site, basta clicar no botão "Cadastrar" na página inicial e preencher o formulário de
                        cadastro com seus dados pessoais.</p>
                    <p>Depois de preencher o formulário, clique em "Enviar" e você receberá um e-mail de confirmação com um link
                        para ativar sua conta.</p>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                            aria-expanded="false" aria-controls="collapseTwo">
                        Como faço para editar meus dados de cadastro?
                    </button>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <p>Para editar seus dados de cadastro, faça login no site e acesse a página "Meu perfil".</p>
                    <p>Lá você poderá atualizar suas informações pessoais, endereço de e-mail, senha, foto de perfil e outras
                        informações.</p>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="container my-5">
                <div class="accordion" id="faqAccordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Qual é o objetivo deste site?
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#faqAccordion">
                            <div class="card-body">
                                O objetivo deste site é fornecer informações e recursos úteis para aqueles que desejam aprender programação, desenvolvimento web e outras habilidades relacionadas à tecnologia.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Quais são os requisitos para utilizar este site?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faqAccordion">
                            <div class="card-body">
                                Para utilizar este site, você precisará de um computador ou dispositivo móvel com acesso à internet e um navegador da web atualizado, como Google Chrome, Mozilla Firefox, Safari ou Microsoft Edge.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Como posso contribuir com este site?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faqAccordion">
                            <div class="card-body">
                                Você pode contribuir com este site enviando sugestões de melhorias, relatando erros ou enviando recursos e tutoriais úteis para serem compartilhados com a comunidade. Para enviar suas contribuições, entre em contato com a equipe de suporte por meio do formulário de contato ou enviando um e-mail diretamente para suporte@exemplo.com.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

</html>