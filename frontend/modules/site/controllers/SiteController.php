<?php

namespace frontend\modules\site\controllers;

use common\modules\user\models\LoginForm;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use common\models\Pais;
use common\components\Utils;
use yii\helpers\Url;
use yii\web\Response;


class SiteController extends Controller {
    public function actionIndex() : string {
        return $this->render('index');
    }
    
    public function actionPolitica() : string {
        return $this->render('politica');
    }
    public function actionAjuda() : string {
        return $this->render('ajuda');
    }
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->render('login');
        }
        
        $authenticationService = Yii::$app->getModule('adminGw')->get('authenticationService');
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $authenticationService->loginAdministrator($model);
            if ($user) {
                return $this->redirect('/');
            } else {
                $this->returnsUserOrPasswordIncorrect($model);
            }
        }
        return $this->render('login', ['model' => $model]);
    }
    
    private function returnsUserOrPasswordIncorrect($model) {
        $model->addError('login', 'Os dados informados não conferem');
    }
}
