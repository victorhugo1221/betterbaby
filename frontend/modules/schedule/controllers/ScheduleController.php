<?php

namespace frontend\modules\schedule\controllers;

use api\components\utils\HttpCode;
use common\components\utils\DefaultMessage;
use common\modules\solicitation\models\Solicitation;
use common\modules\user\models\LoginForm;
use Yii;
use yii\db\Expression;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use common\models\Pais;
use common\components\Utils;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;


class ScheduleController extends Controller {
    
    public function actionIndex()
    {
        $id = Yii::$app->user->identity->id;
        $solicitation = Solicitation::find()->select([
            'solicitation.id',
            'IFNULL(name_client, user.name) AS title',
            'user.name AS clientname',
            'scheduled_date AS start',
            'price',
            'health_plan',
            'online_service',
            'note',
            'classname AS className',
        ])
            ->andWhere(['user_provider' => $id])
            ->leftJoin('user', 'solicitation.user_client = user.id')
            ->asArray()
            ->all();
        
        return $this->render('index', ['response' => $solicitation]);
    }
    
    public function actionCreate() {
        $model = new Solicitation();
        $providerId = Yii::$app->user->identity->id;
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            
            if (Yii::$app->request->get('validation', FALSE)) {
                $erros = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $erros = array_merge($erros, ActiveForm::validate($model));
                return $erros;
            }
            
            if ($this->dataService->createSolicitation($model, $providerId)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnInsert($this->pageTitle, 'a'));
                return $this->redirect(['/atendimentos']);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsertFront());
            }
        }
        return $this->render('create', ['model' => $model,]);
    }
    
    
}
