<?php

use common\modules\healthPlan\models\HealthPlan;
use common\widgets\Alert;
use kartik\datetime\DateTimePicker;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['/'], TRUE),
    ],
    'validationUrl'          => FALSE,
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>
<!-- Add Modal -->
<div class="modal fade" id="schedule-add">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h5 class="modal-title">Novo Atendimento</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="form-group">
                    <?= $form->field($model, 'name_client')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]); ?>
                </div>
                <div class="form-group">
                    <?php
                    echo $form->field($model, 'health_plan')->widget(Select2::class, [
                        'data'     => ArrayHelper::map(healthPlan::find()->all(), 'id', 'title'),
                        'language' => 'pt',
                        'options'  => [
                            'id'          => 'especialidades',
                            'name'        => 'especialidades',
                            'multiple'    => TRUE,
                            'placeholder' => 'Selecione uma especialidade...',
                        ],
                    ]);
                    ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'scheduled_date')->widget(
                        
                        DateTimePicker::class, [
                        'name'          => 'datetime',
                        'options'       => ['placeholder' => 'Selecione uma data ...'],
                        'convertFormat' => TRUE,
                        'removeButton'  => FALSE,
                        'pickerButton'  => ['icon' => 'time'],
                        'pluginOptions' => [
                            'language'       => 'pt',
                            'format'         => 'yyyy-MM-dd HH:i:00',
                            'startDate'      => '01-Jan-2021 12:00 AM',
                            'todayHighlight' => TRUE,
                        ],
                    
                    
                    ]); ?>
                </div>

                <div class="form-group">
                    <?= $form->field($model, 'price')->widget(MaskMoney ::class, [
                        'pluginOptions' => [
                            'prefix'        => 'R$',
                            'allowNegative' => FALSE,
                        ],
                    ]);
                    ?>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Observação:</label>
                    <input type="text" class="form-control" id="recipient-name">
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <?= Html::submitButton('Cadastrar', ['class' => 'btn btn-primary btn', 'name' => 'cadastrar']) ?>
            </div>
        </div