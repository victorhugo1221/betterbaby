<?php


use yii\helpers\Url;

?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        Agenda
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">

    <link href="<?= Url::base() ?>./css/fullcalendar.css" rel="stylesheet"/>
    <link href="<?= Url::base() ?>./css/fullcalendar.print.css" rel="stylesheet" media='print'/>
    <script src='<?= Url::base() ?>./js/jquery-1.10.2.js' type="text/javascript"></script>
    <script src='<?= Url::base() ?>./js/jquery-ui.custom.min.js' type="text/javascript"></script>
    <script src='<?= Url::base() ?>./js/fullcalendar.js' type="text/javascript"></script>


    <script>

        $(document).ready(function () {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            

            $('#external-events div.external-event').each(function () {
                
                var eventObject = {
                    title: $.trim($(this).text())
                };
                
                $(this).data('eventObject', eventObject);
                
                $(this).draggable({
                    zIndex: 999,
                    revert: true,
                    revertDuration: 0
                });

            });
            

            var calendar = $('#calendar').fullCalendar({
                header: {
                    left: 'title',
                    center: 'agendaDay,agendaWeek,month',
                    right: 'prev,next today'
                },
                editable: true,
                firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
                selectable: true,
                defaultView: 'month',

                axisFormat: 'h:mm',
                columnFormat: {
                    month: 'ddd',    // Mon
                    week: 'ddd d', // Mon 7
                    day: 'dddd M/d',  // Monday 9/7
                    agendaDay: 'dddd d'
                },
                titleFormat: {
                    month: 'MMMM yyyy', // September 2009
                    week: "MMMM yyyy", // September 2009
                    day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
                },
                allDaySlot: false,
                selectHelper: true,
                select: function (start, end, allDay) {
                    $.ajax({
                        type: 'POST',
                        //Caminho do arquivo do seu modal
                        url: '..create.php',
                        success: function (data) {
                            $('.modal').html(data);
                            $('#schedule-add').modal('show');
                        }
                    });
                },
                eventClick: function (event) {

                    $('#solicitationdetails #id').text(event.id);
                    $('#solicitationdetails #title').text(event.title);
                    $('#solicitationdetails #start').text(event.start.toLocaleString('pt-BR'));

                    function formatCurrency(amount) {
                        var formatter = new Intl.NumberFormat('pt-BR', {
                            style: 'currency',
                            currency: 'BRL',
                            minimumFractionDigits: 2,
                        });
                        return formatter.format(amount);
                    }


                    $('#solicitationdetails #price').text(formatCurrency(event.price));

                    function getServiceType(serviceValue) {
                        if (serviceValue === '1') {
                            return 'Online';
                        } else if (serviceValue === '2') {
                            return 'Presencial';
                        } else {
                            return '';
                        }
                    }

                    var serviceText = getServiceType(event.online_service);
                    $('#solicitationdetails #onlineservice').text(serviceText);

                    $('#solicitationdetails #note').text(event.note);
                    $('#solicitationdetails').modal('show');
                    return false;
                },
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function (date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }

                },

                events:
                <?php echo json_encode($response);?>
            });

        });
    </script>

    <style>


        #wrap {
            width: 1000px;
            margin: 0 auto;
        }

        #external-events {
            float: left;
            width: 150px;
            padding: 0 10px;
            text-align: left;
        }

        #external-events h4 {
            font-size: 16px;
            margin-top: 0;
            padding-top: 1em;
        }

        .external-event { /* try to mimick the look of a real event */
            margin: 10px 0;
            padding: 2px 4px;
            background: #3366CC;
            color: #fff;
            font-size: .85em;
            cursor: pointer;
        }

        #external-events p {
            margin: 1.5em 0;
            font-size: 11px;
            color: #666;
        }

        #external-events p input {
            margin: 0;
            vertical-align: middle;
        }

        #calendar {
            /* 		float: right; */
            margin: 0 auto;
            width: 1000px;
            background-color: #FFFFFF;
            border-radius: 6px;
            box-shadow: 0 1px 2px #C3C3C3;
        }

    </style>
</head>
<body class="">
<?php $url = Yii::$app->basePath . '/views/layouts/menu.php';
include $url; ?>
<?php $url = Yii::$app->basePath . '/views/layouts/header.php';
include $url; ?>
<div class="main-panel" id="main-panel">
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Minha agenda</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">

                            <div id='wrap'>

                                <div id='calendar'></div>

                                <div style='clear:both'></div>
                            </div>

                            <!-- Modal -->

                            <div class="modal fade" id="solicitationdetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Detalhes do atendimento</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <dl class="dl-horizontal">
                                                <dt>N.º atendimento:</dt>
                                                <dd id="id"></dd>
                                                <dt>Nome:</dt>
                                                <dd id="title"></dd>
                                                <dt>Data:</dt>
                                                <dd id="start"></dd>
                                                <dt>Atendimento:</dt>
                                                <dd id="onlineservice"></dd>
                                                <dt>Preço:</dt>
                                                <dd id="price"></dd>
                                                <dt>Observação:</dt>
                                                <dd id="note"></dd>
                                            </dl>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col align-left">
                                                <a href="https://apps.google.com/meet/">
                                                    <button type="button" class="btn btn-light"><i class="fas fa-calendar-plus" style="margin:5px"></i>Google Meet</button>
                                                </a>
                                            </div>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                        </div>
                                    </div>
                                </div>

</body>

</div>
</div>
</div>
<footer class="footer">
    <?php $url = Yii::$app->basePath . '/views/layouts/footer.php';
    include $url; ?>
</footer>
</div>
</div>
</html>

</div>
</div>
<!--   Core JS Files   -->
<script src="<?= Url::base() ?>./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="<?= Url::base() ?>./js/core/bootstrap.min.js"></script>
<script src="<?= Url::base() ?>./js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->

</body>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
