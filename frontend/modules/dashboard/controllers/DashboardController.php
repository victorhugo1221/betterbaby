<?php

namespace frontend\modules\dashboard\controllers;

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\solicitation\models\Solicitation;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use common\components\web\Controller;
use common\modules\user\models\User;
use function Webmozart\Assert\Tests\StaticAnalysis\isArray;

class DashboardController extends Controller {
    private $dataService;
    
    public function init() {
        $this->pageTitle = $this->getPageName(Solicitation::class, 'Solicitation');
        $this->pageIdentifier = $this->getPageUrl(Solicitation::class, 'Solicitation');
        $this->dataService = $this->dataService ?? Yii::$app->getModule('solicitation')->get('solicitationService');
        parent::init();
    }
    
    public function actionIndex() {
        $id = Yii::$app->user->identity->id;
        $model = Solicitation::find()->andWhere(['user_provider' => $id]);
        $filter = Yii::$app->request->get();
        $readonly = Yii::$app->user->id > 0;
        
        $filter = $this->manipulatingTheFilterInSession($filter);
        
        $dataProvider = new ActiveDataProvider([
            'query'      => $model->limit(5)->orderBy('scheduled_date DESC')->filter($filter)->innerJoin('user', 'solicitation.user_client = user.id'),
            'pagination' => FALSE,
        ]);
        
        
        $date = strtotime("+1 day");
        
    
        $currentDate = new \DateTime();
        $revenueArray = [];
    
        for ($i = 0; $i < 12; $i++) {
            $startDateRating = new \DateTime(sprintf('first day of -%d month', $i), $currentDate->getTimezone());
            $endDateRating = new \DateTime(sprintf('last day of -%d month', $i), $currentDate->getTimezone());
        
            $revenue = Solicitation::find()
                ->andWhere(['user_provider' => $id])
                ->andWhere(['status' => 2])
                ->andWhere(['>', 'scheduled_date', $startDateRating->format('Y-m-d')])
                ->andWhere(['<', 'scheduled_date', $endDateRating->format('Y-m-d')])
                ->sum('price');
        
            if ($revenue == NULL) {
                $revenue = 0;
            }
            $revenueArray[] = $revenue;
        }
    
        $RevenueResult = array_reverse($revenueArray);
       
        
        
    
    
        $currentDate = new \DateTime();
        $avarageArray = [];
    
        for ($i = 0; $i < 12; $i++) {
            $startDateRating = new \DateTime(sprintf('first day of -%d month', $i), $currentDate->getTimezone());
            $endDateRating = new \DateTime(sprintf('last day of -%d month', $i), $currentDate->getTimezone());
        
            $avarage = Solicitation::find()
                ->andWhere(['user_provider' => $id])
                ->andWhere(['status' => 2])
                ->andWhere(['>', 'scheduled_date', $startDateRating->format('Y-m-d')])
                ->andWhere(['<', 'scheduled_date', $endDateRating->format('Y-m-d')])
                ->average('rating_stars');
    
            if ($avarage == NULL) {
                $avarage = 0;
            }
            $avarageArray[] = $avarage;
        }
    
        $RatingResult = array_reverse($avarageArray);
        
        
       
        
        
        $currentDate = new \DateTime();
        $countArray = [];
        
        for ($i = 0; $i < 12; $i++) {
            $startDate = new \DateTime(sprintf('first day of -%d month', $i), $currentDate->getTimezone());
            $endDate = new \DateTime(sprintf('last day of -%d month', $i), $currentDate->getTimezone());
            $monthName = strftime('%b', $startDate->getTimestamp());
            
            $count = Solicitation::find()
                ->andWhere(['user_provider' => $id])
                ->andWhere(['status' => 2])
                ->andWhere(['>', 'scheduled_date', $startDate->format('Y-m-d')])
                ->andWhere(['<', 'scheduled_date', $endDate->format('Y-m-d')])
                ->count();
            
            if ($count == NULL) {
                $count = 0;
            }
            
            $countArray[] = $count;
            $monthArray[] = $monthName;
            
        }
        
        $countArray = array_reverse($countArray);
        $monthArray = array_reverse($monthArray);
        
        $saida_array = array_map(function($item) { return "\"$item\""; }, $monthArray); // adiciona as aspas duplas em cada elemento
        $SolicitationResultMonth = implode(", ", $saida_array); // junta novamente os elementos do array em uma string
        
        $SolicitationResult = $countArray;
        
        
        return $this->render('index', ['dataProvider' => $dataProvider, 'RevenueResult' => $RevenueResult, 'SolicitationResult' => $SolicitationResult, 'SolicitationResultMonth' => $SolicitationResultMonth, 'RatingResult' => $RatingResult, 'readonly' => $readonly, 'filter' => $filter]);
        
    }
    
    
}


