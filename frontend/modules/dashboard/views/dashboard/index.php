<?php

use common\components\utils\DefaultMessage;
use common\modules\solicitation\models\Solicitation;
use common\widgets\Alert;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;

$readonly = Yii::$app->user->id > 0;
$formName = substr($dataProvider->query->modelClass, strrpos($dataProvider->query->modelClass, '\\') + 1);
$formName = strtolower($formName);
$urlDefault = '/dashboard';

?>

<?php $this->beginPage() ?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Better Baby</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?= Url::base() ?>./css/demo.css" rel="stylesheet"/>
</head>

<body class="">
<?php $url = Yii::$app->basePath . '/views/layouts/menu.php';
include $url; ?>
<?php $url = Yii::$app->basePath . '/views/layouts/header.php';
include $url; ?>
<div class="main-panel" id="main-panel">
    <div class="panel-header panel-header-lg">
        <canvas id="bigDashboardChart"></canvas>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="card card-chart">
                    <div class="card-header">
                        <h4 class="card-title">Avaliação média</h4>
                    </div>
                    <div class="card-body">
                        <div class="chart-area">
                            <canvas id="lineChartExample"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card card-chart">
                    <div class="card-header">
                        <h4 class="card-title">Faturamento</h4>
                    </div>
                    <div class="card-body">
                        <div class="chart-area">
                            <canvas id="lineChartExampleWithNumbersAndGrid"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card card-chart">
                    <div class="card-header">
                        <h4 class="card-title">Novos Clientes</h4>
                    </div>
                    <div class="card-body">
                        <div class="chart-area">
                            <canvas id="barChartSimpleGradientsNumbers"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> Próximos Atendimentos</h4>
                </div>
                <div class="card-body">
                    <?php
                    Pjax::begin([
                        'enablePushState'    => FALSE,
                        'enableReplaceState' => FALSE,
                        'formSelector'       => '#options-form-' . $formName,
                        'submitEvent'        => 'submit',
                        'timeout'            => 5000,
                        'clientOptions'      => ['maxCacheLength' => 0],
                    ]);
                    
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'tableOptions' => [
                            'id'    => $formName . '-table',
                            'class' => 'table table-striped table-hover',
                        ],
                        'emptyText'    => '<div class="text-center">' . DefaultMessage::theSearchDidNotReturnData() . '</div>',
                        'layout'       => '{items}{pager}',
                        'options'      => [
                            'class'     => 'table-responsive',
                            'data-pjax' => TRUE,
                        ],
                        'columns'      => [
                            [
                                'attribute'      => 'user_client',
                                'label'          => 'Nome',
                                'format'         => 'html',
                                'enableSorting'  => TRUE,
                                'headerOptions'  => [
                                    'class' => 'text-left',
                                    'style' => 'min-width:150px',
                                ],
                                'contentOptions' => ['class' => 'client-link'],
                                'value'          => function ($data) use ($urlDefault) {
                                    return '<a>' . ($data->name_client !== NULL ? $data->name_client : $data->userClientRel->name) . '</a>';
                                },
                            ],
                            [
                                'attribute'      => 'online_service',
                                'label'          => 'Atendimento',
                                'format'         => 'html',
                                'enableSorting'  => TRUE,
                                'headerOptions'  => [
                                    'class' => 'text-left',
                                    'style' => 'min-width:150px',
                                ],
                                'contentOptions' => ['class' => 'client-link'],
                                'value'          => function ($data) use ($urlDefault) {
                                    return '<a>' . ($data->online_service == 1 ? 'Online' : 'Presencial') . '</a>';
                                },
                            ],
                            [
                                'attribute'      => 'user_client',
                                'label'          => 'Celular',
                                'format'         => 'html',
                                'enableSorting'  => TRUE,
                                'headerOptions'  => [
                                    'class' => 'text-left',
                                    'style' => 'min-width:150px',
                                ],
                                'contentOptions' => ['class' => 'client-link'],
                                'value'          => function ($data) use ($urlDefault) {
                                    return '<a>' . $data->userProviderRel->cellphone . '</a>';
                                },
                            ],
                            [
                                'attribute'      => 'scheduled_date',
                                'label'          => 'Data Agendada',
                                'format'         => 'html',
                                'enableSorting'  => TRUE,
                                'headerOptions'  => [
                                    'class' => 'text-left',
                                    'style' => 'min-width:150px',
                                ],
                                'contentOptions' => ['class' => 'client-link'],
                                'value'          => function ($data) use ($urlDefault) {
                                    return '<a>' . date('d/m/Y H:i', strtotime($data->scheduled_date)) . '</a>';
                                },
                            ],
                            [
                                'attribute'      => 'status',
                                'label'          => 'Status',
                                'format'         => 'html',
                                'enableSorting'  => TRUE,
                                'headerOptions'  => [
                                    'class' => 'text-left',
                                    'style' => 'min-width:150px',
                                ],
                                'contentOptions' => ['class' => 'text-left'],
                                'value'          => function ($data) {
                                    if ($data->status == Solicitation::STATUS_PENDENT) {
                                        return '<i class="fas fa-user-clock"></i> ' . Solicitation::$_status[$data->status];
                                    } else if ($data->status == Solicitation::STATUS_ACTIVE) {
                                        return '<i class="fas fa-user-check"></i> ' . Solicitation::$_status[$data->status];
                                    } else {
                                        return '<i class="fas fa-user-lock"></i> ' . Solicitation::$_status[$data->status];
                                    }
                                },
                            ],
                            [
                                'attribute'      => 'price',
                                'label'          => 'Valor',
                                'format'         => 'html',
                                'enableSorting'  => TRUE,
                                'headerOptions'  => [
                                    'class' => 'text-left',
                                    'style' => 'min-width:150px',
                                ],
                                'contentOptions' => ['class' => 'client-link'],
                                'value'          => function ($data) use ($urlDefault) {
                                    return '<a>' . 'R$ ' . number_format($data->price, 2, ',', '.') . '</a>';
                                },
                            ],
                        ],
                    ]);
                    
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
        <footer class="footer">
            <?php $url = Yii::$app->basePath . '/views/layouts/footer.php';
            include $url; ?>
        </footer>
    </div>
</div>
<!--   Core JS Files   -->
<script src="<?= Url::base() ?>./js/core/jquery.min.js"></script>
<script src="<?= Url::base() ?>./js/core/popper.min.js"></script>
<script src="<?= Url::base() ?>./js/core/bootstrap.min.js"></script>
<script src="<?= Url::base() ?>./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Chart JS -->
<script src="<?= Url::base() ?>./js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= Url::base() ?>./js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= Url::base() ?>./js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script>
    $(document).ready(function () {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>
<script>

    demo = {
        initPickColor: function () {
            $('.pick-class-label').click(function () {
                var new_class = $(this).attr('new-class');
                var old_class = $('#display-buttons').attr('data-class');
                var display_div = $('#display-buttons');
                if (display_div.length) {
                    var display_buttons = display_div.find('.btn');
                    display_buttons.removeClass(old_class);
                    display_buttons.addClass(new_class);
                    display_div.attr('data-class', new_class);
                }
            });
        },

        initDocChart: function () {
            chartColor = "#FFFFFF";

            // General configuration for the charts with Line gradientStroke
            gradientChartOptionsConfiguration = {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                tooltips: {
                    bodySpacing: 4,
                    mode: "nearest",
                    intersect: 0,
                    position: "nearest",
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10
                },
                responsive: true,
                scales: {
                    yAxes: [{
                        display: 0,
                        gridLines: 0,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            zeroLineColor: "transparent",
                            drawTicks: false,
                            display: false,
                            drawBorder: false
                        }
                    }],
                    xAxes: [{
                        display: 0,
                        gridLines: 0,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            zeroLineColor: "transparent",
                            drawTicks: false,
                            display: false,
                            drawBorder: false
                        }
                    }]
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 15,
                        bottom: 15
                    }
                }
            };

            ctx = document.getElementById('lineChartExample').getContext("2d");

            gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
            gradientStroke.addColorStop(0, '#80b6f4');
            gradientStroke.addColorStop(1, chartColor);

            gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
            gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
            gradientFill.addColorStop(1, "rgba(95, 194, 209)");

            myChart = new Chart(ctx, {
                type: 'line',
                responsive: true,
                data: {
                    labels: [<?=  $SolicitationResultMonth ?>],
                    datasets: [{
                        label: "Active Users",
                        borderColor: "#6EC3E7",
                        pointBorderColor: "#FFF",
                        pointBackgroundColor: "#6EC3E7",
                        pointBorderWidth: 2,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 1,
                        pointRadius: 4,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [<?= implode(",", $SolicitationResult) ?>]
                    }]
                },
                options: gradientChartOptionsConfiguration
            });
        },

        initDashboardPageCharts: function () {

            chartColor = "#FFFFFF";

            // General configuration for the charts with Line gradientStroke
            gradientChartOptionsConfiguration = {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                tooltips: {
                    bodySpacing: 4,
                    mode: "nearest",
                    intersect: 0,
                    position: "nearest",
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10
                },
                responsive: 1,
                scales: {
                    yAxes: [{
                        display: 0,
                        gridLines: 0,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            zeroLineColor: "transparent",
                            drawTicks: false,
                            display: false,
                            drawBorder: false
                        }
                    }],
                    xAxes: [{
                        display: 0,
                        gridLines: 0,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            zeroLineColor: "transparent",
                            drawTicks: false,
                            display: false,
                            drawBorder: false
                        }
                    }]
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 15,
                        bottom: 15
                    }
                }
            };

            gradientChartOptionsConfigurationWithNumbersAndGrid = {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                tooltips: {
                    bodySpacing: 4,
                    mode: "nearest",
                    intersect: 0,
                    position: "nearest",
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10
                },
                responsive: true,
                scales: {
                    yAxes: [{
                        gridLines: 0,
                        gridLines: {
                            zeroLineColor: "transparent",
                            drawBorder: false
                        }
                    }],
                    xAxes: [{
                        display: 0,
                        gridLines: 0,
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            zeroLineColor: "transparent",
                            drawTicks: false,
                            display: false,
                            drawBorder: false
                        }
                    }]
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 15,
                        bottom: 15
                    }
                }
            };

            var ctx = document.getElementById('bigDashboardChart').getContext("2d");

            var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
            gradientStroke.addColorStop(0, '#80b6f4');
            gradientStroke.addColorStop(1, chartColor);

            var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
            gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
            gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: [<?= $SolicitationResultMonth ?>],
                    datasets: [{
                        label: "Atendimentos",
                        borderColor: chartColor,
                        pointBorderColor: chartColor,
                        pointBackgroundColor: "#e0e0e0",
                        pointHoverBackgroundColor: "#e0e0e0",
                        pointHoverBorderColor: chartColor,
                        pointBorderWidth: 1,
                        pointHoverRadius: 7,
                        pointHoverBorderWidth: 2,
                        pointRadius: 5,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [<?= implode(",", $SolicitationResult) ?>]
                    }]
                },
                options: {
                    layout: {
                        padding: {
                            left: 20,
                            right: 20,
                            top: 0,
                            bottom: 0
                        }
                    },
                    maintainAspectRatio: false,
                    tooltips: {
                        backgroundColor: '#fff',
                        titleFontColor: '#333',
                        bodyFontColor: '#666',
                        bodySpacing: 4,
                        xPadding: 12,
                        mode: "nearest",
                        intersect: 0,
                        position: "nearest"
                    },
                    legend: {
                        position: "bottom",
                        fillStyle: "#FFF",
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                fontColor: "rgba(255,255,255,0.4)",
                                fontStyle: "bold",
                                beginAtZero: true,
                                maxTicksLimit: 5,
                                padding: 10
                            },
                            gridLines: {
                                drawTicks: true,
                                drawBorder: false,
                                display: true,
                                color: "rgba(255,255,255,0.1)",
                                zeroLineColor: "transparent"
                            }

                        }],
                        xAxes: [{
                            gridLines: {
                                zeroLineColor: "transparent",
                                display: false,

                            },
                            ticks: {
                                padding: 10,
                                fontColor: "rgba(255,255,255,0.4)",
                                fontStyle: "bold"
                            }
                        }]
                    }
                }
            });

            var cardStatsMiniLineColor = "#fff",
                cardStatsMiniDotColor = "#fff";

            ctx = document.getElementById('lineChartExample').getContext("2d");

            gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
            gradientStroke.addColorStop(0, '#6EC3E7');
            gradientStroke.addColorStop(1, chartColor);

            gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
            gradientFill.addColorStop(0, "rgba(150, 36, 36, 0)");
            gradientFill.addColorStop(1, hexToRGB('#4596a4', 0.4));

            myChart = new Chart(ctx, {
                type: 'line',
                responsive: true,
                data: {
                    labels: [<?= $SolicitationResultMonth ?>],
                    datasets: [{
                        label: "Média",
                        borderColor: "#5fbfd0",
                        pointBorderColor: "#FFF",
                        pointBackgroundColor: "#5fbfd0",
                        pointBorderWidth: 2,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 1,
                        pointRadius: 4,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [ <?= implode(",", $RatingResult) ?>]
                    }]
                },
                options: gradientChartOptionsConfigurationWithNumbersAndGrid
            });


            ctx = document.getElementById('lineChartExampleWithNumbersAndGrid').getContext("2d");

            gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
            gradientStroke.addColorStop(0, '#6EC3E7');
            gradientStroke.addColorStop(1, chartColor);

            gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
            gradientFill.addColorStop(0, "rgba(150, 36, 36, 0)");
            gradientFill.addColorStop(1, hexToRGB('#4596a4', 0.4));

            myChart = new Chart(ctx, {
                type: 'line',
                responsive: true,
                data: {
                    labels: [<?= $SolicitationResultMonth ?>],
                    datasets: [{
                        label: "R$",
                        borderColor: "#5fbfd0",
                        pointBorderColor: "#FFF",
                        pointBackgroundColor: "#5fbfd0",
                        pointBorderWidth: 2,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 1,
                        pointRadius: 4,
                        fill: true,
                        backgroundColor: gradientFill,
                        borderWidth: 2,
                        data: [<?= implode(",", $RevenueResult) ?>]
                    }]
                },
                options: gradientChartOptionsConfigurationWithNumbersAndGrid
            });

            var e = document.getElementById("barChartSimpleGradientsNumbers").getContext("2d");

            gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
            gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
            gradientFill.addColorStop(1, hexToRGB('#2CA8FF', 0.6));

            var a = {
                type: "line",
                data: {
                    labels: [<?= $SolicitationResultMonth ?>],
                    datasets: [{
                        label: "novos clientes",
                        backgroundColor: gradientFill,
                        borderColor: "#5fbfd0",
                        pointBorderColor: "#FFF",
                        pointBackgroundColor: "#5fbfd0",
                        pointBorderWidth: 2,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 1,
                        pointRadius: 4,
                        fill: true,
                        borderWidth: 2,
                        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2]
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        bodySpacing: 4,
                        mode: "nearest",
                        intersect: 0,
                        position: "nearest",
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10
                    },
                    responsive: 1,
                    scales: {
                        yAxes: [{
                            gridLines: 0,
                            gridLines: {
                                zeroLineColor: "transparent",
                                drawBorder: false
                            }
                        }],
                        xAxes: [{
                            display: 0,
                            gridLines: 0,
                            ticks: {
                                display: false
                            },
                            gridLines: {
                                zeroLineColor: "transparent",
                                drawTicks: false,
                                display: false,
                                drawBorder: false
                            }
                        }]
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 15,
                            bottom: 15
                        }
                    }
                }
            };

            var viewsChart = new Chart(e, a);
        },

        initGoogleMaps: function () {
            var myLatlng = new google.maps.LatLng(40.748817, -73.985428);
            var mapOptions = {
                zoom: 13,
                center: myLatlng,
                scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
                styles: [{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e9e9e9"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 29
                    }, {
                        "weight": 0.2
                    }]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 18
                    }]
                }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#dedede"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "visibility": "on"
                    }, {
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "saturation": 36
                    }, {
                        "color": "#333333"
                    }, {
                        "lightness": 40
                    }]
                }, {
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }, {
                        "lightness": 19
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 17
                    }, {
                        "weight": 1.2
                    }]
                }]
            };

            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                title: "Hello World!"
            });

            // To add the marker to the map, call setMap();
            marker.setMap(map);
        }
    };

</script>
</body>

</html>