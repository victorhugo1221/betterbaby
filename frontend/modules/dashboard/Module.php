<?php

namespace frontend\modules\dashboard;

class Module extends \common\modules\solicitation\Module {

    public $controllerNamespace = __NAMESPACE__ . '\controllers';

    public function init()
    {
        parent::init();
    }
}
