<?php

namespace frontend\modules\settings\controllers;

use common\components\utils\DefaultMessage;
use common\modules\address\models\Address;
use common\modules\user\models\LoginForm;
use common\modules\user\models\User;
use common\modules\userProvider\models\UserProvider;
use common\modules\userProviderHealthPlan\models\UserProviderHealthPlan;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\Controller;
use common\models\Pais;
use common\components\Utils;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;


class SettingsController extends Controller {
  
    public function actionSettingsdoctor() {
        $id = Yii::$app->user->identity->id;
        $errors = [];
        $model = User::find()->where(['IS', User::tableName() . '.deleted', NULL])->andWhere(['id' => $id])->one();
        $modelUserProvider = UserProvider::find()->where(['user_id' => $model->id])->one();
        $modelAddress = Address::find()->andWhere(['user_id' => $model->id])->one();
        $modelUserProviderHealthPlan = UserProviderHealthPlan::find()->andWhere(['user_id' => $id])->one();
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            $modelUserProvider->load($post);
            $modelAddress->load($post);
            $modelUserProviderHealthPlan->load($post);
            
            $errors = [];
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors = array_merge($errors, ActiveForm::validate($model));
                $errors = array_merge($errors, ActiveForm::validate($modelUserProvider));
                $errors = array_merge($errors, ActiveForm::validate($modelAddress));
                $errors = array_merge($errors, ActiveForm::validate($modelUserProviderHealthPlan));
                return $errors;
            }
            
            if ($this->userProviderService->update($model,$modelUserProvider,$modelAddress)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdateFront());
                return $this->redirect(["/dashboard"]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdateFront());
                return $this->redirect(['/minha-conta/medico']);
            }
        } else {
            return $this->render('settingsDoctor', ['model' => $model, 'modelUserProvider' => $modelUserProvider, 'modelAddress' => $modelAddress, 'modelUserProviderHealthPlan' => $modelUserProviderHealthPlan]);
        }
    }
    
}
