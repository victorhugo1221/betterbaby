<?php

use common\modules\healthPlan\models\HealthPlan;
use common\widgets\Alert;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        Configuracoes
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?= Url::base() ?>./css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Url::base() ?>./css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
</head>

<body class="">
<?php $url = Yii::$app->basePath . '/views/layouts/menu.php';
include $url; ?>
<?php $url = Yii::$app->basePath . '/views/layouts/header.php';
include $url; ?>

<?php
$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['/'], TRUE),
    ],
    'enableAjaxValidation'   => TRUE,
    'validationUrl'          => FALSE,
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>
<div class="main-panel" id="main-panel">
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Configurações da conta</h4>
                        <p>As informações de valores médios abaixo de cada tipo de consulta serão utilizadas em seu demonstrativo de faturamento. </p>
                    </div>
                    <div class="card-body">
                        <form action="">
                            <div class="row">
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?php
                                        echo $form->field($modelUserProvider, 'price_range')->widget(Select2::classname(), [
                                            'name'    => 'kv-type-01',
                                            'data'    => [1 => "Até R$150", 2 => "De R$150 Até R$300", 3 => "De R$300 Até R$500", 4 => "Acima de R$500"],
                                            'options' => [
                                                'placeholder' => 'Selecione uma faixa de valor ...',
                                                'options'     => [
                                                    1 => ['data-level' => '1'],
                                                    2 => ['data-level' => '2'],
                                                    3 => ['data-level' => '3'],
                                                    4 => ['data-level' => '4'],
                                                ],
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?php
                                        echo $form->field($modelUserProvider, 'health_plan')->widget(Select2::classname(), [
                                            'data'     => ArrayHelper::map(healthPlan::find()->all(), 'id', 'title'),
                                            'language' => 'pt',
                                            'options'  => ['multiple' => TRUE, 'placeholder' => 'Selecione um convenio ...'],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($modelUserProvider, 'price_range_health_plan')->widget(MaskMoney ::class, [
                                            'pluginOptions' => [
                                                'prefix'        => 'R$',
                                                'allowNegative' => FALSE,
                                            ],
                                            'options' => [
                                                'maxlength' => 10,
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class=" col-md-6">
                                    <div class="form-group">
                                        <?php
                                        echo $form->field($modelUserProvider, 'accept_pne')->widget(Select2::classname(), [
                                            'name'    => 'accept-pne',
                                            'data'    => [1 => "Sim", 2 => "Não"],
                                            'options' => [
                                                'placeholder' => 'Selecione uma opção ...',
                                                'options'     => [
                                                    1 => ['data-level' => '1'],
                                                    2 => ['data-level' => '2'],
                                                ],
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Orientações para o atendimento</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"> Escreva um texto com orientações para o atendimento... </textarea>
                            </div>
                            <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary btn-lg', 'name' => 'salvar']) ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <footer class="footer">
        <?php $url = Yii::$app->basePath . '/views/layouts/footer.php';
        include $url; ?>
    </footer>
</div>
</div>
<!--   Core JS Files   -->
<script src="<?= Url::base() ?>./js/core/jquery.min.js"></script>
<script src="<?= Url::base() ?>./js/core/popper.min.js"></script>
<script src="<?= Url::base() ?>./js/core/bootstrap.min.js"></script>
<script src="<?= Url::base() ?>./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="<?= Url::base() ?>./js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?= Url::base() ?>./js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= Url::base() ?>./js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="<?= Url::base() ?>./js/demo.js"></script>
</body>

</html>