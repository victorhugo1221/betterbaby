let crm = document.getElementById('crm')
let uf = document.getElementById('uf')
let especialidade = document.getElementById('especialidade')
let nome = document.getElementById('nome')
let cpf = document.getElementById('cpf')
let situacao = document.querySelector('[data-situacao]')

let parser = new DOMParser();
let dataCrm

function fechData(){
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    var urlencoded = new URLSearchParams();
    urlencoded.append("crm", crm.value);
    urlencoded.append("uf", uf.value.toUpperCase());

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
    };

    fetch("https://vitor-app.herokuapp.com/ServicoConsultaMedicos", requestOptions)
        .then(response => response.text())
        .then(result => {
            console.log(result)

            let situacaoAtual = parser.parseFromString(result,'application/xml').getElementsByTagName('situacao')[0].textContent
            let crmValidator = document.querySelector('[data-crm-validator]')
            if(situacaoAtual == 'A' || situacaoAtual == 'T'){
                console.log('is valid')
                uf.value =  parser.parseFromString(result,'application/xml').getElementsByTagName('uf')[0].textContent
                nome.value =  parser.parseFromString(result,'application/xml').getElementsByTagName('nome')[0].textContent
                especialidade.value =  parser.parseFromString(result,'application/xml').getElementsByTagName('especialidade')[0].textContent
                situacao.textContent =  parser.parseFromString(result,'application/xml').getElementsByTagName('situacao')[0].textContent
            }else{
                console.log('is not valid')
                crmValidator.textContent = 'CRM não é válido'
                uf.value = null
                nome.value = null
                especialidade.value = null
                situacao.textContent = null
            }
        })
        .catch(error => console.log('error', error));
}


uf.addEventListener('blur',()=>{
    fechData()
})

