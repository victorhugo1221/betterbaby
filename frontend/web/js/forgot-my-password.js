define(['./plugins/toastr/toastr.min', 'plugins/sweetalert/sweetalert.min'], function (toastr) {
    var formName = '#forgot-my-password';
    var successMsg = 'Um e-mail foi enviado com as instruções de troca de senha.';

    $(document).on('beforeSubmit', formName, submitForm);

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "5000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };


    function submitForm(e) {

        var form = $(formName);
        $('button').prop('disabled', true);

        $.post(
            form.attr('action'),
            form.serialize()
        )
            .then(function (data) {
                data.message = successMsg;
                swal({
                    title: 'Feito!',
                    text: data.message,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK',
                    confirmButtonClass: 'confirm',
                    buttonsStyling: false
                }).then(function () {
                    window.location.href = '/login';
                });
            })
            .catch(function (e) {

                if (e.responseJSON == undefined) {
                    e.responseJSON = {};
                }

                if (e.responseJSON.message == undefined) {
                    e.responseJSON.message = errorMsg;
                }

                toastr.error(e.responseJSON.message);

            });

        return false;
    }


});