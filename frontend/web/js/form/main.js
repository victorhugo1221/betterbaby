define('form', ['bootstrap-sweetalert/dist/sweetalert.min', 'css!bootstrap-sweetalert/dist/sweetalert', 'plugins/toastr/toastr.min', 'Component', 'xhr'], function(){

	// Declarações
	var self = new Component('form');
	var request = require('xhr');
	self.install = install;
	self.elements = {
		'field-input':{
			focus: onFocus,
			blur: onBlur,
			keyup: checkValidity,
			keydown: checkValidity,
		},
		remove:{
			click: confirmRemove
		}
	};
	var masks = {
		phone: phoneMask,

	}

	toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "5000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

	// Processamento
	self.bootstrap();
	return self;

	// Funções
	function install()
	{
		if(this.getAttribute('data-ajax')) this.onsubmit = onSubmit;

		$('.timepicker').datetimepicker({
            format: 'LT'
        });

        $('.datetimepicker').datetimepicker({
   			format: 'DD/MM/YYYY'
   		});
	}

	function confirmRemove(component, event){

		var $this = this;
		var url_delete = $this.getAttribute('data-url');


		 swal({
            title: "Deseja realmente remover?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Sim",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        }, function () {
            $.ajax({
			    url: url_delete ,
			    type: "GET",
			    success : function(resposta){
			    	if(resposta){
			    		if(resposta.sucesso === false){
			    			toastr.error(resposta.mensagem, 'Ação de Deletar');
			    		}
			    	}
			    }
			});
        });
	}

	function onSubmit(event){
		event.preventDefault();
		var self = this;
		request
			.post( this.getAttribute('action') )
			.data(new FormData(this))
			.status(200, function(){ 
				self.element('message')
						.state('error', false)
						.state('success', true)
						.last()
						.innerHTML='Mensagem enviada com sucesso, em breve entraremos em contato';
			 })
			.status(401, function(){ 
				self.element('message')
						.state('error', true)
						.state('success', false)
						.last()
						.innerHTML='Erro ao enviar o email, confira os dados e tente novamente';
			})
			.status(500, function(){ 
				self.element('message')
						.state('error', true)
						.state('success', false)
						.last()
						.innerHTML='Erro ao enviar o email, tente novamente mais tarde';
			})
	}

	function checkValidity(component, event){
	}

	function onFocus(component, event){
		this.parentElement.BEM.state('focused', true);
	}
	
	function onBlur(component, event){
		this.impure = true;
		this.parentElement.BEM.state('focused', false);
		checkValidity.bind(this)(component, event);
	}

	function mask( mask, event ){
		if(!mask) return;
		if( !masks[mask] ) return;
		masks[mask].bind(this)(event);
	}

	function phoneMask(event){
		if( 
			isNaN(parseInt(event.key) ) 
			&& event.which !== 9
			&& event.which !== 8
			&& event.which !== 13
		) event.preventDefault();

		var clean = this.value.split(' ').join('').split('-').join('');
		if( clean.length < 3) return;

		var nv = clean.slice(0,2) + " ";

		if( clean.length < 7 ){
			nv += clean.slice(2);
			this.value = nv;
			return;
		}

		if( clean.length < 11 || clean[3] !== 9){
			clean = clean.slice(0,10);
			nv += clean.slice(2,6);
			nv += '-'
			nv += clean.slice(6);
			this.value = nv;
			return;
		}

		clean = clean.slice(0,11);
		nv += clean.slice(2,7);
		nv += '-'
		nv += clean.slice(7);
		this.value = nv;

	}


	function createElementFromHTML(htmlString) {
	  var div = document.createElement('div');
	  div.innerHTML = htmlString.trim();
	  return div.firstChild; 
	}
});