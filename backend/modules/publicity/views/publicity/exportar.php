<?php

use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\components\Excelutil;

$readonly = !\Yii::$app->user->can('sysadmin');
$nome_arquivo = "Publicidades  - " . date("d-m-y H-m-s");

$html = '<!DOCTYPE html>';
$html .= '<table border="1">';
$html .= '<tr>';
$html .= '<td colspan="5"><h3> Listagem de Noticias</h3><td>';
$html .= '<tr>';

$html .= '<tr>';
$html .= '<th>Publicidade</th>';
$html .= '<th>Nome Cliente</th>';
$html .= '<th>Celular cliente</th>';
$html .= '<th>email cliente</th>';
$html .= '<th>Data de visualização</th>';
$html .= '</tr>';

foreach ($sql as $row) {
    $html .= '<tr>';
    $html .= '<td>' . $row->publicityRel['title'] . '</td>';
    $html .= '<td>' . $row->userClientRel['name'] . '</td>';
    $html .= '<td>' . $row->userClientRel['cellphone'] . '</td>';
    $html .= '<td>' . $row->userClientRel['email'] . '</td>';
    $html .= '<td>' . date('d/m/Y', strtotime($row['created'])) . '</td>';
    $html .= '</tr>';
}

$html .= '</table>';

$time = time();
$filename = 'export-' . $time . '.xls';
$file = Url::to(getenv('BASEURL') . 'uploads/idiomas/' . $filename);




?>
<!DOCTYPE html>
<html>
<head>
    <title>Listagem de dados</title>
</head>
<body>
<?php
header("Content-type: application/vnd.ms-excel");
header("Content-type: application/force-download");
header("Content-Disposition: attachment; filename=$nome_arquivo.xls");
header("Pragma: no-cache");
echo utf8_decode($html);
?>
</body>
</html>
