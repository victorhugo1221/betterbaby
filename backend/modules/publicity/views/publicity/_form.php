<?php

use common\components\utils\CrudButton;
use common\modules\publicity\models\Publicity;
use moonland\tinymce\TinyMCE;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly'         => $readonly ? 1 : 0,
    ],
    'validationUrl'          => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>

    <div class="form-content">
        <div class="form-fields">
            <div class="row">
                <div class="col">
                    <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-12">
                                    <?= $form->field($model, 'title')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source']); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <?= $form->field($model, 'phone')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source']); ?>
                                </div>
                                <div class="col-6" >
                                    <?= $form->field($model, 'instagram')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source']); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <?= $form->field($model, 'facebook')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source']); ?>
                                </div>
                                <div class="col-6">
                                    <?= $form->field($model, 'site')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source']); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <?= $form->field($model, 'description')->textarea(['class' => 'form-control form_field-input slugify_target slugify_source']); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <?= $form->field($model, 'photo')->fileInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE,]) ?>
                                    <?php if ($model->photo && !isset($model->errors['photo'])) { ?>
                                        <div class="col-xs-12 col-sm-12 col-md-12 mb-3">
                                            Imagem atual - <a href="" data-fancybox="single" data-src="<?= $_ENV['BACKENDURL'] . Publicity::$_directory_file['photo']['folder'] . '/' . $model->photo; ?>"> Clique aqui </a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-footer row m-0">
        <div class='d-flex bd-highlight col-12 p-0 m-0 dv-btn-actions'>
            <?php
            if (!$readonly) {
                $crudButton = new CrudButton();
                $crudButton->page = '/publicity/publicity/';
                $crudButton->model = $model;
                $crudButton->permissionsToCreate = 'managePublicity;createPublicity;';
                $crudButton->permissionsToUpdate = 'managePublicity;updatePublicity';
                $crudButton->showDelete = !empty($model->editable_by_customer) ? FALSE : TRUE;
                echo $crudButton->showCrudButtons($crudButton);
            }
            ?>
        </div>
    </div>
    </div>
<?php

function tinyConfiguration(string $name) : array {
    return [
        'name'              => "Rating[{$name}]",
        'selector'          => 'textarea',
        'menubar'           => FALSE,
        'height'            => 100,
        'contextmenu'       => 'undo redo',
        'force_p_newlines'  => FALSE,
        'forced_root_block' => '',
        'toolbar'           => [
            'undo redo',
            'bold italic underline strikethrough',
            'alignleft aligncenter alignright alignjustify',
            'numlist bullist',
            'link',
            'formatpainter removeformat',
        ],
    ];
}

ActiveForm::end();
?>