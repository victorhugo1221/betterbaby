<?php

namespace backend\modules\publicity;

class Module extends \common\modules\publicity\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}