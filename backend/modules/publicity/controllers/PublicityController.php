<?php

namespace backend\modules\publicity\controllers;

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\news\models\baseCrm;
use common\modules\publicity\models\Publicity;
use common\modules\publicityView\models\PublicityView;
use common\modules\rating\models\Rating;
use common\modules\solicitation\models\Solicitation;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use common\components\web\Controller;
use common\modules\user\models\User;
use function Webmozart\Assert\Tests\StaticAnalysis\isArray;

class PublicityController extends Controller {
    private $dataService;
    
    public function init() {
        $this->pageTitle = $this->getPageName(Publicity::class);
        $this->pageIdentifier = $this->getPageUrl(Publicity::class);
        $this->dataService = $this->dataService ?? Yii::$app->getModule('publicity')->get('publicityService');
        parent::init();
    }
    
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['exportar'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow'   => TRUE,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $model = Publicity::find();
        $filter = Yii::$app->request->get();
        
        $filter = $this->manipulatingTheFilterInSession($filter);
        
        $dataProvider = new ActiveDataProvider([
            'query'      => $model->filter($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);
        
        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }
    
    
    public function actionCreate() {
        $model = new Publicity();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
    
            foreach (Publicity::$_directory_file as $key => $file){
                $this->uploadedFile($key, $model);
            }
            
            if ($this->dataService->create($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnInsert($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_CREATE);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsert($this->pageTitle, 'a'));
            }
        }
        return $this->render('create', ['model' => $model, 'pageTitle' => $this->pageTitle,]);
    }
    
    
    public function actionUpdate($id) {
        $model = Publicity::find()->andWhere(['id' => $id])->one();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
    
            foreach (Publicity::$_directory_file as $key => $file) {
                $this->uploadedFile($key, $model);
            }
            
            if (Yii::$app->request->get('validation', FALSE)) {
                $erros = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $erros = array_merge($erros, ActiveForm::validate($model));
                return $erros;
            }
            
            if ($this->dataService->update($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'publicity', $model->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'publicity', $model->id);
                return $this->redirect([$redirect]);
            }
        } else {
            return $this->render('update', ['model' => $model, 'pageTitle' => $this->pageTitle]);
        }
    }
    
    
    public function uploadedFile($field, $model) {
        
        if (!empty(UploadedFile::getInstance($model, $field))) {
            $model->{$field} = UploadedFile::getInstance($model, $field);
            $model->validate();
            if (!isset($model->errors[$field])) {
                $model->upload($field);
            }
        } else {
            $model->{$field} = $model->getOldAttribute($field);
        }
    }
    
    public function actionExportar() {
        
        $model = PublicityView::find()->all();;
        
        
        $this->layout = FALSE;
        return $this->render('exportar', ['sql' => $model]);
    }
    
}