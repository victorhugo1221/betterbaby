<?php

namespace backend\modules\specialty;

class Module extends \common\modules\specialty\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}