<?php

namespace backend\modules\adminGw;

class Module extends \common\modules\adminGw\Module {
    public $controllerNamespace = 'backend\modules\adminGw\controllers';
    
    public function init() {
        parent::init();
    }
}
