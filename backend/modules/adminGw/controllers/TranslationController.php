<?php

namespace backend\modules\adminGw\controllers;

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\adminGw\models\Translation;
use common\modules\user\models\User;
use yii\filters\AccessControl;
use Yii;
use common\components\web\Controller;
use yii\base\BaseObject;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;

class TranslationController extends Controller {
    private $dataService;

    public function init() {
        $this->pageTitle = $this->getPageName(Translation::class);
        $this->pageIdentifier = $this->getPageUrl(Translation::class);
        $this->dataService = $this->dataService ?? Yii::$app->getModule('admin-gw')->get('translationService');
        parent::init();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ['manageTranslation','createTranslation','updateTranslation','deleteTranslation'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow'   => TRUE,
                        'roles'   => ['manageTranslation','updateTranslation'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow' => TRUE,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model = Translation::find();
    
        $user = new User();
        if (!$user->userLoggedIsGw()) {
            $model->andWhere(['editable_by_customer' => 1]);
        }

        $filter = Yii::$app->request->get();

        $filter = $this->manipulatingTheFilterInSession($filter);

        $dataProvider = new ActiveDataProvider([
            'query'      => $model->filter($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }

    public function actionCreate() {
        $model = new Translation();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);

            if ($this->dataService->create($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnInsert($this->pageTitle,'a'));
                $redirect = $this->redirectAfterSave($post['button_submit']??$post['button_submit-and-back'], self::ACTION_CREATE);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsert($this->pageTitle,'a'));
            }
        }
        return $this->render('create', ['model' => $model, 'pageTitle' => $this->pageTitle]);
    }

    public function actionUpdate($id) {
        $model = Translation::findOne($id);

        if(!Yii::$app->user->identity->userLoggedIsGw() && empty($model->editable_by_customer)){
            return $this->redirect('/index');
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);

            if ($this->dataService->update($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle,'a'));
                $redirect = $this->redirectAfterSave($post['button_submit']??$post['button_submit-and-back'], self::ACTION_UPDATE, 'translation', $model->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle,'a'));
                $redirect = $this->redirectAfterSave($post['button_submit']??$post['button_submit-and-back'], self::ACTION_UPDATE, 'translation', $model->id);
                return $this->redirect([$redirect]);
            }
        } else {
            return $this->render('update', ['model' => $model, 'pageTitle' => $this->pageTitle]);
        }
    }

    public function actionDelete($id) {

        if ($this->dataService->delete($id)) {
            Yii::$app->session->setFlash('success', DefaultMessage::sucessOnDelete($this->pageTitle,'a'));
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle,'a'));
        }
    }
}