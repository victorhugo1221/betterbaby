<?php

namespace backend\modules\adminGw\controllers;

use common\components\utils\Text;
use common\modules\adminGw\models\Log;
use common\modules\adminGw\models\Page;
use common\modules\user\models\User;
use Yii;
use common\components\web\Controller;
use yii\base\BaseObject;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

class LogController extends Controller {
    private $dataService;

    public function init() {
        $this->pageTitle = Log::getPageName();
        $this->pageIdentifier = Log::tableName();
        $this->dataService = $this->dataService ?? Yii::$app->getModule('admin-gw')->get('logService');
        parent::init();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ['viewLog'],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow' => TRUE,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $filter = Yii::$app->request->get();

        $start = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 30, date("Y")));
        $end = date('Y-m-d', mktime(23, 59, 59, date("m"), date("d"), date("Y")));

        $startDate = empty($filter['startDate']) ? $start : implode('-', array_reverse(explode('/', $filter['startDate'])));
        $endDate = empty($filter['endDate']) ? $end : implode('-', array_reverse(explode('/', $filter['endDate'])));

        $startDate .= ' 00:00:00';
        $endDate .= ' 23:59:59';
    
        $model = Log::find();
        $user = new User();
        if (!$user->userLoggedIsGw()) {
            $model->leftJoin('translation', 'log.entity_id = translation.id AND log.entity_type = \'Tradução\'')
                ->andWhere([
                    'or',
                    ['editable_by_customer' => 1],
                    ['editable_by_customer' => NULL],
                ]);
        }
        $model->andWhere(['between', 'log.created', $startDate, $endDate])->orderBy('log.created DESC');


        $filter = $this->manipulatingTheFilterInSession($filter);

        $dataProvider = new ActiveDataProvider([
            'query'      => $model->filter($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }
}