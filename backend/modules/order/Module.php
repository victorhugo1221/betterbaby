<?php

namespace backend\modules\order;

class Module extends \common\modules\order\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}