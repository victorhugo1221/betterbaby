<?php

use common\components\utils\DefaultMessage;
use common\components\utils\Filter;
use common\modules\order\models\Order;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
$filterdButton = new Filter();
$this->title = 'Listagem de ' . $pageTitle;
$this->params['breadcrumbs'][] = ['label' => $pageTitle];
$formName = substr($dataProvider->query->modelClass, strrpos($dataProvider->query->modelClass, '\\') + 1);
$formName = strtolower($formName);
$urlDefault = '/order/order';

$readonly = !Yii::$app->user->can('managePlan');

$fields = [
    'id'  => 'ID',
    'name_user_provider'  => 'Nome',
];


$filter =  empty($filter) ? [] : $filter;

?>
<div class="row wrapper border-bottom page-heading p-0">
    <div class="col-xs-12 col-sm-12 col-md-6 page-title">
        <h2><?= $this->title ?></h2>
    </div>
    
    <div class="breadcrumb col-xs-12 col-sm-12 col-md-6">
        <?php
        echo Breadcrumbs::widget([
            'tag'                => 'ol',
            'itemTemplate'       => "<li>{link}&nbsp;&nbsp;/&nbsp;&nbsp;</li>\n",
            'activeItemTemplate' => "<li class=\"active\"><strong>{link}</strong></li>\n",
            'links'              => $this->params['breadcrumbs'],
        ]);
        ?>
    </div>
</div>

<div class="row action-area">
    <div class="col-12 filter-area">
        <div class="row">
            <div class="col-12">
                <form id="options-form-<?= $formName ?>" method="GET" action="">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-7 pb-2 col-lg-5 input-group">
                            <?= $filterdButton->showFilterField($fields, $filter)?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2 pb-2">
                            <select class="input form-control filter-select" name="status" id="status">
                                <option value="">Status</option>
                                <?php foreach (Order::$_status as $key => $status) {
                                    $field = !(isset($filter) && count($filter) > 0 && isset($filter['status'])) ? NULL : $filter['status']; ?>
                                    <option value="<?= $key; ?>" <?= $filterdButton->fieldSelected($field, $key); ?> >
                                        <?= $status; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-3 pb-2 pr-0 dv-filter">
                            <?= $filterdButton->showFieldButtons($urlDefault) ?>
                            <form method="get" action=".csv">
                                <a href="<?= Url::to(['order/exportar'], true) ?>" class="btn btn-success btn-sm ml-2" style="color:#FFF;"><i class="far fa-file-excel" aria-hidden="true"></i> Excel</a>
                            </form>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="mail-box">
                <div class="row p-3 pt-0">
                    <div class="col-12 pt-0">
                        <?php
                        Pjax::begin([
                            'enablePushState'    => FALSE,
                            'enableReplaceState' => FALSE,
                            'formSelector'       => '#options-form-' . $formName,
                            'submitEvent'        => 'submit',
                            'timeout'            => 5000,
                            'clientOptions'      => ['maxCacheLength' => 0],
                        ]);
                        
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'tableOptions' => [
                                'id'    => $formName . '-table',
                                'class' => 'table table-striped table-hover',
                            ],
                            'emptyText'    => '<div class="text-center">' . DefaultMessage::theSearchDidNotReturnData() . '</div>',
                            'layout'       => '{items}{pager}',
                            'options'      => [
                                'class'     => 'table-responsive',
                                'data-pjax' => TRUE
                            ],
                            'columns'      => [
                                [
                                    'attribute'      => 'id',
                                    'label'          => 'Número',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . $data->id . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'name',
                                    'label'          => 'Nome',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . $data->userRel->name . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'price',
                                    'label'          => 'Preço',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . 'R$ ' . number_format($data->price, 2, ",", ".") . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'price',
                                    'label'          => 'Data',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . date('d/m/Y h:m', strtotime($data->created)) . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'status',
                                    'label'          => 'Status',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:200px'
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) {
                                        if ($data->status == Order::STATUS_PENDENT) {
                                            return '<i class="fas fa-user-clock color-status-pendent"></i> '  . Order::$_status[$data->status];
                                        } else if ($data->status == Order::STATUS_ACTIVE) {
                                            return '<i class="fas fa-user-check color-status-active"></i> '  . Order::$_status[$data->status];
                                        }
                                    },
                                ],

                            ],
                        ]);
                        
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>