<?php

use common\components\utils\CrudButton;
use common\modules\news\models\baseCrm;
use common\modules\news\models\News;
use moonland\tinymce\TinyMCE;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly'         => $readonly ? 1 : 0,
    ],
    'validationUrl'          => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>

    <div class="form-content">
        <div class="form-fields">
            <div class="row">
                <div class="col">
                    <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-6">
                                    <?= $form->field($model, 'title')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source']); ?>
                                </div>
                                <div class="col-6">
                                    <?= $form->field($model, 'photo')->fileInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE,]) ?>
                                    <?php if ($model->photo && !isset($model->errors['photo'])) { ?>
                                        <div class="col-xs-12 col-sm-12 col-md-12 mb-3">
                                            Imagem atual - <a href="" data-fancybox="single" data-src="<?= $_ENV['BACKENDURL'] . News::$_directory_file['photo']['folder'] . '/' . $model->photo; ?>"> Clique aqui </a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12"
                                     data-component="tinymce-editor-html"
                                     data-selector="#news-descriptiosn"
                                     data-plugins="searchreplace autolink autosave save directionality visualblocks visualchars link  template codesample charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount textpattern noneditable help charmap quickbars"
                                     data-toolbar="undo redo | bold italic underline strikethrough | fontsizeselect formatselect | alignleft aligncenter alignright alignjustify |
                                                     outdent indent |  numlist bullist | forecolor backcolor casechange removeformat | link | "
                                     data-mobile-plugins=" print preview importcss tinydrive searchreplace autolink directionality visualblocks visualchars link  charmap
                                                     hr toc insertdatetime advlist lists wordcount tinymcespellchecker textpattern noneditable help charmap mentions quickbars linkchecker emoticons advtable"
                                     data-mobile-menu-bar="edit view format tools"
                                >
                                    <?= $form->field($model, 'description')->textarea(['rows' => '8']); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <?= $form->field($model, 'source')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-footer row m-0">
        <div class='d-flex bd-highlight col-12 p-0 m-0 dv-btn-actions'>
            <?php
            if (!$readonly) {
                $crudButton = new CrudButton();
                $crudButton->page = '/news/news/';
                $crudButton->model = $model;
                $crudButton->permissionsToCreate = 'manageNews;createNews;';
                $crudButton->permissionsToUpdate = 'manageNews;updateNews';
                $crudButton->permissionsToDelete = 'manageNews;updateNews;deleteNews';
                $crudButton->showDelete = !empty($model->editable_by_customer) ? FALSE : TRUE;
                echo $crudButton->showCrudButtons($crudButton);
            }
            ?>
        </div>
    </div>
    </div>
<?php

function tinyConfiguration(string $name) : array {
    return [
        'name'              => "Rating[{$name}]",
        'selector'          => 'textarea',
        'menubar'           => FALSE,
        'height'            => 100,
        'contextmenu'       => 'undo redo',
        'force_p_newlines'  => FALSE,
        'forced_root_block' => '',
        'toolbar'           => [
            'undo redo',
            'bold italic underline strikethrough',
            'alignleft aligncenter alignright alignjustify',
            'numlist bullist',
            'link',
            'formatpainter removeformat',
        ],
    ];
}

ActiveForm::end();
?>