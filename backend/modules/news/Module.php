<?php

namespace backend\modules\news;

class Module extends \common\modules\news\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}