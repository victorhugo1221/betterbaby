<?php

use common\components\utils\DefaultMessage;
use common\components\utils\Filter;
use common\modules\plan\models\Plan;
use common\modules\solicitation\models\Solicitation;
use common\modules\userProvider\models\UserProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

$filterdButton = new Filter();
$this->title = 'Listagem de ' . $pageTitle;
$this->params['breadcrumbs'][] = ['label' => $pageTitle];
$formName = substr($dataProvider->query->modelClass, strrpos($dataProvider->query->modelClass, '\\') + 1);
$formName = strtolower($formName);
$urlDefault = '/solicitation/solicitation';

$readonly = !Yii::$app->user->can('managePlan');

$fields = [
    'price' => 'Preço',
];


$filter = empty($filter) ? [] : $filter;

?>
<div class="row wrapper border-bottom page-heading p-0">
    <div class="col-xs-12 col-sm-12 col-md-6 page-title">
        <h2><?= $this->title ?></h2>
    </div>
    <div class="breadcrumb col-xs-12 col-sm-12 col-md-6">
        <?php
        echo Breadcrumbs::widget([
            'tag'                => 'ol',
            'itemTemplate'       => "<li>{link}&nbsp;&nbsp;/&nbsp;&nbsp;</li>\n",
            'activeItemTemplate' => "<li class=\"active\"><strong>{link}</strong></li>\n",
            'links'              => $this->params['breadcrumbs'],
        ]);
        ?>
    </div>
</div>

<div class="row action-area">
    <div class="col-12 filter-area">
        <div class="row">
            <div class="col-12">
                <form id="options-form-<?= $formName ?>" method="GET" action="">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-7 pb-2 col-lg-5 input-group">
                            <?= $filterdButton->showFilterField($fields, $filter) ?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2 pb-2">
                            <select class="input form-control filter-select" name="status" id="status">
                                <option value="">Status</option>
                                <?php foreach (Solicitation::$_status as $key => $status) {
                                    $field = !(isset($filter) && count($filter) > 0 && isset($filter['status'])) ? NULL : $filter['status']; ?>
                                    <option value="<?= $key; ?>" <?= $filterdButton->fieldSelected($field, $key); ?> >
                                        <?= $status; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-3 pb-2 pr-0 dv-filter">
                            <?= $filterdButton->showFieldButtons($urlDefault) ?>
                            <a href="<?= Url::to(['solicitation/exportar'], TRUE) ?>" class="btn btn-success btn-sm ml-2" style="color:#FFF;"><i class="far fa-file-excel" aria-hidden="true"></i> Excel </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="mail-box">
                <div class="row p-3 pt-0">
                    <div class="col-12 pt-0">
                        <?php
                        Pjax::begin([
                            'enablePushState'    => FALSE,
                            'enableReplaceState' => FALSE,
                            'formSelector'       => '#options-form-' . $formName,
                            'submitEvent'        => 'submit',
                            'timeout'            => 5000,
                            'clientOptions'      => ['maxCacheLength' => 0],
                        ]);
                        
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'tableOptions' => [
                                'id'    => $formName . '-table',
                                'class' => 'table table-striped table-hover',
                            ],
                            'emptyText'    => '<div class="text-center">' . DefaultMessage::theSearchDidNotReturnData() . '</div>',
                            'layout'       => '{items}{pager}',
                            'options'      => [
                                'class'     => 'table-responsive',
                                'data-pjax' => TRUE,
                            ],
                            'columns'      => [
                                [
                                    'attribute'      => 'user_client',
                                    'label'          => 'Cliente',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . ($data->name_client !== NULL ? $data->name_client : $data->userClientRel->name) . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'user_provider',
                                    'label'          => 'Profissional',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . $data->userProviderRel->name . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'category',
                                    'label'          => 'Categoria',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'client-link'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        if ($data->userProviderRel->providerRel->category_id == UserProvider::IS_DOCTOR) {
                                            return UserProvider::$_category[$data->userProviderRel->providerRel->category_id];
                                            
                                        }
                                        if ($data->userProviderRel->providerRel->category_id == UserProvider::IS_CLINICAL) {
                                            return UserProvider::$_category[$data->userProviderRel->providerRel->category_id];
                                            
                                        } else if ($data->userProviderRel->providerRel->category_id == UserProvider::IS_SERVICE) {
                                            return UserProvider::$_category[$data->userProviderRel->providerRel->category_id];
                                        }
                                    },
                                ],
                                [
                                    'attribute'      => 'status',
                                    'label'          => 'Status',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:200px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) {
                                        if ($data->status == Solicitation::STATUS_PENDENT) {
                                            return '<i class="fas fa-user-clock color-status-pendent"></i> ' . Solicitation::$_status[$data->status];
                                        }
                                        if ($data->status == Solicitation::STATUS_INACTIVE) {
                                            return '<i class="fas fa-user-clock color-status-inactive"></i> ' . Solicitation::$_status[$data->status];
                                        } else if ($data->status == Solicitation::STATUS_ACTIVE) {
                                            return '<i class="fas fa-user-check color-status-active"></i> ' . Solicitation::$_status[$data->status];
                                        }
                                    },
                                ],
                                [
                                    'attribute'      => 'scheduled_date',
                                    'label'          => 'Data de Agendamento',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . date('d/m/Y H:i', strtotime($data->scheduled_date)) . '</a>';
                                    },
                                ],
                                [
                                    'attribute'      => 'created',
                                    'label'          => 'Data de criação',
                                    'format'         => 'html',
                                    'enableSorting'  => TRUE,
                                    'headerOptions'  => [
                                        'class' => 'text-left',
                                        'style' => 'min-width:150px',
                                    ],
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value'          => function ($data) use ($urlDefault) {
                                        return '<a href="' . Url::to([$urlDefault . "/update/" . $data->id], TRUE) . '" class="client-link">' . date('d/m/Y H:i', strtotime($data->created)) . '</a>';
                                    },
                                ],
                            
                            ],
                        ]);
                        
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>