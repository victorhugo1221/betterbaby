<?php

use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\components\Excelutil;

$readonly = !\Yii::$app->user->can('sysadmin');
$nome_arquivo = "Listagem de Atendimentos  - " . date("d-m-y H-m-s");

$html = '<!DOCTYPE html>';
$html .= '<table border="1">';
$html .= '<tr>';
$html .= '<td colspan="5"><h3> Listagem de Atendimentos </h3><td>';
$html .= '<tr>';

$html .= '<tr>';
$html .= '<th>ID do atendimento</th>';
$html .= '<th>Nome Cliente</th>';
$html .= '<th>Nome Profissional</th>';
$html .= '<th>Link Videoconferencia</th>';
$html .= '<th>Motivo cancelamento</th>';
$html .= '<th>Avaliação</th>';
$html .= '<th>Nota Avaliação</th>';
$html .= '<th>Status</th>';
$html .= '<th>Profissional</th>';
$html .= '<th>Data de criação</th>';
$html .= '<th>Data de Agendamento</th>';
$html .= '</tr>';

foreach ($sql as $row) {
    $html .= '<tr>';
    $html .= '<td>' . $row['id'] . '</td>';
    $html .= '<td>' . $row->userClientRel['name'] . '</td>';
    $html .= '<td>' . $row->userProviderRel['name'] . '</td>';
    $html .= '<td>' . $row['call_url'] . '</td>';
    $html .= '<td>' . $row['cancellation_reason'] . '</td>';
    $html .= '<td>' . $row['rating_title'] . '</td>';
    $html .= '<td>' . $row['rating_stars'] . '</td>';
    $html .= '<td>' . $row['status'] . '</td>';
    $html .= '<td>' . $row['created'] . '</td>';
    $html .= '<td>' . $row['scheduled_date'] . '</td>';
    $html .= '</tr>';
}

$html .= '</table>';

$time = time();
$filename = 'export-' . $time . '.xls';
$file = Url::to(getenv('BASEURL') . 'uploads/idiomas/' . $filename);


?>
<!DOCTYPE html>
<html>
<head>
    <title>Listagem de dados</title>
</head>
<body>
<?php
header("Content-type: application/vnd.ms-excel");
header("Content-type: application/force-download");
header("Content-Disposition: attachment; filename=$nome_arquivo.xls");
header("Pragma: no-cache");
echo utf8_decode($html);
?>
</body>
</html>
