<?php

namespace backend\modules\userProvider;

class Module extends \common\modules\userProvider\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}