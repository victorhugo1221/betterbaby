<?php

namespace backend\modules\userProvider\controllers;

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\address\models\Address;
use common\modules\user\models\User;
use common\modules\userProvider\models\UserProvider;
use common\modules\userProviderPlan\models\UserProviderPlan;
use stdClass;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use common\components\Excelutil;
use common\components\web\Controller;
use function Webmozart\Assert\Tests\StaticAnalysis\isArray;

class UserProviderController extends Controller {
    private $dataService;
    
    public function init() {
        $this->pageTitle = $this->getPageName(UserProvider::class, 'user_provider');
        $this->pageIdentifier = $this->getPageUrl(UserProvider::class, 'user_provider');
        $this->dataService = $this->dataService ?? Yii::$app->getModule('user-provider')->get('userProviderService');
        parent::init();
    }
    
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['exportar'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow'   => TRUE,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $model = User::find()->innerJoinWith('providerRel')->orderBy(['created' => SORT_DESC]);
        $filter = Yii::$app->request->get();
        
        $filter = $this->manipulatingTheFilterInSession($filter);
        
        $dataProvider = new ActiveDataProvider([
            'query'      => $model->filterUserProvider($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);
        
        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }
    
    
    public function actionUpdate($id) {
        $model = User::find()->andWhere(['id' => $id])->one();
        $modelUserProvider = UserProvider::find()->andWhere(['user_id' => $id])->one();
        $modelAddress = Address::find()->andWhere(['user_id' => $id])->one();
        $modelPlan = UserProviderPlan::find()->andWhere(['user_id' => $id])->one();
        $date = date_create($modelPlan->end_date);
        $modelPlan->end_date = date_format($date, "d-m-Y H:i:s");
        
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            $modelUserProvider->load($post);
            $modelPlan->load($post);
            $date = date_create($modelPlan->end_date);
            $modelPlan->end_date = date_format($date, "Y-m-d H:i:s");
            
            if (Yii::$app->request->get('validation', FALSE)) {
                $erros = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $erros = array_merge($erros, ActiveForm::validate($model));
                $erros = array_merge($erros, ActiveForm::validate($modelUserProvider));
                return $erros;
            }
            
            if ($this->dataService->update($model) && $this->dataService->update($modelUserProvider) && $this->dataService->update($modelPlan)) {
                $this->dataService->update($modelUserProvider);
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'user-provider', $model->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'user-provider', $model->id);
                return $this->redirect([$redirect]);
            }
        } else {
            return $this->render('update', ['model' => $model, 'pageTitle' => $this->pageTitle, 'modelUserProvider' => $modelUserProvider, 'modelAddress' => $modelAddress, 'modelPlan' => $modelPlan]);
        }
    }
    
    public function actionExportar() {
        
        $model = UserProvider::find()->all();
        $this->layout = FALSE;
        return $this->render('exportar', ['sql' => $model]);
    }
    
    public function actionDelete($id) {
        if ($this->dataService->delete($id)) {
            Yii::$app->session->setFlash('success', DefaultMessage::sucessOnDelete($this->pageTitle, 'o'));
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', DefaultMessage::errorOnDelete($this->pageTitle, 'o'));
            return $this->redirect(['index']);
        }
    }
    
    
}