<?php

use common\modules\user\models\User;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\components\Excelutil;

$readonly = !\Yii::$app->user->can('sysadmin');
$nome_arquivo = "Listagem de Profissionais  - " . date("d-m-y H-m-s");

$html = '<!DOCTYPE html>';
$html .= '<table border="1">';
$html .= '<tr>';
$html .= '<td colspan="5"><h3> Listagem de Profissionais</h3><td>';
$html .= '<tr>';

$html .= '<tr>';
$html .= '<th>ID</th>';
$html .= '<th>Nome Completo</th>';
$html .= '<th>Categoria</th>';
$html .= '<th>CPF/CNPJ</th>';
$html .= '<th>E-mail</th>';
$html .= '<th>Celular</th>';
$html .= '<th>CRM</th>';
$html .= '<th>Especialidade CRM</th>';
$html .= '<th>Especialidade Adicional</th>';
$html .= '<th>Avaliação média</th>';
$html .= '<th>Plano</th>';
$html .= '<th>Vencimento Plano</th>';
$html .= '<th>Estado</th>';
$html .= '<th>Cidade</th>';
$html .= '<th>Status</th>';
$html .= '</tr>';

foreach ($sql as $row) {
    $html .= '<tr>';
    $html .= '<td>' . $row['id'] . '</td>';
    $html .= '<td>' . $row->userRel['name'] . '</td>';
    $html .= '<td>' . $row['category_id'] . '</td>';
    $html .= '<td>' . $row->userRel['cpf'] . '</td>';
    $html .= '<td>' . $row->userRel['email'] . '</td>';
    $html .= '<td>' . $row->userRel['cellphone'] . '</td>';
    $html .= '<td>' . $row['crm'] . '</td>';
    $html .= '<td>' . $row['crm_specialty'] . '</td>';
    $html .= '<td>' . $row['additional_specialty'] . '</td>';
    $html .= '<td>' . $row['average_rating'] . '</td>';
    $html .= '<td>' . $row->providerPlanRel->planRel['title'] . '</td>';
    $html .= '<td>' . $row->providerPlanRel['end_date'] . '</td>';
    $html .= '<td>' . $row->addressRel['state'] . '</td>';
    $html .= '<td>' . $row->addressRel['city'] . '</td>';
    $html .= '<td>' . User::$_status[$row->userRel['status']] . '</td>';
    $html .= '</tr>';
}

$html .= '</table>';

$time = time();
$filename = 'export-' . $time . '.xls';
$file = Url::to(getenv('BASEURL') . 'uploads/idiomas/' . $filename);

?>
<!DOCTYPE html>
<html>
<head>
    <title>Listagem de dados</title>
</head>
<body>
<?php
header("Content-type: application/vnd.ms-excel");
header("Content-type: application/force-download");
header("Content-Disposition: attachment; filename=$nome_arquivo.xls");
header("Pragma: no-cache");
echo utf8_decode($html);
?>
</body>
</html>
