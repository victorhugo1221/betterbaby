<?php

use common\components\utils\CrudButton;
use kartik\datetime\DateTimePicker;
use moonland\tinymce\TinyMCE;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly'         => $readonly ? 1 : 0,
    ],
    'validationUrl'          => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>

    <div class="form-content">
        <div class="form-fields">
            <div class="row">
                <div class="col">
                    <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-4">
                                    <?= $form->field($model, 'name')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source', 'disabled' => TRUE]); ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($model, 'email')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source', 'disabled' => TRUE]); ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($model, 'cpf')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source', 'disabled' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <?= $form->field($model, 'cellphone')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source', 'disabled' => TRUE]); ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($modelUserProvider, 'crm')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source', 'disabled' => TRUE]); ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($modelUserProvider, 'crm_state')->textInput(['class' => 'form-control form_field-input', 'disabled' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <?= $form->field($modelUserProvider, 'instagram_url')->textInput(['class' => 'form-control form_field-input', 'disabled' => TRUE]); ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($modelUserProvider, 'facebook_url')->textInput(['class' => 'form-control form_field-input', 'disabled' => TRUE]); ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($modelUserProvider, 'linkedin_url')->textInput(['class' => 'form-control form_field-input', 'disabled' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <?= $form->field($modelUserProvider, 'start_hour')->textInput(['class' => 'form-control form_field-input', 'disabled' => TRUE]); ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($modelUserProvider, 'end_hour')->textInput(['class' => 'form-control form_field-input', 'disabled' => TRUE]); ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($modelUserProvider, 'gap')->textInput(['class' => 'form-control form_field-input', 'disabled' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <?= $form->field($modelUserProvider, 'ranking')->textInput(['class' => 'form-control form_field-input']); ?>
                                </div>
                                <div class="col-4">
                                    <?php
                                    echo $form->field($modelPlan, 'end_date')->widget(DateTimePicker::class, [
                                        'name'          => 'datetime_400',
                                        'removeButton'  => FALSE,
                                        'pickerButton'  => ['icon' => 'time'],
                                        'pluginOptions' => [
                                            'autoclose' => TRUE,
                                            'format'    => 'dd-mm-yyyy hh:ii:ss',
                                        ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-footer row m-0">
        <div class='d-flex bd-highlight col-12 p-0 m-0 dv-btn-actions'>
            <?php
            if (!$readonly) {
                $crudButton = new CrudButton();
                $crudButton->page = '/user-provider/user-provider/';
                $crudButton->model = $model;
                $crudButton->permissionsToCreate = 'manageUserProvider;createUserProvider;';
                $crudButton->permissionsToUpdate = 'manageUserProvider;updateUserProvider';
                $crudButton->permissionsToDelete = 'manageUserProvider;updateUserProvider;deleteUserProvider';
                $crudButton->showDelete = !empty($model->editable_by_customer) ? FALSE : TRUE;
                echo $crudButton->showCrudButtons($crudButton);
            }
            ?>
        </div>
    </div>
    </div>
<?php

function tinyConfiguration(string $name) : array {
    return [
        'name'              => "UserProvider[{$name}]",
        'selector'          => 'textarea',
        'menubar'           => FALSE,
        'height'            => 100,
        'contextmenu'       => 'undo redo',
        'force_p_newlines'  => FALSE,
        'forced_root_block' => '',
        'toolbar'           => [
            'undo redo',
            'bold italic underline strikethrough',
            'alignleft aligncenter alignright alignjustify',
            'numlist bullist',
            'link',
            'formatpainter removeformat',
        ],
    ];
}

ActiveForm::end();
?>