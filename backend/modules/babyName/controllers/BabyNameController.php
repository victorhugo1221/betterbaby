<?php

namespace backend\modules\babyName\controllers;

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use common\modules\babyName\models\BabyName;
use common\modules\babyTimeline\models\BabyTimeline;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use common\components\web\Controller;
use common\modules\user\models\User;
use function Webmozart\Assert\Tests\StaticAnalysis\isArray;

class BabyNameController extends Controller {
    private $dataService;
    
    public function init() {
        $this->pageTitle = $this->getPageName(BabyName::class);
        $this->pageIdentifier = $this->getPageUrl(BabyName::class);
        $this->dataService = $this->dataService ?? Yii::$app->getModule('baby-name')->get('babyNameService');
        parent::init();
    }
    
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['exportar'],
                        'allow'   => TRUE,
                        'roles'   => ['sysadmin'],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow'   => TRUE,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $model = BabyName::find();
        $filter = Yii::$app->request->get();
        
        $filter = $this->manipulatingTheFilterInSession($filter);
        
        $dataProvider = new ActiveDataProvider([
            'query'      => $model->filter($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);
        
        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }
    
    
    public function actionCreate() {
        $model = new BabyName();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            
            
            if ($this->dataService->create($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnInsert($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_CREATE);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsert($this->pageTitle, 'a'));
            }
        }
        return $this->render('create', ['model' => $model, 'pageTitle' => $this->pageTitle,]);
    }
    
    
    public function actionUpdate($id) {
        $model = BabyName::find()->andWhere(['id' => $id])->one();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            
            if (Yii::$app->request->get('validation', FALSE)) {
                $erros = [];
                Yii::$app->response->format = Response::FORMAT_JSON;
                $erros = array_merge($erros, ActiveForm::validate($model));
                return $erros;
            }
            
            if ($this->dataService->update($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'baby-name', $model->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle, 'a'));
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'baby-name', $model->id);
                return $this->redirect([$redirect]);
            }
        } else {
            return $this->render('update', ['model' => $model, 'pageTitle' => $this->pageTitle]);
        }
    }
    
    
    public function actionExportar() {
        
        $model = BabyName::find()->all();;
        
        
        $this->layout = FALSE;
        return $this->render('exportar', ['sql' => $model]);
    }
    
    
    public function actionDelete($id) {
        if ($this->dataService->delete($id)) {
            Yii::$app->session->setFlash('success', DefaultMessage::sucessOnDelete($this->pageTitle, 'a'));
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', DefaultMessage::errorOnDelete($this->pageTitle, 'a'));
        }
    }
    
}