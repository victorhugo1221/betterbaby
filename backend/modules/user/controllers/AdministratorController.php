<?php

namespace backend\modules\user\controllers;

use common\components\utils\DefaultMessage;
use common\components\utils\Text;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use common\components\web\Controller;
use common\modules\user\models\User;
use function Webmozart\Assert\Tests\StaticAnalysis\isArray;

class AdministratorController extends Controller {
    private $administratorService;
    private $permissionService;

    public function init() {
        $this->pageTitle            = $this->getPageName(User::class, 'administrator');
        $this->pageIdentifier       = $this->getPageUrl(User::class, 'administrator');
        $this->administratorService = $this->administratorService ?? Yii::$app->getModule('user')->get('administratorService');
        $this->permissionService    = $this->permissionService ?? Yii::$app->getModule('admin-gw')->get('permissionService');
        parent::init();
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => TRUE,
                        'roles'   => ['manageAdministrator','createAdministrator','updateAdministrator','deleteAdministrator'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => TRUE,
                        'roles'   => ['manageAdministrator','createAdministrator'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow'   => TRUE,
                        'roles'   => ['manageAdministrator','updateAdministrator'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => TRUE,
                        'roles'   => ['manageAdministrator','deleteAdministrator'],
                    ],
                    [
                        'actions' => ['clear-filter'],
                        'allow' => TRUE,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model  = $this->administratorService->find();
        $filter = Yii::$app->request->get();

        $filter = $this->manipulatingTheFilterInSession($filter);

        $dataProvider = new ActiveDataProvider([
            'query'      => $model->filter($filter),
            'pagination' => [
                'pageSizeLimit' => [1, 100],
            ],
        ]);


        return $this->render('index', ['dataProvider' => $dataProvider, 'filter' => $filter, 'pageTitle' => Text::pluralize($this->pageTitle)]);
    }

    public function actionCreate() {
        $model          = new User();

        $allPermissions = $this->permissionService->getPermissionsListbox();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            $errors = [];
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors                     = array_merge($errors, ActiveForm::validate($model));
                return $errors;
            }

            $id = $this->administratorService->createAdministrator($model);

            if ($id) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnInsert($this->pageTitle, 'o'));
                $permissions = !empty($post['User']['new_permissions']) ? $post['User']['new_permissions'] : [];
                $this->savePermissions($id, $permissions);

                $newPermissions = $this->permissionService->getPermissionsByUserListbox($model->getId());
                if (!empty($newPermissions)) {
                    $this->permissionService->saveLog($id, 1, null, $newPermissions);
                }

                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_CREATE);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnInsert($this->pageTitle, 'o'));
            }
        }
        return $this->render('create', ['model' => $model, 'allPermissions' => $allPermissions, 'pageTitle' => $this->pageTitle]);
    }

    public function actionUpdate($id) {
        $errors = [];
        $model  = User::find()->where(['IS', User::tableName() . '.deleted', null])->andWhere(['id' => $id])->one();
        if ($model == null) {
            Yii::$app->session->setFlash('error', DefaultMessage::registerNotFound($this->pageTitle, 'o'));
            return $this->redirect(['index']);
        }
        $allPermissions = $this->permissionService->getPermissionsListbox();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $model->load($post);
            if (Yii::$app->request->get('validation', FALSE)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $errors                     = array_merge($errors, ActiveForm::validate($model));
                return $errors;
            }
            $newPermissions = !empty($post['User']['new_permissions']) ? $post['User']['new_permissions'] : [];
            $oldPermissions = $this->permissionService->getPermissionsByUserListbox($model->getId());

            if ($this->administratorService->updateAdministrator($model)) {
                Yii::$app->session->setFlash('success', DefaultMessage::sucessOnUpdate($this->pageTitle, 'o'));

                if ($model->id != Yii::$app->user->id) {
                    $this->savePermissions($model->id, $newPermissions, $oldPermissions);
                    $newPermissions = $this->permissionService->getPermissionsByUserListbox($model->getId());

                    sort($newPermissions);
                    sort($oldPermissions);
                    if ($oldPermissions != $newPermissions) {
                        $this->permissionService->saveLog($model->id, 2, $oldPermissions, $newPermissions);
                    }
                }
                $redirect = $this->redirectAfterSave($post['button_submit'] ?? $post['button_submit-and-back'], self::ACTION_UPDATE, 'administrator', $model->id);
                return $this->redirect([$redirect]);
            } else {
                Yii::$app->session->setFlash('error', DefaultMessage::errorOnUpdate($this->pageTitle, 'o'));
                return $this->redirect(['administrator/update/' . $model->id]);
            }
        } else {
            return $this->render('update', ['model' => $model, 'allPermissions' => $allPermissions, 'pageTitle' => $this->pageTitle]);
        }
    }

    private function savePermissions(int $modelId, array $permissions, array $oldPermissions = []): void {
        $newPermissions      = $permissions;
        $permissionsInserted = $this->permissionService->save($modelId, $newPermissions, $oldPermissions);
        if (!$permissionsInserted) {
            Yii::$app->session->setFlash('warning', "Ocorreu um erro ao salvar as permissões do {$this->pageTitle}!");
        }
    }
}