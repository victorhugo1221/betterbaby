<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\View;
use common\widgets\Alert;

$this->title = $_ENV['PROJECT_NAME'] . ' - Login';
echo Alert::widget([
    'alertTypes' => [
        'sucess'   => 'alert-success',
        'error'    => 'alert-danger',
        'facebook' => 'alert-warning',
    ],
    'options'    => [
        'class' => '-msg col-sm-12 text-center'
    ]
]);
$message = Yii::$app->session->getFlash('loginerror', false);
?>

<?php if ($message) { ?>
    <div class="alert alert-danger text-center"> <?= $message; ?></div>
<?php } ?>

<?php if ($model->hasErrors()) { ?>
    <div class="alert alert-danger -msg">
        <?php foreach ($model->getFirstErrors() as $error):
            echo $error;
            break;
        endforeach; ?>
    </div>
<?php } ?>

<?php $form = ActiveForm::begin([
    'id'          => 'form-validation',
    'options'     => ['data-component' => 'form', 'class' => 'ajax-form'],
    'fieldConfig' => [
        'template' => "{label}\n{input}\n{error}",
    ],
]); ?>

<?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => 'E-mail'])->label("Usuário") ?>
<?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Senha'])->label("Senha") ?>
    <div class="container-fluid">
        <div class="row action-login">
            <div class="col-md-6 col-sm-12 col-xs-12 p-0 mt-2 pt-1 forgot-password">
                <a href="<?= Url::to(['/user/default/forgot-my-password'], true) ?>" class="forgot-password">Esqueci minha
                    senha</a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <?= Html::submitButton('Entrar', ['class' => 'btn btn-login-w btn-block', 'name' => 'entrar']) ?>
            </div>
        </div>
    </div>


<?php ActiveForm::end(); ?>