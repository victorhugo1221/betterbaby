<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = $_ENV['PROJECT_NAME'] . ' - Esqueci minha senha';
$baseUrl = $_ENV['BACKENDURL'];
$this->registerJsFile('js/require.js', ['data-main' => 'js/forgot-my-password.js']);
if (Yii::$app->session->hasFlash('forgot-my-password')) {
    $this->registerJs();
}
?>

<?php $form = ActiveForm::begin([
    'id'                   => 'forgot-my-password',
    'fieldConfig'          => [
        'template' => "{label}\n{input}\n{error}",
    ],
    'method'               => 'POST',
    'validationUrl'        => Url::to(['/user/default/forgot-my-password', 'validation' => TRUE], TRUE),
    'options'              => ['class' => 'm-t ajax-form'],
    'enableAjaxValidation' => TRUE,
    'validateOnChange'     => FALSE,
    'validateOnBlur'       => FALSE,
]); ?>

    <p>Por favor, informe seu e-mail</p>

<?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => 'E-mail']) ?>

    <div class="container-fluid">
        <div class="row action-forgot-password">
            <div class="col-md-6 col-sm-12 col-xs-12 p-0 mt-2 pt-1 forgot-password">
                <a href="<?= Url::to(['/user/default/login'], true) ?>" class="forgot-password">Voltar para o Login</a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 p-0">
                <?= Html::submitButton('Solicitar nova senha', ['class' => 'btn btn-login-w btn-block', 'name' => 'entrar']) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>