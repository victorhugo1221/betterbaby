<?php

use common\components\utils\CrudButton;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'form',
    'options' => [
        'class' => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly' => $readonly ? 1 : 0,
    ],
    'validationUrl' => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableAjaxValidation' => TRUE,
    'enableClientValidation' => FALSE,
    'validateOnSubmit' => TRUE,
    'validateOnChange' => FALSE,
    'validateOnBlur' => FALSE,
]);
?>


<div class="form-content">
    <div class="form-fields">
        <div class="row">
            <div class="col">
                <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Informações</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <?php if (!$model->isNewRecord && $model->id != Yii::$app->user->id) { ?>
                                <div class="col-xs-12 col-sm-12 col-md-1">
                                    <label>Ativo?</label>
                                    <?= $form->field($model, 'status')->checkbox(['uncheck' => '0', 'data-component' => 'toggle', 'data-switchery' => TRUE, 'checked' => ($model->status > 0 ? 'true' : NULL)], FALSE)->label(FALSE) ?>
                                </div>
                            <?php } ?>
                            <div class="col-xs-12 col-sm-12 col-md-<?= (!$model->isNewRecord && $model->id != Yii::$app->user->id) ? '5' : '6' ?>">
                                <?= $form->field($model, 'name')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?= $form->field($model, 'email')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?= $form->field($model, 'cpf')->textInput(['maxlength' => TRUE, 'data-mask' => '999.999.999-99']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if (
                    (
                        Yii::$app->user->can('sysadmin') ||
                        Yii::$app->user->can('createAdministrator') ||
                        Yii::$app->user->can('updateAdministrator')
                    )
                    && $model->id != Yii::$app->user->id
                )
        { ?>
            <div class="row">
                <div class="col-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Permissões</h5>
                        </div>
                        <div class="ibox-content">
                            <?= $form->field($model, 'new_permissions')
                                ->dropDownList($allPermissions, [
                                    'id'=> 'user-permissions',
                                    'multiple' => TRUE,
                                    'disabled' => $readonly,
                                    'class' => 'multiSelectPermissions',
                                    'data-placeholder' => 'Permissões',
                                    'style' => 'width:100%;',
                                    'value' => array_keys($model->permissions),
                                    'data-component' => 'multiselect-permissions',
                                ])->label(FALSE) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="form-footer row m-0">
        <div class='d-flex bd-highlight col-12 p-0 m-0 dv-btn-actions'>
            <?php
            if (!$readonly) {
                $crudButton = new CrudButton();
                $crudButton->page = '/user/administrator/';
                $crudButton->model = $model;
                $crudButton->permissionsToCreate = 'manageAdministrator;createAdministrator';
                $crudButton->permissionsToUpdate = 'manageAdministrator;updateAdministrator';
                $crudButton->showDelete = FALSE;
                echo $crudButton->showCrudButtons($crudButton);
            }
            ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

