<?php

use common\modules\user\models\User;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\components\Excelutil;

$readonly = !\Yii::$app->user->can('sysadmin');
$nome_arquivo = "Listagem de Usuarios mães  - " . date("d-m-y H-m-s");

$html = '<!DOCTYPE html>';
$html .= '<table border="1">';
$html .= '<tr>';
$html .= '<td colspan="5"><h3> Listagem de Usuarios</h3><td>';
$html .= '<tr>';

$html .= '<tr>';
$html .= '<th>ID</th>';
$html .= '<th>Nome Completo</th>';
$html .= '<th>Celular</th>';
$html .= '<th>E-mail</th>';
$html .= '<th>Estado</th>';
$html .= '<th>Cidade</th>';
$html .= '<th>Nome do bebe</th>';
$html .= '<th>Indicação</th>';
$html .= '<th>Status</th>';
$html .= '<th>Data criação</th>';
$html .= '</tr>';

foreach ($sql as $row) {
    $html .= '<tr>';
    $html .= '<td>' . $row['id'] . '</td>';
    $html .= '<td>' . $row['name'] . '</td>';
    $html .= '<td>' . $row['cellphone'] . '</td>';
    $html .= '<td>' . $row['email'] . '</td>';
    $html .= '<td>' . $row->addressRel['state'] . '</td>';
    $html .= '<td>' . $row->addressRel['city'] . '</td>';
    $html .= '<td>' . $row->clientRel['baby_name'] . '</td>';
    $html .= '<td>' . $row->clientRel['referral_code'] . '</td>';
    $html .= '<td>' . User::$_status[$row['status']] . '</td>';
    $html .= '<td>' . $row['created'] . '</td>';
    $html .= '</tr>';
}

$html .= '</table>';

$time = time();
$filename = 'export-' . $time . '.xls';
$file = Url::to(getenv('BASEURL') . 'uploads/idiomas/' . $filename);

?>
<!DOCTYPE html>
<html>
<head>
    <title>Listagem de usuarios mães</title>
</head>
<body>
<?php
header("Content-type: application/vnd.ms-excel");
header("Content-type: application/force-download");
header("Content-Disposition: attachment; filename=$nome_arquivo.xls");
header("Pragma: no-cache");
echo utf8_decode($html);
?>
</body>
</html>
