<?php

use common\components\utils\CrudButton;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly'         => $readonly ? 1 : 0,
    ],
    'validationUrl'          => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableAjaxValidation'   => TRUE,
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>


<div class="form-content">
    <div class="form-fields">
        <div class="row">
            <div class="col">
                <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Informações</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?= $form->field($model, 'name')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'disabled' => TRUE]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?= $form->field($model, 'email')->textInput(['class' => 'form-control form_field-input', 'maxlength' => TRUE, 'disabled' => TRUE]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?= $form->field($model, 'cellphone')->textInput(['maxlength' => TRUE, 'data-mask' => '(99)99999-9999', 'disabled' => TRUE]) ?>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <?= $form->field($modelUserClient, 'baby_name')->textInput(['maxlength' => TRUE, 'disabled' => TRUE]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-footer row m-0">
        <div class='d-flex bd-highlight col-12 p-0 m-0 dv-btn-actions'>
            <?php
            if (!$readonly) {
                $crudButton = new CrudButton();
                $crudButton->page = '/user/user/';
                $crudButton->model = $model;
                $crudButton->permissionsToUpdate = 'manageUser;updateUser';
                $crudButton->permissionsToDelete = 'manageUser;deleteUser';
                echo $crudButton->showCrudButtons($crudButton);
            }
            ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

