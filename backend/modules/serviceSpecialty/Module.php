<?php

namespace backend\modules\serviceSpecialty;

class Module extends \common\modules\serviceSpecialty\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}