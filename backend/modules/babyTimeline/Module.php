<?php

namespace backend\modules\babyTimeline;

class Module extends \common\modules\babyTimeline\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}