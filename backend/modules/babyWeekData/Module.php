<?php

namespace backend\modules\babyWeekData;

class Module extends \common\modules\babyWeekData\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}