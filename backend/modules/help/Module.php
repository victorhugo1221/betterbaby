<?php

namespace backend\modules\help;

class Module extends \common\modules\help\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}