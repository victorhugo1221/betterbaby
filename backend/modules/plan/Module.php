<?php

namespace backend\modules\plan;

class Module extends \common\modules\plan\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}