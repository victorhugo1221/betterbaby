<?php

use common\components\utils\CrudButton;
use kartik\money\MaskMoney;
use moonland\tinymce\TinyMCE;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id'                     => 'form',
    'options'                => [
        'class'                 => 'form_form',
        'data-redirect-success' => Url::to(['index'], TRUE),
        'data-readonly'         => $readonly ? 1 : 0,
    ],
    'validationUrl'          => Url::to([$model->isNewRecord ? 'create' : 'update', TRUE, 'validation' => TRUE, 'id' => $model->id]),
    'enableClientValidation' => FALSE,
    'validateOnSubmit'       => TRUE,
    'validateOnChange'       => FALSE,
    'validateOnBlur'         => FALSE,
]);
?>

    <div class="form-content">
        <div class="form-fields">
            <div class="row">
                <div class="col">
                    <?php echo $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-4" >
                                    <?= $form->field($model, 'title')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source', 'disabled' => !Yii::$app->user->can('sysadmin')]); ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($model, 'price')->textInput(['class' => 'form-control form_field-input slugify_target slugify_source', 'disabled' => !Yii::$app->user->can('sysadmin')]); ?>
                                </div>
                            <div class="row">
                                <div class="col-3">
                                    <label>Ativo?</label>
                                    <?= $form->field($model, 'status')->checkbox(['uncheck' => '0', 'data-component' => 'toggle', 'data-switchery' => TRUE, 'checked' => ($model->status > 0 ? 'true' : NULL)], FALSE)->label(FALSE) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-footer row m-0">
        <div class='d-flex bd-highlight col-12 p-0 m-0 dv-btn-actions'>
            <?php
            if (!$readonly) {
                $crudButton = new CrudButton();
                $crudButton->page = '/plan/plan/';
                $crudButton->model = $model;
                $crudButton->permissionsToCreate = 'managePlan;createPlan;';
                $crudButton->permissionsToUpdate = 'managePlan;updatePlan';
                $crudButton->showDelete = !empty($model->editable_by_customer) ? FALSE : TRUE;
                echo $crudButton->showCrudButtons($crudButton);
            }
            ?>
        </div>
    </div>
    </div>
<?php

function tinyConfiguration(string $name) : array {
    return [
        'name'              => "Plan[{$name}]",
        'selector'          => 'textarea',
        'menubar'           => FALSE,
        'height'            => 100,
        'contextmenu'       => 'undo redo',
        'force_p_newlines'  => FALSE,
        'forced_root_block' => '',
        'toolbar'           => [
            'undo redo',
            'bold italic underline strikethrough',
            'alignleft aligncenter alignright alignjustify',
            'numlist bullist',
            'link',
            'formatpainter removeformat',
        ],
    ];
}

ActiveForm::end();
?>