<?php

namespace backend\modules\dashboard\controllers;

use common\modules\baseCrm\models\BaseCrm;
use common\modules\solicitation\models\Solicitation;
use common\modules\user\models\UserClient;
use common\modules\userProvider\models\UserProvider;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

class DashboardController extends Controller {
    
    public function init() {
        $this->view->title = $_ENV['PROJECT_NAME'];
        parent::init();
    }
    
    
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index'],
                'rules' => [
                    [
                        'allow'   => TRUE,
                        'actions' => ['index',],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $modelClient = UserClient::find()->count();
        $modelClinic = UserProvider::find()->andWhere(['category_id' => 2])->andWhere(['IS', UserProvider::tableName() . '.deleted', NULL])->count();
        $modelDoctor = UserProvider::find()->andWhere(['category_id' => 1])->andWhere(['IS', UserProvider::tableName() . '.deleted', NULL])->count();
        $modelService = UserProvider::find()->andWhere(['category_id' => 3])->andWhere(['IS', UserProvider::tableName() . '.deleted', NULL])->count();
        $modelSolicitation = Solicitation::find()->count();
        
        
        $currentDate = new \DateTime();
        $countArray = [];
        
        for ($i = 0; $i < 12; $i++) {
            $startDate = new \DateTime(sprintf('first day of -%d month', $i), $currentDate->getTimezone());
            $endDate = new \DateTime(sprintf('last day of -%d month', $i), $currentDate->getTimezone());
            $monthName = strftime('%b', $startDate->getTimestamp());
            
            $count = Solicitation::find()
                ->andWhere(['status' => 2])
                ->andWhere(['>', 'scheduled_date', $startDate->format('Y-m-d')])
                ->andWhere(['<', 'scheduled_date', $endDate->format('Y-m-d')])
                ->count();
            
            $countDoctor = Solicitation::find()
                ->andWhere(['solicitation.status' => 2])
                ->andWhere(['user_provider.category_id' => 1])
                ->andWhere(['>', 'scheduled_date', $startDate->format('Y-m-d')])
                ->andWhere(['<', 'scheduled_date', $endDate->format('Y-m-d')])
                ->innerJoin('user', 'solicitation.user_provider = user.id')
                ->innerJoin('user_provider', 'user.id = user_provider.user_id')
                ->count();
            
            if ($countDoctor == NULL) {
                $countDoctor = 0;
            }
            
            if ($count == NULL) {
                $count = 0;
            }
            
            $countArray[] = $count;
            $countDoctorArray[] = $countDoctor;
            $monthArray[] = $monthName;
            
        }
        
        $countArray = array_reverse($countArray);
        $countDoctorArray = array_reverse($countDoctorArray);
        $monthArray = array_reverse($monthArray);
        
        $saida_array = array_map(function ($item) {
            return "\"$item\"";
        }, $monthArray); // adiciona as aspas duplas em cada elemento
        $SolicitationResultMonth = implode(", ", $saida_array); // junta novamente os elementos do array em uma string
        
        $SolicitationResult = $countArray;
        $SolicitationResultDoctor = $countDoctorArray;
        
        
        return $this->render('index', ['modelClient' => $modelClient, 'modelService' => $modelService, 'modelClinic' => $modelClinic, 'modelDoctor' => $modelDoctor, 'SolicitationResult' => $SolicitationResult, 'SolicitationResultMonth' => $SolicitationResultMonth, 'SolicitationResultDoctor' => $SolicitationResultDoctor, 'modelSolicitation' => $modelSolicitation]);
    }
    
    public function actionError() {
        $this->layout = FALSE;
        
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== NULL)
            return $this->render('error', ['exception' => $exception]);
        return $this->redirect('/');
    }
}