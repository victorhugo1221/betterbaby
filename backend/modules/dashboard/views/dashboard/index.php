<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Dashboard</h2>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-3">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Médicos Cadastrados</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins"> <?= $modelDoctor ?></h1>
                            <small>Usuarios Cadastrados</small>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Clínicas Cadastradas</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins"><?= $modelClinic ?></h1>
                            <small>Usuarios Cadastrados</small>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Prestadores de serviço</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins"><?= $modelService ?></h1>
                            <small>Usuarios Cadastrados</small>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Mamães Cadastradas</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins"><?= $modelClient ?></h1>
                            <small>Usuarios</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div>
                                <h3 class="font-bold no-margins">Atendimentos</h3>
                            </div>
                            <div>
                                <canvas id="lineChart" height="70"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ChartJS-->

            <script>

                function myFunction() {
                    var x = document.getElementById('divConteudo');
                    var y = document.getElementById('botaoEsconde');
                    var z = document.getElementById('botaoMostra');
                    var w = document.getElementById('divEsconde');
                    if (x.style.display === 'none') {
                        x.style.display = 'block';
                        y.style.display = 'block';
                        z.style.display = 'none';
                        w.style.display = 'none';
                    } else {
                        x.style.display = 'none';
                        y.style.display = 'none';
                        z.style.display = 'block';
                        w.style.display = 'block';
                    }
                }

            </script>
            <script src="../../js/plugins/chartJs/Chart.min.js"></script>
            <script src="../../js/jquery-3.1.1.min.js"></script>
            <script>

                $(document).ready(function () {

                    var lineData = {
                            labels: [<?= $SolicitationResultMonth ?>],
                            datasets: [
                                {
                                    label: "Atendimentos Serviços",
                                    backgroundColor: "rgba(169, 124, 219,0.5)",
                                    borderColor: "rgba(169, 124, 219,0.7)",
                                    pointBackgroundColor: "rgba(169, 124, 219,1)",
                                    pointBorderColor: "#fff",
                                    data: [<?= implode(",", $SolicitationResult) ?>]
                        },
                        {
                            label: "Atendimentos Médicos",
                            backgroundColor: "rgba(117, 218, 236,0.5)",
                            borderColor: "rgba(117, 218, 236,1)",
                            pointBackgroundColor: "rgba(117, 218, 236,1)",
                            pointBorderColor: "#fff",
                            data: [<?= implode(",", $SolicitationResultDoctor) ?>]

                        },

                ]
                }
                    ;

                    var lineOptions = {
                        responsive: true
                    };


                    var ctx = document.getElementById("lineChart").getContext("2d");
                    new Chart(ctx, {type: 'line', data: lineData, options: lineOptions});

                });
            </script>
            