<?php

namespace backend\modules\healthPlan;

class Module extends \common\modules\healthPlan\Module{
    
    public $controllerNamespace = __NAMESPACE__ . '\controllers';
    
    public function init() {
        parent::init();
        
    }
}