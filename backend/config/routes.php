<?php
return [
    'enablePrettyUrl' => TRUE,
    'enableStrictParsing' => FALSE,
    'showScriptName' => FALSE,
    'rules' => [
        '' => 'dashboard/dashboard/index',
        'login' => 'user/default/login',
        'logout' => 'user/default/logout',
        'forgot-my-password' => 'user/default/forgot-my-password',
        'change-password/<token:[\\w-]+>' => 'user/default/change-password',
        
        
        '<module>/<controller>/<action>/<id:[\d]+>' => '<module>/<controller>/<action>',
        '<module>/<controller>/<id:[\d]+>' => '<module>/<controller>/view',
    ],
];