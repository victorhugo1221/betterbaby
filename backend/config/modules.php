<?php

use yii\filters\Cors;

return [
    'gii'                      => [
        'class' => 'yii\gii\Module',
    ],
    'as corsFilter'            => [
        'class' => Cors::className(),
        'cors'  => [
            'Origin'                           => ['*'],
            'Access-Control-Request-Method'    => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
            'Access-Control-Request-Headers'   => ['*'],
            'Access-Control-Allow-Credentials' => NULL,
            'Access-Control-Max-Age'           => 86400,
            'Access-Control-Expose-Headers'    => [],
        ],
    ],
    'rbac'                     => [
        'class'         => 'mdm\admin\Module',
        'controllerMap' => [
            'assignment' => [
                'class'         => 'mdm\admin\controllers\AssignmentController',
                'userClassName' => 'common\modules\user\models\User',
                'idField'       => 'id',
                'usernameField' => 'email',
                'fullnameField' => 'name',
            ],
        ],
    ],
    'dashboard'                => [
        'class' => 'backend\modules\dashboard\Module',
    ],
    'user'                     => [
        'class' => 'backend\modules\user\Module',
    ],
    'admin-gw'                 => [
        'class' => 'backend\modules\adminGw\Module',
    ],
    'profile'                  => [
        'class' => 'backend\modules\profile\Module',
    ],
    'profileProfilePermission' => [
        'class' => 'common\modules\profileProfilePermission\Module',
    ],
    'rating'                   => [
        'class' => 'backend\modules\rating\Module',
    ],
    'news'                     => [
        'class' => 'backend\modules\news\Module',
    ],
    'help'                     => [
        'class' => 'backend\modules\help\Module',
    ],
    'publicity'                => [
        'class' => 'backend\modules\publicity\Module',
    ],
    'baby-timeline'            => [
        'class' => 'backend\modules\babyTimeline\Module',
    ],
    'user-provider'            => [
        'class' => 'backend\modules\userProvider\Module',
    ],
    'order'                    => [
        'class' => 'backend\modules\order\Module',
    ],
    'solicitation'             => [
        'class' => 'backend\modules\solicitation\Module',
    ],
    'plan'                     => [
        'class' => 'backend\modules\plan\Module',
    ],
    'health-plan'              => [
        'class' => 'backend\modules\healthPlan\Module',
    ],
    'service-specialty'        => [
        'class' => 'backend\modules\serviceSpecialty\Module',
    ],
    'specialty'                => [
        'class' => 'backend\modules\specialty\Module',
    ],
    'baby-week-data'           => [
        'class' => 'backend\modules\babyWeekData\Module',
    ],
    'baby-name'                => [
        'class' => 'backend\modules\babyName\Module',
    ],
];