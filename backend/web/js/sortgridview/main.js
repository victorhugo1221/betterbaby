define('sortgridview', ['Component', 'css!./style'], function () {
    // Declarações
    var self = new Component('sortgridview');
    self.install = install;
    var url = '';
    // Processamento
    return self;

    // Funções
    function install(base) {

        url = this.getAttribute('data-url') || '';

        $(".table-sort > tbody").sortable({
            items: "> tr",
            appendTo: "parent",
            helper: "clone",
            placeholder: "placeholder-style",
            start: function (event, ui) {
                $(this).find('.placeholder-style td:nth-child(2)').addClass('hidden-td');
                ui.placeholder.html(ui.item.html());
                ui.placeholder.css('visibility', 'hidden');
            },
            stop: function (event, ui) {
                ui.item.css('display', '');
            },
            helper: function (e, tr) {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function (index) {
                    $(this).width($originals.eq(index).width());
                });
                return $helper;
            },
            create: function (event, ui) {
                var form = document.querySelector('.form-sort');
                Array.from(form).forEach(element => {

                    if (element.value && element.name != 'operation' && element.name != 'field') {
                        console.log(element.name);
                        console.log(element);
                        console.log(element.value);
                        $(".table-sort > tbody").sortable("disable");
                    }
                });
            },
            update: function (event, ui) {
                let newOrder = $(this).sortable('toArray');
                Notiflix.Loading.Circle('Salvando...');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {ids: newOrder}
                })
                .done(function () {
                    Notiflix.Loading.Remove();
                });
            }
        }).disableSelection();
    }
});