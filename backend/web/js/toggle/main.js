/**
 * Descrição do componente toggle
 * <link href="css/plugins/switchery/switchery.css" rel="stylesheet">
 * <script src="js/plugins/switchery/switchery.js"></script>
 * 
 */
define('toggle', ['Component', 'plugins/switchery/switchery', 'css!css/plugins/switchery/switchery'], function(){

	// Declarações
	var self = new Component('toggle');
	var defaults = { 
		
	};
	self.install = install;
	self.elements = {
		// 'element': { event: function }
	};
	// Processamento
	return self;

	// Funções
	function install()
	{
		var _this = this;
		var options = 	{	 
							color: this.getAttribute('data-color') ||'#1AB394',
							secondaryColor: this.getAttribute('data-secondaryColor'),
							jackColor: this.getAttribute('data-jackColor'),
							jackSecondaryColor: this.getAttribute('data-jackSecondaryColor'),
							className: this.getAttribute('data-className'),
							disabled: this.getAttribute('data-disabled'),
							disabledOpacity: this.getAttribute('data-disabledOpacity'),
							speed: this.getAttribute('data-speed'),
							size: this.getAttribute('data-size'),
						};
		setTimeout(function(){ new Switchery(_this, options); });
	}
});