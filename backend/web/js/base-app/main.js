/**
 * Descrição do componente base-app
 * 
 */
define('base-app', [
	'Component',
	'jquery',
	'popper',
	'bootstrap',
	'plugins/metisMenu/jquery.metisMenu',
	'plugins/slimscroll/jquery.slimscroll.min',
	'inspinia'
], function () {


	// Declarações
	var self = new Component('base-app');
	self.install = install;
	self.elements = {
		// 'element': { event: function }
	};

	// Processamento
	self.bootstrap();
	return self;

	// Funções
	function install(base) {
		menuCollapse(base);
	}

	function closeMenu(body) {
		if (window.innerWidth > 768) return;
		body.classList.remove('mini-navbar');
	}

	function menuCollapse(base) {
		var navlinks = base.querySelectorAll('.navbar-default.navbar-static-side .nav-link');
		var buttonMenuCloseMobile = base.querySelector('.menu-close-mobile');
		var body = document.querySelector('body');

		navlinks.forEach(element => {
			if (!element.nextElementSibling) {
				element.addEventListener('click', function () { closeMenu(body) });
			}
		});
		buttonMenuCloseMobile.addEventListener('click', function () { closeMenu(body) });

	}

});