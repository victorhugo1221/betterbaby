define('tinymce-editor-html', ['Component', 'css!./style.css'], function () {
    var self = new Component('tinymce-editor-html');
    self.install = install;
    self.elements = {};

    return self;

    function install(base) {

        var images_upload_url = base.getAttribute('data-images-upload-url');
        var selector = base.getAttribute('data-selector');
        var plugins = base.getAttribute('data-plugins');
        var toolbar = base.getAttribute('data-toolbar');
        var mobilePlugins = base.getAttribute('data-mobile-plugins');
        var menuBar = base.getAttribute('data-mobile-menu-bar');

        if(plugins == null || plugins == undefined){
            plugins = ' searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor' +
                ' toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars';
        }

        if(toolbar == null || toolbar == undefined){
            toolbar = 'undo redo | bold italic underline strikethrough | fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor casechange removeformat | insertfile image media link |  ';
        }

        if(mobilePlugins == null || mobilePlugins == undefined){
            mobilePlugins = 'print preview importcss tinydrive searchreplace autolink directionality visualblocks visualchars image link  ' +
                    'table charmap hr toc insertdatetime advlist lists wordcount tinymcespellchecker textpattern noneditable ' +
                    'help charmap mentions quickbars linkchecker emoticons advtable';
        }

        if(menuBar == null || menuBar == undefined){
            menuBar = 'file edit view insert format tools table';
        }

        tinymce.init({
            script_url: 'https://cdn.tiny.cloud/1/z88rkmette6oqwt3zhzxj5qbc91343hehuubhzpjtkndok3k/tinymce/5/tinymce.min.js',
            selector: selector,
            mode: "textareas",
            language: 'pt_BR',
            language_url: 'http://painel.ped-app.localhost/js/tinymce-editor-html/language/pt_BR.js',
            plugins: plugins,

            toolbar: toolbar,
            mobile: {
                theme: 'mobile',
                plugins: mobilePlugins
            },
            menubar: menuBar,
            image_advtab: false,
            importcss_append: true,
            template_cdate_format: '[Date Created (CDATE): %d/%m/%Y : %H:%M:%S]',
            template_mdate_format: '[Date Modified (MDATE): %d/%m/%Y : %H:%M:%S]',
            height: 600,
            automatic_uploads: true,
            image_caption: false,
            noneditable_noneditable_class: 'mceNonEditable',
            images_upload_url: images_upload_url,
        });
    }
});