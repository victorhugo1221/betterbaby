define('load-cities', ['Component'], function () {

	var self = new Component('load-cities');
	var estadoId;

	self.install = install;
	self.elements = {};

	self.bootstrap();
	return self;

	function install()
	{
		$(document).off( 'change', '.states', mudacidade );
		$(document).on( 'change', '.states', mudacidade );
	}

	function mudacidade()
	{
		estadoId = $( ".states" ).val();
		if(estadoId > 0)
		{
			$('.cities').find('option').remove();
			$.ajax({
				       url: "/address/city/find-cities-by-state?state_id=" + estadoId,
				       dataType: 'json',
				       success: function( data ) {
					       $('.cities').append($('<option>', {value: 0,text : 'Selecione'}));
					       $.each(data, function (i, item) {
						       $('.cities').append($('<option>', {value: item.id,text : item.name}));
					       });
				       },
				       error: function( data ) {
					       console.log( "ERROR:  " + data );
				       }
			       });
		}
	}
});