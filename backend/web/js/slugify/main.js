define('slugify', ['./slug_function', 'Component'], function (slugify) {

    var self = new Component('slugify');
    self.install = install;
    self.elements = {
        'source': {keyup: updateInput}
    };

    return self;

    function install() {
    }

    function updateInput(component, event) {
        component.element('target').value = slugify(this.value, {lower: true});
    }

});