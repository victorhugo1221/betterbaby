define([], function(){

new MutationObserver(componentObserver).observe(document.body, {
    childList: true,
    subtree: true
});

function componentObserver(mutations, observer) {
    var update = false;
    for (var index = 0; index < mutations.length; index++) {
        var mutation = mutations[index];
        if (mutation.type === 'childList' && mutation.addedNodes.length) update = true;
    }
    if (update) installComponents();
}

function installComponents() {
    var components = [].slice.call(document.querySelectorAll('[data-component]'));
    components.forEach(function(component){
        var self = component;
        if(component.installed) return;
        require.config({packages:[ self.attributes['data-component'].value ]});
        require([self.attributes['data-component'].value], function(module) {
            if(self.installed) return;
            self.component_name = self.attributes['data-component'].value;
            module.bootstrap();
            module.install.call(self, self);
            self.installed = true;
        });
    });
}
})