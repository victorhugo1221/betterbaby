define('multiselect', ['Component', 'css!./style'], function () {
    // Declarações
    var self = new Component('multiselect');
    self.install = install;
    // Processamento
    return self;

    // Funções
    function install(base) {
        var separator = $('.select2', base).attr('data-separador');
        var placeholder = $('.select2', base).attr('data-placeholder');
        var noResults = $('.select2', base).attr('data-msg-no-results');
        var allowClear = ($('.select2', base).attr('data-allow-clear') === undefined) ? false : $('.select2', base).attr('data-allow-clear');
        var tokenSeparators = (separator == null || false) ? false : [separator, ' '];
        var acceptInsert = !$('.select2', base).attr('data-accept-insert') ? false : $('.select2', base).attr('data-accept-insert');

        $('.select2', base).select2({
            tags:            acceptInsert,
            placeholder:     placeholder,
            tokenSeparators: tokenSeparators,
            allowClear:      allowClear,
            language:        {
                "noResults": function () {
                    return noResults;
                }
            }
        });
    }

});