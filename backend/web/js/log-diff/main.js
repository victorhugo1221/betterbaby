define('log-diff', ['text!./diff.html', 'jquery', 'Component'], function (template, $) {
    var self = new Component('log-diff');
    self.install = install;
    self.elements = {
        'toggle': {'click': toggle_size}
    };

    var template_update = "<tr><td>{{key}}</td><td>{{oldData}}</td><td>{{newData}}</td></tr>";
    var template_insert = "<tr><td>{{key}}</td><td>{{oldData}}</td></tr>";

    var head_update = "<tr><th>Campo</th><th>Antes</th><th>Depois</th></tr>";
    var head_insert = "<tr><th>Campo</th><th>Valor</th></tr>";

    return self;

    function install(base) {
        base.element('container')
            .all()
            .forEach(function (element) {
                var json = element.innerHTML && JSON.parse(atob(element.innerHTML));
                if (!json.newData) {

                    var changes = Object
                        .keys(json.oldData)
                        .map(function (e) {
                            var temp = template_insert + '';
                            return temp
                                .split('{{key}}').join(e)
                                .split('{{oldData}}').join(json.oldData[e])
                        })
                        .join('');

                    element.innerHTML = template
                        .split('{{thead}}')
                        .join(head_insert)
                        .split('{{tbody}}')
                        .join(changes);
                    return;
                }

                var changes = Object
                    .keys(json.oldData)
                    .filter(function (e) {
                        return json.oldData[e] != json.newData[e];
                    })
                    .map(function (e) {
                        var temp = template_update + '';
                        return temp
                            .split('{{key}}').join(e)
                            .split('{{oldData}}').join(json.oldData[e])
                            .split('{{newData}}').join(json.newData[e])
                    })
                    .join('');

                element.innerHTML = template
                    .split('{{thead}}')
                    .join(head_update)
                    .split('{{tbody}}')
                    .join(changes);
            });
    }

    function toggle_size(base, e) {
        e.preventDefault();
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.children('.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    }
});