define('multiselect-permissions', ['Component'], function(){
    var self = new Component('multiselect-permissions');
    self.install = install;
    self.elements = {

    };

    self.bootstrap();
    return self;

    function install()
    {
        $('.multiSelectPermissions').multiSelect({
            selectableOptgroup: true,
            selectableHeader: "<div class='form-group'><input type='text' class='search-multi-select find-options form-control search-input' autocomplete='off' placeholder='Filtrar...'></div>",
            selectionHeader: "<div class='form-group'><input type='text' class='search-multi-select find-selected form-control search-input' autocomplete='off' placeholder='Filtrar...'></div>",
            afterInit: function(ms)
            {
                var $input_find_options = $('.search-multi-select.find-options');
                var $input_find_selected = $('.search-multi-select.find-selected');

                $input_find_options.quicksearch('.ms-selectable .ms-optgroup-label, .ms-elem-selectable:not(.ms-selected)', {});
                $input_find_selected.quicksearch('.ms-selection .ms-optgroup-label, .ms-elem-selection.ms-selected', {
                    show: function()
                    {
                        if ( $(this).hasClass('ms-optgroup-label') && $(this).parent().find('.ms-selected').length <= 0 )
                            return false;
                        $(this).show();
                    }
                });
            }
        });
    }

});