define('modal', ['Component', 'xhr'], function () {

    var self = new Component('modal');
    self.install = install;
    self.elements = {
        submit: {
            click: submitForm
        }
    };

    return self;

    function install() {

    }

    function submitForm(component, event) {
        var url = component.getAttribute('data-url');
        var data = $("form._model").serializeArray().reduce(function (obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        $.post(url, data);
    }
});