<?php


namespace backend\controllers;
use yii\web\Controller;
use yii\helpers\Url;
use Yii;

class SwaggerController extends Controller {
    public function actions(): array
    {
        return [
            'docs' => [
                'class' => 'yii2mod\swagger\SwaggerUIRenderer',
                'restUrl' => Url::to(['swagger/json']),
            ],
            'json' => [
                'class' => 'yii2mod\swagger\OpenAPIRenderer',
                'cache' => NULL,
                'scanDir' => [
                    Yii::getAlias('@api/components/utils/'),
                    Yii::getAlias('@api/modules/user/controllers/'),
                    Yii::getAlias('@api/modules/userProvider/controllers/'),
                    Yii::getAlias('@api/modules/auth/controllers/'),
                    Yii::getAlias('@api/modules/plan/controllers/'),
                    Yii::getAlias('@api/modules/email/controllers'),
                    Yii::getAlias('@api/modules/dashboard/controllers'),
                    Yii::getAlias('@api/modules/solicitation/controllers'),
                    Yii::getAlias('@api/modules/healthPlan/controllers'),
                    Yii::getAlias('@api/modules/specialty/controllers'),
                    Yii::getAlias('@api/modules/news/controllers'),
                    Yii::getAlias('@api/modules/baseCrm/controllers'),
                    Yii::getAlias('@api/modules/publicity/controllers'),
                    Yii::getAlias('@api/modules/babyName/controllers'),
                    Yii::getAlias('@api/modules/babyWeekData/controllers'),
                    Yii::getAlias('@api/modules/serviceSpecialty/controllers'),
                    Yii::getAlias('@api/modules/tempAddress/controllers'),
                    Yii::getAlias('@api/modules/babyTimeline/controllers'),
                    Yii::getAlias('@api/modules/help/controllers'),
                    Yii::getAlias('@common/modules/user/models/'),
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
}